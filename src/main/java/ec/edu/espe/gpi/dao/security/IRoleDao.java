package ec.edu.espe.gpi.dao.security;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.security.Role;

public interface IRoleDao extends CrudRepository<Role, Long> {

	@Query(value = "SELECT u FROM Role u")
	public Optional<List<Role>> findAllEnable();

	@Query(value = "SELECT u FROM Role u WHERE u.roleName = ?1")
	public Optional<Role> findByRoleName(String roleName);
}