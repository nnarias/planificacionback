package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Coverage;

public interface ICoverageDao extends CrudRepository<Coverage, Long> {

	@Query(value = "SELECT u FROM Coverage u WHERE u.state = 1")
	public Optional<List<Coverage>> findAllEnable();

	@Query(value = "SELECT u FROM Coverage u WHERE u.state = 1 and u.id=?1")
	public Optional<Coverage> findByIdEnable(Long id);

	public Optional<Coverage> findByName(String name);

	@Query(value = "SELECT u FROM Coverage u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Coverage> findByNameNotPresent(Long id, String name);
}