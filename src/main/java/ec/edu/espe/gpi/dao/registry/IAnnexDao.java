package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Annex;

public interface IAnnexDao extends CrudRepository<Annex, Long> {

	@Query(value = "SELECT u FROM Annex u WHERE u.state = 1")
	public Optional<List<Annex>> findAllEnable();

	@Query(value = "SELECT u FROM Annex u WHERE u.state = 1 and u.id=?1")
	public Optional<Annex> findByIdEnable(Long id);

	public Optional<Annex> findByName(String name);

	@Query(value = "SELECT u FROM Annex u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Annex> findByNameNotPresent(Long id, String name);
}