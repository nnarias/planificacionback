package ec.edu.espe.gpi.dao.evaluation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.Criteria;

public interface ICriteriaDao extends CrudRepository<Criteria, Long> {
	@Query(value = "SELECT u FROM Criteria u WHERE u.state = 1")
	public Optional<List<Criteria>> findAllEnable();

	@Query(value = "SELECT u FROM Criteria u WHERE u.state = 1 and u.id=?1")
	public Optional<Criteria> findByIdEnable(Long id);

	public Optional<Criteria> findByName(String name);

	@Query(value = "SELECT u FROM Criteria u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Criteria> findByNameNotPresent(Long id, String name);
}