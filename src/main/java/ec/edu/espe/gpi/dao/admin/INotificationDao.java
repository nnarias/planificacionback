package ec.edu.espe.gpi.dao.admin;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.admin.Notification;

public interface INotificationDao extends CrudRepository<Notification, Long> {

	@Query(value = "SELECT u FROM Notification u WHERE u.user=?1")
	public Optional<List<Notification>> findAllByUser(String username);

	@Query(value = "SELECT u FROM Notification u WHERE u.id=?1 and u.user = ?2")
	public Optional<Notification> findByUser(Long id, String username);

	@Query(value = "SELECT u FROM Notification u WHERE u.user=?1 and u.recentState = 1")
	public Optional<List<Notification>> findByUserRecentNotification(String username);

	@Query(value = "SELECT u FROM Notification u")
	public Optional<List<Notification>> findList();
}