package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Program;

public interface IProgramDao extends CrudRepository<Program, Long> {

	@Query(value = "SELECT u FROM Program u WHERE u.state = 1")
	public Optional<List<Program>> findAllEnable();

	@Query(value = "SELECT u FROM Program u WHERE u.state = 1 and u.id=?1")
	public Optional<Program> findByIdEnable(Long id);

	public Optional<Program> findByName(String name);

	@Query(value = "SELECT u FROM Program u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Program> findByNameNotPresent(Long id, String name);
}