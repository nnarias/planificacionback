package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ScientificDiscipline;

public interface IScientificDisciplineDao extends CrudRepository<ScientificDiscipline, Long> {

	@Query(value = "SELECT u FROM ScientificDiscipline u WHERE u.state = 1")
	public Optional<List<ScientificDiscipline>> findAllEnable();

	@Query(value = "SELECT u FROM ScientificDiscipline u WHERE u.state = 1 and u.id=?1")
	public Optional<ScientificDiscipline> findByIdEnable(Long id);

	public Optional<ScientificDiscipline> findByName(String name);

	@Query(value = "SELECT u FROM ScientificDiscipline u WHERE u.id != ?1 and u.name = ?2")
	public Optional<ScientificDiscipline> findByNameNotPresent(Long id, String name);
}