package ec.edu.espe.gpi.dao.degrees;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;


import ec.edu.espe.gpi.model.degress.Degrees;

public interface IDegreesDao extends CrudRepository<Degrees, Long>{
	@Query(value = "SELECT u FROM Degrees u WHERE u.removed = 0")
	public Optional<List<Degrees>> findAllEnable();

	@Query(value = "SELECT u FROM Degrees u WHERE u.removed = 0 and u.id=?1")
	public Optional<Degrees> findByIdEnable(Long id);

}
