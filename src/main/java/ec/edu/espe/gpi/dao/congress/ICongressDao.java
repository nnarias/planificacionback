package ec.edu.espe.gpi.dao.congress;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface ICongressDao extends CrudRepository<Congress, Long> {
	@Query(value = "SELECT u FROM Congress u WHERE u.removed = 0")
	public Optional<List<Congress>> findAllEnable();
	
	@Query(value = "SELECT u FROM Congress u WHERE u.removed = 0 and u.planning=?1")
	public Optional<List<Congress>> findAllByPlanningID(Planning id);

	@Query(value = "SELECT u FROM Congress u WHERE u.removed = 0 and u.id=?1")
	public Optional<Congress> findByIdEnable(Long id);
}
