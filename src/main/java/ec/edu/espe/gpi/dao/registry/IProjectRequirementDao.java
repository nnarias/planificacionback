package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ProjectRequirement;

public interface IProjectRequirementDao extends CrudRepository<ProjectRequirement, Long> {

	@Query(value = "SELECT u FROM ProjectRequirement u WHERE u.project.id = ?1 and u.state = 1")
	public Optional<List<ProjectRequirement>> findAllByProject(Long idProject);

	@Query(value = "SELECT u FROM ProjectRequirement u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectRequirement> findByIdEnable(Long id);
}