package ec.edu.espe.gpi.dao.bookchapters;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IBookChaptersDao extends CrudRepository<BookChapters, Long>{

	
	@Query(value = "SELECT u FROM BookChapters u WHERE u.removed = 0")
	public Optional<List<BookChapters>> findAllEnable();
	
	@Query(value = "SELECT u FROM BookChapters u WHERE u.removed = 0 and u.planning=?1")
	public Optional<List<BookChapters>> findAllByPlanningID(Planning id);

	@Query(value = "SELECT u FROM BookChapters u WHERE u.removed = 0 and u.id=?1")
	public Optional<BookChapters> findByIdEnable(Long id);

}
