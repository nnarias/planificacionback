package ec.edu.espe.gpi.dao.books;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IBooksDao extends CrudRepository<Books, Long>{
	@Query(value = "SELECT u FROM Books u WHERE u.removed = 0")
	public Optional<List<Books>> findAllEnable();
	
	@Query(value = "SELECT u FROM Books u WHERE u.removed = 0 and u.planning=?1")
	public Optional<List<Books>> findAllByPlanningID(Planning id);

	@Query(value = "SELECT u FROM Books u WHERE u.removed = 0 and u.id=?1")
	public Optional<Books> findByIdEnable(Long id);
}
