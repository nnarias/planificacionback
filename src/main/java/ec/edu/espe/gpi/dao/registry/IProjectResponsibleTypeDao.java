package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;

public interface IProjectResponsibleTypeDao extends CrudRepository<ProjectResponsibleType, Long> {

	@Query(value = "SELECT u FROM ProjectResponsibleType u WHERE u.state = 1")
	public Optional<List<ProjectResponsibleType>> findAllEnable();

	@Query(value = "SELECT u FROM ProjectResponsibleType u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectResponsibleType> findByIdEnable(Long id);

	public Optional<ProjectResponsibleType> findByName(String name);

	@Query(value = "SELECT u FROM ProjectResponsibleType u WHERE u.id != ?1 and u.name = ?2")
	public Optional<ProjectResponsibleType> findByNameNotPresent(Long id, String name);
}