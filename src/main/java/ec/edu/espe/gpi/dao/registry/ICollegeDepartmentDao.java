package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.CollegeDepartment;

public interface ICollegeDepartmentDao extends CrudRepository<CollegeDepartment, Long> {

	@Query(value = "SELECT u FROM CollegeDepartment u WHERE u.state = 1")
	public Optional<List<CollegeDepartment>> findAllEnable();

	@Query(value = "SELECT u FROM CollegeDepartment u WHERE u.state = 1 and u.id=?1")
	public Optional<CollegeDepartment> findByIdEnable(Long id);

	public Optional<CollegeDepartment> findByName(String name);

	@Query(value = "SELECT u FROM CollegeDepartment u WHERE u.id != ?1 and u.name = ?2")
	public Optional<CollegeDepartment> findByNameNotPresent(Long id, String name);
}