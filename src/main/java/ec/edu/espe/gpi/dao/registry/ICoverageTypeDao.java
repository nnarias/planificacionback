package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.CoverageType;

public interface ICoverageTypeDao extends CrudRepository<CoverageType, Long> {

	@Query(value = "SELECT u FROM CoverageType u WHERE u.state = 1")
	public Optional<List<CoverageType>> findAllEnable();

	@Query(value = "SELECT u FROM CoverageType u WHERE u.state = 1 and u.id=?1")
	public Optional<CoverageType> findByIdEnable(Long id);

	public Optional<CoverageType> findByName(String name);

	@Query(value = "SELECT u FROM CoverageType u WHERE u.id != ?1 and u.name = ?2")
	public Optional<CoverageType> findByNameNotPresent(Long id, String name);
}