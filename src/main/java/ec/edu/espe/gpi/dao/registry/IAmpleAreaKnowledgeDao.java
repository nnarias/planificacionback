package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;

public interface IAmpleAreaKnowledgeDao extends CrudRepository<AmpleAreaKnowledge, Long> {

	@Query(value = "SELECT u FROM AmpleAreaKnowledge u WHERE u.state = 1")
	public Optional<List<AmpleAreaKnowledge>> findAllEnable();

	@Query(value = "SELECT u FROM AmpleAreaKnowledge u WHERE u.state = 1 and u.id=?1")
	public Optional<AmpleAreaKnowledge> findByIdEnable(Long id);

	public Optional<AmpleAreaKnowledge> findByName(String name);

	@Query(value = "SELECT u FROM AmpleAreaKnowledge u WHERE u.id != ?1 and u.name = ?2")
	public Optional<AmpleAreaKnowledge> findByNameNotPresent(Long id, String name);
}