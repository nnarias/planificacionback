package ec.edu.espe.gpi.dao.planning;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ec.edu.espe.gpi.model.planning.Book;

public interface IBookDao extends CrudRepository<Book, Long>{

	@Query(value = "SELECT u FROM Book u WHERE u.state = 1")
	public Optional<List<Book>> findAllEnable();

	@Query(value = "SELECT u FROM Book u WHERE u.state = 1 and u.id=?1")
	public Optional<Book> findByIdEnable(Long id);
	
	
}
