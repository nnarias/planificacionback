package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.CallArea;

public interface ICallAreaDao extends CrudRepository<CallArea, Long> {

	@Query(value = "SELECT u FROM CallArea u WHERE u.state = 1")
	public Optional<List<CallArea>> findAllEnable();

	@Query(value = "SELECT u FROM CallArea u WHERE u.state = 1 and u.id=?1")
	public Optional<CallArea> findByIdEnable(Long id);

	public Optional<CallArea> findByName(String name);

	@Query(value = "SELECT u FROM CallArea u WHERE u.id != ?1 and u.name = ?2")
	public Optional<CallArea> findByNameNotPresent(Long id, String name);
}