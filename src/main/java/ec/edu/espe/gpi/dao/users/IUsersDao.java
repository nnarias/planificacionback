package ec.edu.espe.gpi.dao.users;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.model.users.Users;

public interface IUsersDao extends CrudRepository<Users, String> {
	
}
