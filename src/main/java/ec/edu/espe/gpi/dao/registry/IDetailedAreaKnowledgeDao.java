package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.DetailedAreaKnowledge;

public interface IDetailedAreaKnowledgeDao extends CrudRepository<DetailedAreaKnowledge, Long> {

	@Query(value = "SELECT u FROM DetailedAreaKnowledge u WHERE u.state = 1")
	public Optional<List<DetailedAreaKnowledge>> findAllEnable();

	@Query(value = "SELECT u FROM DetailedAreaKnowledge u WHERE u.state = 1 and u.id=?1")
	public Optional<DetailedAreaKnowledge> findByIdEnable(Long id);

	public Optional<DetailedAreaKnowledge> findByName(String name);

	@Query(value = "SELECT u FROM DetailedAreaKnowledge u WHERE u.id != ?1 and u.name = ?2")
	public Optional<DetailedAreaKnowledge> findByNameNotPresent(Long id, String name);
}