package ec.edu.espe.gpi.dao.otheractivities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IOtherActivitiesDao extends CrudRepository<OtherActivities, Long> {
	@Query(value = "SELECT u FROM OtherActivities u WHERE u.removed = 0")
	public Optional<List<OtherActivities>> findAllEnable();
	
	@Query(value = "SELECT u FROM OtherActivities u WHERE u.removed = 0 and u.planning=?1")
	public Optional<List<OtherActivities>> findAllByPlanningID(Planning id);

	@Query(value = "SELECT u FROM OtherActivities u WHERE u.removed = 0 and u.id=?1")
	public Optional<OtherActivities> findByIdEnable(Long id);
}
