package ec.edu.espe.gpi.dao.activities;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.activities.Activities;



public interface IActivitiesDao extends CrudRepository<Activities, Long> {
	@Query(value = "SELECT u FROM Activities u WHERE u.removed = 0")
	public Optional<List<Activities>> findAllEnable();

	@Query(value = "SELECT u FROM Activities u WHERE u.removed = 0 and u.id=?1")
	public Optional<Activities> findByIdEnable(Long id);  
	
	@Query(value = "SELECT u FROM Activities u WHERE u.removed = 0 and u.maxDuration=?1")
	public Optional<List<Activities>> findByMaxDurationEnable(Long maxDuration);  

}
