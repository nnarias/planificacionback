package ec.edu.espe.gpi.dao.tracking;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.tracking.Resource;

public interface IResourceDao extends CrudRepository<Resource, Long> {
	@Query(value = "SELECT u FROM Resource u WHERE u.state = 1")
	public Optional<List<Resource>> findAllEnable();

	@Query(value = "SELECT u FROM Resource u WHERE u.state = 1 and u.id=?1")
	public Optional<Resource> findByIdEnable(Long id);

	public Optional<Resource> findByName(String name);

	@Query(value = "SELECT u FROM Resource u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Resource> findByNameNotPresent(Long id, String name);
}