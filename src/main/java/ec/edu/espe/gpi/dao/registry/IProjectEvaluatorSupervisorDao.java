package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ec.edu.espe.gpi.model.registry.ProjectEvaluatorSupervisor;

public interface IProjectEvaluatorSupervisorDao extends CrudRepository<ProjectEvaluatorSupervisor, Long> {

	@Query(value = "SELECT u FROM ProjectEvaluatorSupervisor u WHERE u.role.roleName=?1")
	public Optional<List<ProjectEvaluatorSupervisor>> findAllRole(String role);

	@Query(value = "SELECT u FROM ProjectEvaluatorSupervisor u WHERE u.username=?1 and u.role.roleName=?2")
	public Optional<ProjectEvaluatorSupervisor> findByEvaluatorSupervisorRole(String username, String role);

}
