package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.EconomicPartnerGoal;

public interface IEconomicPartnerGoalDao extends CrudRepository<EconomicPartnerGoal, Long> {

	@Query(value = "SELECT u FROM EconomicPartnerGoal u WHERE u.state = 1")
	public Optional<List<EconomicPartnerGoal>> findAllEnable();

	@Query(value = "SELECT u FROM EconomicPartnerGoal u WHERE u.state = 1 and u.id=?1")
	public Optional<EconomicPartnerGoal> findByIdEnable(Long id);

	public Optional<EconomicPartnerGoal> findByName(String name);

	@Query(value = "SELECT u FROM EconomicPartnerGoal u WHERE u.id != ?1 and u.name = ?2")
	public Optional<EconomicPartnerGoal> findByNameNotPresent(Long id, String name);
}