package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.LineResearch;

public interface ILineResearchDao extends CrudRepository<LineResearch, Long> {

	@Query(value = "SELECT u FROM LineResearch u WHERE u.state = 1")
	public Optional<List<LineResearch>> findAllEnable();

	@Query(value = "SELECT u FROM LineResearch u WHERE u.state = 1 and u.id=?1")
	public Optional<LineResearch> findByIdEnable(Long id);

	public Optional<LineResearch> findByName(String name);

	@Query(value = "SELECT u FROM LineResearch u WHERE u.id != ?1 and u.name = ?2")
	public Optional<LineResearch> findByNameNotPresent(Long id, String name);
}