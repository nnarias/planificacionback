package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ResearchGroup;

public interface IResearchGroupDao extends CrudRepository<ResearchGroup, Long> {

	@Query(value = "SELECT u FROM ResearchGroup u WHERE u.state = 1")
	public Optional<List<ResearchGroup>> findAllEnable();

	@Query(value = "SELECT u FROM ResearchGroup u WHERE u.state = 1 and u.id=?1")
	public Optional<ResearchGroup> findByIdEnable(Long id);

	public Optional<ResearchGroup> findByName(String name);

	@Query(value = "SELECT u FROM ResearchGroup u WHERE u.id != ?1 and u.name = ?2")
	public Optional<ResearchGroup> findByNameNotPresent(Long id, String name);

}
