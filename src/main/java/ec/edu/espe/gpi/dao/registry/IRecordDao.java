package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Record;

public interface IRecordDao extends CrudRepository<Record, Long> {

	@Query(value = "SELECT u FROM Record u WHERE u.state = 1")
	public List<Record> findAllEnable();

	@Query(value = "SELECT u FROM Record u WHERE u.state = 1 and u.id=?1")
	public Record findByIdEnable(Long id);
}