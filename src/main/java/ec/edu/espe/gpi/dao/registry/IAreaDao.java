package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Area;

public interface IAreaDao extends CrudRepository<Area, Long> {
	
	@Query(value = "SELECT u FROM Area u WHERE u.state = 1")
	public Optional<List<Area>> findAllEnable();

	@Query(value = "SELECT u FROM Area u WHERE u.state = 1 and u.id=?1")
	public Optional<Area> findByIdEnable(Long id);
}