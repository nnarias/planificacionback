package ec.edu.espe.gpi.dao.security;

import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.security.Menu;

public interface IMenuDao extends CrudRepository<Menu, Long> {

}