package ec.edu.espe.gpi.dao.admin;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.admin.News;

public interface INewsDao extends CrudRepository<News, Long> {

	@Query(value = "SELECT u FROM News u")
	public Optional<List<News>> findList();
}