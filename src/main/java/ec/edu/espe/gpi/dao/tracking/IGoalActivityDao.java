package ec.edu.espe.gpi.dao.tracking;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.tracking.GoalActivity;

public interface IGoalActivityDao extends CrudRepository<GoalActivity, Long> {

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.projectGoal.project.id = ?1 and u.projectGoal.project.projectDirector = ?2 and u.projectGoal.project.state = 7 and u.state = 1")
	public Optional<List<GoalActivity>> findAllPending(Long projectId, String projectDirector);

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.projectGoal.project.id = ?1 and u.projectGoal.project.technicalSupervisor = ?2 and u.projectGoal.project.state = 7 and u.state = 2")
	public Optional<List<GoalActivity>> findAllTracking(Long projectId, String technicalSupervisor);

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.id=?1 and u.state != 0 and u.state != 1")
	public Optional<GoalActivity> findByDeliverable(Long id);

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.id=?1 and u.state != 0")
	public Optional<GoalActivity> findByIdEnable(Long id);

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.id=?1 and u.projectGoal.project.projectDirector = ?2 and u.projectGoal.project.state = 7 and u.state = 1")
	public Optional<GoalActivity> findByPending(Long id, String projectDirector);

	@Query(value = "SELECT u FROM GoalActivity u WHERE u.id=?1 and u.projectGoal.project.technicalSupervisor = ?2 and u.projectGoal.project.state = 7 and u.state = 2")
	public Optional<GoalActivity> findByTracking(Long id, String technicalSupervisor);
}