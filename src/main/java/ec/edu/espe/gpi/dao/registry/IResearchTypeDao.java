package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ResearchType;

public interface IResearchTypeDao extends CrudRepository<ResearchType, Long> {

	@Query(value = "SELECT u FROM ResearchType u WHERE u.state = 1")
	public Optional<List<ResearchType>> findAllEnable();

	@Query(value = "SELECT u FROM ResearchType u WHERE u.state = 1 and u.id=?1")
	public Optional<ResearchType> findByIdEnable(Long id);

	public Optional<ResearchType> findByName(String name);

	@Query(value = "SELECT u FROM ResearchType u WHERE u.id != ?1 and u.name = ?2")
	public Optional<ResearchType> findByNameNotPresent(Long id, String name);
}