package ec.edu.espe.gpi.dao.registry;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Bibliography;

public interface IBibliographyDao extends CrudRepository<Bibliography, Long> {

	@Query(value = "SELECT u FROM Bibliography u WHERE u.state = 1 and u.id=?1")
	public Optional<Bibliography> findByIdEnable(Long id);
}