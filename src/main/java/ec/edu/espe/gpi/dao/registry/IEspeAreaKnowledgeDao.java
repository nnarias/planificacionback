package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.EspeAreaKnowledge;

public interface IEspeAreaKnowledgeDao extends CrudRepository<EspeAreaKnowledge, Long> {

	@Query(value = "SELECT u FROM EspeAreaKnowledge u WHERE u.state = 1")
	public Optional<List<EspeAreaKnowledge>> findAllEnable();

	@Query(value = "SELECT u FROM EspeAreaKnowledge u WHERE u.state = 1 and u.id=?1")
	public Optional<EspeAreaKnowledge> findByIdEnable(Long id);

	public Optional<EspeAreaKnowledge> findByName(String name);

	@Query(value = "SELECT u FROM EspeAreaKnowledge u WHERE u.id != ?1 and u.name = ?2")
	public Optional<EspeAreaKnowledge> findByNameNotPresent(Long id, String name);
}