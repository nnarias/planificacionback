package ec.edu.espe.gpi.dao.evaluation;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.EvaluationCriteria;

public interface IEvaluationCriteriaDao extends CrudRepository<EvaluationCriteria, Long> {
	@Query(value = "SELECT u FROM EvaluationCriteria u WHERE u.state = 1 and u.id=?1")
	public Optional<EvaluationCriteria> findByIdEnable(Long id);
}