package ec.edu.espe.gpi.dao.evaluation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.Subcriteria;

public interface ISubcriteriaDao extends CrudRepository<Subcriteria, Long> {
	@Query(value = "SELECT u FROM Subcriteria u WHERE u.state = 1")
	public Optional<List<Subcriteria>> findAllEnable();

	@Query(value = "SELECT u FROM Subcriteria u WHERE u.state = 1 and u.id=?1")
	public Optional<Subcriteria> findByIdEnable(Long id);

	public Optional<Subcriteria> findByName(String name);

	@Query(value = "SELECT u FROM Subcriteria u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Subcriteria> findByNameNotPresent(Long id, String name);
}