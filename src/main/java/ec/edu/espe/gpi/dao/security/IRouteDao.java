package ec.edu.espe.gpi.dao.security;

import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.security.Route;

public interface IRouteDao extends CrudRepository<Route, Long> {

}