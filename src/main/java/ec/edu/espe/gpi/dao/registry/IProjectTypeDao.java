package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ProjectType;

public interface IProjectTypeDao extends CrudRepository<ProjectType, Long> {

	@Query(value = "SELECT u FROM ProjectType u WHERE u.state = 1")
	public Optional<List<ProjectType>> findAllEnable();

	@Query(value = "SELECT u FROM ProjectType u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectType> findByIdEnable(Long id);

	public Optional<ProjectType> findByName(String name);

	@Query(value = "SELECT u FROM ProjectType u WHERE u.id != ?1 and u.name = ?2")
	public Optional<ProjectType> findByNameNotPresent(Long id, String name);
}