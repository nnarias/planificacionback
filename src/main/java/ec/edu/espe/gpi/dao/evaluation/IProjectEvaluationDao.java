package ec.edu.espe.gpi.dao.evaluation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.ProjectEvaluation;

public interface IProjectEvaluationDao extends CrudRepository<ProjectEvaluation, Long> {
	@Query(value = "SELECT u FROM ProjectEvaluation u WHERE u.state = 1")
	public Optional<List<ProjectEvaluation>> findAllEnable();

	@Query(value = "SELECT u FROM ProjectEvaluation u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectEvaluation> findByIdEnable(Long id);
}