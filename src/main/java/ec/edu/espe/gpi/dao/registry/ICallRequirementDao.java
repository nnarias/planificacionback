package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.CallRequirement;

public interface ICallRequirementDao extends CrudRepository<CallRequirement, Long> {

	@Query(value = "SELECT u FROM CallRequirement u WHERE u.call.id = ?1 and u.state=1")
	public Optional<List<CallRequirement>> findByCall(Long idCall);
}