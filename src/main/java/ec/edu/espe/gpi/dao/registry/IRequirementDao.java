package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.Requirement;

public interface IRequirementDao extends CrudRepository<Requirement, Long> {

	@Query(value = "SELECT u FROM Requirement u WHERE u.state = 1")
	public Optional<List<Requirement>> findAllEnable();

	@Query(value = "SELECT u FROM Requirement u WHERE u.state = 1 and u.id=?1")
	public Optional<Requirement> findByIdEnable(Long id);

	public Optional<Requirement> findByName(String name);

	@Query(value = "SELECT u FROM Requirement u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Requirement> findByNameNotPresent(Long id, String name);
}