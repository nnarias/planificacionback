package ec.edu.espe.gpi.dao.tracking;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.tracking.ProjectGoal;

public interface IGoalDao extends CrudRepository<ProjectGoal, Long> {

	@Query(value = "SELECT u FROM ProjectGoal u WHERE u.state = 1")
	public Optional<List<ProjectGoal>> findAllEnable();

	@Query(value = "SELECT u FROM ProjectGoal u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectGoal> findByIdEnable(Long id);

	@Query(value = "SELECT u FROM ProjectGoal u WHERE u.state = 1 and u.project.id=?1 and u.project.state=1")
	public Optional<List<ProjectGoal>> findByProject(Long project);
}