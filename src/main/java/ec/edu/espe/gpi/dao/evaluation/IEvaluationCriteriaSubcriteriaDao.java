package ec.edu.espe.gpi.dao.evaluation;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.EvaluationCriteriaSubcriteria;

public interface IEvaluationCriteriaSubcriteriaDao extends CrudRepository<EvaluationCriteriaSubcriteria, Long> {

	@Query(value = "SELECT u FROM EvaluationCriteriaSubcriteria u WHERE u.state = 1 and u.id=?1")
	public Optional<EvaluationCriteriaSubcriteria> findByIdEnable(Long id);
}