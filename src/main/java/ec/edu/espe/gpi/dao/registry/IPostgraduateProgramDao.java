package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.PostgraduateProgram;

public interface IPostgraduateProgramDao extends CrudRepository<PostgraduateProgram, Long> {

	@Query(value = "SELECT u FROM PostgraduateProgram u WHERE u.state = 1")
	public Optional<List<PostgraduateProgram>> findAllEnable();

	@Query(value = "SELECT u FROM PostgraduateProgram u WHERE u.state = 1 and u.id=?1")
	public Optional<PostgraduateProgram> findByIdEnable(Long id);

	public Optional<PostgraduateProgram> findByName(String name);

	@Query(value = "SELECT u FROM PostgraduateProgram u WHERE u.id != ?1 and u.name = ?2")
	public Optional<PostgraduateProgram> findByNameNotPresent(Long id, String name);
}