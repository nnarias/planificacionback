package ec.edu.espe.gpi.dao.planning;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IPlanningDao extends CrudRepository<Planning, Long>{
	@Query(value = "SELECT u FROM Planning u WHERE u.removed = 0")
	public Optional<List<Planning>> findAllEnable();
	
	@Query(value = "SELECT u FROM Planning u")
	public Optional<List<Planning>> findAllIR();

	@Query(value = "SELECT u FROM Planning u WHERE u.removed = 0 and u.id=?1")
	public Optional<Planning> findByIdEnable(Long id);

}
