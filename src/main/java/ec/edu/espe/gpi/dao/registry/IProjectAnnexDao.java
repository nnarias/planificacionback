package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ProjectAnnex;

public interface IProjectAnnexDao extends CrudRepository<ProjectAnnex, Long> {

	@Query(value = "SELECT u FROM ProjectAnnex u WHERE u.project.id = ?1 and u.state = 1")
	public Optional<List<ProjectAnnex>> findAllByProject(Long projectId);

	@Query(value = "SELECT u FROM ProjectAnnex u WHERE u.state = 1 and u.id=?1")
	public Optional<ProjectAnnex> findByIdEnable(Long id);
}