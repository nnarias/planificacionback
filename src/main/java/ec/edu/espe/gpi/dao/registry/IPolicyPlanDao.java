package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.PolicyPlan;

public interface IPolicyPlanDao extends CrudRepository<PolicyPlan, Long> {

	@Query(value = "SELECT u FROM PolicyPlan u WHERE u.state = 1")
	public Optional<List<PolicyPlan>> findAllEnable();

	@Query(value = "SELECT u FROM PolicyPlan u WHERE u.state = 1 and u.id=?1")
	public Optional<PolicyPlan> findByIdEnable(Long id);

	@Query(value = "SELECT u FROM PolicyPlan u WHERE u.state = 1 and u.goalPlan.id=?1")
	public Optional<List<PolicyPlan>> findByGoalPlan(Long id);
}