package ec.edu.espe.gpi.dao.publications;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;

public interface IPublicationsDao extends CrudRepository<Publications, Long> {
	@Query(value = "SELECT u FROM Publications u WHERE u.removed = 0")
	public Optional<List<Publications>> findAllEnable();
	
	@Query(value = "SELECT u FROM Publications u WHERE u.removed = 0 and u.planning=?1")
	public Optional<List<Publications>> findAllByPlanningID(Planning id);

	@Query(value = "SELECT u FROM Publications u WHERE u.removed = 0 and u.id=?1")
	public Optional<Publications> findByIdEnable(Long id);
}
