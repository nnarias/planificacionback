package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.SpecificAreaKnowledge;

public interface ISpecificAreaKnowledgeDao extends CrudRepository<SpecificAreaKnowledge, Long> {

	@Query(value = "SELECT u FROM SpecificAreaKnowledge u WHERE u.state = 1")
	public Optional<List<SpecificAreaKnowledge>> findAllEnable();

	@Query(value = "SELECT u FROM SpecificAreaKnowledge u WHERE u.state = 1 and u.id=?1")
	public Optional<SpecificAreaKnowledge> findByIdEnable(Long id);

	public Optional<SpecificAreaKnowledge> findByName(String name);

	@Query(value = "SELECT u FROM SpecificAreaKnowledge u WHERE u.id != ?1 and u.name = ?2")
	public Optional<SpecificAreaKnowledge> findByNameNotPresent(Long id, String name);
}