package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.GoalPlan;

public interface IGoalPlanDao extends CrudRepository<GoalPlan, Long> {

	@Query(value = "SELECT u FROM GoalPlan u WHERE u.state = 1")
	public Optional<List<GoalPlan>> findAllEnable();

	@Query(value = "SELECT u FROM GoalPlan u WHERE u.state = 1 and u.id=?1")
	public Optional<GoalPlan> findByIdEnable(Long id);
}