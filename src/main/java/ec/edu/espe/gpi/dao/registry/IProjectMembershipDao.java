package ec.edu.espe.gpi.dao.registry;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.ProjectMembership;

public interface IProjectMembershipDao extends CrudRepository<ProjectMembership, Long> {

	@Query(value = "SELECT u FROM ProjectMembership u WHERE u.state = 1")
	public List<ProjectMembership> findAllEnable();

	@Query(value = "SELECT u FROM ProjectMembership u WHERE u.state = 1 and u.id=?1")
	public ProjectMembership findByIdEnable(Long id);
}