package ec.edu.espe.gpi.dao.admin;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.admin.ProjectNotification;

public interface IProjectNotificationDao extends CrudRepository<ProjectNotification, Long> {

	@Query(value = "SELECT u FROM ProjectNotification u WHERE u.user=?1")
	public Optional<List<ProjectNotification>> findByUser(String user);
	
	@Query(value = "SELECT u FROM ProjectNotification u WHERE u.project.id=?1")
	public Optional<List<ProjectNotification>> findByProject(Long projectId);
}