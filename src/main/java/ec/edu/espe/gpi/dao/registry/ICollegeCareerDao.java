package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.CollegeCareer;

public interface ICollegeCareerDao extends CrudRepository<CollegeCareer, Long> {

	@Query(value = "SELECT u FROM CollegeCareer u WHERE u.state = 1")
	public Optional<List<CollegeCareer>> findAllEnable();

	@Query(value = "SELECT u FROM CollegeCareer u WHERE u.state = 1 and u.id=?1")
	public Optional<CollegeCareer> findByIdEnable(Long id);

	public Optional<CollegeCareer> findByName(String name);

	@Query(value = "SELECT u FROM CollegeCareer u WHERE u.id != ?1 and u.name = ?2")
	public Optional<CollegeCareer> findByNameNotPresent(Long id, String name);
}