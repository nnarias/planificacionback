package ec.edu.espe.gpi.dao.tracking;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.tracking.ActivityResource;

public interface IActivityResourceDao extends CrudRepository<ActivityResource, Long> {

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.goalActivity.projectGoal.project.id = ?1 and u.goalActivity.projectGoal.project.projectDirector = ?2 and u.goalActivity.projectGoal.project.state = 7 and u.state = 1")
	public Optional<List<ActivityResource>> findAllPending(Long projectId, String projectDirector);

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.goalActivity.projectGoal.project.id = ?1 and u.goalActivity.projectGoal.project.financialSupervisor = ?2 and u.goalActivity.projectGoal.project.state = 7 and u.state = 2")
	public Optional<List<ActivityResource>> findAllTracking(Long projectId, String financialSupervisor);

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.id=?1 and u.state != 0")
	public Optional<ActivityResource> findByIdEnable(Long id);

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.id = ?1 and u.state != 0 and u.state != 1")
	public Optional<ActivityResource> findByIdInvoice(Long activityResourceId);

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.id = ?1 and u.goalActivity.projectGoal.project.projectDirector = ?2 and u.goalActivity.projectGoal.project.state = 7 and u.state = 1")
	public Optional<ActivityResource> findByIdPending(Long activityResourceId, String projectDirector);

	@Query(value = "SELECT u FROM ActivityResource u WHERE u.id = ?1 and u.goalActivity.projectGoal.project.financialSupervisor = ?2 and u.goalActivity.projectGoal.project.state = 7 and u.state = 2")
	public Optional<ActivityResource> findByIdTracking(Long activityResourceId, String financialSupervisor);
}