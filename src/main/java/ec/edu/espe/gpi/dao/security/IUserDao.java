package ec.edu.espe.gpi.dao.security;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.security.UserModel;

public interface IUserDao extends CrudRepository<UserModel, Long> {

	@Query(value = "SELECT u FROM UserModel u WHERE u.state = 1")
	public Optional<List<UserModel>> findAllEnable();

	public Optional<UserModel> findByEmail(String email);

	@Query(value = "SELECT u FROM UserModel u WHERE u.id != ?2 and u.email = ?1")
	public Optional<UserModel> findByEmailNotPresent(String email, Long id);

	@Query(value = "SELECT u FROM UserModel u WHERE u.state = 1 and u.id=?1")
	public Optional<UserModel> findByIdEnable(Long id);

	public Optional<UserModel> findByUsername(String username);

	@Query("SELECT u FROM UserModel u WHERE  u.state = 1 and u.username=?1")
	public Optional<UserModel> findByUsernameEnable(String username);
}