package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import ec.edu.espe.gpi.model.registry.Project;

@Repository
public interface IProjectDao extends CrudRepository<Project, Long> {

	@Query(value = "SELECT u FROM Project u WHERE u.projectDirector=?1 and u.state = 5")
	public Optional<List<Project>> findAllApproved(String projectDirector);

	@Query(value = "SELECT u FROM Project u WHERE u.projectDirector=?1 and u.state = 3")
	public Optional<List<Project>> findAllApproving(String projectDirector);

	@Query(value = "SELECT u FROM Project u WHERE u.projectDirector=?1 and u.state = 7")
	public Optional<List<Project>> findAllDeveloping(String projectDirector);

	@Query(value = "SELECT u FROM Project u WHERE u.state != 0")
	public List<Project> findAllEnable();

	@Query(value = "SELECT u FROM Project u WHERE u.state = 5")
	public Optional<List<Project>> findAllEvaluating();

	@Query(value = "SELECT u FROM Project u WHERE u.state = 1")
	public Optional<List<Project>> findAllRegistered();

	@Query(value = "SELECT u FROM Project u WHERE u.call.id=?1 and u.state != 0")
	public Optional<List<Project>> findAllByCall(Long idCall);

	@Query(value = "SELECT u FROM Project u WHERE u.departmentalEvaluator=?1 and u.state = 3")
	public Optional<List<Project>> findAllByDepartmentalEvaluator(String departmentalEvaluator);

	@Query(value = "SELECT u FROM Project u WHERE u.call.id=?1 and u.state = 4")
	public Optional<List<Project>> findAllByEvaluatorAssigning(Long callId);

	@Query(value = "SELECT u FROM Project u WHERE u.financialSupervisor=?1 and u.state = 7")
	public Optional<List<Project>> findAllByFinancialSupervisor(String financialSupervisor);

	@Query(value = "SELECT u FROM Project u WHERE u.projectDirector=?1 and u.state != 0")
	public Optional<List<Project>> findAllByProjectDirector(String projectDirector);

	@Query(value = "SELECT u FROM Project u WHERE u.projectEvaluator=?1 and u.state = 5")
	public Optional<List<Project>> findAllByProjectEvaluator(String projectEvaluator);

	@Query(value = "SELECT u FROM Project u WHERE u.technicalSupervisor=?1 and u.state = 7")
	public Optional<List<Project>> findAllByTechnicalSupervisor(String technicalSupervisor);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.departmentalEvaluator=?2 and u.state != 0")
	public Optional<Project> findByDepartmentalEvaluator(Long id, String departmentalEvaluator);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.state = 4")
	public Optional<Project> findByEvaluatorAssigning(Long id);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.financialSupervisor=?2 and u.state != 0")
	public Optional<Project> findByFinancialSupervisor(Long id, String financialSupervisor);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.state != 0")
	public Optional<Project> findByIdEnable(Long id);

	public Optional<Project> findByNameEnglish(String name);

	@Query(value = "SELECT u FROM Project u WHERE u.id != ?1 and u.nameEnglish = ?2")
	public Optional<Project> findByNameEnglishNotPresent(Long id, String name);

	public Optional<Project> findByNameSpanish(String name);

	@Query(value = "SELECT u FROM Project u WHERE u.id != ?1 and u.nameSpanish = ?2")
	public Optional<Project> findByNameSpanishNotPresent(Long id, String name);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.departmentalEvaluator=?2 and u.state != 0")
	public Optional<Project> findByProjectDepartmentalEvaluator(Long id, String departmentalEvaluator);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.projectDirector=?2 and u.state != 0")
	public Optional<Project> findByProjectDirector(Long id, String projectDirector);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.projectEvaluator=?2 and u.state != 0")
	public Optional<Project> findByProjectEvaluator(Long id, String projectEvaluator);

	@Query(value = "SELECT u FROM Project u WHERE u.id=?1 and u.technicalSupervisor=?2 and u.state != 0")
	public Optional<Project> findByTechnicalSupervisor(Long id, String technicalSupervisor);
}