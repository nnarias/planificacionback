package ec.edu.espe.gpi.dao.evaluation;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.evaluation.Evaluation;

public interface IEvaluationDao extends CrudRepository<Evaluation, Long> {
	@Query(value = "SELECT u FROM Evaluation u WHERE u.state = 1")
	public Optional<List<Evaluation>> findAllEnable();

	@Query(value = "SELECT u FROM Evaluation u WHERE u.state = 1 and u.id=?1")
	public Optional<Evaluation> findByIdEnable(Long id);

	public Optional<Evaluation> findByName(String name);

	@Query(value = "SELECT u FROM Evaluation u WHERE u.id != ?1 and u.name = ?2")
	public Optional<Evaluation> findByNameNotPresent(Long id, String name);
}