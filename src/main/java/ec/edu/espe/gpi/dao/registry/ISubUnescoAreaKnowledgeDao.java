package ec.edu.espe.gpi.dao.registry;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import ec.edu.espe.gpi.model.registry.SubUnescoAreaKnowledge;

public interface ISubUnescoAreaKnowledgeDao extends CrudRepository<SubUnescoAreaKnowledge, Long> {

	@Query(value = "SELECT u FROM SubUnescoAreaKnowledge u WHERE u.state = 1")
	public Optional<List<SubUnescoAreaKnowledge>> findAllEnable();

	@Query(value = "SELECT u FROM SubUnescoAreaKnowledge u WHERE u.state = 1 and u.id=?1")
	public Optional<SubUnescoAreaKnowledge> findByIdEnable(Long id);

	@Query(value = "SELECT u FROM SubUnescoAreaKnowledge u WHERE u.state = 1 and u.unescoAreaKnowledge.id=?1")
	public Optional<List<SubUnescoAreaKnowledge>> findByUnesco(Long id);

	public Optional<SubUnescoAreaKnowledge> findByName(String name);

	@Query(value = "SELECT u FROM SubUnescoAreaKnowledge u WHERE u.id != ?1 and u.name = ?2")
	public Optional<SubUnescoAreaKnowledge> findByNameNotPresent(Long id, String name);
}