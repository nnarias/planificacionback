package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IGoalPlanDao;
import ec.edu.espe.gpi.dto.registry.GoalPlanDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.GoalPlan;

@Service
public class GoalPlanService implements IGoalPlanService {

	@Autowired
	private IGoalPlanDao goalPlanDao;

	@Override
	@Transactional(readOnly = true)
	public List<GoalPlan> findAll() {
		return goalPlanDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public GoalPlan findById(Long id) {
		return goalPlanDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(GoalPlan.class, "id", id));
	}

	@Override
	@Transactional
	public void save(GoalPlanDTO goalPlanDTO) {
		GoalPlan goalPlan = new GoalPlan();
		try {
			Long id = goalPlanDTO.getId();
			if (id != null) {
				goalPlan = findById(id);
			} else {
				goalPlan.setState(true);
			}
			goalPlan.setName(goalPlanDTO.getName());
			goalPlanDao.save(goalPlan);
		} catch (DataAccessException e) {
			throw new DataException(GoalPlan.class, "Error al guardar el objetivo del plan de desarrollo",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			GoalPlan goalPlan = findById(id);
			goalPlan.setState(false);
			goalPlanDao.save(goalPlan);
		} catch (DataAccessException e) {
			throw new DataException(GoalPlan.class, "Error al eliminar el objetivo del plan de desarrollo",
					e.getMostSpecificCause().getMessage());
		}
	}
}