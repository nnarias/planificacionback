package ec.edu.espe.gpi.service.security;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;

import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.security.PerfilMiespe;
import ec.edu.espe.gpi.utils.security.UserCedulaMiespe;
import ec.edu.espe.gpi.utils.security.UserProfile;
import ec.edu.espe.gpi.utils.security.UserUsernameMiespe;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;
import ec.edu.espe.gpi.utils.security.UsuarioRolMiespe;

@Service
public class MiespeService implements IMiespeService {
	private static final Logger logger = Logger.getLogger(MiespeService.class.getName());
	private static final String SERVER_PROBLEM = "Problemas con el servidor";

	@Autowired
	private Environment env;

	@Autowired
	private IRoleService roleService;

	@Override
	public UserldapMiespe findUserldap(String username) {
		String url = env.getProperty("app.config.miespe.login");
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeadersBasic());
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<UserldapMiespe> resultado = restTemplate.exchange(url + username, HttpMethod.GET, request,
					UserldapMiespe.class);
			switch (resultado.getStatusCodeValue()) {
			case 200:
				UserldapMiespe userldapMiespe = resultado.getBody();
				if (userldapMiespe == null) {
					return null;
				}
				userldapMiespe.setPassword("{SSHA}" + userldapMiespe.getPassword());
				return userldapMiespe;
			case 500:
				logger.log(Level.SEVERE, SERVER_PROBLEM);
				return null;
			default:
				return null;
			}
		} catch (HttpStatusCodeException exception) {
			return null;
		}
	}

	@Override
	public UserUsernameMiespe findUserUsername(String username) {
		String url = env.getProperty("app.config.miespe.persona.username");
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeadersBasic());
		RestTemplate restTemplate = new RestTemplate();

		try {
			ResponseEntity<UserUsernameMiespe[]> resultado = restTemplate.exchange(url + username, HttpMethod.GET,
					request, UserUsernameMiespe[].class);

			switch (resultado.getStatusCodeValue()) {
			case 200:
				UserUsernameMiespe[] userUsernameMiespeList = resultado.getBody();
				if (userUsernameMiespeList != null && userUsernameMiespeList.length > 0) {
					return userUsernameMiespeList[0];
				} else {
					return null;
				}

			case 500:
				logger.log(Level.SEVERE, SERVER_PROBLEM);
				return null;
			default:
				return null;
			}
		} catch (HttpStatusCodeException exception) {
			return null;
		}

	}

	@Override
	public UserCedulaMiespe findUserCedula(String cedula) {
		return null;
	}

	@Override
	public List<Role> findUserListRole(List<PerfilMiespe> perfilList) {
		List<Role> roleCurrentList = new ArrayList<>();
		List<Role> roleUserList = new ArrayList<>();
		List<Role> roleList = roleService.findAll();
		for (PerfilMiespe perfilMiespe : perfilList) {
			for (Role role : roleList) {
				if (role.getRoleName().equals(perfilMiespe.getPerfil())) {
					roleCurrentList.add(role);
				}
			}
		}
		for (Role role : roleCurrentList) {
			Role roleRoute = roleService.findById(role.getId());
			roleUserList.add(roleRoute);
		}

		return roleUserList;
	}

	@Override
	public UserProfile[] findUserProfileList(String profile) {
		String url = env.getProperty("app.config.miespe.perfil.persona");
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeadersBasic());
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<UserProfile[]> resultado = restTemplate.exchange(url + profile, HttpMethod.GET, request,
					UserProfile[].class);
			switch (resultado.getStatusCodeValue()) {
			case 200:
				return resultado.getBody();
			case 500:
				logger.log(Level.SEVERE, SERVER_PROBLEM);
				return new UserProfile[] {};
			default:
				return new UserProfile[] {};
			}
		} catch (HttpStatusCodeException exception) {
			return new UserProfile[] {};
		}
	}

	@Override
	public UsuarioRolMiespe findUserRole(String codId) {
		String url = env.getProperty("app.config.miespe.persona.rol");
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeadersBasic());
		RestTemplate restTemplate = new RestTemplate();
		try {
			if (url == null) {
				return null;
			}
			url = url.replace("L00ID", codId);
			ResponseEntity<UsuarioRolMiespe> resultado = restTemplate.exchange(url, HttpMethod.GET, request,
					UsuarioRolMiespe.class);
			switch (resultado.getStatusCodeValue()) {
			case 200:
				UsuarioRolMiespe usuarioRolMiespe = resultado.getBody();
				return usuarioRolMiespe;
			case 500:
				logger.log(Level.SEVERE, SERVER_PROBLEM);
				return null;
			default:
				return null;
			}
		} catch (HttpStatusCodeException | NullPointerException exception) {
			return null;
		}
	}
}