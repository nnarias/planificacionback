package ec.edu.espe.gpi.service.tracking;

import java.io.IOException;
import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.dto.tracking.GoalActivityApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectDeliverableDTO;
import ec.edu.espe.gpi.model.tracking.GoalActivity;

public interface IGoalActivityService {

	public void deliverableApproval(GoalActivityApprovalDTO goalActivityApprovalDTO, Authentication authentication);

	public GoalActivity find(Long id);

	public List<GoalActivity> findAllPending(Long idProject, Authentication authentication);

	public List<GoalActivity> findAllTracking(Long idProject, Authentication authentication);

	public GoalActivity findByDeliverable(Long id);

	public GoalActivity findPending(Long id, Authentication authentication);

	public GoalActivity findTracking(Long id, Authentication authentication);

	public void saveDeliverable(ProjectDeliverableDTO projectDeliverableDTO, Authentication authentication)
			throws IOException;
}