package ec.edu.espe.gpi.service.registry;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectAnnexDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ProjectAnnex;

@Service
public class ProjectAnnexService implements IProjectAnnexService {

	@Autowired
	private IProjectAnnexDao projectAnnexDao;

	@Override
	@Transactional(readOnly = true)
	public ProjectAnnex find(Long id) {
		return projectAnnexDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ProjectAnnex.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectAnnex> findAllByProject(Long projectId) {
		return projectAnnexDao.findAllByProject(projectId)
				.orElseThrow(() -> new NotFoundException(ProjectAnnex.class, "id project", projectId)).stream()
				.map(temp -> {
					temp.setProject(null);
					return temp;
				}).collect(Collectors.toList());
	}
}