package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CoverageTypeDTO;
import ec.edu.espe.gpi.model.registry.CoverageType;

public interface ICoverageTypeService {

	public List<CoverageType> findAll();

	public CoverageType findById(Long id);

	public void save(CoverageTypeDTO coverageTypeDTO);

	public void updateState(Long id);
}