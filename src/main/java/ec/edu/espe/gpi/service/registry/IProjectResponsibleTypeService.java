package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectResponsibleTypeDTO;
import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;

public interface IProjectResponsibleTypeService {

	public List<ProjectResponsibleType> findAll();

	public ProjectResponsibleType findById(Long id);

	public void save(ProjectResponsibleTypeDTO projectResponsibleTypeDTO);

	public void updateState(Long id);
}