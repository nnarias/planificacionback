package ec.edu.espe.gpi.service.planning;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.espe.gpi.dao.planning.IBookDao;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class BookService implements IBookService {

	@Autowired
	private IBookDao bookDao;

	@Override
	@Transactional(readOnly = true)
	public List<Book> findAll() {
		return bookDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Book findById(Long id) {
		return bookDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Book.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Book> findByIdStateFalse(Long id) {
		return bookDao.findById(id);
	}

	@Override
	@Transactional
	public void save(BookDTO bookDTO) {
		Book book = new Book();
		try {
			Long id = bookDTO.getId();
			if (id != null) {
				//modificar
				book = findById(id);
				book.setCodies(bookDTO.getCodies());
				book.setPublicationCode(bookDTO.getPublicationCode());
				book.setPublicationDate(bookDTO.getPublicationDate());
				book.setPublicationType(bookDTO.getPublicationType());
				
			} else {
				//crear
				book.setCodies(bookDTO.getCodies());
				book.setPublicationCode(bookDTO.getPublicationCode());
				book.setPublicationDate(bookDTO.getPublicationDate());
				book.setPublicationType(bookDTO.getPublicationType());
				book.setState(true);
			}
			bookDao.save(book);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar el libro", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Book book = findById(id);
			book.setState(false);
			bookDao.save(book);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar el libro", e.getMessage());
		}

	}

}
