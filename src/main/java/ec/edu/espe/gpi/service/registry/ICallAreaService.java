package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CallAreaDTO;
import ec.edu.espe.gpi.model.registry.CallArea;

public interface ICallAreaService {

	public List<CallArea> findAll();

	public CallArea findById(Long id);

	public void save(CallAreaDTO callAreaDTO);

	public void updateState(Long id);
}