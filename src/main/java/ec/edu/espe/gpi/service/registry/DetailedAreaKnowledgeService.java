package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IDetailedAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.DetailedAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.DetailedAreaKnowledge;

@Service
public class DetailedAreaKnowledgeService implements IDetailedAreaKnowledgeService {

	@Autowired
	private IDetailedAreaKnowledgeDao detailedAreaKnowledgeDao;

	@Override
	@Transactional(readOnly = true)
	public List<DetailedAreaKnowledge> findAll() {
		return detailedAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public DetailedAreaKnowledge findById(Long id) {
		return detailedAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(DetailedAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional
	public void save(DetailedAreaKnowledgeDTO detailedAreaKnowledgeDTO) {
		DetailedAreaKnowledge detailedAreaKnowledge = new DetailedAreaKnowledge();
		try {
			Long id = detailedAreaKnowledgeDTO.getId();
			if (id != null) {
				detailedAreaKnowledge = findById(id);
				if (detailedAreaKnowledgeDao.findByNameNotPresent(id, detailedAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(DetailedAreaKnowledge.class, "name",
							detailedAreaKnowledge.getName());
				}
			} else {
				if (detailedAreaKnowledgeDao.findByName(detailedAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(DetailedAreaKnowledge.class, "name",
							detailedAreaKnowledgeDTO.getName());
				}
				detailedAreaKnowledge.setState(true);
			}
			detailedAreaKnowledge.setName(detailedAreaKnowledgeDTO.getName());
			detailedAreaKnowledgeDao.save(detailedAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(DetailedAreaKnowledge.class, "Error al guardar el campo detallado",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			DetailedAreaKnowledge detailedAreaKnowledge = findById(id);
			detailedAreaKnowledge.setState(false);
			detailedAreaKnowledgeDao.save(detailedAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(DetailedAreaKnowledge.class, "Error al eliminar campo detallado",
					e.getMostSpecificCause().getMessage());
		}
	}
}