package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectResponsibleDTO;
import ec.edu.espe.gpi.model.registry.ProjectResponsible;

public interface IProjectResponsibleService {

	public List<ProjectResponsible> findAll();

	public ProjectResponsible findById(Long id);

	public void save(ProjectResponsibleDTO projectResponsibleDTO);

	public void updateState(Long id);
}