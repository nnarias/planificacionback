package ec.edu.espe.gpi.service.evaluation;

import ec.edu.espe.gpi.dto.registry.ProjectEvaluatorSupervisorDTO;

public interface IProjectEvaluatorSupervisorService {

	public void assignTechnicalSupervisorProject(ProjectEvaluatorSupervisorDTO projectEvaluatorSupervisorDTO);

	public void assignFinancialSupervisorProject(ProjectEvaluatorSupervisorDTO projectEvaluatorSupervisorDTO);
}