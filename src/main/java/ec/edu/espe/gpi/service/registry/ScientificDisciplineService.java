package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IScientificDisciplineDao;
import ec.edu.espe.gpi.dto.registry.ScientificDisciplineDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ScientificDiscipline;

@Service
public class ScientificDisciplineService implements IScientificDisciplineService {

	@Autowired
	private IScientificDisciplineDao scientificDisciplineDao;

	@Override
	@Transactional(readOnly = true)
	public List<ScientificDiscipline> findAll() {
		return scientificDisciplineDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ScientificDiscipline findById(Long id) {
		return scientificDisciplineDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ScientificDiscipline.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ScientificDisciplineDTO scientificDisciplineDTO) {
		ScientificDiscipline scientificDiscipline = new ScientificDiscipline();
		try {
			Long id = scientificDisciplineDTO.getId();
			if (id != null) {
				scientificDiscipline = findById(id);
				if (scientificDisciplineDao.findByNameNotPresent(id, scientificDisciplineDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ScientificDiscipline.class, "name",
							scientificDisciplineDTO.getName());
				}
			} else {
				if (scientificDisciplineDao.findByName(scientificDisciplineDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ScientificDiscipline.class, "name",
							scientificDisciplineDTO.getName());
				}
				scientificDiscipline.setState(true);
			}
			scientificDiscipline.setName(scientificDisciplineDTO.getName());
			scientificDisciplineDao.save(scientificDiscipline);
		} catch (DataAccessException e) {
			throw new DataException(ScientificDiscipline.class, "Error al guardar la disciplina científica",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ScientificDiscipline scientificDiscipline = findById(id);
			scientificDiscipline.setState(false);
			scientificDisciplineDao.save(scientificDiscipline);
		} catch (DataAccessException e) {
			throw new DataException(ScientificDiscipline.class, "Error al guardar la disciplina científica",
					e.getMostSpecificCause().getMessage());
		}
	}
}