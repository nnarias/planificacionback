package ec.edu.espe.gpi.service.registry;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dao.registry.IProjectDao;
import ec.edu.espe.gpi.dto.registry.BibliographyDTO;
import ec.edu.espe.gpi.dto.registry.ProjectAnnexDTO;
import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.dto.registry.ProjectRequirementDTO;
import ec.edu.espe.gpi.dto.registry.ProjectResponsibleDTO;
import ec.edu.espe.gpi.exception.BadRequest;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Annex;
import ec.edu.espe.gpi.model.registry.Bibliography;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;
import ec.edu.espe.gpi.model.registry.Coverage;
import ec.edu.espe.gpi.model.registry.InstitutionInvolved;
import ec.edu.espe.gpi.model.registry.PolicyPlan;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.model.registry.ProjectAnnex;
import ec.edu.espe.gpi.model.registry.ProjectRequirement;
import ec.edu.espe.gpi.model.registry.ProjectResponsible;
import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;
import ec.edu.espe.gpi.model.registry.Requirement;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;
import ec.edu.espe.gpi.utils.registry.ProjectAssignment;
import ec.edu.espe.gpi.utils.registry.ProjectStateEnum;
import ec.edu.espe.gpi.utils.security.UserGeneral;

@Service
public class ProjectService implements IProjectService {

	private static final String ERROR = "error";

	@Autowired
	private IAmpleAreaKnowledgeService ampleAreaKnowledgeService;

	@Autowired
	private IAnnexService annexService;

	@Autowired
	private IBibliographyService bibliographyService;

	@Autowired
	private ICallService callService;

	@Autowired
	private ICollegeCareerService collegeCareerService;

	@Autowired
	private ICollegeDepartmentService collegeDepartmentService;

	@Autowired
	private ICoverageService coverageService;

	@Autowired
	private IDetailedAreaKnowledgeService detailedAreaKnowledgeService;

	@Autowired
	private IEconomicPartnerGoalService economicPartnerGoalService;

	@Autowired
	private IEspeAreaKnowledgeService espeAreaKnowledgeService;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Autowired
	private IInstitutionInvolvedService institutionInvolvedService;

	@Autowired
	private ILineResearchService lineResearchService;

	@Autowired
	private IPolicyPlanService policyPlanService;

	@Autowired
	private IPostgraduateProgramService postgraduateProgramService;

	@Autowired
	private IProgramService programService;

	@Autowired
	private IProjectDao projectDao;

	@Autowired
	private IProjectAnnexService projectAnnexService;

	@Autowired
	private IProjectRequirementService projectRequirementService;

	@Autowired
	private IProjectResponsibleService projectResponsibleService;

	@Autowired
	private IProjectResponsibleTypeService projectResponsibleTypeService;

	@Autowired
	private IProjectStatusService projectStatusService;

	@Autowired
	private IRequirementService requirementService;

	@Autowired
	private IResearchGroupService researchGroupService;

	@Autowired
	private IResearchTypeService researchTypeService;

	@Autowired
	private IScientificDisciplineService scientificDisciplineService;

	@Autowired
	private ISpecificAreaKnowledgeService specificAreaKnowledgeService;

	@Autowired
	private ISubUnescoAreaKnowledgeService subUnescoAreaKnowledgeService;

	@Autowired
	private IUserService userService;

	private List<Bibliography> bibliographyListAssignment(List<BibliographyDTO> bibliographyDTOList, Project project) {
		List<Bibliography> bibliographyList = new ArrayList<>();
		try {
			for (BibliographyDTO bibliographyDTO : bibliographyDTOList) {
				Bibliography bibliography = new Bibliography();
				Long id = bibliographyDTO.getId();
				if (id != null) {
					bibliography = bibliographyService.find(id);
				} else {
					bibliography.setState(true);
				}
				if (Boolean.TRUE.equals(bibliographyDTO.getWebsite())) {
					bibliography.setCity(null);
					bibliography.setCountry(null);
					bibliography.setEditorial(null);
					bibliography.setPageName(bibliographyDTO.getPageName());
					bibliography.setUrl(bibliographyDTO.getUrl());
				} else {
					bibliography.setCity(bibliographyDTO.getCity());
					bibliography.setCountry(bibliographyDTO.getCountry());
					bibliography.setEditorial(bibliographyDTO.getEditorial());
					bibliography.setPageName(null);
					bibliography.setUrl(null);
				}
				bibliography.setAuthor(bibliographyDTO.getAuthor());
				bibliography.setTitle(bibliographyDTO.getTitle());
				bibliography.setWebsite(bibliographyDTO.getWebsite());
				bibliography.setYear(bibliographyDTO.getYear());
				bibliography.setProject(project);
				bibliographyList.add(bibliography);
			}
			return bibliographyList;
		} catch (Exception e) {
			throw new DataException(Bibliography.class, "Error al asignar Bibliograf�a", e.getMessage());
		}
	}

	private List<Coverage> coverageListAssignment(List<Long> coverageDTOList) {
		Coverage coverage = new Coverage();
		List<Coverage> listCoverage = new ArrayList<>();
		try {
			for (Long coverageId : coverageDTOList) {
				coverage = coverageService.findById(coverageId);
				listCoverage.add(coverage);
			}
			return listCoverage;
		} catch (Exception e) {
			throw new DataException(Coverage.class, "Error al asignar Coberturas", e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Project find(Long id) {
		return projectDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAll() {
		return projectDao.findAllEnable().stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllApproved(String projectDirector) {
		return projectDao.findAllApproved(projectDirector).orElse(new ArrayList<>()).stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllApproving(String projectDirector) {
		return projectDao.findAllApproving(projectDirector).orElse(new ArrayList<>()).stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllDeveloping(String projectDirector) {
		return projectDao.findAllDeveloping(projectDirector).orElse(new ArrayList<>()).stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByDepartmentalEvaluator(Authentication authentication) {
		return projectDao.findAllByDepartmentalEvaluator(authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					Project project = new Project();
					project.setId(temp.getId());
					project.setNameSpanish(temp.getNameSpanish());
					project.setNameEnglish(temp.getNameEnglish());
					Call call = new Call();
					call.setId(temp.getCall().getId());
					call.setName(temp.getCall().getName());
					call.setDescription(temp.getCall().getDescription());
					call.setStartDate(temp.getCall().getStartDate());
					call.setEndDate(temp.getCall().getEndDate());
					project.setCall(call);
					project.setState(temp.getState());
					return project;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByEvaluatorAssigning(Long id) {
		List<Project> projectList = new ArrayList<>();
		projectDao.findAllByEvaluatorAssigning(id).orElse(new ArrayList<>()).forEach(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			projectList.add(project);
		});
		return projectList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByFinancialSupervisor(Authentication authentication) {
		return projectDao.findAllByFinancialSupervisor(authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					Project project = new Project();
					project.setId(temp.getId());
					project.setNameSpanish(temp.getNameSpanish());
					project.setNameEnglish(temp.getNameEnglish());
					Call call = new Call();
					call.setId(temp.getCall().getId());
					call.setName(temp.getCall().getName());
					call.setDescription(temp.getCall().getDescription());
					call.setStartDate(temp.getCall().getStartDate());
					call.setEndDate(temp.getCall().getEndDate());
					project.setCall(call);
					project.setState(temp.getState());
					return project;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByProjectDirector(Authentication authentication) {
		return projectDao.findAllByProjectDirector(authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					Project project = new Project();
					project.setId(temp.getId());
					project.setNameSpanish(temp.getNameSpanish());
					project.setNameEnglish(temp.getNameEnglish());
					Call call = new Call();
					call.setId(temp.getCall().getId());
					call.setName(temp.getCall().getName());
					call.setDescription(temp.getCall().getDescription());
					call.setStartDate(temp.getCall().getStartDate());
					call.setEndDate(temp.getCall().getEndDate());
					project.setCall(call);
					project.setState(temp.getState());
					return project;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByProjectEvaluator(Authentication authentication) {
		return projectDao.findAllByProjectEvaluator(authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					Project project = new Project();
					project.setId(temp.getId());
					project.setNameSpanish(temp.getNameSpanish());
					project.setNameEnglish(temp.getNameEnglish());
					Call call = new Call();
					call.setId(temp.getCall().getId());
					call.setName(temp.getCall().getName());
					call.setDescription(temp.getCall().getDescription());
					call.setStartDate(temp.getCall().getStartDate());
					call.setEndDate(temp.getCall().getEndDate());
					project.setCall(call);
					project.setState(temp.getState());
					return project;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllByTechnicalSupervisor(Authentication authentication) {
		return projectDao.findAllByTechnicalSupervisor(authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					Project project = new Project();
					project.setId(temp.getId());
					project.setNameSpanish(temp.getNameSpanish());
					project.setNameEnglish(temp.getNameEnglish());
					Call call = new Call();
					call.setId(temp.getCall().getId());
					call.setName(temp.getCall().getName());
					call.setDescription(temp.getCall().getDescription());
					call.setStartDate(temp.getCall().getStartDate());
					call.setEndDate(temp.getCall().getEndDate());
					project.setCall(call);
					project.setState(temp.getState());
					return project;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllEvaluating() {
		return projectDao.findAllEvaluating().orElse(new ArrayList<>()).stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Project> findAllRegistered() {
		return projectDao.findAllRegistered().orElse(new ArrayList<>()).stream().map(temp -> {
			Project project = new Project();
			project.setId(temp.getId());
			project.setNameSpanish(temp.getNameSpanish());
			project.setNameEnglish(temp.getNameEnglish());
			Call call = new Call();
			call.setId(temp.getCall().getId());
			call.setName(temp.getCall().getName());
			call.setDescription(temp.getCall().getDescription());
			call.setStartDate(temp.getCall().getStartDate());
			call.setEndDate(temp.getCall().getEndDate());
			project.setCall(call);
			project.setState(temp.getState());
			return project;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByDepartmentalEvaluator(Authentication authentication, Long id) {
		return projectDao.findByDepartmentalEvaluator(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByEvaluatorAssigning(Long id) {
		return projectDao.findByEvaluatorAssigning(id)
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByFinancialSupervisor(Authentication authentication, Long id) {
		return projectDao.findByFinancialSupervisor(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByProjectDepartmentalEvaluator(Authentication authentication, Long id) {
		return projectDao.findByProjectDepartmentalEvaluator(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByProjectDirector(Authentication authentication, Long id) {
		return projectDao.findByProjectDirector(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByProjectEvaluator(Authentication authentication, Long id) {
		return projectDao.findByProjectEvaluator(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public Project findByTechnicalSupervisor(Authentication authentication, Long id) {
		return projectDao.findByTechnicalSupervisor(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional
	public Resource getFile(String name) {
		Path path = Paths.get("upload").resolve(name).toAbsolutePath();
		try {
			Resource resource = new UrlResource(path.toUri());
			if (!resource.exists() || !resource.isReadable()) {
				throw new NotFoundException(File.class, "Error al leer archivo", "nombre", name);
			}
			return resource;
		} catch (MalformedURLException e) {
			throw new DataException(File.class, "Error al leer archivo", "nombre", name);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public InputStreamResource getFileAnnexed(String fileUUID, TypeUserDocument typeUserDocument) throws IOException {
		return impDocumentService.getDocument(fileUUID, typeUserDocument);
	}

	private List<PolicyPlan> policyPlanListAssignment(List<Long> policyPlanIdList) {
		PolicyPlan policyPlan = new PolicyPlan();
		List<PolicyPlan> policyPlanList = new ArrayList<>();
		try {
			for (Long policyPlanId : policyPlanIdList) {
				policyPlan = policyPlanService.findById(policyPlanId);
				policyPlanList.add(policyPlan);
			}
			return policyPlanList;
		} catch (Exception e) {
			throw new DataException(PolicyPlan.class, "Error al asignar Pol�ticas del Plan de Desarrollo",
					e.getMessage());
		}
	}

	private String getMessage(ProjectStateEnum state) {
		switch (state) {
		case APPROVING:
			return "Tu proyecto se encuentra en aprobaci�n departamental, y ya no puede ser modificado";
		case EVALUATORASSIGNING:
			return "Tu proyecto se encuentra en asignaci�n de evaluador, no puede ser modificado";
		case EVALUATING:
			return "Tu proyecto se encuentra en evaluaci�n, no puede ser modificado";
		case COUNCILAPPROVAL:
			return "Tu proyecto se encuentra en aprobaci�n del concejo, no puede ser modificado";
		case DEVELOPING:
			return "Tu proyecto se encuentra en desarrollo, ya no puede ser modificado, pero puedes ingresar sus respectivos entregables y facturas";
		default:
			return "Este proyecto est� finalizado y no puede modificarse";
		}
	}

	private List<InstitutionInvolved> institutionInvolvedListAssignment(List<Long> institutionInvolvedDTOList) {
		InstitutionInvolved institutionInvolved = new InstitutionInvolved();
		List<InstitutionInvolved> institutionInvolvedList = new ArrayList<>();
		try {
			for (Long institutionInvolvedId : institutionInvolvedDTOList) {
				institutionInvolved = institutionInvolvedService.findById(institutionInvolvedId);
				institutionInvolvedList.add(institutionInvolved);
			}
			return institutionInvolvedList;
		} catch (Exception e) {
			throw new DataException(Coverage.class, "Error al asignar Instituciones involucradas", e.getMessage());
		}
	}

	private List<ProjectAnnex> projectAnnexAssignment(Project project, List<ProjectAnnexDTO> projectAnnexDTOList,
			List<MultipartFile> annexFiles) {
		List<ProjectAnnex> projectAnnexList = new ArrayList<>();
		String pathFile = "/okm:root/" + project.getProjectDirector() + "/ProjectAnnex/";
		for (ProjectAnnexDTO projectAnnexDTO : projectAnnexDTOList) {
			MultipartFile file = annexFiles.stream()
					.filter(annexFile -> annexFile.getOriginalFilename().equals(projectAnnexDTO.getFileName()))
					.collect(Collectors.toList()).get(0);
			ProjectAnnex projectAnnex = new ProjectAnnex();
			if (projectAnnexDTO.getId() == null) {
				Document document = sendDocument(file, pathFile, TypeUserDocument.PROJECT_DIRECTOR);
				Annex annex = annexService.find(projectAnnexDTO.getAnnexId());
				projectAnnex.setAnnex(annex);
				projectAnnex.setDescription(projectAnnexDTO.getDescription());
				projectAnnex.setFileName(document.getFileName());
				projectAnnex.setFileUUID(document.getUuid());
				projectAnnex.setProject(project);
				projectAnnex.setState(true);
			} else {
				projectAnnex = projectAnnexService.find(projectAnnexDTO.getId());
				if (Boolean.TRUE.equals(projectAnnexDTO.getUpdated())) {
					Document document = sendDocument(file, pathFile, TypeUserDocument.PROJECT_DIRECTOR);
					projectAnnex.setDescription(projectAnnexDTO.getDescription());
					projectAnnex.setFileName(document.getFileName());
					projectAnnex.setFileUUID(document.getUuid());
				}
			}
			projectAnnexList.add(projectAnnex);
		}
		return projectAnnexList;
	}

	private List<ProjectRequirement> projectRequirementAssignment(Project project,
			List<ProjectRequirementDTO> projectRequirementDTOList, List<MultipartFile> requirementFiles) {
		List<ProjectRequirement> projectRequirementList = new ArrayList<>();
		String pathFile = "/okm:root/" + project.getProjectDirector() + "/ProjectRequirement/";
		for (ProjectRequirementDTO projectRequirementDTO : projectRequirementDTOList) {
			MultipartFile file = requirementFiles.stream().filter(
					requireentFile -> requireentFile.getOriginalFilename().equals(projectRequirementDTO.getFileName()))
					.collect(Collectors.toList()).get(0);
			ProjectRequirement projectRequirement = new ProjectRequirement();
			if (projectRequirementDTO.getId() == null) {
				Document document = sendDocument(file, pathFile, TypeUserDocument.PROJECT_DIRECTOR);
				Requirement requirement = requirementService.find(projectRequirementDTO.getRequirementId());
				projectRequirement.setFileName(document.getFileName());
				projectRequirement.setFileUUID(document.getUuid());
				projectRequirement.setProject(project);
				projectRequirement.setRequirement(requirement);
				projectRequirement.setState(true);
			} else {
				projectRequirement = projectRequirementService.find(projectRequirementDTO.getId());
				if (Boolean.TRUE.equals(projectRequirementDTO.getUpdated())) {
					Document document = sendDocument(file, pathFile, TypeUserDocument.PROJECT_DIRECTOR);
					projectRequirement.setFileName(document.getFileName());
					projectRequirement.setFileUUID(document.getUuid());
				}
			}
			projectRequirementList.add(projectRequirement);
		}
		return projectRequirementList;
	}

	private List<ProjectResponsible> projectResponsibleListAssignment(Project project,
			List<ProjectResponsibleDTO> projectResponsibleDTOList) {
		List<ProjectResponsible> projectResponsibleList = new ArrayList<>();
		try {
			for (ProjectResponsibleDTO projectResponsibleDTO : projectResponsibleDTOList) {
				ProjectResponsible projectResponsible = new ProjectResponsible();
				Long id = projectResponsibleDTO.getId();
				if (id != null) {
					projectResponsible = projectResponsibleService.findById(id);
				} else {
					projectResponsible.setState(true);
				}
				ProjectResponsibleType projectResponsibleType = projectResponsibleTypeService
						.findById(projectResponsibleDTO.getProjectResponsibleTypeId());
				if (Boolean.TRUE.equals(projectResponsibleDTO.getExternalResponsible())) {
					projectResponsible.setExternalInstitution(projectResponsibleDTO.getExternalInstitution());
				} else {
					CollegeDepartment collegeDepartment = collegeDepartmentService
							.findById(projectResponsibleDTO.getCollegeDepartmentId());
					projectResponsible.setCollegeDepartment(collegeDepartment);
				}
				projectResponsible.setEmail(projectResponsibleDTO.getEmail());
				projectResponsible.setExternalResponsible(projectResponsibleDTO.getExternalResponsible());
				projectResponsible.setLastname(projectResponsibleDTO.getLastname());
				projectResponsible.setName(projectResponsibleDTO.getName());
				projectResponsible.setNumberIdentification(projectResponsibleDTO.getNumberIdentification());
				projectResponsible.setProjectResponsibleType(projectResponsibleType);
				projectResponsible.setTelephone(projectResponsibleDTO.getTelephone());
				projectResponsible.setProject(project);
				projectResponsibleList.add(projectResponsible);
			}
			return projectResponsibleList;
		} catch (Exception e) {
			throw new DataException(Coverage.class, "Error al asignar Responsables del proyecto", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void save(List<MultipartFile> annexFiles, Authentication authentication, ProjectDTO projectDTO,
			List<MultipartFile> requirementFiles) {
		Project project = new Project();
		try {
			Long id = projectDTO.getId();
			if (id != null) {
				project = find(id);
				if (projectDao.findByNameEnglishNotPresent(id, projectDTO.getNameEnglish()).isPresent()) {
					throw new IntegrityViolationException(Project.class, "nameEnglish", projectDTO.getNameEnglish());
				}
				if (projectDao.findByNameSpanishNotPresent(id, projectDTO.getNameSpanish()).isPresent()) {
					throw new IntegrityViolationException(Project.class, "nameSpanish", projectDTO.getNameSpanish());
				}
				if (!project.getState().equals(ProjectStateEnum.REGISTERED)
						&& !project.getState().equals(ProjectStateEnum.MODIFICATION)
						&& !project.getState().equals(ProjectStateEnum.REJECTED)) {
					String state = getMessage(project.getState());
					throw new DataException(Coverage.class, "Proyecto no habilitado para edici�n", state);
				}
			} else {
				UserGeneral user = userService.findUserGeneralByUsername(authentication.getName());
				if (projectDao.findByNameEnglish(projectDTO.getNameEnglish()).isPresent()) {
					throw new IntegrityViolationException(Project.class, "nameEnglish", projectDTO.getNameEnglish());
				}
				if (projectDao.findByNameSpanish(projectDTO.getNameSpanish()).isPresent()) {
					throw new IntegrityViolationException(Project.class, "nameSpanish", projectDTO.getNameSpanish());
				}
				project.setCreationDate(new Date());
				project.setProjectDirector(user.getUsername());
				project.setState(ProjectStateEnum.REGISTERED);
			}

			project.setAmpleAreaKnowledge(ampleAreaKnowledgeService.findById(projectDTO.getAmpleAreaKnowledgeId()));
			project.setCall(callService.find(projectDTO.getCallId()));
			project.setCollegeCareer(collegeCareerService.findById(projectDTO.getCollegeCareerId()));
			project.setCollegeDepartment(collegeDepartmentService.findById(projectDTO.getCollegeDepartmentId()));
			project.setDetailedAreaKnowledge(
					detailedAreaKnowledgeService.findById(projectDTO.getDetailedAreaKnowledgeId()));
			project.setEconomicPartnerGoal(economicPartnerGoalService.findById(projectDTO.getEconomicPartnerGoalId()));
			project.setEspeAreaKnowledge(espeAreaKnowledgeService.findById(projectDTO.getEspeAreaKnowledgeId()));
			project.setLineResearch(lineResearchService.findById(projectDTO.getLineResearchId()));
			project.setPostgraduateProgram(postgraduateProgramService.findById(projectDTO.getPostgraduateProgramId()));
			project.setProgram(programService.findById(projectDTO.getProgramId()));
			project.setResearchGroup(researchGroupService.findById(projectDTO.getResearchGroupId()));
			project.setResearchType(researchTypeService.findById(projectDTO.getResearchTypeId()));
			project.setScientificDiscipline(
					scientificDisciplineService.findById(projectDTO.getScientificDisciplineId()));
			project.setProjectStatus(projectStatusService.findById(projectDTO.getProjectStatusId()));
			project.setSubUnescoAreaKnowledge(
					subUnescoAreaKnowledgeService.findById(projectDTO.getSubUnescoAreaKnowledgeId()));
			project.setSpecificAreaKnowledge(
					specificAreaKnowledgeService.findById(projectDTO.getSpecificAreaKnowledgeId()));

			validateRequirementFiles(project.getCall(), requirementFiles, projectDTO.getProjectRequirementList());

			if (annexFiles != null) {
				validateAnnexFiles(annexService.findAll(), annexFiles, projectDTO.getProjectAnnexList());
				project.setProjectAnnexList(
						projectAnnexAssignment(project, projectDTO.getProjectAnnexList(), annexFiles));
			}

			project.setBibliographyList(bibliographyListAssignment(projectDTO.getBibliographyList(), project));
			project.setCoverageList(coverageListAssignment(projectDTO.getCoverageList()));
			project.setInstitutionInvolvedList(
					institutionInvolvedListAssignment(projectDTO.getInstitutionInvolvedList()));
			project.setPolicyPlanList(policyPlanListAssignment(projectDTO.getPolicyPlanList()));
			project.setProjectRequirementList(
					projectRequirementAssignment(project, projectDTO.getProjectRequirementList(), requirementFiles));
			project.setProjectResponsibleList(
					projectResponsibleListAssignment(project, projectDTO.getProjectResponsibleList()));

			project = ProjectAssignment.projectAssignment(project, projectDTO);
			projectDao.save(project);
		} catch (DataAccessException e) {
			throw new DataException(Project.class, "Error al guardar el proyecto", e.getMessage());
		}
	}

	private Document sendDocument(MultipartFile file, String pathFile, TypeUserDocument typeUserDocument) {
		try {
			impDocumentService.createMultipleFolder(pathFile, typeUserDocument);
			return impDocumentService.sendDocument(file, pathFile, typeUserDocument);
		} catch (IOException e) {
			throw new DataException(Project.class, "Error al guardar archivo: " + file.getOriginalFilename(),
					e.getMessage());
		}
	}

	@Override
	@Transactional
	public List<Document> sendFileListAnnexed(List<MultipartFile> fileList, String pathFile,
			TypeUserDocument typeUserDocument) throws IOException {
		List<Document> documentList = new ArrayList<>();
		impDocumentService.createMultipleFolder(pathFile, typeUserDocument);

		for (MultipartFile file : fileList) {
			Document document = impDocumentService.sendDocument(file, pathFile, typeUserDocument);
			documentList.add(document);
		}
		return documentList;
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Project project = find(id);
			project.setState(ProjectStateEnum.DELETED);
			projectDao.save(project);
		} catch (DataAccessException e) {
			throw new DataException(Project.class, "Error al guardar el proyecto", e.getMessage());
		}
	}

	private void validateAnnexFiles(List<Annex> annexList, List<MultipartFile> annexFiles,
			List<ProjectAnnexDTO> projectAnnexList) {
		if (projectAnnexList.stream().filter(annex -> annex.getAnnexId().equals(projectAnnexList.get(0).getAnnexId()))
				.collect(Collectors.toList()) == null) {
			throw new BadRequest(ProjectAnnexDTO.class, ERROR, "Elementos repetidos en la lista de anexos");
		}
		if (annexFiles.stream()
				.filter(file -> file.getOriginalFilename().equals(annexFiles.get(0).getOriginalFilename()))
				.collect(Collectors.toList()) == null) {
			throw new BadRequest(ProjectAnnexDTO.class, ERROR, "Archivo repetido");
		}
		annexList.forEach(annex -> {
			if (Boolean.TRUE.equals(annex.getRequired()) && projectAnnexList.stream()
					.filter(projectAnnex -> projectAnnex.getAnnexId().equals(annex.getId())) == null) {
				throw new BadRequest(ProjectAnnexDTO.class, ERROR, "No se agregaron todos los archivos requeridos");
			}
		});
		if (annexFiles.size() != projectAnnexList.size()) {
			throw new BadRequest(ProjectAnnexDTO.class, ERROR,
					"No hay concordancia entre la lista de anexos del proyecto y la cantidad de archivos enviada");
		}
	}

	private void validateRequirementFiles(Call call, List<MultipartFile> requirementFiles,
			List<ProjectRequirementDTO> projectRequirementList) {
		if (projectRequirementList.stream()
				.filter(projectRequirement -> projectRequirement.getRequirementId()
						.equals(projectRequirementList.get(0).getRequirementId()))
				.collect(Collectors.toList()) == null) {
			throw new BadRequest(ProjectRequirementDTO.class, ERROR,
					"Elementos repetidos en la lista de requerimientos");
		}
		if (requirementFiles.stream()
				.filter(file -> file.getOriginalFilename().equals(requirementFiles.get(0).getOriginalFilename()))
				.collect(Collectors.toList()) == null) {
			throw new BadRequest(ProjectRequirementDTO.class, ERROR, "Archivo repetido");
		}
		call.getCallRequirementList().forEach(callRequirement -> {
			if (Boolean.TRUE.equals(callRequirement.getRequired())
					&& projectRequirementList.stream().filter(projectRequirement -> projectRequirement
							.getRequirementId().equals(callRequirement.getRequirement().getId())) == null) {
				throw new BadRequest(ProjectRequirementDTO.class, ERROR,
						"No se agregaron todos los archivos requeridos");
			}
		});
		if (requirementFiles.size() != projectRequirementList.size()) {
			throw new BadRequest(ProjectRequirementDTO.class, ERROR,
					"No hay concordancia entre la lista de requerimientos del proyecto y la cantidad de archivos enviada");
		}
	}
}