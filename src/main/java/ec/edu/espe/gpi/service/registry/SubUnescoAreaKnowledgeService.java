package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ISubUnescoAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.SubUnescoAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.SubUnescoAreaKnowledge;
import ec.edu.espe.gpi.model.registry.UnescoAreaKnowledge;

@Service
public class SubUnescoAreaKnowledgeService implements ISubUnescoAreaKnowledgeService {

	@Autowired
	private ISubUnescoAreaKnowledgeDao subUnescoAreaKnowledgeDao;

	@Autowired
	private IUnescoAreaKnowledgeService unescoAreaKnowledgeService;

	@Override
	@Transactional(readOnly = true)
	public List<SubUnescoAreaKnowledge> findAll() {
		return subUnescoAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public SubUnescoAreaKnowledge findById(Long id) {
		return subUnescoAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(SubUnescoAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<SubUnescoAreaKnowledge> findByUnesco(Long subUnescoAreaKnowledgeId) {
		return subUnescoAreaKnowledgeDao.findByUnesco(subUnescoAreaKnowledgeId)
				.orElseThrow(() -> new NotFoundException(SubUnescoAreaKnowledge.class, "id", subUnescoAreaKnowledgeId));
	}

	@Override
	@Transactional
	public void save(SubUnescoAreaKnowledgeDTO subUnescoAreaKnowledgeDTO) {
		SubUnescoAreaKnowledge subUnescoAreaKnowledge = new SubUnescoAreaKnowledge();
		try {
			Long id = subUnescoAreaKnowledgeDTO.getId();
			UnescoAreaKnowledge unescoAreaKnowledge = unescoAreaKnowledgeService
					.findById(subUnescoAreaKnowledgeDTO.getUnescoAreaKnowledgeId());
			if (id != null) {
				subUnescoAreaKnowledge = findById(id);
				if (subUnescoAreaKnowledgeDao.findByNameNotPresent(id, subUnescoAreaKnowledgeDTO.getName())
						.isPresent()) {
					throw new IntegrityViolationException(SubUnescoAreaKnowledge.class, "name",
							subUnescoAreaKnowledgeDTO.getName());
				}
			} else {
				if (subUnescoAreaKnowledgeDao.findByName(subUnescoAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(SubUnescoAreaKnowledge.class, "name",
							subUnescoAreaKnowledgeDTO.getName());
				}
				subUnescoAreaKnowledge.setState(true);
			}
			subUnescoAreaKnowledge.setName(subUnescoAreaKnowledgeDTO.getName());
			subUnescoAreaKnowledge.setUnescoAreaKnowledge(unescoAreaKnowledge);
			subUnescoAreaKnowledgeDao.save(subUnescoAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(SubUnescoAreaKnowledge.class, "Error al guardar el sub�rea de conocimiento",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			SubUnescoAreaKnowledge subUnescoAreaKnowledge = findById(id);
			subUnescoAreaKnowledge.setState(false);
			subUnescoAreaKnowledgeDao.save(subUnescoAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(SubUnescoAreaKnowledge.class, "Error al eliminar el sub�rea de conocimiento",
					e.getMostSpecificCause().getMessage());
		}
	}
}