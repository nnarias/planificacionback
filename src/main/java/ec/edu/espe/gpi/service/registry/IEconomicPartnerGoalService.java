package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.EconomicPartnerGoalDTO;
import ec.edu.espe.gpi.model.registry.EconomicPartnerGoal;

public interface IEconomicPartnerGoalService {

	public List<EconomicPartnerGoal> findAll();

	public EconomicPartnerGoal findById(Long id);

	public void save(EconomicPartnerGoalDTO economicPartnerGoalDTO);

	public void updateState(Long id);
}