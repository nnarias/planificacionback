package ec.edu.espe.gpi.service.evaluation;

import ec.edu.espe.gpi.model.evaluation.EvaluationCriteriaSubcriteria;

public interface IEvaluationCriteriaSubcriteriaService {

	public EvaluationCriteriaSubcriteria findById(Long id);
}