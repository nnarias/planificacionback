package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICoverageTypeDao;
import ec.edu.espe.gpi.dto.registry.CoverageTypeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.CallArea;
import ec.edu.espe.gpi.model.registry.CoverageType;

@Service
public class CoverageTypeService implements ICoverageTypeService {

	@Autowired
	private ICoverageTypeDao coverageTypeDao;

	@Override
	@Transactional(readOnly = true)
	public List<CoverageType> findAll() {
		return coverageTypeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public CoverageType findById(Long id) {
		return coverageTypeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(CoverageType.class, "id", id));
	}

	@Override
	@Transactional
	public void save(CoverageTypeDTO coverageTypeDTO) {
		CoverageType coverageType = new CoverageType();
		try {
			Long id = coverageTypeDTO.getId();
			if (id != null) {
				coverageType = findById(id);
				if (coverageTypeDao.findByNameNotPresent(id, coverageTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CoverageType.class, "name", coverageTypeDTO.getName());
				}
			} else {
				if (coverageTypeDao.findByName(coverageTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CoverageType.class, "name", coverageTypeDTO.getName());
				}
				coverageType.setState(true);
			}
			coverageType.setName(coverageTypeDTO.getName());
			coverageTypeDao.save(coverageType);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al guardar el Tipo de Cobertura",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			CoverageType coverageType = findById(id);
			coverageType.setState(false);
			coverageTypeDao.save(coverageType);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al eliminar el Tipo de Cobertura",
					e.getMostSpecificCause().getMessage());
		}
	}

}
