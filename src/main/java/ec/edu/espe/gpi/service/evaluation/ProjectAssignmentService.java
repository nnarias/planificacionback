package ec.edu.espe.gpi.service.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.registry.IProjectDao;
import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.dto.evaluation.AssignEvaluatorDTO;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.service.admin.INotificationService;
import ec.edu.espe.gpi.service.registry.IProjectService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.security.UserGeneral;

@Service
public class ProjectAssignmentService implements IProjectAssignmentService {
	private static final String EVALUER_DESCRIPTION = "Nombre del evaluador: ";
	private static final String NOTIFICATION_TITLE = " asignado al proyecto";
	private static final String PROJECT_DESCRIPTION = "Nombre del proyecto: ";

	@Autowired
	private INotificationService notificationService;

	@Autowired
	private IProjectDao projectDao;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	@Override
	public void assignEvaluator(AssignEvaluatorDTO evaluatorAssignDTO) {
		Project project = projectService.findByEvaluatorAssigning(evaluatorAssignDTO.getProjectId());
		UserGeneral evaluer = userService.findUserGeneralByUsername(evaluatorAssignDTO.getEvaluator());
		UserGeneral departamentalEvaluer = userService
				.findUserGeneralByUsername(evaluatorAssignDTO.getDepartamentalEvaluator());
		assignProjectEvaluator(project, evaluer);
		assignProjectDepartamentalEvaluer(project, departamentalEvaluer);
	}

	private void assignProjectDepartamentalEvaluer(Project project, UserGeneral userGeneral) {
		NotificationDTO evaluerNotificationDTO = new NotificationDTO();
		NotificationDTO directorNotificationDTO = new NotificationDTO();
		String title = "Evaluador departamental" + NOTIFICATION_TITLE;
		String descriptionEvaluer = PROJECT_DESCRIPTION + project.getNameSpanish();
		String descriptionDirector = PROJECT_DESCRIPTION + project.getNameSpanish() + " " + EVALUER_DESCRIPTION
				+ userGeneral.getName();

		project.setDepartmentalEvaluator(userGeneral.getUsername());

		evaluerNotificationDTO.setTitle(title);
		evaluerNotificationDTO.setDescription(descriptionEvaluer);
		evaluerNotificationDTO.setUser(userGeneral.getUsername());

		directorNotificationDTO.setTitle(title);
		directorNotificationDTO.setDescription(descriptionDirector);
		directorNotificationDTO.setUser(project.getProjectDirector());

		projectDao.save(project);
		notificationService.save(evaluerNotificationDTO);
		notificationService.save(directorNotificationDTO);
	}

	private void assignProjectEvaluator(Project project, UserGeneral userGeneral) {
		NotificationDTO evaluerNotificationDTO = new NotificationDTO();
		NotificationDTO directorNotificationDTO = new NotificationDTO();
		String title = "Evaluador" + NOTIFICATION_TITLE;
		String descriptionEvaluer = PROJECT_DESCRIPTION + project.getNameSpanish();
		String descriptionDirector = PROJECT_DESCRIPTION + project.getNameSpanish() + " " + EVALUER_DESCRIPTION
				+ userGeneral.getName();

		project.setProjectEvaluator(userGeneral.getUsername());

		evaluerNotificationDTO.setTitle(title);
		evaluerNotificationDTO.setDescription(descriptionEvaluer);
		evaluerNotificationDTO.setUser(userGeneral.getUsername());

		directorNotificationDTO.setTitle(title);
		directorNotificationDTO.setDescription(descriptionDirector);
		directorNotificationDTO.setUser(project.getProjectDirector());

		projectDao.save(project);
		notificationService.save(evaluerNotificationDTO);
		notificationService.save(directorNotificationDTO);
	}
}