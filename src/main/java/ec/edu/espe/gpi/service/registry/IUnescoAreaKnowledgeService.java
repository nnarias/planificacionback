package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.UnescoAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.UnescoAreaKnowledge;

public interface IUnescoAreaKnowledgeService {

	public List<UnescoAreaKnowledge> findAll();

	public UnescoAreaKnowledge findById(Long id);

	public void save(UnescoAreaKnowledgeDTO unescoAreaKnowledgeDTO);

	public void updateState(Long id);
}