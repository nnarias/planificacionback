package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IEconomicPartnerGoalDao;
import ec.edu.espe.gpi.dto.registry.EconomicPartnerGoalDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.EconomicPartnerGoal;

@Service
public class EconomicPartnerGoalService implements IEconomicPartnerGoalService {

	@Autowired
	private IEconomicPartnerGoalDao economicPartnerGoalDao;

	@Override
	@Transactional(readOnly = true)
	public List<EconomicPartnerGoal> findAll() {
		return economicPartnerGoalDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public EconomicPartnerGoal findById(Long id) {
		return economicPartnerGoalDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(EconomicPartnerGoal.class, "id", id));
	}

	@Override
	@Transactional
	public void save(EconomicPartnerGoalDTO economicPartnerGoalDTO) {
		EconomicPartnerGoal economicPartnerGoal = new EconomicPartnerGoal();
		try {
			Long id = economicPartnerGoalDTO.getId();
			if (id != null) {
				economicPartnerGoal = findById(id);
				if (economicPartnerGoalDao.findByNameNotPresent(id, economicPartnerGoalDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(EconomicPartnerGoal.class, "name",
							economicPartnerGoalDTO.getName());
				}
			} else {
				if (economicPartnerGoalDao.findByName(economicPartnerGoalDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(EconomicPartnerGoal.class, "name",
							economicPartnerGoalDTO.getName());
				}
				economicPartnerGoal.setState(true);
			}
			economicPartnerGoal.setName(economicPartnerGoalDTO.getName());
			economicPartnerGoalDao.save(economicPartnerGoal);
		} catch (DataAccessException e) {
			throw new DataException(EconomicPartnerGoal.class, "Error al guardar el Objetivo Socio Econůmico",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			EconomicPartnerGoal economicPartnerGoal = findById(id);
			economicPartnerGoal.setState(false);
			economicPartnerGoalDao.save(economicPartnerGoal);
		} catch (DataAccessException e) {
			throw new DataException(EconomicPartnerGoal.class, "Error al eliminar el Objetivo Socio Econůmico",
					e.getMostSpecificCause().getMessage());
		}
	}
}