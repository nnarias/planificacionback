package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.InstitutionInvolvedDTO;
import ec.edu.espe.gpi.model.registry.InstitutionInvolved;

public interface IInstitutionInvolvedService {
	public List<InstitutionInvolved> findAll();

	public InstitutionInvolved findById(Long id);

	public void save(InstitutionInvolvedDTO institutionInvolvedDTO);

	public void updateState(Long id);
}