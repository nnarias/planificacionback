package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.EspeAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.EspeAreaKnowledge;

public interface IEspeAreaKnowledgeService {

	public List<EspeAreaKnowledge> findAll();

	public EspeAreaKnowledge findById(Long id);

	public void save(EspeAreaKnowledgeDTO espeAreaKnowledgeDTO);

	public void updateState(Long id);
}