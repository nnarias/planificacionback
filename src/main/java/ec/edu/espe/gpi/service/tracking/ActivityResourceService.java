package ec.edu.espe.gpi.service.tracking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dao.tracking.IActivityResourceDao;
import ec.edu.espe.gpi.dto.admin.ProjectNotificationDTO;
import ec.edu.espe.gpi.dto.tracking.ActivityResourceApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectInvoiceDTO;
import ec.edu.espe.gpi.exception.BadRequest;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.model.tracking.ActivityResource;
import ec.edu.espe.gpi.model.tracking.GoalActivity;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.admin.IProjectNotificationService;
import ec.edu.espe.gpi.utils.ValidationError;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;
import ec.edu.espe.gpi.utils.registry.DateValidation;
import ec.edu.espe.gpi.utils.tracking.ActivityResourceApprovalEnum;
import ec.edu.espe.gpi.utils.tracking.ActivityResourceEnum;

@Service
public class ActivityResourceService implements IActivityResourceService {

	@Autowired
	private IProjectNotificationService projectNotificationService;

	@Autowired
	private IActivityResourceDao activityResourceDao;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Override
	@Transactional(readOnly = true)
	public ActivityResource find(Long id) {
		return activityResourceDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ActivityResource.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<ActivityResource> findAllPending(Long idProject, Authentication authentication) {
		List<ActivityResource> activityResourceList = new ArrayList<>();
		activityResourceDao.findAllPending(idProject, authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					ActivityResource activityResource = new ActivityResource();
					activityResource.setCurrentBudget(temp.getCurrentBudget());
					activityResource.setId(temp.getId());
					activityResource.setInvestmentBudget(temp.getInvestmentBudget());
					activityResource.setResource(temp.getResource());
					activityResource.setState(temp.getState());
					GoalActivity goalActivity = new GoalActivity();
					goalActivity.setId(temp.getGoalActivity().getId());
					goalActivity.setName(temp.getGoalActivity().getName());
					goalActivity.setStartDate(temp.getGoalActivity().getStartDate());
					goalActivity.setEndDate(temp.getGoalActivity().getEndDate());
					activityResource.setGoalActivity(goalActivity);
					ValidationError validationError = DateValidation.dateRangeValidation(goalActivity.getStartDate(),
							goalActivity.getEndDate());
					if (!validationError.isError()) {
						activityResourceList.add(activityResource);
					}
					return activityResource;
				}).collect(Collectors.toList());
		return activityResourceList;
	}

	@Override
	@Transactional(readOnly = true)
	public ActivityResource findPending(Long id, Authentication authentication) {
		ActivityResource activityResource = activityResourceDao.findByIdPending(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(ActivityResource.class, "id", id));
		validationInvoiceDate(activityResource);
		return activityResource;
	}

	@Override
	@Transactional(readOnly = true)
	public ActivityResource findInvoice(Long id) {
		return activityResourceDao.findByIdInvoice(id)
				.orElseThrow(() -> new NotFoundException(ActivityResource.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<ActivityResource> findAllTracking(Long idProject, Authentication authentication) {
		return activityResourceDao.findAllTracking(idProject, authentication.getName()).orElse(new ArrayList<>())
				.stream().map(temp -> {
					ActivityResource activityResource = new ActivityResource();
					activityResource.setCurrentBudget(temp.getCurrentBudget());
					activityResource.setId(temp.getId());
					activityResource.setInvestmentBudget(temp.getInvestmentBudget());
					activityResource.setResource(temp.getResource());
					activityResource.setState(temp.getState());
					GoalActivity goalActivity = new GoalActivity();
					goalActivity.setId(temp.getGoalActivity().getId());
					goalActivity.setName(temp.getGoalActivity().getName());
					goalActivity.setStartDate(temp.getGoalActivity().getStartDate());
					goalActivity.setEndDate(temp.getGoalActivity().getEndDate());
					activityResource.setGoalActivity(goalActivity);
					return activityResource;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public ActivityResource findTracking(Long id, Authentication authentication) {
		return activityResourceDao.findByIdTracking(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(ActivityResource.class, "id", id));
	}

	@Override
	@Transactional
	public void saveInvoice(ProjectInvoiceDTO projectInvoiceDTO, Authentication authentication) throws IOException {
		Date currentDate = new Date();
		ActivityResource activityResource = findPending(projectInvoiceDTO.getId(), authentication);
		GoalActivity goalActivity = activityResource.getGoalActivity();

		validationInvoiceDate(activityResource);
		String pathFile = "/okm:root/" + goalActivity.getProjectGoal().getProject().getProjectDirector()
				+ "/activityResource/";

		Document document = sendDocument(pathFile, TypeUserDocument.PROJECT_DIRECTOR, projectInvoiceDTO.getFile());
		activityResource.setInvoiceFileUUID(document.getUuid());
		activityResource.setInvoiceFileName(document.getFileName());
		activityResource.setInvoiceDate(currentDate);
		activityResource.setInvoiceDescription(projectInvoiceDTO.getDescription());
		activityResource.setInvoiceCurrentBudget(projectInvoiceDTO.getCurrentBudget());
		activityResource.setInvoiceInvestmentBudget(projectInvoiceDTO.getInvestmentBudget());
		activityResource.setState(ActivityResourceEnum.EVALUATING);
		activityResourceDao.save(activityResource);
	}

	@Override
	@Transactional
	public void invoiceApproval(ActivityResourceApprovalDTO activityResourceApprovalDTO,
			Authentication authentication) {
		ActivityResource activityResource = findTracking(activityResourceApprovalDTO.getId(), authentication);

		Project project = activityResource.getGoalActivity().getProjectGoal().getProject();
		ProjectNotificationDTO projectNotificationDTO = new ProjectNotificationDTO();
		ActivityResourceEnum state = null;
		String title = "";
		if (activityResourceApprovalDTO.getActivityResourceApprovalEnum()
				.equals(ActivityResourceApprovalEnum.APPROVED)) {
			title = "Factura Aprobada por el supervisor financiero";
			state = ActivityResourceEnum.APPROVING;
		} else {
			title = "Factura Rechazada por el supervisor financiero";
			state = ActivityResourceEnum.REJECTED;
		}
		projectNotificationDTO.setComment(activityResourceApprovalDTO.getComment());
		projectNotificationDTO.setProjectId(project.getId());
		projectNotificationDTO.setTitle(title);
		projectNotificationDTO.setUser(authentication.getName());
		projectNotificationService.save(projectNotificationDTO);
		activityResource.setState(state);
		activityResourceDao.save(activityResource);

	}

	public Document sendDocument(String pathFile, TypeUserDocument typeUserDocument, MultipartFile file)
			throws IOException {
		if (file == null) {
			throw new NotFoundException(Document.class, "documento", "vacio");
		}
		impDocumentService.createMultipleFolder(pathFile, typeUserDocument);
		return impDocumentService.sendDocument(file, pathFile, typeUserDocument);
	}

	public void validationInvoiceDate(ActivityResource activityResource) {
		GoalActivity goalActivity = activityResource.getGoalActivity();
		ValidationError validationError = DateValidation.dateRangeValidation(goalActivity.getStartDate(),
				goalActivity.getEndDate());
		if (validationError.isError()) {
			throw new BadRequest(GoalActivity.class, "Error en la operacion", "Fecha fuera de rango del rango");
		}

	}

}