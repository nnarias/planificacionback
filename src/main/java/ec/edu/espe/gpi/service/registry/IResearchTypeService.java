package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ResearchTypeDTO;
import ec.edu.espe.gpi.model.registry.ResearchType;

public interface IResearchTypeService {

	public List<ResearchType> findAll();

	public ResearchType findById(Long id);

	public void save(ResearchTypeDTO researchTypeDTO);

	public void updateState(Long id);
}