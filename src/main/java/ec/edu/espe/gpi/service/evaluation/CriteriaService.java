package ec.edu.espe.gpi.service.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.evaluation.ICriteriaDao;
import ec.edu.espe.gpi.dto.evaluation.CriteriaDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.Criteria;

@Service
public class CriteriaService implements ICriteriaService {
	@Autowired
	private ICriteriaDao criteriaDao;

	@Override
	public List<Criteria> findAll() {
		return criteriaDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	public Criteria find(Long id) {
		return criteriaDao.findById(id).orElseThrow(() -> new NotFoundException(Criteria.class, "id", id));
	}

	@Override
	public void save(CriteriaDTO criteriaDTO) {
		Criteria criteria = new Criteria();
		try {
			Long id = criteriaDTO.getId();
			if (id != null) {
				criteria = find(id);
				if (criteriaDao.findByNameNotPresent(id, criteriaDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Criteria.class, "name", criteriaDTO.getName());
				}
			} else {
				if (criteriaDao.findByName(criteriaDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Criteria.class, "name", criteriaDTO.getName());
				}
				criteria.setState(true);
			}
			criteria.setName(criteriaDTO.getName());
			criteriaDao.save(criteria);
		} catch (DataAccessException e) {
			throw new DataException(Criteria.class, "Error al guardar el criterio de evaluación",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public void updateState(Long id) {
		try {
			Criteria criteria = find(id);
			criteria.setState(false);
			criteriaDao.save(criteria);
		} catch (DataAccessException e) {
			throw new DataException(Criteria.class, "Error al eliminar el criterio de evaluación",
					e.getMostSpecificCause().getMessage());
		}
	}
}