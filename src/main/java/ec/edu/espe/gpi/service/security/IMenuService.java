package ec.edu.espe.gpi.service.security;

import java.util.List;

import ec.edu.espe.gpi.model.security.Menu;

public interface IMenuService {

	public List<Menu> findAll();

	public Menu findById(Long id);
}