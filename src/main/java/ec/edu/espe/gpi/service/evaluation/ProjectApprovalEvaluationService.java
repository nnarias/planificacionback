package ec.edu.espe.gpi.service.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectDao;
import ec.edu.espe.gpi.dto.admin.ProjectNotificationDTO;
import ec.edu.espe.gpi.dto.evaluation.ProjectApprovalEvaluationDTO;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.service.admin.IProjectNotificationService;
import ec.edu.espe.gpi.service.registry.IProjectService;
import ec.edu.espe.gpi.utils.evaluation.ProjectApprovalEvaluationEnum;
import ec.edu.espe.gpi.utils.registry.ProjectStateEnum;

@Service
public class ProjectApprovalEvaluationService implements IProjectApprovalEvaluationService {

	@Autowired
	private IProjectNotificationService projectNotificationService;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IProjectDao projectDao;

	@Override
	@Transactional
	public void projectApproval(ProjectApprovalEvaluationDTO projectApprovalEvaluationDTO,
			Authentication authentication) {
		ProjectNotificationDTO projectNotificationDTO = new ProjectNotificationDTO();

		Project project = projectService.findByProjectDepartmentalEvaluator(authentication,
				projectApprovalEvaluationDTO.getProjectId());

		ProjectStateEnum state = null;
		String title = "";
		if (projectApprovalEvaluationDTO.getProjectApprovalEvaluationEnum() == ProjectApprovalEvaluationEnum.APPROVED) {
			title = "Proyecto Aprobado por el evaluador departamental";
			state = ProjectStateEnum.EVALUATING;
		} else {
			title = "Proyecto Rechazado por el evaluador departamental";
			state = ProjectStateEnum.MODIFICATION;
		}

		projectNotificationDTO.setComment(projectApprovalEvaluationDTO.getComment());
		projectNotificationDTO.setProjectId(projectApprovalEvaluationDTO.getProjectId());
		projectNotificationDTO.setTitle(title);
		projectNotificationDTO.setUser(authentication.getName());
		projectNotificationService.save(projectNotificationDTO);
		project.setState(state);
		projectDao.save(project);
	}
}