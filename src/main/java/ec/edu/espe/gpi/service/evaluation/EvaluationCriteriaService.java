package ec.edu.espe.gpi.service.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.evaluation.IEvaluationCriteriaDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.EvaluationCriteria;

@Service
public class EvaluationCriteriaService implements IEvaluationCriteriaService {

	@Autowired
	private IEvaluationCriteriaDao evaluationCriteriaDao;

	@Override
	public EvaluationCriteria find(Long id) {
		return evaluationCriteriaDao.findById(id).map(temp -> {
			EvaluationCriteria evCriteria = new EvaluationCriteria();
			evCriteria.setEvaluation(temp.getEvaluation());
			evCriteria.setId(temp.getId());
			evCriteria.setState(temp.getState());
			return evCriteria;
		}).orElseThrow(() -> new NotFoundException(EvaluationCriteria.class, "id", id));
	}
}