package ec.edu.espe.gpi.service.security;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.security.IRoleDao;
import ec.edu.espe.gpi.dao.security.IUserDao;
import ec.edu.espe.gpi.dto.security.UserDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.exception.RoleModificationException;
import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.utils.CodeGeneration;
import ec.edu.espe.gpi.utils.registry.StateValidation;
import ec.edu.espe.gpi.utils.security.SSHAEncrypt;
import ec.edu.espe.gpi.utils.security.UserAssignment;
import ec.edu.espe.gpi.utils.security.UserGeneral;
import ec.edu.espe.gpi.utils.security.UserUsernameMiespe;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;
import ec.edu.espe.gpi.utils.security.UsuarioRolMiespe;

@Service
public class UserService implements UserDetailsService, IUserService {

	@Autowired
	private IRoleDao roleDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IMiespeService miespeService;

	private static final String VARIABLE_ERROR = "username";

	@Override
	@Transactional(readOnly = true)
	public List<UserModel> findAll() {
		return userDao.findAllEnable().orElse(new ArrayList<>()).stream().map(temp -> {
			temp.setPassword("");
			temp.setRoleList(null);
			return temp;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public UserModel findByAuthentication(Authentication authentication) {
		return userDao.findByUsername(authentication.getName())
				.orElseThrow(() -> new NotFoundException(UserModel.class, VARIABLE_ERROR, authentication.getName()));
	}

	@Override
	@Transactional(readOnly = true)
	public UserModel findById(Long id) {
		return userDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(UserModel.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public UserGeneral findUserGeneralByUsername(String username) {
		UserGeneral userGeneral = new UserGeneral();
		UserUsernameMiespe userUsernameMiespe = miespeService.findUserUsername(username);
		if (userUsernameMiespe != null) {
			UsuarioRolMiespe usuarioRolMiespe = miespeService.findUserRole(userUsernameMiespe.getId());
			if (usuarioRolMiespe.getPerfil() == null) {
				throw new NotFoundException(UserModel.class, VARIABLE_ERROR, username);
			}
			List<Role> roleCurrentList = miespeService.findUserListRole(usuarioRolMiespe.getPerfil());

			userGeneral.setUsername(username);
			userGeneral.setName(userUsernameMiespe.getNombres());
			userGeneral.setEmail(userUsernameMiespe.getCorreoInstitucional());
			userGeneral.setRoleList(roleCurrentList);
			userGeneral.setInternalUser(false);
		} else {
			UserModel userModel = userDao.findByUsername(username)
					.orElseThrow(() -> new NotFoundException(UserModel.class, VARIABLE_ERROR, username));
			userGeneral.setUsername(userModel.getUsername());
			userGeneral.setName(userModel.getLastName() + " " + userModel.getName());
			userGeneral.setEmail(userModel.getEmail());
			userGeneral.setRoleList(userModel.getRoleList());
			userGeneral.setInternalUser(true);
		}
		return userGeneral;
	}

	@Override
	@Transactional(readOnly = true)
	public UserModel findByUsername(String username) {
		return userDao.findByUsername(username)
				.orElseThrow(() -> new NotFoundException(UserModel.class, VARIABLE_ERROR, username));
	}

	@Override
	@Transactional(readOnly = true)
	public UserModel findByUsernameNotNull(String username) {
		return userDao.findByUsername(username).orElse(null);
	}

	@Override
	@Transactional(readOnly = true)
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserldapMiespe userldapMiespe = miespeService.findUserldap(username);
		if (userldapMiespe.getUserName() != null) {
			UsuarioRolMiespe usuarioRolMiespe = miespeService.findUserRole(userldapMiespe.getCodId());
			if (usuarioRolMiespe.getPerfil() == null) {
				throw new NotFoundException(UserModel.class, VARIABLE_ERROR, username);
			}
			List<GrantedAuthority> authorities = miespeService.findUserListRole(usuarioRolMiespe.getPerfil()).stream()
					.map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
			return new User(userldapMiespe.getUserName(), userldapMiespe.getPassword(), true, true, true, true,
					authorities);
		} else {
			UserModel userModel = userDao.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(
					"Error de validaci�n en el login: no existe el usuario " + username + " en el sistema!"));
			List<GrantedAuthority> authorities = userModel.getRoleList().stream()
					.map(role -> new SimpleGrantedAuthority(role.getRoleName())).collect(Collectors.toList());
			return new User(userModel.getUsername(), userModel.getPassword(), userModel.getState(), true, true, true,
					authorities);
		}

	}

	@Override
	@Transactional
	public void save(UserDTO userDTO) throws NoSuchAlgorithmException {
		CodeGeneration codeGeneration = new CodeGeneration();
		int counter = 0;
		String passwordBcrypt = SSHAEncrypt.generateSSHA(codeGeneration.pwdGeneration(userDTO.getBirthDate()));
		UserModel user = new UserModel();
		UserAssignment userAssignment = new UserAssignment();
		String username = "";
		try {
			Long id = userDTO.getId();
			if (id != null) {
				user = findById(id);
				if (userDao.findByEmailNotPresent(userDTO.getEmail(), id).isPresent()) {
					throw new IntegrityViolationException(UserModel.class, "email", userDTO.getEmail());
				}
				user = userAssignment.userModificationAssignment(user, userDTO);
			} else {
				if (userDao.findByEmail(userDTO.getEmail()).isPresent()) {
					throw new IntegrityViolationException(UserModel.class, "email", userDTO.getEmail());
				}
				do {
					username = codeGeneration.usernameGeneration(userDTO, counter);
					counter++;
				} while (userDao.findByUsername(username).orElse(null) != null);
				if (userDao.findByUsername(username).isPresent()) {
					throw new IntegrityViolationException(UserModel.class, VARIABLE_ERROR, username);
				}
				user = userAssignment.createUserAssignment(userDTO, username, passwordBcrypt);
				List<Role> roleList = new ArrayList<>();
				Role role = roleDao.findById(1L).orElse(null);
				roleList.add(role);
				user.setRoleList(roleList);
			}
			userDao.save(user);
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, "Error al guardar el usuario",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void saveUser(UserModel userModel) {
		userDao.save(userModel);
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			UserModel user = findById(id);
			user.setState(false);
			userDao.save(user);
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, "Error al eliminar el usuario, en la base de datos",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public void validateChangeUserRole(Authentication authentication) {
		UserModel currentUser = findByUsernameNotNull(authentication.getName());
		if (currentUser != null && StateValidation.roleModificationValidation(currentUser)) {
			throw new RoleModificationException(UserModel.class, VARIABLE_ERROR, authentication.getName());
		}
	}
}