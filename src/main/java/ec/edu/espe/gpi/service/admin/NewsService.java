package ec.edu.espe.gpi.service.admin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dao.admin.INewsDao;
import ec.edu.espe.gpi.dto.admin.NewsDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.admin.News;
import ec.edu.espe.gpi.utils.ImageTreatment;
import ec.edu.espe.gpi.utils.ValidationError;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.NewsValidation;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

@Service
public class NewsService implements INewsService {

	@Autowired
	private IImpDocumentService impDocumentService;

	@Autowired
	private INewsDao newsDao;

	@Override
	@Transactional
	public void delete(Long id) {
		try {
			News news = find(id);
			newsDao.delete(news);
		} catch (DataException e) {
			throw new DataException(News.class, "Error al eliminar la noticia", e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public News find(Long id) {
		return newsDao.findById(id).orElseThrow(() -> new NotFoundException(News.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<News> findAll() {
		return newsDao.findList().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public List<News> findLoginNews() {
		List<News> newsList = new ArrayList<>();
		newsDao.findList().orElse(new ArrayList<>()).stream().map(temp -> {
			ValidationError validationError = NewsValidation.currentNewsValidation(temp);
			if (!validationError.isError()) {
				newsList.add(temp);
			}
			return temp;
		}).collect(Collectors.toList());
		return newsList;
	}

	@Override
	@Transactional
	public void save(Document document, NewsDTO newsDTO) {
		News news = new News();
		try {
			Long id = newsDTO.getId();
			if (id != null) {
				news = find(id);
			}
			news.setBody(newsDTO.getBody());
			news.setEndDate(newsDTO.getEndDate());
			news.setFileName(document.getFileName());
			news.setFileUUID(document.getUuid());
			news.setStartDate(newsDTO.getStartDate());
			news.setSubtitle(newsDTO.getSubtitle());
			news.setTitle(newsDTO.getTitle());
			newsDao.save(news);
		} catch (DataAccessException e) {
			throw new DataException(News.class, "Error al guardar la Noticia", e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public Document sendImage(MultipartFile file, NewsDTO newsDTO, String pathFile, TypeUserDocument typeUserDocument)
			throws IOException {
		Long id = newsDTO.getId();
		if ((id == null && file == null) || (Boolean.TRUE.equals(newsDTO.getUpdate()))) {
			file = ImageTreatment.createImage(newsDTO.getTitle(), newsDTO.getSubtitle(), newsDTO.getBody())
					.orElseThrow(() -> new DataException(File.class, "Error al crear imagen"));
		}
		impDocumentService.createMultipleFolder(pathFile, typeUserDocument);
		return impDocumentService.sendDocument(file, pathFile, typeUserDocument);
	}
}