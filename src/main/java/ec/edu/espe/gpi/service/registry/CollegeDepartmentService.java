package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICollegeDepartmentDao;
import ec.edu.espe.gpi.dto.registry.CollegeDepartmentDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;

@Service
public class CollegeDepartmentService implements ICollegeDepartmentService {

	@Autowired
	private ICollegeDepartmentDao collegeDepartmentDao;

	@Override
	@Transactional(readOnly = true)
	public List<CollegeDepartment> findAll() {
		return collegeDepartmentDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public CollegeDepartment findById(Long id) {
		return collegeDepartmentDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(CollegeDepartment.class, "id", id));
	}

	@Override
	@Transactional
	public void save(CollegeDepartmentDTO collegeDepartmentDTO) {
		CollegeDepartment collegeDepartment = new CollegeDepartment();
		try {
			Long id = collegeDepartmentDTO.getId();
			if (id != null) {
				collegeDepartment = findById(id);
				if (collegeDepartmentDao.findByNameNotPresent(id, collegeDepartmentDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CollegeDepartment.class, "name",
							collegeDepartmentDTO.getName());
				}
			} else {
				if (collegeDepartmentDao.findByName(collegeDepartmentDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CollegeDepartment.class, "name",
							collegeDepartmentDTO.getName());
				}
				collegeDepartment.setState(true);
			}
			collegeDepartment.setName(collegeDepartmentDTO.getName());
			collegeDepartmentDao.save(collegeDepartment);
		} catch (DataAccessException e) {
			throw new DataException(CollegeDepartment.class, "Error al guardar el Departamento Universitario",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			CollegeDepartment collegeDepartment = findById(id);
			collegeDepartment.setState(false);
			collegeDepartmentDao.save(collegeDepartment);
		} catch (DataAccessException e) {
			throw new DataException(CollegeDepartment.class, "Error al eliminar el Departamento Universitario",
					e.getMostSpecificCause().getMessage());
		}
	}
}