package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectStatusDao;
import ec.edu.espe.gpi.dto.registry.ProjectStatusDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ProjectStatus;

@Service
public class ProjectStatusService implements IProjectStatusService {

	@Autowired
	private IProjectStatusDao projectStatusDao;

	@Override
	@Transactional(readOnly = true)
	public List<ProjectStatus> findAll() {
		return projectStatusDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectStatus findById(Long id) {
		return projectStatusDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ProjectStatus.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ProjectStatusDTO projectStatusDTO) {
		ProjectStatus projectStatus = new ProjectStatus();
		try {
			Long id = projectStatusDTO.getId();
			if (id != null) {
				projectStatus = findById(id);
				if (projectStatusDao.findByNameNotPresent(id, projectStatusDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectStatus.class, "name", projectStatusDTO.getName());
				}
			} else {
				if (projectStatusDao.findByName(projectStatusDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectStatus.class, "name", projectStatusDTO.getName());
				}
				projectStatus.setState(true);
			}
			projectStatus.setName(projectStatusDTO.getName());
			projectStatusDao.save(projectStatus);
		} catch (DataAccessException e) {
			throw new DataException(ProjectStatus.class, "Error al guardar estado del proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ProjectStatus projectStatus = findById(id);
			projectStatus.setState(false);
			projectStatusDao.save(projectStatus);
		} catch (DataAccessException e) {
			throw new DataException(ProjectStatus.class, "Error al eliminar estado del proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}
}