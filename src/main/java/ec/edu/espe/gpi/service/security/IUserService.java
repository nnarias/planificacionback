package ec.edu.espe.gpi.service.security;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.dto.security.UserDTO;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.utils.security.UserGeneral;

public interface IUserService {

	public List<UserModel> findAll();

	public UserModel findByAuthentication(Authentication authentication);

	public UserModel findById(Long id);

	public UserModel findByUsername(String username);

	public UserModel findByUsernameNotNull(String username);

	public UserGeneral findUserGeneralByUsername(String username);

	public void save(UserDTO userDTO) throws NoSuchAlgorithmException;

	public void saveUser(UserModel userModel);

	public void updateState(Long id);

	public void validateChangeUserRole(Authentication authentication);
}