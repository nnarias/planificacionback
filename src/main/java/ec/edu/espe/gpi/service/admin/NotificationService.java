package ec.edu.espe.gpi.service.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.admin.INotificationDao;
import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.admin.Notification;
import ec.edu.espe.gpi.service.security.IUserService;

@Service
public class NotificationService implements INotificationService {

	@Autowired
	private INotificationDao notificationDao;

	@Autowired
	private IUserService userService;

	@Override
	@Transactional
	public void delete(Long id) {
		Notification notification = find(id);
		notificationDao.delete(notification);
	}

	@Override
	@Transactional(readOnly = true)
	public Notification find(Long id) {
		return notificationDao.findById(id).orElseThrow(() -> new NotFoundException(Notification.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Notification> findAll() {
		return notificationDao.findList().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Notification> findAllByUser(Authentication authentication) {
		return notificationDao.findAllByUser(authentication.getName()).orElse(new ArrayList<>());

	}

	@Override
	@Transactional
	public Notification findByUser(Authentication authentication, Long id) {
		Notification notification = notificationDao.findByUser(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(Notification.class, "id", id));
		notification.setRecentState(false);
		notificationDao.save(notification);
		return notification;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Notification> findByUserRecentNotification(Authentication authentication) {
		return notificationDao.findByUserRecentNotification(authentication.getName()).orElse(new ArrayList<>());
	}

	@Override
	@Transactional
	public void save(NotificationDTO notificationDTO) {
		Date currentDate = new Date();
		Notification notification = new Notification();
		userService.findUserGeneralByUsername(notificationDTO.getUser());
		try {
			Long id = notificationDTO.getId();
			if (id != null) {
				notification = find(id);
			} else {
				notification.setRecentState(true);
			}
			notification.setDate(currentDate);
			notification.setDescription(notificationDTO.getDescription());
			notification.setTitle(notificationDTO.getTitle());
			notification.setUser(notificationDTO.getUser());
			notificationDao.save(notification);
		} catch (DataAccessException e) {
			throw new DataException(Notification.class, "Error al guardar la notificación", e.getMessage());
		}
	}
}