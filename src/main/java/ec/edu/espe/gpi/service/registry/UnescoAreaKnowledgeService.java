package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IUnescoAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.UnescoAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.UnescoAreaKnowledge;

@Service
public class UnescoAreaKnowledgeService implements IUnescoAreaKnowledgeService {

	@Autowired
	private IUnescoAreaKnowledgeDao unescoAreaKnowledgeDao;

	@Override
	@Transactional(readOnly = true)
	public List<UnescoAreaKnowledge> findAll() {
		return unescoAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public UnescoAreaKnowledge findById(Long id) {
		return unescoAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(UnescoAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional
	public void save(UnescoAreaKnowledgeDTO unescoAreaKnowledgeDTO) {
		UnescoAreaKnowledge unescoAreaKnowledge = new UnescoAreaKnowledge();
		try {
			Long id = unescoAreaKnowledgeDTO.getId();
			if (id != null) {
				unescoAreaKnowledge = findById(id);
				if (unescoAreaKnowledgeDao.findByNameNotPresent(id, unescoAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(UnescoAreaKnowledge.class, "name",
							unescoAreaKnowledgeDTO.getName());
				}
			} else {
				if (unescoAreaKnowledgeDao.findByName(unescoAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(UnescoAreaKnowledge.class, "name",
							unescoAreaKnowledgeDTO.getName());
				}
				unescoAreaKnowledge.setState(true);
			}
			unescoAreaKnowledge.setName(unescoAreaKnowledgeDTO.getName());
			unescoAreaKnowledgeDao.save(unescoAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(UnescoAreaKnowledge.class, "Error al guardar el �rea de conocimiento de la Unuesco",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			UnescoAreaKnowledge unescoAreaKnowledge = findById(id);
			unescoAreaKnowledge.setState(false);
			unescoAreaKnowledgeDao.save(unescoAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(UnescoAreaKnowledge.class,
					"Error al eliminar el �rea de conocimiento de la Unuesco", e.getMostSpecificCause().getMessage());
		}
	}
}