package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IEspeAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.EspeAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.EspeAreaKnowledge;

@Service
public class EspeAreaKnowledgeService implements IEspeAreaKnowledgeService {

	@Autowired
	private IEspeAreaKnowledgeDao espeAreaKnowledgeDao;

	@Override
	@Transactional(readOnly = true)
	public List<EspeAreaKnowledge> findAll() {
		return espeAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public EspeAreaKnowledge findById(Long id) {
		return espeAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(EspeAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional
	public void save(EspeAreaKnowledgeDTO espeAreaKnowledgeDTO) {
		EspeAreaKnowledge espeAreaKnowledge = new EspeAreaKnowledge();
		try {
			Long id = espeAreaKnowledgeDTO.getId();
			if (id != null) {
				espeAreaKnowledge = findById(id);
				if (espeAreaKnowledgeDao.findByNameNotPresent(id, espeAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(EspeAreaKnowledge.class, "name",
							espeAreaKnowledgeDTO.getName());
				}
			} else {
				if (espeAreaKnowledgeDao.findByName(espeAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(EspeAreaKnowledge.class, "name",
							espeAreaKnowledgeDTO.getName());
				}
				espeAreaKnowledge.setState(true);
			}
			espeAreaKnowledge.setName(espeAreaKnowledgeDTO.getName());
			espeAreaKnowledgeDao.save(espeAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(EspeAreaKnowledge.class, "Error al guardar el �rea de conocimiento de la ESPE",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			EspeAreaKnowledge espeAreaKnowledge = findById(id);
			espeAreaKnowledge.setState(false);
			espeAreaKnowledgeDao.save(espeAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(EspeAreaKnowledge.class, "Error al eliminar el �rea de conocimiento de la ESPE",
					e.getMostSpecificCause().getMessage());
		}
	}
}