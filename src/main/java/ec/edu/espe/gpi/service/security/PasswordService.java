package ec.edu.espe.gpi.service.security;

import java.security.NoSuchAlgorithmException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.password.LdapShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.espe.gpi.dao.security.IUserDao;
import ec.edu.espe.gpi.dto.security.PasswordDTO;
import ec.edu.espe.gpi.exception.BadRequest;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.utils.CodeGeneration;
import ec.edu.espe.gpi.utils.security.SSHAEncrypt;

@SuppressWarnings("deprecation")
@Service
public class PasswordService implements IPasswordService {
	private static final String ERROR = "Error de tipo Validaci�n";

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IUserService userService;

	final LdapShaPasswordEncoder sha = new LdapShaPasswordEncoder();

	@Override
	@Transactional
	public void changePassword(UserModel userModel, PasswordDTO password) throws NoSuchAlgorithmException {
		try {
			if (!password.getNewPassword().equals(password.getNewPassword2())) {
				throw new BadRequest(UserModel.class, ERROR, "Las contrase�as no coinciden");
			} else if (password.getNewPassword().length() < 8) {
				throw new BadRequest(UserModel.class, ERROR,
						"La nueva contrase�a, debe contener al menos 8 caracteres");
			} else if (password.getNewPassword().matches("^(?!.*[A-Z])^.*$")) {
				throw new BadRequest(UserModel.class, ERROR,
						"La nueva contrase�a, debe contener al menos una may�scula");
			} else if (password.getNewPassword().matches("^(?!.*[a-z])^.*$")) {
				throw new BadRequest(UserModel.class, ERROR,
						"La nueva contrase�a, debe contener al menos una min�scula");
			} else if (password.getNewPassword().matches("^(?!.*[0-9])^.*$")) {
				throw new BadRequest(UserModel.class, ERROR, "La nueva contrase�a, debe contener al menos un d�gito");
			} else if (password.getNewPassword().matches("^(?!.*[*+��|!\"@�#$~%�&�/()=?����}^{\',;.:\\-\\[\\]])^.*$")) {
				throw new BadRequest(UserModel.class, ERROR, "La nueva contrase�a, debe contener al menos un d�gito");
			} else if (password.getNewPassword().matches("^(?=.*[\\�\\�])^.*")) {
				throw new BadRequest(UserModel.class, ERROR,
						"Evita usar los caracteres � y � por si solos pueden causar confusi�n");
			} else if (!sha.matches(password.getOldPassword(), userModel.getPassword())) {
				throw new BadRequest(UserModel.class, ERROR, "La contrase�a antigua no es correcta");
			} else {
				String passwordEncryp = SSHAEncrypt.generateSSHA(password.getNewPassword());
				userModel.setPassword(passwordEncryp);
				userDao.save(userModel);
			}
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, "Error al cambiar la contrase�a",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void passwordReset(Long id) throws NoSuchAlgorithmException {
		UserModel user = new UserModel();
		String password = "";
		CodeGeneration codeGeneration = new CodeGeneration();
		try {
			user = userService.findById(id);
			password = SSHAEncrypt.generateSSHA(codeGeneration.pwdGeneration(user.getBirthDate()));
			user.setPassword(password);
			userDao.save(user);
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, "Error al resetear la contrase�a",
					e.getMostSpecificCause().getMessage());
		}
	}
}