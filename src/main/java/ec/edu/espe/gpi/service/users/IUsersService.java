package ec.edu.espe.gpi.service.users;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.users.UsersDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.users.Users;

public interface IUsersService {
	
	
	public void save(UsersDTO usersDTO);
	
	
	
	
}
