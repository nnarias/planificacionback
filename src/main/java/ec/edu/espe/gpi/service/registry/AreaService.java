package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IAreaDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Area;

@Service
public class AreaService implements IAreaService {

	@Autowired
	private IAreaDao areaDao;

	@Override
	@Transactional(readOnly = true)
	public List<Area> findAll() {
		return areaDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Area findById(Long id) {
		return areaDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Area.class, "id", id));
	}
}