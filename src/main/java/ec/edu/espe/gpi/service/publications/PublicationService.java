package ec.edu.espe.gpi.service.publications;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.dao.publications.IPublicationsDao;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class PublicationService implements IPublicationsService {
	@Autowired
	private IPublicationsDao publicationsDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Publications> findAll() {
		return publicationsDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Publications> findAllByPlanning(Planning planning) {
		return publicationsDao.findAllByPlanningID(planning).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Publications findById(Long id) {
		return publicationsDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Publications.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Publications> findByIdStateFalse(Long id) {
		return publicationsDao.findById(id);
	}

	@Override
	@Transactional
	public void save(PublicationsDTO publicationsDTO) {
		Publications publications = new Publications();
		boolean flag=false;
		try {
			Long id = publicationsDTO.getId();
			if (id != null) {
				//modificar
				publications = findById(id);
				publications.setCodIES(publicationsDTO.getCodIES());
				publications.setType(publicationsDTO.getType());
				publications.setArticleType(publicationsDTO.getArticleType());
				publications.setCodPUB(publicationsDTO.getCodPUB());
				publications.setTitle(publicationsDTO.getTitle());
				publications.setIndexBase(publicationsDTO.getIndexBase());
				publications.setCodeISS(publicationsDTO.getCodeISS());
				publications.setMagazineName(publicationsDTO.getMagazineName());
				publications.setQuartile(publicationsDTO.getQuartile());
				publications.setMagazineNumber(publicationsDTO.getMagazineNumber());
				publications.setSjr(publicationsDTO.getSjr());
				publications.setPublicationDate(publicationsDTO.getPublicationDate());
				publications.setDetailField(publicationsDTO.getDetailField());
	            publications.setCondition(publicationsDTO.getCondition());
	            publications.setPublicationLink(publicationsDTO.getPublicationLink());
	            publications.setMagazineLink(publicationsDTO.getMagazineLink());
	            publications.setFiliation(publicationsDTO.getFiliation());
				publications.setCompetitor(publicationsDTO.getCompetitor());
				publications.setObservation(publicationsDTO.getObservation());
				publications.setDuration(publicationsDTO.getDuration());
				publications.setAprobation(publicationsDTO.getAprobation());
				publications.setObservations(publicationsDTO.getObservations());
				
				//evaluar si esta completo
				if(!(publications.getId()==null) 
						&& !(publications.getPlanning()==null)
						&& !(publications.getCodIES()==null)
						&& !(publications.getType()==null)
						&& !(publications.getArticleType()==null)
						&& !(publications.getCodPUB()==null)
						&& !(publications.getTitle()==null)
						&& !(publications.getIndexBase()==null)
						&& !(publications.getCodeISS()==null)
						&& !(publications.getMagazineName()==null)
						&& !(publications.getQuartile()==null)
						&& !(publications.getMagazineNumber()==null)
						&& !(publications.getSjr()==null)
						&& !(publications.getPublicationDate()==null)
						&& !(publications.getDetailField()==null)
						&& !(publications.getCondition()==null)
						&& !(publications.getPublicationLink()==null)
						&& !(publications.getMagazineLink()==null)
						&& !(publications.getFiliation()==null)
				        && !(publications.getCompetitor()==null)
				        && !(publications.getObservation()==null)
				        && !(publications.getDuration()==null)) {
					flag = true;
				}	
				publications.setComplete(flag);
				
			} else {
				//crear
				publications.setPlanning(publicationsDTO.getPlanning());
				publications.setCodIES(publicationsDTO.getCodIES());
				publications.setType(publicationsDTO.getType());
				publications.setArticleType(publicationsDTO.getArticleType());
				publications.setCodPUB(publicationsDTO.getCodPUB());
				publications.setTitle(publicationsDTO.getTitle());
				publications.setIndexBase(publicationsDTO.getIndexBase());
				publications.setCodeISS(publicationsDTO.getCodeISS());
				publications.setMagazineName(publicationsDTO.getMagazineName());
				publications.setQuartile(publicationsDTO.getQuartile());
				publications.setMagazineNumber(publicationsDTO.getMagazineNumber());
				publications.setSjr(publicationsDTO.getSjr());
				publications.setPublicationDate(publicationsDTO.getPublicationDate());
				publications.setDetailField(publicationsDTO.getDetailField());
	            publications.setCondition(publicationsDTO.getCondition());
	            publications.setPublicationLink(publicationsDTO.getPublicationLink());
	            publications.setMagazineLink(publicationsDTO.getMagazineLink());
	            publications.setFiliation(publicationsDTO.getFiliation());
				publications.setCompetitor(publicationsDTO.getCompetitor());
				publications.setObservation(publicationsDTO.getObservation());
				publications.setDuration(publicationsDTO.getDuration());
				publications.setAprobation(publicationsDTO.getAprobation());
				publications.setObservations(publicationsDTO.getObservations());
				publications.setComplete(false);
				publications.setRemoved(false);
			}
			publicationsDao.save(publications);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar la Publicación", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Publications publications = findById(id);
			publications.setRemoved(true);
			publicationsDao.save(publications);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar la Publicación", e.getMessage());
		}

	}
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			Publications publications = findById(id);
			publications.setComplete(true);
			publicationsDao.save(publications);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar la Publicación", e.getMessage());
		}

	}

}
