package ec.edu.espe.gpi.service.books;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IBooksService {
	public List<Books> findAll();
	
	public List<Books> findAllByPlanning(Planning planning);
	
	public Books findById(Long id);
	
	public void save(BooksDTO booksDTO);
	
	public void updateState(Long id);
	
	public void updateComplete(Long id);
	
	Optional<Books> findByIdStateFalse(Long id);

}
