package ec.edu.espe.gpi.service.registry;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectRequirementDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ProjectRequirement;

@Service
public class ProjectRequirementService implements IProjectRequirementService {

	@Autowired
	private IProjectRequirementDao projectRequirementDao;

	@Override
	@Transactional(readOnly = true)
	public ProjectRequirement find(Long id) {
		return projectRequirementDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ProjectRequirement.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectRequirement> findAllByProject(Long projectId) {
		return projectRequirementDao.findAllByProject(projectId)
				.orElseThrow(() -> new NotFoundException(ProjectRequirement.class, "id project", projectId)).stream()
				.map(temp -> {
					temp.setProject(null);
					return temp;
				}).collect(Collectors.toList());
	}
}