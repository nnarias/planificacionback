package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectStatusDTO;
import ec.edu.espe.gpi.model.registry.ProjectStatus;

public interface IProjectStatusService {

	public List<ProjectStatus> findAll();

	public ProjectStatus findById(Long id);

	public void save(ProjectStatusDTO projectStatusDTO);

	public void updateState(Long id);
}
