package ec.edu.espe.gpi.service.security;

import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.model.security.Menu;
import ec.edu.espe.gpi.model.security.Route;

public interface IRouteService {

	public List<Route> findAll();

	public Route findById(Long id);
	
	public List<Menu> findMenu(Authentication authentication);
}