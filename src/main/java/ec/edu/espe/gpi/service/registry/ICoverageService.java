package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CoverageDTO;
import ec.edu.espe.gpi.model.registry.Coverage;

public interface ICoverageService {

	public List<Coverage> findAll();

	public Coverage findById(Long id);

	public void save(CoverageDTO coverageDTO);

	public void updateState(Long id);
}