package ec.edu.espe.gpi.service.security;

import ec.edu.espe.gpi.utils.security.PerfilMiespe;
import ec.edu.espe.gpi.utils.security.UserCedulaMiespe;
import ec.edu.espe.gpi.utils.security.UserProfile;
import ec.edu.espe.gpi.utils.security.UserUsernameMiespe;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;
import ec.edu.espe.gpi.utils.security.UsuarioRolMiespe;

import java.util.List;

import ec.edu.espe.gpi.model.security.Role;

public interface IMiespeService {

	public UserldapMiespe findUserldap(String username);

	public UserUsernameMiespe findUserUsername(String username);

	public UserCedulaMiespe findUserCedula(String cedula);

	public UsuarioRolMiespe findUserRole(String codId);

	public List<Role> findUserListRole(List<PerfilMiespe> perfilList);

	public UserProfile[] findUserProfileList(String profile);

}
