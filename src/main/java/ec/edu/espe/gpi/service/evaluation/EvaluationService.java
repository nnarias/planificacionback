package ec.edu.espe.gpi.service.evaluation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.evaluation.IEvaluationDao;
import ec.edu.espe.gpi.dao.registry.ICallDao;
import ec.edu.espe.gpi.dao.registry.IProjectDao;
import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.dto.evaluation.EvaluationCriteriaDTO;
import ec.edu.espe.gpi.dto.evaluation.EvaluationCriteriaSubcriteriaDTO;
import ec.edu.espe.gpi.dto.evaluation.EvaluationDTO;
import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.Criteria;
import ec.edu.espe.gpi.model.evaluation.Evaluation;
import ec.edu.espe.gpi.model.evaluation.EvaluationCriteria;
import ec.edu.espe.gpi.model.evaluation.EvaluationCriteriaSubcriteria;
import ec.edu.espe.gpi.model.evaluation.Subcriteria;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.service.admin.INotificationService;
import ec.edu.espe.gpi.service.registry.ICallService;
import ec.edu.espe.gpi.service.registry.IProjectService;

@Service
public class EvaluationService implements IEvaluationService {

	@Autowired
	private ICallDao callDao;

	@Autowired
	private ICallService callService;

	@Autowired
	private ICriteriaService criteriaService;

	@Autowired
	private IEvaluationCriteriaService evaluationCriteriaService;

	@Autowired
	private IEvaluationCriteriaSubcriteriaService evaluationCriteriaSubcriteriaService;

	@Autowired
	private IEvaluationDao evaluationDao;

	@Autowired
	private INotificationService notificationService;

	@Autowired
	private IProjectDao projectDao;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private ISubcriteriaService subcriteriaService;

	@Override
	@Transactional
	public void evaluate(ProjectDTO projectDTO) {
		Call call = callService.find(projectDTO.getCallId());
		String description = "";
		NotificationDTO notificationDTO = new NotificationDTO();
		Project project = projectService.find(projectDTO.getId());
		String title = "";
		project.setEvaluation(projectDTO.getEvaluation());
		project.setProjectEvaluationList(projectDTO.getProjectEvaluationList());
		project.getProjectEvaluationList().stream().map(projectEvaluation -> {
			projectEvaluation.setProject(project);
			projectEvaluation.setState(true);
			return projectEvaluation;
		}).collect(Collectors.toList());
		notificationDTO.setDescription(description);
		notificationDTO.setTitle(title);
		notificationService.save(notificationDTO);
		projectDao.save(project);
		callDao.save(call);
	}

	private List<EvaluationCriteria> evaluationCriteriaAssignment(
			List<EvaluationCriteriaDTO> evaluationCriteriaDTOList) {
		Criteria criteria = new Criteria();
		List<EvaluationCriteria> evaluationCriteriaList = new ArrayList<>();
		List<EvaluationCriteriaSubcriteria> evaluationCriteriaSubcriteriaList = new ArrayList<>();
		try {
			for (EvaluationCriteriaDTO evaluationCriteriaDTO : evaluationCriteriaDTOList) {
				Long criteriaId = evaluationCriteriaDTO.getCriteria().getId();
				EvaluationCriteria evaluationCriteria;
				Long id = evaluationCriteriaDTO.getId();
				if (criteriaId != null) {
					criteria = criteriaService.find(criteriaId);
				} else {
					criteria.setState(true);
				}
				criteria.setName(evaluationCriteriaDTO.getCriteria().getName());
				if (id != null) {
					evaluationCriteria = evaluationCriteriaService.find(id);
				} else {
					evaluationCriteria = new EvaluationCriteria();
					evaluationCriteria.setState(true);
				}
				evaluationCriteriaSubcriteriaList = evaluationCriteriaSubcriteriaAssignment(
						evaluationCriteriaDTO.getEvaluationCriteriaSubcriteriaList());
				evaluationCriteria.setCriteria(criteria);
				if (evaluationCriteria.getEvaluationCriteriaSubcriteriaList() != null) {
					evaluationCriteria.setEvaluationCriteriaSubcriteriaList(evaluationCriteriaSubcriteriaList);
				} else {
					evaluationCriteria.getEvaluationCriteriaSubcriteriaList().clear();
					evaluationCriteria.getEvaluationCriteriaSubcriteriaList().addAll(evaluationCriteriaSubcriteriaList);
				}
				evaluationCriteria.setGrade(evaluationCriteriaDTO.getGrade());
				evaluationCriteriaList.add(evaluationCriteria);
			}
			return evaluationCriteriaList;
		} catch (Exception e) {
			throw new DataException(EvaluationCriteria.class, "Error al asignar Criterios de evaluación",
					e.getMessage());
		}
	}

	private List<EvaluationCriteriaSubcriteria> evaluationCriteriaSubcriteriaAssignment(
			List<EvaluationCriteriaSubcriteriaDTO> evaluationCriteriaDTOList) {
		Subcriteria subcriteria = new Subcriteria();
		List<EvaluationCriteriaSubcriteria> evaluationCriteriaSubcriteriaList = new ArrayList<>();
		try {
			for (EvaluationCriteriaSubcriteriaDTO evaluationCriteriaSubcriteriaDTO : evaluationCriteriaDTOList) {
				EvaluationCriteriaSubcriteria evaluationCriteriaSubcriteria = new EvaluationCriteriaSubcriteria();
				Long id = evaluationCriteriaSubcriteriaDTO.getId();
				Long subcriteriaId = evaluationCriteriaSubcriteriaDTO.getSubcriteria().getId();

				if (subcriteriaId != null) {
					subcriteria = subcriteriaService.findById(subcriteriaId);
				} else {
					subcriteria.setState(true);
				}
				subcriteria.setName(evaluationCriteriaSubcriteriaDTO.getSubcriteria().getName());
				if (id != null) {
					evaluationCriteriaSubcriteria = evaluationCriteriaSubcriteriaService.findById(id);
				} else {
					evaluationCriteriaSubcriteria.setState(true);
				}
				evaluationCriteriaSubcriteria.setSubcriteria(subcriteria);
				evaluationCriteriaSubcriteria.setGrade(evaluationCriteriaSubcriteriaDTO.getGrade());
				evaluationCriteriaSubcriteriaList.add(evaluationCriteriaSubcriteria);
			}
			return evaluationCriteriaSubcriteriaList;
		} catch (Exception e) {
			throw new DataException(EvaluationCriteria.class, "Error al asignar Subcriterios de evaluación",
					e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Evaluation find(Long id) {
		return evaluationDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Evaluation.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Evaluation> findAll() {
		return evaluationDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional
	public void save(EvaluationDTO evaluationDTO) {
		Evaluation evaluation = new Evaluation();
		try {
			Long id = evaluationDTO.getId();
			if (id != null) {
				evaluation = find(id);
				if (evaluationDao.findByNameNotPresent(id, evaluationDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Evaluation.class, "name", evaluationDTO.getName());
				}
			} else {
				if (evaluationDao.findByName(evaluationDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Evaluation.class, "name", evaluationDTO.getName());
				}
				evaluation.setState(true);
			}
			List<EvaluationCriteria> evaluationCriteriaList = evaluationCriteriaAssignment(
					evaluationDTO.getEvaluationCriteriaList());
			if (evaluation.getEvaluationCriteriaList() == null) {
				evaluation.setEvaluationCriteriaList(evaluationCriteriaList);
			} else {
				evaluation.getEvaluationCriteriaList().clear();
				evaluation.getEvaluationCriteriaList().addAll(evaluationCriteriaList);
			}
			evaluation.setGrade(evaluationDTO.getGrade());
			evaluation.setName(evaluationDTO.getName());
			evaluationDao.save(evaluation);
		} catch (DataAccessException e) {
			throw new DataException(Evaluation.class, "Error al eliminar evaluación", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Evaluation evaluation = find(id);
			evaluation.setState(false);
			evaluationDao.save(evaluation);
		} catch (DataAccessException e) {
			throw new DataException(Evaluation.class, "Error al eliminar evaluación", e.getMessage());
		}
	}
}