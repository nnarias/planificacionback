package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectTypeDTO;
import ec.edu.espe.gpi.model.registry.ProjectType;

public interface IProjectTypeService {

	public List<ProjectType> findAll();

	public ProjectType findById(Long id);

	public void save(ProjectTypeDTO projectTypeDTO);

	public void updateState(Long id);
}