package ec.edu.espe.gpi.service.degrees;

import java.util.List;
import java.util.Optional;


import ec.edu.espe.gpi.dto.degress.DegreesDTO;
import ec.edu.espe.gpi.model.degress.Degrees;

public interface IDegreesService {
	public List<Degrees> findAll();
	
	public Degrees findById(Long id);
	
	public void save(DegreesDTO degreesDTO);
	
	public void updateState(Long id);
	
	Optional<Degrees> findByIdStateFalse(Long id);
}
