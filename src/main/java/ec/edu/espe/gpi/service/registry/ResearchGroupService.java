package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IResearchGroupDao;
import ec.edu.espe.gpi.dto.registry.ResearchGroupDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ResearchGroup;

@Service
public class ResearchGroupService implements IResearchGroupService {

	@Autowired
	private IResearchGroupDao researchGroupDao;

	@Override
	@Transactional(readOnly = true)
	public List<ResearchGroup> findAll() {
		return researchGroupDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ResearchGroup findById(Long id) {
		return researchGroupDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ResearchGroup.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ResearchGroupDTO researchGroupDTO) {
		ResearchGroup researchGroup = new ResearchGroup();
		try {
			Long id = researchGroupDTO.getId();
			if (id != null) {
				researchGroup = findById(id);
				if (researchGroupDao.findByNameNotPresent(id, researchGroupDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ResearchGroup.class, "name", researchGroupDTO.getName());
				}
			} else {
				if (researchGroupDao.findByName(researchGroupDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ResearchGroup.class, "name", researchGroupDTO.getName());
				}
				researchGroup.setState(true);
			}
			researchGroup.setName(researchGroupDTO.getName());
			researchGroupDao.save(researchGroup);
		} catch (DataAccessException e) {
			throw new DataException(ResearchGroup.class, "Error al guardar el grupo de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ResearchGroup researchGroup = findById(id);
			researchGroup.setState(false);
			researchGroupDao.save(researchGroup);
		} catch (DataAccessException e) {
			throw new DataException(ResearchGroup.class, "Error al eliminar el grupo de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}
}