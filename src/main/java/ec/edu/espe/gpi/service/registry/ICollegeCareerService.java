package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CollegeCareerDTO;
import ec.edu.espe.gpi.model.registry.CollegeCareer;

public interface ICollegeCareerService {

	public List<CollegeCareer> findAll();

	public CollegeCareer findById(Long id);

	public void save(CollegeCareerDTO collegeCareerDTO);

	public void updateState(Long id);
}