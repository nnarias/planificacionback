package ec.edu.espe.gpi.service.books;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.bookchapters.IBookChaptersDao;
import ec.edu.espe.gpi.dao.books.IBooksDao;
import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class BooksService implements IBooksService{

	@Autowired
	private IBooksDao booksDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Books> findAll() {
		return booksDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Books> findAllByPlanning(Planning planning) {
		return booksDao.findAllByPlanningID(planning).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Books findById(Long id) {
		return booksDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Books.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Books> findByIdStateFalse(Long id) {
		return booksDao.findById(id);
	}

	@Override
	@Transactional
	public void save(BooksDTO booksDTO) {
		Books books = new Books();
		boolean flag=false;
		try {
			Long id = booksDTO.getId();
			if (id != null) {
				//modificar
				books = findById(id);
				books.setCodIES(booksDTO.getCodIES());
				books.setPublicationType(booksDTO.getPublicationType());
				books.setCodPUB(booksDTO.getCodPUB());
				books.setBookTitle(booksDTO.getBookTitle());
				books.setCodISB(booksDTO.getCodISB());
				books.setPublicationDate(booksDTO.getPublicationDate());
				books.setDetailField(booksDTO.getDetailField());
				books.setPeerReviewed(booksDTO.getPeerReviewed());
				books.setFiliation(booksDTO.getFiliation());
				books.setCompetitor(booksDTO.getCompetitor());
				books.setDuration(booksDTO.getDuration());
				books.setAprobation(booksDTO.getAprobation());
				books.setObservations(booksDTO.getObservations());
				
				//evaluar si esta completo
				if(!(books.getId()==null) 
						&& !(books.getPlanning()==null)
						&& !(books.getCodIES()==null)
						&& !(books.getPublicationType()==null)
						&& !(books.getCodPUB()==null)
						&& !(books.getBookTitle()==null)
						&& !(books.getCodISB()==null)
						&& !(books.getPublicationDate()==null)
						&& !(books.getDetailField()==null)
						&& !(books.getPeerReviewed()==null)
				        && !(books.getFiliation()==null)
				        && !(books.getCompetitor()==null)
				        && !(books.getDuration()==null)) {
					flag = true;
				}	
				books.setComplete(flag);
				
			} else {
				//crear
				books.setPlanning(booksDTO.getPlanning());
				books.setCodIES(booksDTO.getCodIES());
				books.setPublicationType(booksDTO.getPublicationType());
				books.setCodPUB(booksDTO.getCodPUB());
				books.setBookTitle(booksDTO.getBookTitle());
				books.setCodISB(booksDTO.getCodISB());
				books.setPublicationDate(booksDTO.getPublicationDate());
				books.setDetailField(booksDTO.getDetailField());
				books.setPeerReviewed(booksDTO.getPeerReviewed());
				books.setFiliation(booksDTO.getFiliation());
				books.setCompetitor(booksDTO.getCompetitor());
				books.setDuration(booksDTO.getDuration());
				books.setAprobation(booksDTO.getAprobation());
				books.setObservations(booksDTO.getObservations());
				books.setComplete(false);
				books.setRemoved(false);
			}
			booksDao.save(books);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar el libro", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Books books = findById(id);
			books.setRemoved(true);
			booksDao.save(books);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar el libro", e.getMessage());
		}

	}
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			Books books = findById(id);
			books.setComplete(true);
			booksDao.save(books);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar el libro", e.getMessage());
		}

	}
}
