package ec.edu.espe.gpi.service.registry;

import java.util.List;
import ec.edu.espe.gpi.dto.registry.SpecificAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.SpecificAreaKnowledge;

public interface ISpecificAreaKnowledgeService {

	public List<SpecificAreaKnowledge> findAll();

	public SpecificAreaKnowledge findById(Long id);

	public void save(SpecificAreaKnowledgeDTO specificAreaKnowledgeDTO);

	public void updateState(Long id);
}
