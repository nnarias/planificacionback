package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICollegeCareerDao;
import ec.edu.espe.gpi.dto.registry.CollegeCareerDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.CollegeCareer;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;

@Service
public class CollegeCareerService implements ICollegeCareerService {

	@Autowired
	private ICollegeCareerDao collegeCareerDao;

	@Autowired
	private ICollegeDepartmentService collegeDepartmentService;

	@Override
	@Transactional(readOnly = true)
	public List<CollegeCareer> findAll() {
		return collegeCareerDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public CollegeCareer findById(Long id) {
		return collegeCareerDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(CollegeCareer.class, "id", id));
	}

	@Override
	@Transactional
	public void save(CollegeCareerDTO collegeCareerDTO) {
		CollegeCareer collegeCareer = new CollegeCareer();
		try {
			Long id = collegeCareerDTO.getId();
			if (id != null) {
				collegeCareer = findById(id);
				if (collegeCareerDao.findByNameNotPresent(id, collegeCareerDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CollegeCareer.class, "name", collegeCareerDTO.getName());
				}
			} else {
				if (collegeCareerDao.findByName(collegeCareerDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CollegeCareer.class, "name", collegeCareerDTO.getName());
				}
				collegeCareer.setState(true);
			}
			CollegeDepartment collegeDepartment = collegeDepartmentService
					.findById(collegeCareerDTO.getCollegeDepartmentId());

			collegeCareer.setCollegeDepartment(collegeDepartment);
			collegeCareer.setName(collegeCareerDTO.getName());
			collegeCareerDao.save(collegeCareer);
		} catch (DataAccessException e) {
			throw new DataException(CollegeCareer.class, "Error al guardar la Carrera Universitaria",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			CollegeCareer collegeCareer = findById(id);
			collegeCareer.setState(false);
			collegeCareerDao.save(collegeCareer);
		} catch (DataAccessException e) {
			throw new DataException(CollegeCareer.class, "Error al eliminar la Carrera Universitaria",
					e.getMostSpecificCause().getMessage());
		}
	}
}