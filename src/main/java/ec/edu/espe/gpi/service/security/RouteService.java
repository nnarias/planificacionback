package ec.edu.espe.gpi.service.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.security.IMenuDao;
import ec.edu.espe.gpi.dao.security.IRouteDao;
import ec.edu.espe.gpi.dao.security.IUserDao;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.security.Menu;
import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.model.security.Route;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;
import ec.edu.espe.gpi.utils.security.UsuarioRolMiespe;

@Service
public class RouteService implements IRouteService {

	@Autowired
	private IMenuDao menuDao;

	@Autowired
	private IRouteDao routeDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IMiespeService miespeService;

	@Override
	@Transactional(readOnly = true)
	public List<Route> findAll() {
		return (List<Route>) routeDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public Route findById(Long id) {
		return routeDao.findById(id).orElseThrow(() -> new NotFoundException(Route.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Menu> findMenu(Authentication authentication) {
		List<Menu> currentListMenu = findMenuUserInternal(authentication);
		if (!currentListMenu.isEmpty()) {
			return currentListMenu;
		} else {
			return findMenuUserExternal(authentication);
		}

	}

	public List<Menu> findMenuUserInternal(Authentication authentication) {
		List<Menu> currentListMenu;
		UserldapMiespe userldapMiespe = miespeService.findUserldap(authentication.getName());
		if (userldapMiespe.getUserName() != null) {
			UsuarioRolMiespe usuarioRolMiespe = miespeService.findUserRole(userldapMiespe.getCodId());
			List<Role> roleCurrentList = miespeService.findUserListRole(usuarioRolMiespe.getPerfil());
			currentListMenu = roleMenu(roleCurrentList);
			return currentListMenu;
		} else {
			return new ArrayList<>();
		}

	}

	public List<Menu> findMenuUserExternal(Authentication authentication) {
		UserModel userModel = new UserModel();
		List<Menu> currentListMenu = new ArrayList<>();
		List<Role> roleList = new ArrayList<>();
		try {
			userModel = userDao.findByUsername(authentication.getName())
					.orElseThrow(() -> new NotFoundException(UserModel.class, "username", authentication.getName()));
			roleList = userModel.getRoleList();
			currentListMenu = roleMenu(roleList);
			return currentListMenu;
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, "Error al realizar la consulta en la base de datos",
					e.getMostSpecificCause().getMessage());
		}
	}

	public List<Menu> roleMenu(List<Role> roleList) {
		List<Menu> currentListMenu = new ArrayList<>();
		List<Menu> menuList = (List<Menu>) menuDao.findAll();
		for (Menu menu : menuList) {
			List<Route> routeList = new ArrayList<>();
			for (Route route : menu.getRouteList()) {
				for (Role role : roleList) {
					if (role.getRouteList().contains(route) && !routeList.contains(route))
						routeList.add(route);
				}
			}
			if (!routeList.isEmpty()) {
				menu.setRouteList(routeList);
				currentListMenu.add(menu);
			}
		}
		return currentListMenu;
	}
}