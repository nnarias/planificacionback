package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IResearchTypeDao;
import ec.edu.espe.gpi.dto.registry.ResearchTypeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ResearchType;

@Service
public class ResearchTypeService implements IResearchTypeService {

	@Autowired
	private IResearchTypeDao researchTypeDao;

	@Override
	@Transactional(readOnly = true)
	public List<ResearchType> findAll() {
		return researchTypeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ResearchType findById(Long id) {
		return researchTypeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ResearchType.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ResearchTypeDTO researchTypeDTO) {
		ResearchType researchType = new ResearchType();
		try {
			Long id = researchTypeDTO.getId();
			if (id != null) {
				researchType = findById(id);
				if (researchTypeDao.findByNameNotPresent(id, researchTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ResearchType.class, "name", researchTypeDTO.getName());
				}
			} else {
				if (researchTypeDao.findByName(researchTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ResearchType.class, "name", researchTypeDTO.getName());
				}
				researchType.setState(true);
			}
			researchType.setName(researchTypeDTO.getName());
			researchTypeDao.save(researchType);
		} catch (DataAccessException e) {
			throw new DataException(ResearchType.class, "Error al guardar el tipo de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ResearchType researchType = findById(id);
			researchType.setState(false);
			researchTypeDao.save(researchType);
		} catch (DataAccessException e) {
			throw new DataException(ResearchType.class, "Error al eliminar el tipo de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}
}