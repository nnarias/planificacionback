package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.RequirementDTO;
import ec.edu.espe.gpi.model.registry.Requirement;

public interface IRequirementService {

	public List<Requirement> findAll();

	public List<Requirement> findByCall(Long idCall);

	public Requirement find(Long id);

	public void save(RequirementDTO requirementDTO);

	public void updateState(Long id);
}