package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.SubUnescoAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.SubUnescoAreaKnowledge;

public interface ISubUnescoAreaKnowledgeService {

	public List<SubUnescoAreaKnowledge> findAll();

	public SubUnescoAreaKnowledge findById(Long id);

	public List<SubUnescoAreaKnowledge> findByUnesco(Long subUnescoAreaKnowledgeId);

	public void save(SubUnescoAreaKnowledgeDTO subUnescoAreaKnowledgeDTO);

	public void updateState(Long id);
}