package ec.edu.espe.gpi.service.evaluation;

import ec.edu.espe.gpi.model.evaluation.EvaluationCriteria;

public interface IEvaluationCriteriaService {

	public EvaluationCriteria find(Long id);
}