package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IPostgraduateProgramDao;
import ec.edu.espe.gpi.dto.registry.PostgraduateProgramDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.PostgraduateProgram;

@Service
public class PostgraduateProgramService implements IPostgraduateProgramService {

	@Autowired
	private IPostgraduateProgramDao postgraduateProgramDao;

	@Override
	@Transactional(readOnly = true)
	public List<PostgraduateProgram> findAll() {
		return postgraduateProgramDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public PostgraduateProgram findById(Long id) {
		return postgraduateProgramDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(PostgraduateProgram.class, "id", id));
	}

	@Override
	@Transactional
	public void save(PostgraduateProgramDTO postgraduateProgramDTO) {
		PostgraduateProgram postgraduateProgram = new PostgraduateProgram();
		try {
			Long id = postgraduateProgramDTO.getId();
			if (id != null) {
				postgraduateProgram = findById(id);
				if (postgraduateProgramDao.findByNameNotPresent(id, postgraduateProgramDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(PostgraduateProgram.class, "name",
							postgraduateProgramDTO.getName());
				}
			} else {
				if (postgraduateProgramDao.findByName(postgraduateProgramDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(PostgraduateProgram.class, "name",
							postgraduateProgramDTO.getName());
				}
				postgraduateProgram.setState(true);
			}
			postgraduateProgram.setName(postgraduateProgramDTO.getName());
			postgraduateProgramDao.save(postgraduateProgram);
		} catch (DataAccessException e) {
			throw new DataException(PostgraduateProgram.class, "Error al guardar el programa de postgrado",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			PostgraduateProgram postgraduateProgram = findById(id);
			postgraduateProgram.setState(false);
			postgraduateProgramDao.save(postgraduateProgram);
		} catch (DataAccessException e) {
			throw new DataException(PostgraduateProgram.class, "Error al eliminar el programa de postgrado",
					e.getMostSpecificCause().getMessage());
		}
	}
}