package ec.edu.espe.gpi.service.activities;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.activities.ActivitiesDTO;
import ec.edu.espe.gpi.model.activities.Activities;

public interface IActivitiesService {
	public List<Activities> findAll();
	
	public Activities findById(Long id);
	
	public void save(ActivitiesDTO activitiesDTO);
	
	public void updateState(Long id);
	
	Optional<Activities> findByIdStateFalse(Long id);
	
	public List<Activities>  findByMaxDuration(Long id);

}
