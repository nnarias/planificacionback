package ec.edu.espe.gpi.service.security;

import java.security.NoSuchAlgorithmException;

import ec.edu.espe.gpi.dto.security.PasswordDTO;
import ec.edu.espe.gpi.model.security.UserModel;

public interface IPasswordService {

	public void changePassword(UserModel userModel, PasswordDTO password) throws NoSuchAlgorithmException;

	public void passwordReset(Long id) throws NoSuchAlgorithmException;
}