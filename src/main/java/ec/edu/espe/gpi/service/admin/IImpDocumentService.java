package ec.edu.espe.gpi.service.admin;

import java.io.IOException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

public interface IImpDocumentService {

	public void createMultipleFolder(String pathFile, TypeUserDocument typeUserDocument);

	public Boolean createSimpleFolder(String pathFile, TypeUserDocument typeUserDocument);

	public InputStreamResource getDocument(String fileUUID, TypeUserDocument typeUserDocument) throws IOException;

	public Document sendDocument(MultipartFile file, String pathFile, TypeUserDocument typeUserDocument)
			throws IOException;

	public String getFilenamePrefix(String name);
}