package ec.edu.espe.gpi.service.evaluation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.evaluation.ISubcriteriaDao;
import ec.edu.espe.gpi.dto.evaluation.SubcriteriaDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.Subcriteria;

@Service
public class SubcriteriaService implements ISubcriteriaService {
	@Autowired
	private ISubcriteriaDao subcriteriaDao;

	@Override
	public List<Subcriteria> findAll() {
		return subcriteriaDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	public Subcriteria findById(Long id) {
		return subcriteriaDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Subcriteria.class, "id", id));
	}

	@Override
	public void save(SubcriteriaDTO subcriteriaDTO) {
		Subcriteria subcriteria = new Subcriteria();
		try {
			Long id = subcriteriaDTO.getId();
			if (id != null) {
				subcriteria = findById(id);
			} else {
				subcriteria.setState(true);
			}
			subcriteria.setName(subcriteriaDTO.getName());
			subcriteriaDao.save(subcriteria);
		} catch (DataAccessException e) {
			throw new DataException(Subcriteria.class, "Error al guardar el subcriterio de evaluación",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	public void updateState(Long id) {
		try {
			Subcriteria subcriteria = findById(id);
			subcriteria.setState(false);
			subcriteriaDao.save(subcriteria);
		} catch (DataAccessException e) {
			throw new DataException(Subcriteria.class, "Error al guardar el subcriterio de evaluación",
					e.getMostSpecificCause().getMessage());
		}
	}
}