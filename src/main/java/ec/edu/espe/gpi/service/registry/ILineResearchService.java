package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.LineResearchDTO;
import ec.edu.espe.gpi.model.registry.LineResearch;

public interface ILineResearchService {

	public List<LineResearch> findAll();

	public LineResearch findById(Long id);

	public void save(LineResearchDTO lineResearchDTO);

	public void updateState(Long id);
}