package ec.edu.espe.gpi.service.evaluation;

import java.util.List;

import ec.edu.espe.gpi.dto.evaluation.CriteriaDTO;
import ec.edu.espe.gpi.model.evaluation.Criteria;

public interface ICriteriaService {

	public Criteria find(Long id);

	public List<Criteria> findAll();

	public void save(CriteriaDTO criteriaDTO);

	public void updateState(Long id);
}