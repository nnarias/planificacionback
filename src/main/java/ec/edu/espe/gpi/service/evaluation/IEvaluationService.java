package ec.edu.espe.gpi.service.evaluation;

import java.util.List;

import ec.edu.espe.gpi.dto.evaluation.EvaluationDTO;
import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.evaluation.Evaluation;

public interface IEvaluationService {

	public void evaluate(ProjectDTO projectDTO);

	public Evaluation find(Long id);

	public List<Evaluation> findAll();

	public void save(EvaluationDTO evaluationDTO);

	public void updateState(Long id);
}