package ec.edu.espe.gpi.service.evaluation;

import java.util.List;

import ec.edu.espe.gpi.dto.evaluation.SubcriteriaDTO;
import ec.edu.espe.gpi.model.evaluation.Subcriteria;

public interface ISubcriteriaService {

	public List<Subcriteria> findAll();

	public Subcriteria findById(Long id);

	public void save(SubcriteriaDTO subcriteriaDTO);

	public void updateState(Long id);
}