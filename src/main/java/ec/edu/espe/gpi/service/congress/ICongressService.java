package ec.edu.espe.gpi.service.congress;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface ICongressService {
	public List<Congress> findAll();
	
	public List<Congress> findAllByPlanning(Planning planning);
	
	public Congress findById(Long id);
	
	public void save(CongressDTO congressDTO);
	
	public void updateState(Long id);
	
	public void updateComplete(Long id);
	
	Optional<Congress> findByIdStateFalse(Long id);
}
