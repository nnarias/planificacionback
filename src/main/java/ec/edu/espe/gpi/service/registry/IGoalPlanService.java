package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.GoalPlanDTO;
import ec.edu.espe.gpi.model.registry.GoalPlan;

public interface IGoalPlanService {

	public List<GoalPlan> findAll();

	public GoalPlan findById(Long id);

	public void save(GoalPlanDTO goalPlanDTO);

	public void updateState(Long id);
}