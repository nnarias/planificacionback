package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ResearchGroupDTO;
import ec.edu.espe.gpi.model.registry.ResearchGroup;

public interface IResearchGroupService {

	public List<ResearchGroup> findAll();

	public ResearchGroup findById(Long id);

	public void save(ResearchGroupDTO researchGroupDTO);

	public void updateState(Long id);
}