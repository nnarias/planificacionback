package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectResponsibleTypeDao;
import ec.edu.espe.gpi.dto.registry.ProjectResponsibleTypeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;

@Service
public class ProjectResponsibleTypeService implements IProjectResponsibleTypeService {

	@Autowired
	private IProjectResponsibleTypeDao projectResponsibleTypeDao;

	@Override
	@Transactional(readOnly = true)
	public List<ProjectResponsibleType> findAll() {
		return projectResponsibleTypeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectResponsibleType findById(Long id) {
		return projectResponsibleTypeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ProjectResponsibleType.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ProjectResponsibleTypeDTO projectResponsibleTypeDTO) {
		ProjectResponsibleType projectResponsibleType = new ProjectResponsibleType();
		try {
			Long id = projectResponsibleTypeDTO.getId();
			if (id != null) {
				projectResponsibleType = findById(id);
				if (projectResponsibleTypeDao.findByNameNotPresent(id, projectResponsibleTypeDTO.getName())
						.isPresent()) {
					throw new IntegrityViolationException(ProjectResponsibleType.class, "name",
							projectResponsibleTypeDTO.getName());
				}
			} else {
				if (projectResponsibleTypeDao.findByName(projectResponsibleTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectResponsibleType.class, "name",
							projectResponsibleTypeDTO.getName());
				}
				projectResponsibleType.setState(true);
			}
			projectResponsibleType.setName(projectResponsibleTypeDTO.getName());
			projectResponsibleTypeDao.save(projectResponsibleType);
		} catch (DataAccessException e) {
			throw new DataException(ProjectResponsibleTypeDTO.class,
					"Error al guardar el tipo de responsable de Proyecto", e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ProjectResponsibleType projectResponsibleType = findById(id);
			projectResponsibleType.setState(false);
			projectResponsibleTypeDao.save(projectResponsibleType);
		} catch (DataAccessException e) {
			throw new DataException(ProjectResponsibleTypeDTO.class,
					"Error al eliminar el tipo de responsable de Proyecto", e.getMostSpecificCause().getMessage());
		}
	}
}