package ec.edu.espe.gpi.service.tracking;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dao.tracking.IGoalActivityDao;
import ec.edu.espe.gpi.dto.admin.ProjectNotificationDTO;
import ec.edu.espe.gpi.dto.tracking.GoalActivityApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectDeliverableDTO;
import ec.edu.espe.gpi.exception.BadRequest;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.model.tracking.GoalActivity;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.admin.IProjectNotificationService;
import ec.edu.espe.gpi.utils.ValidationError;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;
import ec.edu.espe.gpi.utils.registry.DateValidation;
import ec.edu.espe.gpi.utils.tracking.GoalActivityApprovalEnum;
import ec.edu.espe.gpi.utils.tracking.GoalActivityEnum;

@Service
public class GoalActivityService implements IGoalActivityService {

	@Autowired
	private IProjectNotificationService projectNotificationService;

	@Autowired
	private IGoalActivityDao goalActivityDao;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Override
	@Transactional
	public void deliverableApproval(GoalActivityApprovalDTO goalActivityApprovalDTO, Authentication authentication) {
		GoalActivity goalActivity = findTracking(goalActivityApprovalDTO.getId(), authentication);
		Project project = goalActivity.getProjectGoal().getProject();
		GoalActivityEnum state = null;
		ProjectNotificationDTO projectStatusChangeDTO = new ProjectNotificationDTO();
		String title = "";
		if (goalActivityApprovalDTO.getGoalActivityApprovalEnum().equals(GoalActivityApprovalEnum.APPROVED)) {
			title = "Entregable Aprobado por el supervisor tecnico";
			state = GoalActivityEnum.APPROVING;
		} else {
			title = "Entregable Rechazado por el supervisor tecnico";
			state = GoalActivityEnum.REJECTED;
		}
		projectStatusChangeDTO.setComment(goalActivityApprovalDTO.getComment());
		projectStatusChangeDTO.setProjectId(project.getId());
		projectStatusChangeDTO.setTitle(title);
		projectStatusChangeDTO.setUser(authentication.getName());
		projectNotificationService.save(projectStatusChangeDTO);
		goalActivity.setState(state);
		goalActivityDao.save(goalActivity);
	}

	@Override
	@Transactional(readOnly = true)
	public List<GoalActivity> findAllPending(Long idProject, Authentication authentication) {
		List<GoalActivity> goalActivityList = new ArrayList<>();
		goalActivityDao.findAllPending(idProject, authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					GoalActivity goalActivity = new GoalActivity();
					goalActivity.setDeliverable(temp.getDeliverable());
					goalActivity.setEndDate(temp.getEndDate());
					goalActivity.setId(temp.getId());
					goalActivity.setName(temp.getName());
					goalActivity.setStartDate(temp.getStartDate());
					goalActivity.setDeliverableFileUUID(temp.getDeliverableFileUUID());
					goalActivity.setDeliverableFileName(temp.getDeliverableFileName());
					goalActivity.setDeliverableDate(temp.getDeliverableDate());
					goalActivity.setDeliverableDescription(temp.getDeliverableDescription());
					ValidationError validationError = DateValidation.dateRangeValidation(goalActivity.getStartDate(),
							goalActivity.getEndDate());
					if (!validationError.isError()) {
						goalActivityList.add(goalActivity);
					}
					return goalActivity;
				}).collect(Collectors.toList());
		return goalActivityList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<GoalActivity> findAllTracking(Long idProject, Authentication authentication) {
		return goalActivityDao.findAllTracking(idProject, authentication.getName()).orElse(new ArrayList<>()).stream()
				.map(temp -> {
					GoalActivity goalActivity = new GoalActivity();
					goalActivity.setDeliverable(temp.getDeliverable());
					goalActivity.setEndDate(temp.getEndDate());
					goalActivity.setId(temp.getId());
					goalActivity.setName(temp.getName());
					goalActivity.setStartDate(temp.getStartDate());
					goalActivity.setDeliverableFileUUID(temp.getDeliverableFileUUID());
					goalActivity.setDeliverableFileName(temp.getDeliverableFileName());
					goalActivity.setDeliverableDate(temp.getDeliverableDate());
					goalActivity.setDeliverableDescription(temp.getDeliverableDescription());
					return goalActivity;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public GoalActivity findByDeliverable(Long id) {
		return goalActivityDao.findByDeliverable(id)
				.orElseThrow(() -> new NotFoundException(GoalActivity.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public GoalActivity find(Long id) {
		return goalActivityDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(GoalActivity.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public GoalActivity findPending(Long id, Authentication authentication) {
		GoalActivity goalActivity = goalActivityDao.findByPending(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(GoalActivity.class, "id", id));
		validationDeliverableDate(goalActivity);
		return goalActivity;
	}

	@Override
	@Transactional(readOnly = true)
	public GoalActivity findTracking(Long id, Authentication authentication) {
		return goalActivityDao.findByTracking(id, authentication.getName())
				.orElseThrow(() -> new NotFoundException(GoalActivity.class, "id", id));
	}

	@Override
	@Transactional
	public void saveDeliverable(ProjectDeliverableDTO projectDeliverableDTO, Authentication authentication)
			throws IOException {
		Date currentDate = new Date();
		GoalActivity goalActivity = findPending(projectDeliverableDTO.getId(), authentication);
		validationDeliverableDate(goalActivity);
		String pathFile = "/okm:root/" + goalActivity.getProjectGoal().getProject().getProjectDirector()
				+ "/goalActivity/";
		Document document = sendDocument(pathFile, TypeUserDocument.PROJECT_DIRECTOR, projectDeliverableDTO.getFile());
		goalActivity.setDeliverableFileUUID(document.getUuid());
		goalActivity.setDeliverableFileName(document.getFileName());
		goalActivity.setDeliverableDate(currentDate);
		goalActivity.setDeliverableDescription(projectDeliverableDTO.getDescription());
		goalActivity.setState(GoalActivityEnum.EVALUATING);
		goalActivityDao.save(goalActivity);
	}

	public Document sendDocument(String pathFile, TypeUserDocument typeUserDocument, MultipartFile file)
			throws IOException {
		if (file == null) {
			throw new NotFoundException(Document.class, "documento", "vacio");
		}
		impDocumentService.createMultipleFolder(pathFile, typeUserDocument);
		return impDocumentService.sendDocument(file, pathFile, typeUserDocument);
	}

	public void validationDeliverableDate(GoalActivity goalActivity) {
		ValidationError validationError = DateValidation.dateRangeValidation(goalActivity.getStartDate(),
				goalActivity.getEndDate());
		if (validationError.isError()) {
			throw new BadRequest(GoalActivity.class, "Error en la operacion", "Fecha fuera de rango del rango");
		}
	}
}