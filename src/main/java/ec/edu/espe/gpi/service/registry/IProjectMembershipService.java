package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.ProjectMembership;

public interface IProjectMembershipService {

	public void delete(Long id);

	public List<ProjectMembership> findAll();

	public ProjectMembership findById(Long id);

	public ProjectMembership save(ProjectMembership projectMembership);
}