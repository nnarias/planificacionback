package ec.edu.espe.gpi.service.evaluation;

import ec.edu.espe.gpi.dto.evaluation.AssignEvaluatorDTO;

public interface IProjectAssignmentService {

	public void assignEvaluator(AssignEvaluatorDTO evaluatorAssignDTO);
}