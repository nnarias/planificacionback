package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.ProjectRequirement;

public interface IProjectRequirementService {

	public ProjectRequirement find(Long id);

	public List<ProjectRequirement> findAllByProject(Long projectId);
}