package ec.edu.espe.gpi.service.registry;

import ec.edu.espe.gpi.model.registry.Bibliography;

public interface IBibliographyService {

	public Bibliography find(Long id);
}