package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.AnnexDTO;
import ec.edu.espe.gpi.model.registry.Annex;

public interface IAnnexService {

	public List<Annex> findAll();

	public Annex find(Long id);

	public void save(AnnexDTO annexDocumentDTO);

	public void updateState(Long id);
}