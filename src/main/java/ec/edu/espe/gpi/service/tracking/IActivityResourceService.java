package ec.edu.espe.gpi.service.tracking;

import java.io.IOException;
import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.dto.tracking.ActivityResourceApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectInvoiceDTO;
import ec.edu.espe.gpi.model.tracking.ActivityResource;

public interface IActivityResourceService {

	public ActivityResource find(Long id);

	public List<ActivityResource> findAllPending(Long idProject, Authentication authentication);

	public List<ActivityResource> findAllTracking(Long idProject, Authentication authentication);

	public ActivityResource findPending(Long id, Authentication authentication);

	public ActivityResource findTracking(Long id, Authentication authentication);

	public ActivityResource findInvoice(Long id);

	public void saveInvoice(ProjectInvoiceDTO projectInvoiceDTO, Authentication authentication) throws IOException;

	public void invoiceApproval(ActivityResourceApprovalDTO activityResourceApprovalDTO, Authentication authentication);
}