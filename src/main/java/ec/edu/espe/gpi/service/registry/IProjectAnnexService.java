package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.ProjectAnnex;

public interface IProjectAnnexService {

	public ProjectAnnex find(Long id);

	public List<ProjectAnnex> findAllByProject(Long projectId);
}