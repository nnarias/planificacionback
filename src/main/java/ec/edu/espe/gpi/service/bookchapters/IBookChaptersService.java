package ec.edu.espe.gpi.service.bookchapters;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IBookChaptersService {
	public List<BookChapters> findAll();
	
	public List<BookChapters> findAllByPlanning(Planning planning);
	
	public BookChapters findById(Long id);
	
	public void save(BookChaptersDTO bookChaptersDTO);
	
	public void updateState(Long id);
	
	
	public void updateComplete(Long id);
	
	Optional<BookChapters> findByIdStateFalse(Long id);
}
