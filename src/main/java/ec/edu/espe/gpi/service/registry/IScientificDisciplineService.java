package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ScientificDisciplineDTO;
import ec.edu.espe.gpi.model.registry.ScientificDiscipline;

public interface IScientificDisciplineService {

	public List<ScientificDiscipline> findAll();

	public ScientificDiscipline findById(Long id);

	public void save(ScientificDisciplineDTO scientificDisciplineDTO);

	public void updateState(Long id);
}