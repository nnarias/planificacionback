package ec.edu.espe.gpi.service.security;

import java.util.List;

import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.utils.security.UserRoleGeneral;

public interface IRoleService {

	public List<Role> findAll();

	public Role findById(Long id);

	public Role findByRoleName(String roleName);

	public List<Role> findUserRole(Long id);

	public List<UserRoleGeneral> findUserRoleGeneral(String roleName);

	public void updateRoles(List<Long> roleList, Long id);
}