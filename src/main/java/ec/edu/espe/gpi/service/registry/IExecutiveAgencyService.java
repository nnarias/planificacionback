package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.InstitutionInvolved;

public interface IExecutiveAgencyService {

	public List<InstitutionInvolved> findAll();

	public InstitutionInvolved findById(Long id);

	public InstitutionInvolved save(InstitutionInvolved executiveAgency);

	public void delete(Long id);
}
