package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICallRequirementDao;
import ec.edu.espe.gpi.dao.registry.IRequirementDao;
import ec.edu.espe.gpi.dto.registry.RequirementDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.Requirement;

@Service
public class RequirementService implements IRequirementService {

	@Autowired
	private ICallRequirementDao callRequirementDao;

	@Autowired
	private IRequirementDao requirementDao;

	@Override
	@Transactional(readOnly = true)
	public Requirement find(Long id) {
		return requirementDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Requirement.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Requirement> findAll() {
		return requirementDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Requirement> findByCall(Long idCall) {
		return callRequirementDao.findByCall(idCall).orElse(new ArrayList<>()).stream().map(temp -> {
			Requirement requirement = new Requirement();
			requirement = temp.getRequirement();
			return requirement;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public void save(RequirementDTO requirementDTO) {
		Requirement requirement = new Requirement();
		try {
			Long id = requirementDTO.getId();
			if (id != null) {
				requirement = find(id);
				if (requirementDao.findByNameNotPresent(id, requirementDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Requirement.class, "name", requirementDTO.getName());
				}
			} else {
				Optional<Requirement> deletedRequirement = requirementDao.findByName(requirementDTO.getName());
				if (deletedRequirement.isPresent()) {
					requirement = deletedRequirement.get();
					if (Boolean.TRUE.equals(requirement.getState())) {
						throw new IntegrityViolationException(Requirement.class, "name", requirementDTO.getName());
					}
				}
				requirement.setState(true);
			}
			requirement.setName(requirementDTO.getName());
			requirementDao.save(requirement);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al guardar el requisito",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Requirement requirement = find(id);
			requirement.setState(false);
			requirementDao.save(requirement);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al actualizar el requisito",
					e.getMostSpecificCause().getMessage());
		}
	}
}