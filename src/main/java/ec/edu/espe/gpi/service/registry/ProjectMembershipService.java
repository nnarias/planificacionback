package ec.edu.espe.gpi.service.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectMembershipDao;
import ec.edu.espe.gpi.model.registry.ProjectMembership;

@Service
public class ProjectMembershipService implements IProjectMembershipService {

	@Autowired
	private IProjectMembershipDao projectMembershipDao;

	@Override
	@Transactional
	public void delete(Long id) {
		projectMembershipDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectMembership> findAll() {
		return projectMembershipDao.findAllEnable();
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectMembership findById(Long id) {
		return projectMembershipDao.findByIdEnable(id);
	}

	@Override
	@Transactional
	public ProjectMembership save(ProjectMembership projectMembership) {
		return projectMembershipDao.save(projectMembership);
	}
}