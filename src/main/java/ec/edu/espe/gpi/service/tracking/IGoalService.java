package ec.edu.espe.gpi.service.tracking;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.tracking.ProjectGoal;

public interface IGoalService {

	public ProjectGoal find(Long id);

	public List<ProjectGoal> findAll();

	public List<ProjectGoal> findByProject(Long userId);

	public void save(ProjectDTO projectDTO);

	public void updateState(Long id);
}