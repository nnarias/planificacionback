package ec.edu.espe.gpi.service.otheractivities;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IOtherActivitiesService {
	public List<OtherActivities> findAll();
	
	public List<OtherActivities> findAllByPlanning(Planning planning);
	
	public OtherActivities findById(Long id);
	
	public void save(OtherActivitiesDTO otherActivitiesDTO);
	
	public void updateState(Long id);
	
	public void updateComplete(Long id);
	
	Optional<OtherActivities> findByIdStateFalse(Long id);
}
