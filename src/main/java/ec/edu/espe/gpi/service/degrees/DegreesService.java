package ec.edu.espe.gpi.service.degrees;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.degrees.IDegreesDao;
import ec.edu.espe.gpi.dto.degress.DegreesDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.degress.Degrees;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class DegreesService implements IDegreesService{
	@Autowired
	private IDegreesDao degreesDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Degrees> findAll() {
		return degreesDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Degrees findById(Long id) {
		return degreesDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Activities.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Degrees> findByIdStateFalse(Long id) {
		return degreesDao.findById(id);
	}

	@Override
	@Transactional
	public void save(DegreesDTO degreesDTO) {
		Degrees degrees = new Degrees();
		try {
			Long id = degreesDTO.getId();
			if (id != null) {
				//modificar
				degrees = findById(id);
				degrees.setName(degreesDTO.getName());
				
			} else {
				//crear
				degrees.setName(degreesDTO.getName());
				degrees.setRemoved(false);
			}
			degreesDao.save(degrees);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar grado", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Degrees degrees = findById(id);
			degrees.setRemoved(true);
			degreesDao.save(degrees);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar el grado", e.getMessage());
		}

	}
}
