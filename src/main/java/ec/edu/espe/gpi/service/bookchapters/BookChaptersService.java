 package ec.edu.espe.gpi.service.bookchapters;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.bookchapters.IBookChaptersDao;
import ec.edu.espe.gpi.dao.planning.IBookDao;
import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class BookChaptersService implements IBookChaptersService{
	
	@Autowired
	private IBookChaptersDao bookChaptersDao;

	@Override
	@Transactional(readOnly = true)
	public List<BookChapters> findAll() {
		return bookChaptersDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<BookChapters> findAllByPlanning(Planning planning) {
		return bookChaptersDao.findAllByPlanningID(planning).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public BookChapters findById(Long id) {
		return bookChaptersDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(BookChapters.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<BookChapters> findByIdStateFalse(Long id) {
		return bookChaptersDao.findById(id);
	}

	@Override
	@Transactional
	public void save(BookChaptersDTO bookChaptersDTO) {
		BookChapters bookChapters = new BookChapters();
		boolean flag=false;
		try {
			Long id = bookChaptersDTO.getId();
			if (id != null) {
				//modificar
				bookChapters = findById(id);
				bookChapters.setCodIES(bookChaptersDTO.getCodIES());
				bookChapters.setPublicationType(bookChaptersDTO.getPublicationType());
				bookChapters.setChapterCode(bookChaptersDTO.getChapterCode());
				bookChapters.setCodPUB(bookChaptersDTO.getCodPUB());
				bookChapters.setCapTitle(bookChaptersDTO.getCapTitle());
				bookChapters.setBookTitle(bookChaptersDTO.getBookTitle());
				bookChapters.setCodISB(bookChaptersDTO.getCodISB());
				bookChapters.setEditor(bookChaptersDTO.getEditor());
				bookChapters.setNumberPages(bookChaptersDTO.getNumberPages());
				bookChapters.setPublicationDate(bookChaptersDTO.getPublicationDate());
				bookChapters.setDetailField(bookChaptersDTO.getDetailField());
				bookChapters.setFiliation(bookChaptersDTO.getFiliation());
				bookChapters.setCompetitor(bookChaptersDTO.getCompetitor());
				bookChapters.setDuration(bookChaptersDTO.getDuration());
				bookChapters.setObservations(bookChaptersDTO.getObservations());
				bookChapters.setAprobation(bookChaptersDTO.getAprobation());
				
				//evaluar si esta completo
				if(!(bookChapters.getId()==null) 
						&& !(bookChapters.getPlanning()==null)
						&& !(bookChapters.getCodIES()==null)
						&& !(bookChapters.getPublicationType()==null)
						&& !(bookChapters.getChapterCode()==null)
						&& !(bookChapters.getCodPUB()==null)
				        && !(bookChapters.getCapTitle()==null)
				        && !(bookChapters.getBookTitle()==null)
				        && !(bookChapters.getCodISB()==null)
				        && !(bookChapters.getEditor()==null)
				        && !(bookChapters.getNumberPages()==null)
				        && !(bookChapters.getPublicationDate()==null)
				        && !(bookChapters.getDetailField()==null)
						&& !(bookChapters.getFiliation()==null)
						&& !(bookChapters.getCompetitor()==null)
				        && !(bookChapters.getDuration()==null)) {
					flag = true;
				}	
				bookChapters.setComplete(flag);
				
			} else {
				//crear
				bookChapters.setPlanning(bookChaptersDTO.getPlanning());
				bookChapters.setCodIES(bookChaptersDTO.getCodIES());
				bookChapters.setPublicationType(bookChaptersDTO.getPublicationType());
				bookChapters.setChapterCode(bookChaptersDTO.getChapterCode());
				bookChapters.setCodPUB(bookChaptersDTO.getCodPUB());
				bookChapters.setCapTitle(bookChaptersDTO.getCapTitle());
				bookChapters.setBookTitle(bookChaptersDTO.getBookTitle());
				bookChapters.setCodISB(bookChaptersDTO.getCodISB());
				bookChapters.setEditor(bookChaptersDTO.getEditor());
				bookChapters.setNumberPages(bookChaptersDTO.getNumberPages());
				bookChapters.setPublicationDate(bookChaptersDTO.getPublicationDate());
				bookChapters.setDetailField(bookChaptersDTO.getDetailField());
				bookChapters.setFiliation(bookChaptersDTO.getFiliation());
				bookChapters.setCompetitor(bookChaptersDTO.getCompetitor());
				bookChapters.setDuration(bookChaptersDTO.getDuration());
				bookChapters.setObservations(bookChaptersDTO.getObservations());
				bookChapters.setAprobation(bookChaptersDTO.getAprobation());
				bookChapters.setComplete(false);
				bookChapters.setRemoved(false);
			}
			bookChaptersDao.save(bookChapters);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar el Cap�tulo del libro", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			BookChapters bookChapters = findById(id);
			bookChapters.setRemoved(true);
			bookChaptersDao.save(bookChapters);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar el Cap�tulo del libro", e.getMessage());
		}

	}
	
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			BookChapters bookChapters = findById(id);
			bookChapters.setComplete(true);
			bookChaptersDao.save(bookChapters);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar el Cap�tulo del libro", e.getMessage());
		}

	}

}
