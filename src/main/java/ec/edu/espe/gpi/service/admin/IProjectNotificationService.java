package ec.edu.espe.gpi.service.admin;

import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.dto.admin.ProjectNotificationDTO;
import ec.edu.espe.gpi.model.admin.ProjectNotification;

public interface IProjectNotificationService {

	public List<ProjectNotification> findByProject(Long projectId);

	public List<ProjectNotification> findByUser(Authentication authentication);

	public ProjectNotification save(ProjectNotificationDTO projectNotificationDTO);
}