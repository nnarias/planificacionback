package ec.edu.espe.gpi.service.admin;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.admin.IProjectNotificationDao;
import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.dto.admin.ProjectNotificationDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.admin.ProjectNotification;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.service.registry.IProjectService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.security.UserGeneral;

@Service
public class ProjectNotificationService implements IProjectNotificationService {

	@Autowired
	private IProjectNotificationDao projectNotificationDao;

	@Autowired
	private IUserService userService;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private INotificationService notificationService;

	@Override
	@Transactional(readOnly = true)
	public List<ProjectNotification> findByProject(Long projectId) {
		return projectNotificationDao.findByProject(projectId)
				.orElseThrow(() -> new NotFoundException(ProjectNotification.class, "id", projectId)).stream()
				.map(projectNotification -> {
					if (projectNotification.getProject() != null) {
						Project project = new Project();
						project.setCall(projectNotification.getProject().getCall());
						project.setId(projectNotification.getProject().getId());
						project.setNameEnglish(projectNotification.getProject().getNameEnglish());
						project.setNameSpanish(projectNotification.getProject().getNameSpanish());
						project.setState(projectNotification.getProject().getState());
						projectNotification.setProject(project);
					}
					return projectNotification;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectNotification> findByUser(Authentication authentication) {
		return projectNotificationDao.findByUser(authentication.getName())
				.orElseThrow(
						() -> new NotFoundException(ProjectNotification.class, "username", authentication.getName()))
				.stream().map(projectNotification -> {
					if (projectNotification.getProject() != null) {
						Project project = new Project();
						project.setCall(projectNotification.getProject().getCall());
						project.setId(projectNotification.getProject().getId());
						project.setNameEnglish(projectNotification.getProject().getNameEnglish());
						project.setNameSpanish(projectNotification.getProject().getNameSpanish());
						project.setState(projectNotification.getProject().getState());
						projectNotification.setProject(project);
					}
					return projectNotification;
				}).collect(Collectors.toList());
	}

	@Override
	@Transactional
	public ProjectNotification save(ProjectNotificationDTO projectNotificationDTO) {
		try {
			Date currentDate = new Date();
			Project project = projectService.find(projectNotificationDTO.getProjectId());
			ProjectNotification projectNotification = new ProjectNotification();
			UserGeneral userGeneral = userService.findUserGeneralByUsername(projectNotificationDTO.getUser());
			NotificationDTO notificationDTO = new NotificationDTO();
			projectNotification.setCommentary(projectNotificationDTO.getComment());
			projectNotification.setCreationDate(currentDate);
			projectNotification.setProject(project);
			projectNotification.setTitle(projectNotificationDTO.getTitle());
			projectNotification.setUser(userGeneral.getUsername());
			projectNotification = projectNotificationDao.save(projectNotification);

			notificationDTO.setTitle(projectNotificationDTO.getTitle());
			notificationDTO.setDescription("Nombre del proyecto: " + project.getNameSpanish() + " Descripción: "
					+ projectNotificationDTO.getComment());
			notificationDTO.setUser(project.getProjectDirector());
			notificationService.save(notificationDTO);

			return projectNotification;
		} catch (DataAccessException e) {
			throw new DataException(ProjectNotification.class,
					"Error al guardar la aprobación del proyecto de evaluación", e.getMostSpecificCause().getMessage());
		}
	}
}