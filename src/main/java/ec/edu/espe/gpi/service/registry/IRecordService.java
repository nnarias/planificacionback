package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.Record;

public interface IRecordService {

	public void delete(Long id);

	public List<Record> findAll();

	public Record findById(Long id);

	public Record save(Record rec);
}