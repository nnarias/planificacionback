package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICallAreaDao;
import ec.edu.espe.gpi.dto.registry.CallAreaDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.CallArea;

@Service
public class CallAreaService implements ICallAreaService {

	@Autowired
	private ICallAreaDao callAreaDao;

	@Override
	@Transactional(readOnly = true)
	public List<CallArea> findAll() {
		return callAreaDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public CallArea findById(Long id) {
		return callAreaDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(CallArea.class, "id", id));
	}

	@Override
	@Transactional
	public void save(CallAreaDTO callAreaDTO) {
		CallArea callArea = new CallArea();
		try {
			Long id = callAreaDTO.getId();
			if (id != null) {
				callArea = findById(id);
				if (callAreaDao.findByNameNotPresent(id, callAreaDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(CallArea.class, "name", callAreaDTO.getName());
				}
			} else {
				Optional<CallArea> deletedCallArea = callAreaDao.findByName(callAreaDTO.getName());
				if (deletedCallArea.isPresent()) {
					callArea = deletedCallArea.get();
					if (Boolean.TRUE.equals(callArea.getState())) {
						throw new IntegrityViolationException(CallArea.class, "name", callAreaDTO.getName());
					}
				}
				callArea.setState(true);
			}
			callArea.setName(callAreaDTO.getName());
			callAreaDao.save(callArea);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al guardar el �rea de convocatoria",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			CallArea callArea = findById(id);
			callArea.setState(false);
			callAreaDao.save(callArea);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al eliminar el �rea de convocatoria",
					e.getMostSpecificCause().getMessage());
		}
	}
}