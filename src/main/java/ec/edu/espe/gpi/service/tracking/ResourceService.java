package ec.edu.espe.gpi.service.tracking;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.tracking.IResourceDao;
import ec.edu.espe.gpi.dto.tracking.ResourceDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.tracking.Resource;

@Service
public class ResourceService implements IResourceService {
	@Autowired
	private IResourceDao resourceDao;

	@Override
	@Transactional(readOnly = true)
	public List<Resource> findAll() {
		return resourceDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Resource findById(Long id) {
		return resourceDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Resource.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ResourceDTO resourceDTO) {
		Resource resource = new Resource();
		try {
			Long id = resourceDTO.getId();
			if (id != null) {
				resource = findById(id);
				if (resourceDao.findByNameNotPresent(id, resourceDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Resource.class, "name", resourceDTO.getName());
				}
			} else {
				if (resourceDao.findByName(resourceDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Resource.class, "name", resourceDTO.getName());
				}
				resource.setState(true);
			}
			resource.setName(resourceDTO.getName());
			resourceDao.save(resource);
		} catch (DataAccessException e) {
			throw new DataException(Resource.class, "Error al guardar recurso", e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Resource resource = findById(id);
			resource.setState(false);
			resourceDao.save(resource);
		} catch (DataAccessException e) {
			throw new DataException(Resource.class, "Error al eliminar recurso", e.getMostSpecificCause().getMessage());
		}
	}
}