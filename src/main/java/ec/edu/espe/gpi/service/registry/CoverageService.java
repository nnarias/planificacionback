package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICoverageDao;
import ec.edu.espe.gpi.dto.registry.CoverageDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.CallArea;
import ec.edu.espe.gpi.model.registry.Coverage;
import ec.edu.espe.gpi.model.registry.CoverageType;

@Service
public class CoverageService implements ICoverageService {

	@Autowired
	private ICoverageDao coverageDao;

	@Autowired
	private ICoverageTypeService coverageTypeService;

	@Override
	@Transactional(readOnly = true)
	public List<Coverage> findAll() {
		return coverageDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Coverage findById(Long id) {
		return coverageDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Coverage.class, "id", id));
	}

	@Override
	@Transactional
	public void save(CoverageDTO coverageDTO) {
		Coverage coverage = new Coverage();
		try {
			Long id = coverageDTO.getId();
			if (id != null) {
				coverage = findById(id);
				if (coverageDao.findByNameNotPresent(id, coverageDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Coverage.class, "name", coverageDTO.getName());
				}
			} else {
				if (coverageDao.findByName(coverageDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Coverage.class, "name", coverageDTO.getName());
				}
				coverage.setState(true);
			}
			CoverageType coverageType = coverageTypeService.findById(coverageDTO.getCoverageTypeId());
			coverage.setName(coverageDTO.getName());
			coverage.setCoverageType(coverageType);
			coverageDao.save(coverage);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al guardar la cobertura",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Coverage coverage = findById(id);
			coverage.setState(false);
			coverageDao.save(coverage);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al eliminar la cobertura",
					e.getMostSpecificCause().getMessage());
		}
	}
}