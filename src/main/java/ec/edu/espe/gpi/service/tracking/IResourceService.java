package ec.edu.espe.gpi.service.tracking;

import java.util.List;

import ec.edu.espe.gpi.dto.tracking.ResourceDTO;
import ec.edu.espe.gpi.model.tracking.Resource;

public interface IResourceService {

	public List<Resource> findAll();

	public Resource findById(Long id);

	public void save(ResourceDTO resourceDTO);

	public void updateState(Long id);
}