package ec.edu.espe.gpi.service.otheractivities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.dao.otheractivities.IOtherActivitiesDao;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class OtherActivitiesService implements IOtherActivitiesService {
	@Autowired
	private IOtherActivitiesDao otherActivitiesDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<OtherActivities> findAll() {
		return otherActivitiesDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<OtherActivities> findAllByPlanning(Planning planning) {
		return otherActivitiesDao.findAllByPlanningID(planning).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public OtherActivities findById(Long id) {
		return otherActivitiesDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(OtherActivities.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<OtherActivities> findByIdStateFalse(Long id) {
		return otherActivitiesDao.findById(id);
	}

	@Override
	@Transactional
	public void save(OtherActivitiesDTO otherActivitiesDTO) {
		OtherActivities otherActivities = new OtherActivities();
		boolean flag=false;
		try {
			Long id = otherActivitiesDTO.getId();
			if (id != null) {
				//modificar
				otherActivities = findById(id);
				otherActivities.setName(otherActivitiesDTO.getName());
				otherActivities.setDuration(otherActivitiesDTO.getDuration());
				otherActivities.setLink(otherActivitiesDTO.getLink());
				otherActivities.setAprobation(otherActivitiesDTO.getAprobation());
				otherActivities.setObservations(otherActivitiesDTO.getObservations());
				//evaluar si esta completo
				if(!(otherActivities.getId()==null) 
						&& !(otherActivities.getPlanning()==null)
						&& !(otherActivities.getName()==null)
						&& !(otherActivities.getDuration()==null)
						&& !(otherActivities.getLink()==null)) {
					flag = true;
				}	
				otherActivities.setComplete(flag);
				
			} else {
				//crear
				otherActivities.setPlanning(otherActivitiesDTO.getPlanning());
				otherActivities.setName(otherActivitiesDTO.getName());
				otherActivities.setDuration(otherActivitiesDTO.getDuration());
				otherActivities.setLink(otherActivitiesDTO.getLink());
				otherActivities.setAprobation(otherActivitiesDTO.getAprobation());
				otherActivities.setObservations(otherActivitiesDTO.getObservations());
				otherActivities.setComplete(false);
				otherActivities.setRemoved(false);
			}
			otherActivitiesDao.save(otherActivities);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar la Actividad", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			OtherActivities otherActivities = findById(id);
			otherActivities.setRemoved(true);
			otherActivitiesDao.save(otherActivities);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar la Actividad", e.getMessage());
		}

	}
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			OtherActivities otherActivities = findById(id);
			otherActivities.setComplete(true);
			otherActivitiesDao.save(otherActivities);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar la Actividad", e.getMessage());
		}

	}


}
