package ec.edu.espe.gpi.service.registry;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IRecordDao;
import ec.edu.espe.gpi.model.registry.Record;

@Service
public class RecordService implements IRecordService {

	@Autowired
	private IRecordDao recordDao;

	@Override
	@Transactional
	public void delete(Long id) {
		recordDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public List<Record> findAll() {
		return recordDao.findAllEnable();
	}

	@Override
	@Transactional(readOnly = true)
	public Record findById(Long id) {
		return recordDao.findByIdEnable(id);
	}

	@Override
	@Transactional
	public Record save(Record rec) {
		return recordDao.save(rec);
	}
}