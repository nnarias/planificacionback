package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IPolicyPlanDao;
import ec.edu.espe.gpi.dto.registry.PolicyPlanDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.GoalPlan;
import ec.edu.espe.gpi.model.registry.PolicyPlan;

@Service
public class PolicyPlanService implements IPolicyPlanService {

	@Autowired
	private IGoalPlanService goalPlanService;

	@Autowired
	private IPolicyPlanDao policyPlanDao;

	@Override
	@Transactional(readOnly = true)
	public List<PolicyPlan> findAll() {
		return policyPlanDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public PolicyPlan findById(Long id) {
		return policyPlanDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(PolicyPlan.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<PolicyPlan> findByGoalPlan(Long goalId) {
		return policyPlanDao.findByGoalPlan(goalId)
				.orElseThrow(() -> new NotFoundException(GoalPlan.class, "id", goalId));
	}

	@Override
	@Transactional
	public void save(PolicyPlanDTO policyPlanDTO) {
		PolicyPlan policyPlan = new PolicyPlan();
		try {
			Long id = policyPlanDTO.getId();
			if (id != null) {
				policyPlan = findById(id);
			} else {
				policyPlan.setState(true);
			}
			GoalPlan goalPlan = goalPlanService.findById(policyPlanDTO.getGoalPlanId());
			policyPlan.setGoalPlan(goalPlan);
			policyPlan.setName(policyPlanDTO.getName());
			policyPlanDao.save(policyPlan);
		} catch (DataAccessException e) {
			throw new DataException(PolicyPlan.class, "Error al guardar pol�tica del objetivo del plan de desarrollo",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			PolicyPlan policyPlan = findById(id);
			policyPlan.setState(false);
			policyPlanDao.save(policyPlan);
		} catch (DataAccessException e) {
			throw new DataException(PolicyPlan.class, "Error al eliminar pol�tica del objetivo del plan de desarrollo",
					e.getMostSpecificCause().getMessage());
		}
	}
}