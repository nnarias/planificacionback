package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IInstitutionInvolvedDao;
import ec.edu.espe.gpi.dto.registry.InstitutionInvolvedDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.InstitutionInvolved;

@Service
public class InstitutionInvolvedService implements IInstitutionInvolvedService {

	@Autowired
	private IInstitutionInvolvedDao institutionInvolvedDao;

	@Override
	@Transactional(readOnly = true)
	public List<InstitutionInvolved> findAll() {
		return institutionInvolvedDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public InstitutionInvolved findById(Long id) {
		return institutionInvolvedDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(InstitutionInvolved.class, "id", id));
	}

	@Override
	@Transactional
	public void save(InstitutionInvolvedDTO institutionInvolvedDTO) {
		InstitutionInvolved institutionInvolved = new InstitutionInvolved();
		try {
			Long id = institutionInvolvedDTO.getId();
			if (id != null) {
				institutionInvolved = findById(id);
				if (institutionInvolvedDao.findByNameNotPresent(id, institutionInvolvedDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(InstitutionInvolved.class, "name",
							institutionInvolvedDTO.getName());
				}
			} else {
				if (institutionInvolvedDao.findByName(institutionInvolvedDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(InstitutionInvolved.class, "name",
							institutionInvolvedDTO.getName());
				}
				institutionInvolved.setState(true);
			}
			institutionInvolved.setAddress(institutionInvolvedDTO.getAddress());
			institutionInvolved.setEmail(institutionInvolvedDTO.getEmail());
			institutionInvolved.setExecutingAgency(institutionInvolvedDTO.getExecutingAgency());
			institutionInvolved.setFax(institutionInvolvedDTO.getFax());
			institutionInvolved.setLegalRepresentative(institutionInvolvedDTO.getLegalRepresentative());
			institutionInvolved.setLegalRepresentativeId(institutionInvolvedDTO.getLegalRepresentativeId());
			institutionInvolved.setName(institutionInvolvedDTO.getName());
			institutionInvolved.setPhone(institutionInvolvedDTO.getPhone());
			institutionInvolved.setUrl(institutionInvolvedDTO.getUrl());
			institutionInvolvedDao.save(institutionInvolved);
		} catch (DataAccessException e) {
			throw new DataException(InstitutionInvolved.class, "Error al guardar la institución involucrada",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			InstitutionInvolved institutionInvolved = findById(id);
			institutionInvolved.setState(false);
			institutionInvolvedDao.save(institutionInvolved);
		} catch (DataAccessException e) {
			throw new DataException(InstitutionInvolved.class, "Error al eliminar la institución involucrada",
					e.getMostSpecificCause().getMessage());
		}
	}
}