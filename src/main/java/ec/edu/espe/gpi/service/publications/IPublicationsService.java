package ec.edu.espe.gpi.service.publications;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;

public interface IPublicationsService {
	
	public List<Publications> findAll();
	
	public List<Publications> findAllByPlanning(Planning planning);
	
	public Publications findById(Long id);
	
	public void save(PublicationsDTO PublicationsDTO);
	
	public void updateState(Long id);
	
	public void updateComplete(Long id);
	
	Optional<Publications> findByIdStateFalse(Long id);
}
