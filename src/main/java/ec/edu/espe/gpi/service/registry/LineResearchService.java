package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ILineResearchDao;
import ec.edu.espe.gpi.dto.registry.LineResearchDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.LineResearch;

@Service
public class LineResearchService implements ILineResearchService {

	@Autowired
	private ILineResearchDao lineResearchDao;

	@Override
	@Transactional(readOnly = true)
	public List<LineResearch> findAll() {
		return lineResearchDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public LineResearch findById(Long id) {
		return lineResearchDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(LineResearch.class, "id", id));
	}

	@Override
	@Transactional
	public void save(LineResearchDTO lineResearchDTO) {
		LineResearch lineResearch = new LineResearch();
		try {
			Long id = lineResearchDTO.getId();
			if (id != null) {
				lineResearch = findById(id);
				if (lineResearchDao.findByNameNotPresent(id, lineResearchDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(LineResearch.class, "name", lineResearchDTO.getName());
				}
			} else {
				if (lineResearchDao.findByName(lineResearchDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(LineResearch.class, "name", lineResearchDTO.getName());
				}
				lineResearch.setState(true);
			}
			lineResearch.setName(lineResearchDTO.getName());
			lineResearchDao.save(lineResearch);
		} catch (DataAccessException e) {
			throw new DataException(LineResearch.class, "Error al guardar la línea de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			LineResearch lineResearch = findById(id);
			lineResearch.setState(false);
			lineResearchDao.save(lineResearch);
		} catch (DataAccessException e) {
			throw new DataException(LineResearch.class, "Error al guardar la línea de investigación",
					e.getMostSpecificCause().getMessage());
		}
	}
}