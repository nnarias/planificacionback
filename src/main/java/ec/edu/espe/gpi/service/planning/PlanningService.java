package ec.edu.espe.gpi.service.planning;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.bookchapters.IBookChaptersDao;
import ec.edu.espe.gpi.dao.books.IBooksDao;
import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.dao.otheractivities.IOtherActivitiesDao;
import ec.edu.espe.gpi.dao.planning.IPlanningDao;
import ec.edu.espe.gpi.dao.publications.IPublicationsDao;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.service.bookchapters.BookChaptersService;
import ec.edu.espe.gpi.service.books.BooksService;
import ec.edu.espe.gpi.service.congress.CongressService;
import ec.edu.espe.gpi.service.otheractivities.OtherActivitiesService;
import ec.edu.espe.gpi.service.publications.PublicationService;

@Service
public class PlanningService implements IPlanningService {
	@Autowired
	private IPlanningDao planningDao;
	
	@Autowired
	private IOtherActivitiesDao otherActivitiesDao;
	
	@Autowired
	private IBooksDao booksDao;
	
	@Autowired
	private IBookChaptersDao bookChaptersDao;
	
	@Autowired
	private ICongressDao congressDao;
	
	@Autowired
	private IPublicationsDao publicationsDao;
	
	@Autowired
	private OtherActivitiesService otherActivitiesService;
	
	@Autowired
	private BooksService booksService;
	
	@Autowired
	private BookChaptersService bookChaptersService;
	
	@Autowired
	private CongressService congressService;
	
	@Autowired
	private PublicationService publicationsService;
	
	@Override
	@Transactional(readOnly = true)
	public List<Planning> findAll() {
		
		List<Planning> planning = planningDao.findAllEnable().orElse(new ArrayList<>());
		
		for(int j=0;j<planning.size();j++) {
			List<Activities> activities= new ArrayList<Activities>();
			int countFalse=0;
			activities=findAllActivitiesbyPlanning(planning.get(j).getId());
			for(int i=0; i<activities.size();i++) {
				if(!activities.get(i).getComplete()) {
					countFalse++;
				}
			}
			if(countFalse==0)
				planning.get(j).setComplete(true);
			else
				planning.get(j).setComplete(false);
		}
		return planning;
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Planning> findAllIR() {
		return planningDao.findAllIR().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Planning findById(Long id) {
		return planningDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Planning.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Planning> findByIdStateFalse(Long id) {
		return planningDao.findById(id);
	}

	@Override
	@Transactional
	public void save(PlanningDTO planningDTO) {
		Planning planning = new Planning();
		
		try {
			Long id = planningDTO.getId();
			if (id != null) {
				//modificar
				planning = findById(id);
				planning.setMaxDuration(planningDTO.getMaxDuration());
				planning.setAprobation(planningDTO.getAprobation());
				planning.setResponsibleId(planningDTO.getResponsibleId());
				planning.setRegistrationDate(planningDTO.getRegistrationDate());
				planning.setEvaluatorId(planningDTO.getEvaluatorId());
				planning.setObservations(planningDTO.getObservations());
				planning.setDegree(planningDTO.getDegree());		
				List<Activities> activities= new ArrayList<Activities>();
				int countFalse=0;
				activities=findAllActivitiesbyPlanning(id);
				for(int i=0; i<activities.size();i++) {
					if(!activities.get(i).getComplete()) {
						countFalse++;
					}
				}
				if(countFalse==0)
					planning.setComplete(true);
				else
					planning.setComplete(false);
				
			} else {
				//crear
				planning.setUsers(planningDTO.getUsers());
				planning.setMaxDuration(planningDTO.getMaxDuration());
				planning.setAprobation(planningDTO.getAprobation());
				planning.setResponsibleId(planningDTO.getResponsibleId());
				planning.setRegistrationDate(planningDTO.getRegistrationDate());
				planning.setEvaluatorId(planningDTO.getEvaluatorId());
				planning.setObservations(planningDTO.getObservations());
				planning.setDegree(planningDTO.getDegree());
				planning.setComplete(false);
				planning.setRemoved(false);
			}
			planningDao.save(planning);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar la Planificaciíon", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Planning planning = findById(id);
			deleteOtherActivities(planning);
			deleteBookChapters(planning);
			deleteBooks(planning);
			deletePublications(planning);
			deleteCongress(planning);
			planning.setRemoved(true);
			planningDao.save(planning);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar la Planificación", e.getMessage());
		}

	}
	
	public void deleteOtherActivities(Planning planning) {
		List<OtherActivities> otherActivities = new ArrayList<OtherActivities>();
		otherActivities = otherActivitiesService.findAllByPlanning(planning);
		for(int i=0; i<otherActivities.size(); i++) {
			otherActivities.get(i).setRemoved(true);
			otherActivitiesDao.save(otherActivities.get(i));
		}	
	}
	
	public void deletePublications(Planning planning) {
		List<Publications> publications = new ArrayList<Publications>();
		publications = publicationsService.findAllByPlanning(planning);
		for(int i=0; i<publications.size(); i++) {
			publications.get(i).setRemoved(true);
			publicationsDao.save(publications.get(i));
		}	
	}
	
	public void deleteBooks(Planning planning) {
		List<Books> books = new ArrayList<Books>();
		books = booksService.findAllByPlanning(planning);
		for(int i=0; i<books.size(); i++) {
			books.get(i).setRemoved(true);
			booksDao.save(books.get(i));
		}	
	}
	
	public void deleteCongress(Planning planning) {
		List<Congress> congress = new ArrayList<Congress>();
		congress = congressService.findAllByPlanning(planning);
		for(int i=0; i<congress.size(); i++) {
			congress.get(i).setRemoved(true);
			congressDao.save(congress.get(i));
		}	
	}
	
	public void deleteBookChapters(Planning planning) {
		List<BookChapters> bookChapters = new ArrayList<BookChapters>();
		bookChapters = bookChaptersService.findAllByPlanning(planning);
		for(int i=0; i<bookChapters.size(); i++) {
			bookChapters.get(i).setRemoved(true);
			bookChaptersDao.save(bookChapters.get(i));
		}	
	}
	
	@Override
	@Transactional
	public List<Activities> findAllActivitiesbyPlanning(Long id){
		Planning planning = findById(id);
		List<Activities> actividades = new ArrayList<Activities>();
		List<BookChapters> bookChapters = new ArrayList<BookChapters>();
		List<Congress> congress = new ArrayList<Congress>();
		List<Books> books = new ArrayList<Books>();
		List<Publications> publications = new ArrayList<Publications>();
		List<OtherActivities> otherActivities = new ArrayList<OtherActivities>();
		otherActivities = otherActivitiesService.findAllByPlanning(planning);
		publications = publicationsService.findAllByPlanning(planning);
		books = booksService.findAllByPlanning(planning);
		congress = congressService.findAllByPlanning(planning);
		bookChapters = bookChaptersService.findAllByPlanning(planning);
		
		for(int i=0; i<otherActivities.size();i++) {
			Activities activity = new Activities();
			activity.setName(otherActivities.get(i).getName());
			activity.setDuration(otherActivities.get(i).getDuration());
			activity.setId(otherActivities.get(i).getId());
			activity.setComplete(otherActivities.get(i).getComplete());
			activity.setAprobation(otherActivities.get(i).getAprobation());
			actividades.add(activity);
		}
		
		for(int i=0; i<publications.size();i++) {
			Activities activity = new Activities();
			activity.setName("Publicaciones científicas");
			activity.setDuration(publications.get(i).getDuration());
			activity.setId(publications.get(i).getId());
			activity.setComplete(publications.get(i).getComplete());
			activity.setAprobation(publications.get(i).getAprobation());
			actividades.add(activity);
		}
		
		for(int i=0; i<books.size();i++) {
			Activities activity = new Activities();
			activity.setName("Libros");
			activity.setDuration(books.get(i).getDuration());
			activity.setId(books.get(i).getId());
			activity.setComplete(books.get(i).getComplete());
			activity.setAprobation(books.get(i).getAprobation());
			actividades.add(activity);
		}
		
		for(int i=0; i<congress.size();i++) {
			Activities activity = new Activities();
			activity.setName("Congresos");
			activity.setDuration(congress.get(i).getDuration());
			activity.setId(congress.get(i).getId());
			activity.setComplete(congress.get(i).getComplete());
			activity.setAprobation(congress.get(i).getAprobation());
			actividades.add(activity);
		}
		
		for(int i=0; i<bookChapters.size();i++) {
			Activities activity = new Activities();
			activity.setName("Capítulo de Libro");
			activity.setDuration(bookChapters.get(i).getDuration());
			activity.setId(bookChapters.get(i).getId());
			activity.setComplete(bookChapters.get(i).getComplete());
			activity.setAprobation(bookChapters.get(i).getAprobation());
			actividades.add(activity);
		}
		
		return actividades;	
	}
	
	@Override
	@Transactional
	public void updateAprobation(Long id) {
		try {
			Planning planning = findById(id);
			planning.setAprobation(true);
			planningDao.save(planning);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al aprobar la Planificación", e.getMessage());
		}

	}
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			Planning planning = findById(id);
			planning.setComplete(true);
			planningDao.save(planning);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar la Planificación", e.getMessage());
		}

	}
}
