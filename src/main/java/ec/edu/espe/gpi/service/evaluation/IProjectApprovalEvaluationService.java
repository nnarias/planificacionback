package ec.edu.espe.gpi.service.evaluation;

import org.springframework.security.core.Authentication;
import ec.edu.espe.gpi.dto.evaluation.ProjectApprovalEvaluationDTO;


public interface IProjectApprovalEvaluationService {
	public void projectApproval(ProjectApprovalEvaluationDTO projectApprovalEvaluationDTO,
			Authentication authentication);
}