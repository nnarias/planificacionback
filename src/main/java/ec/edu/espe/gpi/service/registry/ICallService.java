package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CallDTO;
import ec.edu.espe.gpi.dto.registry.CallExtensionDTO;
import ec.edu.espe.gpi.model.registry.Call;

public interface ICallService {

	public void extensionDate(CallExtensionDTO callExtensionDTO, Long id);

	public List<Call> findActive();

	public List<Call> findAll();

	public List<Call> findAllByEvaluationActive();

	public Call findByEvaluationActive(Long id);
	
	public Call findByActive(Long id);

	public Call find(Long id);

	public void save(CallDTO callDTO);

	public void updateState(Long id);
}