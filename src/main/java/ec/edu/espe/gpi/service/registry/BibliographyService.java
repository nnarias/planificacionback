package ec.edu.espe.gpi.service.registry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.registry.IBibliographyDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Bibliography;

@Service
public class BibliographyService implements IBibliographyService {

	@Autowired
	private IBibliographyDao bibliographyDao;

	@Override
	public Bibliography find(Long id) {
		return bibliographyDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(Bibliography.class, "id", id));
	}
}