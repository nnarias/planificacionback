package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.model.registry.Area;

public interface IAreaService {

	public List<Area> findAll();

	public Area findById(Long id);
}