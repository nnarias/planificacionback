package ec.edu.espe.gpi.service.registry;

import java.io.IOException;
import java.util.List;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.security.core.Authentication;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

public interface IProjectService {

	public Project find(Long id);

	public List<Project> findAll();

	public List<Project> findAllApproved(String projectDirector);

	public List<Project> findAllApproving(String projectDirector);

	public List<Project> findAllDeveloping(String projectDirector);

	public List<Project> findAllByDepartmentalEvaluator(Authentication authentication);

	public List<Project> findAllByEvaluatorAssigning(Long id);

	public List<Project> findAllByFinancialSupervisor(Authentication authentication);

	public List<Project> findAllByProjectDirector(Authentication authentication);

	public List<Project> findAllByProjectEvaluator(Authentication authentication);

	public List<Project> findAllByTechnicalSupervisor(Authentication authentication);

	public List<Project> findAllEvaluating();

	public List<Project> findAllRegistered();

	public Project findByDepartmentalEvaluator(Authentication authentication, Long id);

	public Project findByEvaluatorAssigning(Long id);

	public Project findByFinancialSupervisor(Authentication authentication, Long id);

	public Project findByProjectDepartmentalEvaluator(Authentication authentication, Long id);

	public Project findByProjectDirector(Authentication authentication, Long id);

	public Project findByProjectEvaluator(Authentication authentication, Long id);

	public Project findByTechnicalSupervisor(Authentication authentication, Long id);

	public Resource getFile(String name);

	public InputStreamResource getFileAnnexed(String fileUUID, TypeUserDocument typeUserDocument) throws IOException;

	public void save(List<MultipartFile> annextList, Authentication authentication, ProjectDTO projectDTO,
			List<MultipartFile> requirementList);

	public List<Document> sendFileListAnnexed(List<MultipartFile> fileList, String pathFile,
			TypeUserDocument typeUserDocument) throws IOException;

	public void updateState(Long id);
}