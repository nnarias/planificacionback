package ec.edu.espe.gpi.service.registry;

import java.util.List;
import ec.edu.espe.gpi.dto.registry.CollegeDepartmentDTO;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;

public interface ICollegeDepartmentService {

	public List<CollegeDepartment> findAll();

	public CollegeDepartment findById(Long id);

	public void save(CollegeDepartmentDTO collegeDepartmentDTO);
	
	public void updateState(Long id);
}