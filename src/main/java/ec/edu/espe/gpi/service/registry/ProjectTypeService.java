package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectTypeDao;
import ec.edu.espe.gpi.dto.registry.ProjectTypeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.ProjectType;

@Service
public class ProjectTypeService implements IProjectTypeService {

	@Autowired
	private IProjectTypeDao projectTypeDao;

	@Override
	@Transactional(readOnly = true)
	public List<ProjectType> findAll() {
		return projectTypeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectType findById(Long id) {
		return projectTypeDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(ProjectType.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ProjectTypeDTO projectTypeDTO) {
		ProjectType projectType = new ProjectType();
		try {
			Long id = projectTypeDTO.getId();
			if (id != null) {
				projectType = findById(id);
				if (projectTypeDao.findByNameNotPresent(id, projectTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectType.class, "name", projectTypeDTO.getName());
				}
			} else {
				if (projectTypeDao.findByName(projectTypeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectType.class, "name", projectTypeDTO.getName());
				}
				projectType.setState(true);
			}
			projectType.setName(projectTypeDTO.getName());
			projectTypeDao.save(projectType);
		} catch (DataAccessException e) {
			throw new DataException(ProjectType.class, "Error al guardar tipo de proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ProjectType projectType = findById(id);
			projectType.setState(false);
			projectTypeDao.save(projectType);
		} catch (DataAccessException e) {
			throw new DataException(ProjectType.class, "Error al eliminar tipo de proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}
}