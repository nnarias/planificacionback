package ec.edu.espe.gpi.service.admin;

import java.io.IOException;
import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dto.admin.NewsDTO;
import ec.edu.espe.gpi.model.admin.News;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

public interface INewsService {

	public void delete(Long id);

	public List<News> findAll();

	public News find(Long id);

	public void save(Document document, NewsDTO newsDTO);

	public Document sendImage(MultipartFile file, NewsDTO newsDTO, String pathFile, TypeUserDocument typeUserDocument)
			throws IOException;

	public List<News> findLoginNews();
}