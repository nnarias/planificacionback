package ec.edu.espe.gpi.service.admin;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.InputStreamResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.InOutException;
import ec.edu.espe.gpi.exception.NotAcceptable;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;
import ec.edu.espe.gpi.utils.admin.UserDocument;

@Service
public class ImpDocumentService implements IImpDocumentService {
	private static final String ADMIN_MAME = "app.config.openKM.user.name.projectAdmin";
	private static final String ADMIN_PASSWORD = "app.config.openKM.user.password.projectAdmin";
	private static final String DIRECTOR_MAME = "app.config.openKM.user.name.projectDirector";
	private static final String DIRECTOR_PASSWORD = "app.config.openKM.user.password.projectAdmin";
	private static final String PREFIX = "app.config.openKM.prefix";
	private static final Logger logger = Logger.getLogger(ImpDocumentService.class.getName());

	@Autowired
	private IDocumentService documentService;

	@Autowired
	private Environment env;

	@Override
	@Transactional
	public void createMultipleFolder(String pathFile, TypeUserDocument typeUserDocument) {
		String[] arrSplit = pathFile.split("/");
		StringBuilder builder = new StringBuilder();
		for (String path : arrSplit) {
			if (!path.equals("")) {
				builder.append("/" + path);
				if (!path.equals("okm:root")) {
					createSimpleFolder(builder.toString(), typeUserDocument);
				}
			}
		}
	}

	@Override
	@Transactional
	public Boolean createSimpleFolder(String pathFile, TypeUserDocument typeUserDocument) {

		UserDocument userDocument = new UserDocument();
		String urlCreateFolder = env.getProperty("app.config.openKM.URL.createSimple");
		String urlGetFolder = env.getProperty("app.config.openKM.URL.getFolder");
		switch (typeUserDocument) {
		case PROJECT_ADMIN:
			userDocument.setUserName(env.getProperty(ADMIN_MAME));
			userDocument.setUserPassword(env.getProperty(ADMIN_PASSWORD));
			break;

		case PROJECT_DIRECTOR:
			userDocument.setUserName(env.getProperty(DIRECTOR_MAME));
			userDocument.setUserPassword(env.getProperty(DIRECTOR_PASSWORD));
			break;
		default:
			break;
		}
		if (Boolean.TRUE.equals(documentService.getFolder(urlGetFolder, pathFile, userDocument))) {
			return false;
		} else {
			return documentService.createFolder(urlCreateFolder, pathFile, userDocument);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public InputStreamResource getDocument(String uuid, TypeUserDocument typeUserDocument) throws IOException {
		UserDocument userDocument = new UserDocument();
		String url = env.getProperty("app.config.openKM.URL.getDocument");
		switch (typeUserDocument) {
		case PROJECT_ADMIN:
			userDocument.setUserName(env.getProperty(ADMIN_MAME));
			userDocument.setUserPassword(env.getProperty(ADMIN_PASSWORD));
			break;

		case PROJECT_DIRECTOR:
			userDocument.setUserName(env.getProperty(DIRECTOR_MAME));
			userDocument.setUserPassword(env.getProperty(DIRECTOR_PASSWORD));
			break;

		default:
			break;
		}
		return documentService.getDocument(url, uuid, userDocument);
	}

	@Override
	@Transactional(readOnly = true)
	public String getFilenamePrefix(String name) {
		String[] filesName = name.split(env.getProperty(PREFIX));
		return filesName[1];
	}

	@Override
	@Transactional
	public Document sendDocument(MultipartFile file, String pathFile, TypeUserDocument typeUserDocument)
			throws InOutException, IOException {
		UserDocument userDocument = new UserDocument();
		String url = env.getProperty("app.config.openKM.URL.sendDocument");
		if (file == null || file.isEmpty()) {
			throw new NotAcceptable(Document.class, "Error en la operación", "Subida del archivo");
		}

		String fileName = saveFileName(file)
				.orElseThrow(() -> new NotAcceptable(Document.class, "Error en la operación", "Subida del archivo"));
		switch (typeUserDocument) {
		case PROJECT_ADMIN:
			userDocument.setUserName(env.getProperty(ADMIN_MAME));
			userDocument.setUserPassword(env.getProperty(ADMIN_PASSWORD));
			break;

		case PROJECT_DIRECTOR:
			userDocument.setUserName(env.getProperty(DIRECTOR_MAME));
			userDocument.setUserPassword(env.getProperty(DIRECTOR_PASSWORD));
			break;

		default:
			break;
		}
		Document document = documentService.sendDocument(url, fileName, pathFile, userDocument, file);
		document.setFileName(fileName);
		return document;
	}

	public Optional<String> saveFileName(MultipartFile file) {
		logger.log(Level.FINE, env.getProperty(PREFIX));
		String originalFileName = file.getOriginalFilename();
		if (originalFileName == null) {
			throw new DataException(String.class, "No se puede obtener el nombre del archivo");
		}
		String fileName = UUID.randomUUID().toString() + env.getProperty(PREFIX) + originalFileName.replace(" ", "_");
		return Optional.of(fileName);
	}
}