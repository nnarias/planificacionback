package ec.edu.espe.gpi.service.planning;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Book;


public interface IBookService {
	
	public List<Book> findAll();
	
	public Book findById(Long id);
	
	public void save(BookDTO bookDTO);
	
	public void updateState(Long id);
	
	Optional<Book> findByIdStateFalse(Long id);
}
