package ec.edu.espe.gpi.service.activities;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.activities.IActivitiesDao;
import ec.edu.espe.gpi.dto.activities.ActivitiesDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class ActivitiesService implements IActivitiesService {
	@Autowired
	private IActivitiesDao activitiesDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Activities> findAll() {
		return activitiesDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Activities> findByMaxDuration(Long maxDuration) {
		return activitiesDao.findByMaxDurationEnable(maxDuration).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Activities findById(Long id) {
		return activitiesDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Activities.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Activities> findByIdStateFalse(Long id) {
		return activitiesDao.findById(id);
	}

	@Override
	@Transactional
	public void save(ActivitiesDTO activitiesDTO) {
		Activities activities = new Activities();
		try {
			Long id = activitiesDTO.getId();
			if (id != null) {
				//modificar
				activities = findById(id);
				activities.setName(activitiesDTO.getName());
				activities.setDegree(activitiesDTO.getDegree());
				activities.setDuration(activitiesDTO.getDuration());
				activities.setMaxDuration(activitiesDTO.getMaxDuration());
				
			} else {
				//crear
				activities.setName(activitiesDTO.getName());
				activities.setDuration(activitiesDTO.getDuration());
				activities.setMaxDuration(activitiesDTO.getMaxDuration());
				activities.setDegree(activitiesDTO.getDegree());
				activities.setRemoved(false);
			}
			activitiesDao.save(activities);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar actividad", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Activities activities = findById(id);
			activities.setRemoved(true);
			activitiesDao.save(activities);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar actividad", e.getMessage());
		}

	}
}
