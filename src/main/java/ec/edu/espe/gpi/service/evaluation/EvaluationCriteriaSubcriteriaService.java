package ec.edu.espe.gpi.service.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.evaluation.IEvaluationCriteriaSubcriteriaDao;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.EvaluationCriteriaSubcriteria;

@Service
public class EvaluationCriteriaSubcriteriaService implements IEvaluationCriteriaSubcriteriaService {

	@Autowired
	private IEvaluationCriteriaSubcriteriaDao evaluationCriteriaSubcriteriaDao;

	@Override
	public EvaluationCriteriaSubcriteria findById(Long id) {
		return evaluationCriteriaSubcriteriaDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(EvaluationCriteriaSubcriteria.class, "id", id));
	}
}