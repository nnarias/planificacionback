package ec.edu.espe.gpi.service.admin;

import java.util.List;

import org.springframework.security.core.Authentication;

import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.model.admin.Notification;

public interface INotificationService {

	public void delete(Long id);

	public Notification find(Long id);

	public List<Notification> findAll();

	public List<Notification> findAllByUser(Authentication authentication);

	public Notification findByUser(Authentication authentication, Long userId);

	public List<Notification> findByUserRecentNotification(Authentication authentication);

	public void save(NotificationDTO notificationDTO);
}