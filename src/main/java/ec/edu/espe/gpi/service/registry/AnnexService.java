package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IAnnexDao;
import ec.edu.espe.gpi.dto.registry.AnnexDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.Annex;
import ec.edu.espe.gpi.model.registry.CallArea;

@Service
public class AnnexService implements IAnnexService {

	@Autowired
	private IAnnexDao annexDocumentDao;

	@Override
	@Transactional(readOnly = true)
	public Annex find(Long id) {
		return annexDocumentDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Annex.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Annex> findAll() {
		return annexDocumentDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional
	public void save(AnnexDTO annexDocumentDTO) {
		Annex annexDocument = new Annex();
		try {
			Long id = annexDocumentDTO.getId();
			if (id != null) {
				annexDocument = find(id);
				if (annexDocumentDao.findByNameNotPresent(id, annexDocumentDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Annex.class, "name", annexDocumentDTO.getName());
				}
			} else {
				if (annexDocumentDao.findByName(annexDocumentDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Annex.class, "name", annexDocumentDTO.getName());
				}
				annexDocument.setState(true);
			}
			annexDocument.setName(annexDocumentDTO.getName());
			annexDocument.setRequired(annexDocumentDTO.getRequired());
			annexDocumentDao.save(annexDocument);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al guardar el tipo del anexo del documento",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Annex annexDocument = find(id);
			annexDocument.setState(false);
			annexDocumentDao.save(annexDocument);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al eliminar el tipo del anexo del documento",
					e.getMostSpecificCause().getMessage());
		}
	}
}