package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.AmpleAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;

public interface IAmpleAreaKnowledgeService {

	public List<AmpleAreaKnowledge> findAll();

	public AmpleAreaKnowledge findById(Long id);

	public void save(AmpleAreaKnowledgeDTO ampleAreaKnowledgeDTO);

	public void updateState(Long id);
}