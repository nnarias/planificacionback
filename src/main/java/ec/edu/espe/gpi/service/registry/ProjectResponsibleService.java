package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectResponsibleDao;
import ec.edu.espe.gpi.dto.registry.ProjectResponsibleDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;
import ec.edu.espe.gpi.model.registry.ProjectResponsible;
import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;

@Service
public class ProjectResponsibleService implements IProjectResponsibleService {

	@Autowired
	private ICollegeDepartmentService collegeDepartmentService;

	@Autowired
	private IProjectResponsibleDao projectResponsibleDao;

	@Autowired
	private IProjectResponsibleTypeService projectResponsibleTypeService;

	@Override
	@Transactional(readOnly = true)
	public List<ProjectResponsible> findAll() {
		return projectResponsibleDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectResponsible findById(Long id) {
		return projectResponsibleDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(ProjectResponsible.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ProjectResponsibleDTO projectResponsibleDTO) {
		ProjectResponsible projectResponsible = new ProjectResponsible();
		try {
			Long id = projectResponsibleDTO.getId();
			if (id != null) {
				projectResponsible = findById(id);
				if (projectResponsibleDao.findByNameNotPresent(id, projectResponsibleDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectResponsible.class, "name",
							projectResponsibleDTO.getName());
				}
			} else {
				if (projectResponsibleDao.findByName(projectResponsibleDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(ProjectResponsible.class, "name",
							projectResponsibleDTO.getName());
				}
				projectResponsible.setState(true);
			}
			CollegeDepartment collegeDepartment = collegeDepartmentService
					.findById(projectResponsibleDTO.getCollegeDepartmentId());
			ProjectResponsibleType projectResponsibleType = projectResponsibleTypeService
					.findById(projectResponsibleDTO.getProjectResponsibleTypeId());
			projectResponsible.setCollegeDepartment(collegeDepartment);
			projectResponsible.setEmail(projectResponsibleDTO.getEmail());
			projectResponsible.setExternalInstitution(projectResponsibleDTO.getExternalInstitution());
			projectResponsible.setExternalResponsible(projectResponsibleDTO.getExternalResponsible());
			projectResponsible.setLastname(projectResponsibleDTO.getLastname());
			projectResponsible.setName(projectResponsibleDTO.getName());
			projectResponsible.setNumberIdentification(projectResponsibleDTO.getNumberIdentification());
			projectResponsible.setProjectResponsibleType(projectResponsibleType);
			projectResponsible.setTelephone(projectResponsibleDTO.getTelephone());
			projectResponsibleDao.save(projectResponsible);
		} catch (DataAccessException e) {
			throw new DataException(ProjectResponsible.class, "Error al guardar responsable del proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			ProjectResponsible projectResponsible = findById(id);
			projectResponsible.setState(false);
			projectResponsibleDao.save(projectResponsible);
		} catch (DataAccessException e) {
			throw new DataException(ProjectResponsible.class, "Error al eliminar responsable del proyecto",
					e.getMostSpecificCause().getMessage());
		}
	}
}