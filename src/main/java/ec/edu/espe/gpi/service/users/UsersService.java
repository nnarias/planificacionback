package ec.edu.espe.gpi.service.users;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.dao.users.IUsersDao;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.users.UsersDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.model.users.Users;

@Service
public class UsersService implements IUsersService{
	@Autowired
	private IUsersDao usersDao;
	
	

	

	@Override
	@Transactional
	public void save(UsersDTO usersDTO) {
		Optional<Users> users = Optional.ofNullable(new Users());
		try {
			String id = usersDTO.getId();
			if (id != null) {
				//modificar
				users = usersDao.findById(id);
				users.get().setIdCard(usersDTO.getIdCard());
				users.get().setName(usersDTO.getName());
				users.get().setLastName(usersDTO.getLastName());
				users.get().setDepartment(usersDTO.getDepartment());
				users.get().setCampus(usersDTO.getCampus());
				users.get().setType(usersDTO.getType());
				users.get().setAcademicDegree(usersDTO.getAcademicDegree());
			} else {
				//crear
				users.get().setIdCard(usersDTO.getIdCard());
				users.get().setName(usersDTO.getName());
				users.get().setLastName(usersDTO.getLastName());
				users.get().setDepartment(usersDTO.getDepartment());
				users.get().setCampus(usersDTO.getCampus());
				users.get().setType(usersDTO.getType());
				users.get().setAcademicDegree(usersDTO.getAcademicDegree());
			}
			usersDao.save(users.get());
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar el Usuario", e.getMessage());
		}

	}

}
