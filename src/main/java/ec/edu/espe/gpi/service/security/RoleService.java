package ec.edu.espe.gpi.service.security;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.security.IRoleDao;
import ec.edu.espe.gpi.dao.security.IUserDao;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.utils.security.UserProfile;
import ec.edu.espe.gpi.utils.security.UserRoleGeneral;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;

@Service
public class RoleService implements IRoleService {
	private static final String DATA_EXCEPTION = "Error al realizar la consulta en la base de datos";

	@Autowired
	private IRoleDao roleDao;

	@Autowired
	private IUserDao userDao;

	@Autowired
	private IUserService userService;

	@Autowired
	private IMiespeService miespeService;

	@Override
	@Transactional(readOnly = true)
	public List<Role> findAll() {
		return roleDao.findAllEnable().orElse(new ArrayList<>()).stream().map(temp -> {
			Role role = new Role();
			role.setId(temp.getId());
			role.setName(temp.getName());
			role.setRoleName(temp.getRoleName());
			return role;
		}).collect(Collectors.toList());
	}

	@Override
	@Transactional(readOnly = true)
	public Role findById(Long id) {
		try {
			return roleDao.findById(id).orElseThrow(() -> new NotFoundException(Role.class, "id", id));
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, DATA_EXCEPTION, e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Role findByRoleName(String roleName) {
		return roleDao.findByRoleName(roleName)
				.orElseThrow(() -> new NotFoundException(Role.class, "roleName", roleName));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Role> findUserRole(Long id) {
		List<Role> roleList = new ArrayList<>();
		UserModel user = new UserModel();
		try {
			user = userService.findById(id);
			roleList = user.getRoleList();
			return roleList;
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, DATA_EXCEPTION, e.getMostSpecificCause().getMessage());
		}

	}

	@Override
	@Transactional(readOnly = true)
	public List<UserRoleGeneral> findUserRoleGeneral(String roleName) {
		List<UserRoleGeneral> userRoleGeneralList = new ArrayList<>();
		Role role = findByRoleName(roleName);
		for (UserModel userModel : role.getUserModelList()) {
			UserRoleGeneral userRoleGeneral = new UserRoleGeneral();
			userRoleGeneral.setUsername(userModel.getUsername());
			userRoleGeneral.setInternalUser(false);
			userRoleGeneralList.add(userRoleGeneral);
		}
		
		UserProfile[] userProfileList = miespeService.findUserProfileList(roleName);
		for (UserProfile userProfile : userProfileList) {
			UserRoleGeneral userRoleGeneral = new UserRoleGeneral();
			UserldapMiespe userldapMiespe = miespeService.findUserldap(userProfile.getCodigo());
			userRoleGeneral.setUsername(userldapMiespe.getUserName());
			userRoleGeneral.setInternalUser(true);
			userRoleGeneralList.add(userRoleGeneral);
		}
		return userRoleGeneralList;
	}

	@Override
	@Transactional
	public void updateRoles(List<Long> roleList, Long id) {
		UserModel userModel = new UserModel();
		List<Role> newRoleList = new ArrayList<>();
		try {
			userModel = userService.findById(id);
			for (Long number : roleList) {
				Role role = findById(number);
				newRoleList.add(role);
			}
			userModel.setRoleList(newRoleList);
			userModel.setLogin(true);
			userDao.save(userModel);
		} catch (DataAccessException e) {
			throw new DataException(UserModel.class, DATA_EXCEPTION, e.getMostSpecificCause().getMessage());
		}
	}
}