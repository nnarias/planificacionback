package ec.edu.espe.gpi.service.admin;

import java.io.IOException;

import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.UserDocument;

public interface IDocumentService {

	public Document sendDocument(String url, String fileName, String pathFile, UserDocument userDocument,
			MultipartFile file);

	public Boolean createFolder(String url, String pathFile, UserDocument userDocument);

	public Boolean getFolder(String url, String pathFile, UserDocument userDocument);

	public InputStreamResource getDocument(String url, String uuid, UserDocument userDocument) throws IOException;

}
