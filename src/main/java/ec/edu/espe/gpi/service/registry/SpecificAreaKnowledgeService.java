package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ec.edu.espe.gpi.dao.registry.ISpecificAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.SpecificAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.SpecificAreaKnowledge;

@Service
public class SpecificAreaKnowledgeService implements ISpecificAreaKnowledgeService {

	@Autowired
	private ISpecificAreaKnowledgeDao specificAreaKnowledgeDao;

	@Override
	@Transactional(readOnly = true)
	public List<SpecificAreaKnowledge> findAll() {
		return specificAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public SpecificAreaKnowledge findById(Long id) {
		return specificAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(SpecificAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional
	public void save(SpecificAreaKnowledgeDTO specificAreaKnowledgeDTO) {

		SpecificAreaKnowledge specificAreaKnowledge = new SpecificAreaKnowledge();
		try {
			Long id = specificAreaKnowledgeDTO.getId();
			if (id != null) {
				specificAreaKnowledge = findById(id);
				if (specificAreaKnowledgeDao.findByNameNotPresent(id, specificAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(SpecificAreaKnowledge.class, "name",
							specificAreaKnowledgeDTO.getName());
				}
			} else {
				if (specificAreaKnowledgeDao.findByName(specificAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(SpecificAreaKnowledge.class, "name",
							specificAreaKnowledgeDTO.getName());
				}
				specificAreaKnowledge.setState(true);
			}
			specificAreaKnowledge.setName(specificAreaKnowledgeDTO.getName());
			specificAreaKnowledgeDao.save(specificAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(SpecificAreaKnowledge.class, "Error al guardar el �rea de conocimiento espec�fico",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			SpecificAreaKnowledge specificAreaKnowledge = findById(id);
			specificAreaKnowledge.setState(false);
			specificAreaKnowledgeDao.save(specificAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(SpecificAreaKnowledge.class, "Error al eliminar el �rea de conocimiento espec�fico",
					e.getMostSpecificCause().getMessage());
		}
	}

}
