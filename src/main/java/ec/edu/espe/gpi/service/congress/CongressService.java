package ec.edu.espe.gpi.service.congress;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.books.IBooksDao;
import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.registry.Call;

@Service
public class CongressService implements ICongressService{
	@Autowired
	private ICongressDao congressDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<Congress> findAll() {
		return congressDao.findAllEnable().orElse(new ArrayList<>());
	}
	
	@Override
	@Transactional(readOnly = true)
	public List<Congress> findAllByPlanning(Planning planning) {
		return congressDao.findAllByPlanningID(planning).orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Congress findById(Long id) {
		return congressDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Congress.class, "id", id));
	}
	

	@Override
	@Transactional(readOnly = true)
	public Optional<Congress> findByIdStateFalse(Long id) {
		return congressDao.findById(id);
	}

	@Override
	@Transactional
	public void save(CongressDTO congressDTO) {
		Congress congress = new Congress();
		boolean flag=false;
		try {
			Long id = congressDTO.getId();
			if (id != null) {
				//modificar
				congress = findById(id);
				congress.setCodIES(congressDTO.getCodIES());
				congress.setType(congressDTO.getType());
				congress.setArticleType(congressDTO.getArticleType());
				congress.setCodPUB(congressDTO.getCodPUB());
				congress.setPresentationName(congressDTO.getPresentationName());
				congress.setEventName(congressDTO.getEventName());
				congress.setEventEdition(congressDTO.getEventEdition());
				congress.setEventOrganizer(congressDTO.getEventOrganizer());
				congress.setOrganizingCommite(congressDTO.getOrganizingCommite());
				congress.setCountry(congressDTO.getCountry());
				congress.setCity(congressDTO.getCity());
				congress.setPublicationDate(congressDTO.getPublicationDate());
				congress.setDetailField(congressDTO.getDetailField());
				congress.setCompetitor(congressDTO.getCompetitor());
				congress.setDuration(congressDTO.getDuration());
				congress.setAprobation(congressDTO.getAprobation());
				congress.setObservations(congressDTO.getObservations());
				
				//evaluar si esta completo
				if(!(congress.getId()==null) 
						&& !(congress.getPlanning()==null)
						&& !(congress.getCodIES()==null)
						&& !(congress.getType()==null)
						&& !(congress.getArticleType()==null)
						&& !(congress.getCodPUB()==null)
						&& !(congress.getPresentationName()==null)
						&& !(congress.getEventName()==null)
						&& !(congress.getEventEdition()==null)
						&& !(congress.getEventOrganizer()==null)
						&& !(congress.getOrganizingCommite()==null)
						&& !(congress.getCountry()==null)
						&& !(congress.getCity()==null)
						&& !(congress.getPublicationDate()==null)
						&& !(congress.getDetailField()==null)
				        && !(congress.getCompetitor()==null)
				        && !(congress.getDuration()==null)) {
					flag = true;
				}	
				congress.setComplete(flag);
				
			} else {
				//crear
				congress.setPlanning(congressDTO.getPlanning());
				congress.setCodIES(congressDTO.getCodIES());
				congress.setType(congressDTO.getType());
				congress.setArticleType(congressDTO.getArticleType());
				congress.setCodPUB(congressDTO.getCodPUB());
				congress.setPresentationName(congressDTO.getPresentationName());
				congress.setEventName(congressDTO.getEventName());
				congress.setEventEdition(congressDTO.getEventEdition());
				congress.setEventOrganizer(congressDTO.getEventOrganizer());
				congress.setOrganizingCommite(congressDTO.getOrganizingCommite());
				congress.setCountry(congressDTO.getCountry());
				congress.setCity(congressDTO.getCity());
				congress.setPublicationDate(congressDTO.getPublicationDate());
				congress.setDetailField(congressDTO.getDetailField());
				congress.setCompetitor(congressDTO.getCompetitor());
				congress.setDuration(congressDTO.getDuration());
				congress.setAprobation(congressDTO.getAprobation());
				congress.setObservations(congressDTO.getObservations());
				congress.setComplete(false);
				congress.setRemoved(false);
			}
			congressDao.save(congress);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar el Congreso", e.getMessage());
		}

	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Congress congress = findById(id);
			congress.setRemoved(true);
			congressDao.save(congress);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar el Congreso", e.getMessage());
		}

	}
	
	@Override
	@Transactional
	public void updateComplete(Long id) {
		try {
			Congress congress = findById(id);
			congress.setComplete(true);
			congressDao.save(congress);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al completar el Congreso", e.getMessage());
		}

	}
}
