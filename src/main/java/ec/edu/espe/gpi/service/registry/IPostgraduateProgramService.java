package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.PostgraduateProgramDTO;
import ec.edu.espe.gpi.model.registry.PostgraduateProgram;

public interface IPostgraduateProgramService {

	public List<PostgraduateProgram> findAll();

	public PostgraduateProgram findById(Long id);

	public void save(PostgraduateProgramDTO postgraduateProgramDTO);

	public void updateState(Long id);
}