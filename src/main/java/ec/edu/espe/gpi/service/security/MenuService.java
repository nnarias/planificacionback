package ec.edu.espe.gpi.service.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.edu.espe.gpi.dao.security.IMenuDao;
import ec.edu.espe.gpi.model.security.Menu;

@Service
public class MenuService implements IMenuService {

	@Autowired
	private IMenuDao menuDao;

	@Override
	public List<Menu> findAll() {
		return (List<Menu>) menuDao.findAll();
	}

	@Override
	public Menu findById(Long id) {
		return menuDao.findById(id).orElse(null);
	}
}