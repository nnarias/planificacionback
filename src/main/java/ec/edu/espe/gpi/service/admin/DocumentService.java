package ec.edu.espe.gpi.service.admin;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotAcceptable;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.UserDocument;

@Service
public class DocumentService implements IDocumentService {
	private static final Logger logger = Logger.getLogger(DocumentService.class.getName());

	@Override
	@Transactional
	public Boolean createFolder(String url, String pathFile, UserDocument userDocument) {
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpEntity<String> requestEntity = new HttpEntity<>(pathFile, Headers.sendHeadersBody(userDocument));

			ResponseEntity<String> resultado = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
			switch (resultado.getStatusCodeValue()) {
			case 200:
				logger.log(Level.FINE, "Carpeta creada con �xito");
				return true;
			case 500:
				logger.log(Level.SEVERE, "roblemas para crear crapeta");
				return false;
			default:
				return false;
			}
		} catch (HttpStatusCodeException exception) {
			return false;
		}

	}

	@Override
	@Transactional(readOnly = true)
	public InputStreamResource getDocument(String url, String uuid, UserDocument userDocument) throws IOException {
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeaders(userDocument));
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Resource> responseEntity = restTemplate.exchange(url + uuid, HttpMethod.GET, request,
				Resource.class);
		Resource resource = responseEntity.getBody();
		if (resource == null) {
			throw new DataException(Resource.class, "No se puede leer el recurso");
		}
		return new InputStreamResource(resource.getInputStream());
	}

	@Override
	@Transactional(readOnly = true)
	public Boolean getFolder(String url, String pathFile, UserDocument userDocument) {
		HttpEntity<String> request = new HttpEntity<>(Headers.sendHeaders(userDocument));
		RestTemplate restTemplate = new RestTemplate();
		try {
			ResponseEntity<Object> resultado = restTemplate.exchange(url + pathFile, HttpMethod.GET, request,
					Object.class);

			switch (resultado.getStatusCodeValue()) {
			case 200:
				return true;
			case 500:
				logger.log(Level.SEVERE, "roblemas para buscar crapeta");
				return false;
			default:
				return false;
			}
		} catch (HttpStatusCodeException exception) {
			return false;
		}
	}

	@Override
	@Transactional
	public Document sendDocument(String url, String fileName, String pathFile, UserDocument userDocument,
			MultipartFile file) {
		LinkedMultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		Resource invoicesResource = file.getResource();
		map.add("content", invoicesResource);
		map.add("docPath", pathFile + "/" + fileName);
		HttpEntity<LinkedMultiValueMap<String, Object>> requestEntity = new HttpEntity<>(map,
				Headers.sendHeadersMultiparth(userDocument));

		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Document> responseDocument = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
				Document.class);

		switch (responseDocument.getStatusCodeValue()) {
		case 401:
			throw new NotAcceptable(Document.class, "Error en la subida del archivo", file.getName(),
					"problemas en la autorizacion", "del cliente");
		case 500:
			throw new NotAcceptable(Document.class, "Error en la subida del archivo", file.getName());
		default:
			break;
		}
		return responseDocument.getBody();
	}
}