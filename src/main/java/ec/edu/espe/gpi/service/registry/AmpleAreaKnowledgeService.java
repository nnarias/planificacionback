package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IAmpleAreaKnowledgeDao;
import ec.edu.espe.gpi.dto.registry.AmpleAreaKnowledgeDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.CallArea;

@Service
public class AmpleAreaKnowledgeService implements IAmpleAreaKnowledgeService {

	@Autowired
	private IAmpleAreaKnowledgeDao ampleAreaKnowledgeDao;

	@Override
	@Transactional(readOnly = true)
	public List<AmpleAreaKnowledge> findAll() {
		return ampleAreaKnowledgeDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public AmpleAreaKnowledge findById(Long id) {
		return ampleAreaKnowledgeDao.findByIdEnable(id)
				.orElseThrow(() -> new NotFoundException(AmpleAreaKnowledge.class, "id", id));
	}

	@Override
	@Transactional
	public void save(AmpleAreaKnowledgeDTO ampleAreaKnowledgeDTO) {
		AmpleAreaKnowledge ampleAreaKnowledge = new AmpleAreaKnowledge();
		try {
			Long id = ampleAreaKnowledgeDTO.getId();
			if (id != null) {
				ampleAreaKnowledge = findById(id);
				if (ampleAreaKnowledgeDao.findByNameNotPresent(id, ampleAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(AmpleAreaKnowledge.class, "name",
							ampleAreaKnowledgeDTO.getName());
				}
			} else {
				if (ampleAreaKnowledgeDao.findByName(ampleAreaKnowledgeDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(AmpleAreaKnowledge.class, "name",
							ampleAreaKnowledgeDTO.getName());
				}
				ampleAreaKnowledge.setState(true);
			}
			ampleAreaKnowledge.setName(ampleAreaKnowledgeDTO.getName());
			ampleAreaKnowledgeDao.save(ampleAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(CallArea.class, "Error al guardar el campo amplio",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			AmpleAreaKnowledge ampleAreaKnowledge = findById(id);
			ampleAreaKnowledge.setState(false);
			ampleAreaKnowledgeDao.save(ampleAreaKnowledge);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al eliminar campo amplio",
					e.getMostSpecificCause().getMessage());
		}
	}
}