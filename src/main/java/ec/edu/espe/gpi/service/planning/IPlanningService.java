package ec.edu.espe.gpi.service.planning;

import java.util.List;
import java.util.Optional;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Planning;

public interface IPlanningService {
	public List<Planning> findAll();
	
	public List<Planning> findAllIR();
	
	public List<Activities> findAllActivitiesbyPlanning(Long id);
	
	public Planning findById(Long id);
	
	public void save(PlanningDTO congressDTO);
	
	public void updateState(Long id);
	
	public void updateAprobation(Long id);
	
	public void updateComplete(Long id);
	
	
	Optional<Planning> findByIdStateFalse(Long id);
}
