package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProgramDao;
import ec.edu.espe.gpi.dto.registry.ProgramDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Program;

@Service
public class ProgramService implements IProgramService {

	@Autowired
	private IProgramDao programDao;

	@Override
	@Transactional(readOnly = true)
	public List<Program> findAll() {
		return programDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public Program findById(Long id) {
		return programDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Program.class, "id", id));
	}

	@Override
	@Transactional
	public void save(ProgramDTO programDTO) {
		Program program = new Program();
		try {
			Long id = programDTO.getId();
			if (id != null) {
				program = findById(id);
				if (programDao.findByNameNotPresent(id, programDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Program.class, "name", programDTO.getName());
				}
			} else {
				if (programDao.findByName(programDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Program.class, "name", programDTO.getName());
				}
				program.setState(true);
			}
			program.setName(programDTO.getName());
			programDao.save(program);
		} catch (DataAccessException e) {
			throw new DataException(Program.class, "Error al guardar el programa",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Program program = findById(id);
			program.setState(false);
			programDao.save(program);
		} catch (DataAccessException e) {
			throw new DataException(Program.class, "Error al eliminar el programa",
					e.getMostSpecificCause().getMessage());
		}
	}
}