package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.PolicyPlanDTO;
import ec.edu.espe.gpi.model.registry.PolicyPlan;

public interface IPolicyPlanService {

	public List<PolicyPlan> findAll();

	public PolicyPlan findById(Long id);

	public List<PolicyPlan> findByGoalPlan(Long idGoal);

	public void save(PolicyPlanDTO policyPlanDTO);

	public void updateState(Long id);
}