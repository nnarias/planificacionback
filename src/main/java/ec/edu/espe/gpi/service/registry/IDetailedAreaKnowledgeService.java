package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.DetailedAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.DetailedAreaKnowledge;

public interface IDetailedAreaKnowledgeService {

	public List<DetailedAreaKnowledge> findAll();

	public DetailedAreaKnowledge findById(Long id);

	public void save(DetailedAreaKnowledgeDTO detailedAreaKnowledgeDTO);

	public void updateState(Long id);
}