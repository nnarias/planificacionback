package ec.edu.espe.gpi.service.tracking;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.IProjectDao;
import ec.edu.espe.gpi.dao.tracking.IGoalDao;
import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.dto.tracking.ActivityResourceDTO;
import ec.edu.espe.gpi.dto.tracking.GoalActivityDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectGoalDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.model.tracking.ActivityResource;
import ec.edu.espe.gpi.model.tracking.GoalActivity;
import ec.edu.espe.gpi.model.tracking.ProjectGoal;
import ec.edu.espe.gpi.model.tracking.Resource;
import ec.edu.espe.gpi.service.registry.IProjectService;
import ec.edu.espe.gpi.utils.tracking.ActivityResourceEnum;
import ec.edu.espe.gpi.utils.tracking.GoalActivityEnum;

@Service
public class GoalService implements IGoalService {
	private static final String NULL = "Valor Nulo";

	@Autowired
	private IActivityResourceService activityResourceService;

	@Autowired
	private IGoalActivityService goalActivityService;

	@Autowired
	private IGoalDao goalDao;

	@Autowired
	private IProjectDao projectDao;

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IResourceService resourceService;

	private List<GoalActivity> activityListAssignment(List<GoalActivityDTO> goalActivityDTOList,
			ProjectGoal projectGoal) {
		List<GoalActivity> activityList = new ArrayList<>();
		List<ActivityResource> resourceList = new ArrayList<>();
		try {
			for (GoalActivityDTO goalActivityDTO : goalActivityDTOList) {
				GoalActivity goalActivity = new GoalActivity();
				Long id = goalActivityDTO.getId();
				if (id != null) {
					goalActivity = goalActivityService.find(id);
				} else {
					goalActivity.setState(GoalActivityEnum.REGISTERED);
				}
				goalActivity.setCurrentBudget(goalActivityDTO.getCurrentBudget());
				goalActivity.setDeliverable(goalActivityDTO.getDeliverable());
				goalActivity.setEndDate(goalActivityDTO.getEndDate());
				goalActivity.setInvestmentBudget(goalActivityDTO.getInvestmentBudget());
				goalActivity.setName(goalActivityDTO.getName());
				goalActivity.setProjectGoal(projectGoal);
				goalActivity.setStartDate(goalActivityDTO.getStartDate());
				resourceList = resourceListAssignment(goalActivityDTO.getActivityResourceList(), goalActivity);
				goalActivity.setActivityResourceList(resourceList);
				activityList.add(goalActivity);
			}
			return activityList;
		} catch (DataAccessException e) {
			throw new DataException(GoalActivity.class, "Error al aasignar actividad del objetivo", e.getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public ProjectGoal find(Long id) {
		return goalDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Project.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectGoal> findAll() {
		return goalDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public List<ProjectGoal> findByProject(Long projectId) {
		return goalDao.findByProject(projectId).orElse(new ArrayList<>());
	}

	private List<ProjectGoal> goalListAssignment(Project project, List<ProjectGoalDTO> projectGoalDTOList) {
		List<GoalActivity> activityList = new ArrayList<>();
		List<ProjectGoal> goalList = new ArrayList<>();
		try {
			for (ProjectGoalDTO projectGoalDTO : projectGoalDTOList) {
				ProjectGoal projectGoal = new ProjectGoal();
				Long id = projectGoalDTO.getId();
				if (id != null) {
					projectGoal = find(id);
				} else {
					projectGoal.setState(true);
				}
				projectGoal.setAssumption(projectGoalDTO.getAssumption());
				projectGoal.setDeliverable(projectGoalDTO.getDeliverable());
				projectGoal.setGeneral(projectGoalDTO.getGeneral());
				projectGoal.setIndicator(projectGoalDTO.getIndicator());
				projectGoal.setName(projectGoalDTO.getName());
				projectGoal.setProject(project);
				if (Boolean.FALSE.equals(projectGoal.getGeneral())) {
					activityList = activityListAssignment(projectGoalDTO.getGoalActivityList(), projectGoal);
					projectGoal.setGoalActivityList(activityList);
				}
				goalList.add(projectGoal);
			}
			return goalList;
		} catch (DataAccessException e) {
			throw new DataException(ProjectGoal.class, "Error al aasignar objetivos del proyecto", e.getMessage());
		}
	}

	private List<ActivityResource> resourceListAssignment(List<ActivityResourceDTO> activityResourceDTOList,
			GoalActivity goalActivity) {
		Resource resource = new Resource();
		List<ActivityResource> resourceList = new ArrayList<>();
		try {
			for (ActivityResourceDTO activityResourceDTO : activityResourceDTOList) {
				ActivityResource activityResource = new ActivityResource();
				Long id = activityResourceDTO.getId();
				if (id != null) {
					activityResource = activityResourceService.find(id);
				} else {
					activityResource.setState(ActivityResourceEnum.REGISTERED);
				}
				activityResource.setCurrentBudget(activityResourceDTO.getCurrentBudget());
				activityResource.setGoalActivity(goalActivity);
				activityResource.setInvestmentBudget(activityResourceDTO.getInvestmentBudget());
				resource = resourceService.findById(activityResourceDTO.getResourceId());
				activityResource.setResource(resource);
				resourceList.add(activityResource);
			}
			return resourceList;
		} catch (DataAccessException e) {
			throw new DataException(ActivityResource.class, "Error al aasignar recursos", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void save(ProjectDTO projectDTO) {
		Project project = new Project();
		try {
			Long id = projectDTO.getId();
			project = projectService.find(id);
			List<ProjectGoal> goalList = goalListAssignment(project, projectDTO.getProjectGoalList());
			if (goalList == null) {
				throw new DataException(ProjectGoal.class, "Error al guardar objetivos del proyecto", NULL);
			}
			project.setProjectGoalList(goalList);
			projectDao.save(project);
		} catch (DataAccessException e) {
			throw new DataException(ProjectGoal.class, "Error al guardar objetivos del proyecto", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		goalDao.deleteById(id);
	}
}