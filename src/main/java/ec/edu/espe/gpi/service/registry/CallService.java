package ec.edu.espe.gpi.service.registry;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ec.edu.espe.gpi.dao.registry.ICallDao;
import ec.edu.espe.gpi.dto.registry.CallDTO;
import ec.edu.espe.gpi.dto.registry.CallExtensionDTO;
import ec.edu.espe.gpi.dto.registry.CallRequirementDTO;
import ec.edu.espe.gpi.exception.DataException;
import ec.edu.espe.gpi.exception.IntegrityViolationException;
import ec.edu.espe.gpi.exception.NotFoundException;
import ec.edu.espe.gpi.model.evaluation.Evaluation;
import ec.edu.espe.gpi.model.registry.AmpleAreaKnowledge;
import ec.edu.espe.gpi.model.registry.Area;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.model.registry.CallArea;
import ec.edu.espe.gpi.model.registry.CallRequirement;
import ec.edu.espe.gpi.model.registry.Requirement;
import ec.edu.espe.gpi.service.evaluation.IEvaluationService;
import ec.edu.espe.gpi.utils.ValidationError;
import ec.edu.espe.gpi.utils.registry.CallAssignment;
import ec.edu.espe.gpi.utils.registry.DateValidation;

@Service
public class CallService implements ICallService {

	@Autowired
	private IAreaService areaService;

	@Autowired
	private ICallAreaService callAreaService;

	@Autowired
	private ICallDao callDao;

	@Autowired
	private IEvaluationService evaluationService;

	@Autowired
	private IRequirementService requirementService;

	private List<Area> areaListAssignment(List<Long> areaIdList) {
		List<Area> areaList = new ArrayList<>();
		try {
			areaIdList.stream().map(id -> {
				Area area = areaService.findById(id);
				areaList.add(area);
				return id;
			}).collect(Collectors.toList());
			return areaList;
		} catch (Exception e) {
			throw new DataException(Area.class, "Error al asignar el Area", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void extensionDate(CallExtensionDTO callExtensionDTO, Long id) {
		Call call = new Call();
		try {
			call = find(id);
			call = CallAssignment.callExtensionAssignment(call, callExtensionDTO);
			callDao.save(call);
		} catch (DataAccessException e) {
			throw new DataException(AmpleAreaKnowledge.class, "Error al actualizar la extensi�n de convocatoria",
					e.getMostSpecificCause().getMessage());
		}
	}

	@Override
	@Transactional(readOnly = true)
	public Call find(Long id) {
		return callDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Call.class, "id", id));
	}

	@Override
	@Transactional(readOnly = true)
	public List<Call> findActive() {
		List<Call> callList = new ArrayList<>();
		callDao.findAllEnable().orElse(new ArrayList<>()).stream().map(call -> {
			ValidationError validationError = DateValidation.callDateValidation(call);
			if (!validationError.isError()) {
				callList.add(call);
			}
			return call;
		}).collect(Collectors.toList());
		return callList;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Call> findAll() {
		return callDao.findAllEnable().orElse(new ArrayList<>());
	}

	@Override
	@Transactional(readOnly = true)
	public List<Call> findAllByEvaluationActive() {
		List<Call> callList = new ArrayList<>();
		callDao.findAllEnable().orElse(new ArrayList<>()).forEach(call -> {
			ValidationError validationError = DateValidation.callEvaluationDateValidation(call);
			if (!validationError.isError()) {
				Call currentCall = new Call();
				currentCall.setId(call.getId());
				currentCall.setName(call.getName());
				currentCall.setStartDate(call.getStartDate());
				currentCall.setEndDate(call.getEndDate());
				currentCall.setEvaluationDate(call.getEvaluationDate());
				callList.add(currentCall);
			}
		});
		return callList;
	}

	@Override
	@Transactional(readOnly = true)
	public Call findByActive(Long id) {
		Call call = callDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Call.class, "id", id));
		ValidationError validationError = DateValidation.callDateValidation(call);
		if (validationError.isError()) {
			throw new NotFoundException(Call.class, "id", id);
		}
		return call;
	}

	@Override
	@Transactional(readOnly = true)
	public Call findByEvaluationActive(Long id) {
		Call call = callDao.findByIdEnable(id).orElseThrow(() -> new NotFoundException(Call.class, "id", id));
		ValidationError validationError = DateValidation.callEvaluationDateValidation(call);
		if (validationError.isError()) {
			throw new NotFoundException(Call.class, "id", id);
		}
		return call;
	}

	private List<CallRequirement> requirementListAssignment(List<CallRequirementDTO> callRequirementDTOList) {
		List<CallRequirement> callRequirementList = new ArrayList<>();
		Requirement requirement = new Requirement();
		try {
			for (CallRequirementDTO callRequirementDTO : callRequirementDTOList) {
				CallRequirement callRequirement = new CallRequirement();
				requirement = requirementService.find(callRequirementDTO.getRequirementId());
				callRequirement.setId(callRequirementDTO.getId());
				callRequirement.setRequired(callRequirementDTO.getRequired());
				callRequirement.setRequirement(requirement);
				callRequirement.setState(true);
				callRequirementList.add(callRequirement);
			}
			return callRequirementList;
		} catch (Exception e) {
			throw new DataException(CallRequirement.class, "Error al asignar requerimiento", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void save(CallDTO callDTO) {
		List<Area> areaList = new ArrayList<>();
		Call call = new Call();
		CallArea callArea = new CallArea();
		Evaluation evaluation = new Evaluation();
		List<CallRequirement> requirementList = new ArrayList<>();
		try {
			Long id = callDTO.getId();
			if (id != null) {
				call = find(id);
				if (callDao.findByNameNotPresent(id, callDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Call.class, "name", callDTO.getName());
				}
			} else {
				if (callDao.findByName(callDTO.getName()).isPresent()) {
					throw new IntegrityViolationException(Call.class, "name", callDTO.getName());
				}
				call.setState(true);
			}
			areaList = areaListAssignment(callDTO.getAreaList());
			callArea = callAreaService.findById(callDTO.getCallAreaId());
			evaluation = evaluationService.find(callDTO.getEvaluationId());
			requirementList = requirementListAssignment(callDTO.getCallRequirementList());
			call = CallAssignment.callAssignment(areaList, call, callArea, callDTO, evaluation, requirementList);
			callDao.save(call);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al guardar la convocatoria", e.getMessage());
		}
	}

	@Override
	@Transactional
	public void updateState(Long id) {
		try {
			Call call = find(id);
			call.setState(false);
			callDao.save(call);
		} catch (DataAccessException e) {
			throw new DataException(Call.class, "Error al eliminar la convocatoria", e.getMessage());
		}
	}
}