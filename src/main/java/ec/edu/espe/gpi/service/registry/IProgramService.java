package ec.edu.espe.gpi.service.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProgramDTO;
import ec.edu.espe.gpi.model.registry.Program;

public interface IProgramService {

	public List<Program> findAll();

	public Program findById(Long id);

	public void save(ProgramDTO programDTO);

	public void updateState(Long id);
}