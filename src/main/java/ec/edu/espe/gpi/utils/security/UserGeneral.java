package ec.edu.espe.gpi.utils.security;

import java.util.List;

import ec.edu.espe.gpi.model.security.Role;
import lombok.Data;

@Data
public class UserGeneral {
	private String email;
	private Boolean internalUser;
	private String name;
	private List<Role> roleList;
	private String username;
}