package ec.edu.espe.gpi.utils.registry;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.utils.ValidationError;

public class DateValidation {
	private static final String CURRENTDATE = " La fecha actual: ";
	private static final String MESSAGE = " mensaje";
	private static final String NOTINCALL = ", no est� dentro del rango de la convocatioria";

	private DateValidation() {

	}

	public static ValidationError callDateValidation(Call call) {
		ValidationError validationError = new ValidationError();
		ZoneId zona = ZoneId.systemDefault();
		Date currentDate = Date.from(LocalDate.now().atStartOfDay(zona).toInstant());

		if (Boolean.FALSE.equals(call.getExtension())) {
			if (currentDate.after(call.getEndDate()) || currentDate.before(call.getStartDate())) {
				validationError.setError(true);
				validationError.setMessageType(MESSAGE);
				validationError.setMessage(CURRENTDATE + currentDate.toString() + NOTINCALL);
			} else {
				validationError.setError(false);
				validationError.setMessageType("");
				validationError.setMessage("");
			}
		} else {
			if (currentDate.after(call.getCallExtension()) || currentDate.before(call.getStartDate())) {
				validationError.setError(true);
				validationError.setMessageType("mensaje");
				validationError.setMessage(CURRENTDATE + currentDate.toString() + NOTINCALL);
			} else {
				validationError.setError(false);
				validationError.setMessageType("");
				validationError.setMessage("");
			}
		}
		return validationError;
	}

	public static ValidationError callEvaluationDateValidation(Call call) {
		ValidationError validationError = new ValidationError();
		ZoneId zone = ZoneId.systemDefault();
		Date currentDate = Date.from(LocalDate.now().atStartOfDay(zone).toInstant());

		if (Boolean.FALSE.equals(call.getExtension())) {
			if (currentDate.after(call.getEvaluationDate()) || currentDate.before(call.getEndDate())) {
				validationError.setError(true);
				validationError.setMessageType(MESSAGE);
				validationError.setMessage(CURRENTDATE + currentDate.toString() + NOTINCALL);
			} else {
				validationError.setError(false);
				validationError.setMessageType("");
				validationError.setMessage("");
			}
		} else {
			if (currentDate.after(call.getEvaluationDate()) || currentDate.before(call.getCallExtension())) {
				validationError.setError(true);
				validationError.setMessageType(MESSAGE);
				validationError.setMessage(CURRENTDATE + currentDate.toString() + NOTINCALL);
			} else {
				validationError.setError(false);
				validationError.setMessageType("");
				validationError.setMessage("");
			}
		}
		return validationError;
	}

	public static ValidationError dateRangeValidation(Date startDate, Date endDate) {
		ValidationError validationError = new ValidationError();
		ZoneId zone = ZoneId.systemDefault();
		Date currentDate = Date.from(LocalDate.now().atStartOfDay(zone).toInstant());

		if (currentDate.after(endDate) || currentDate.before(startDate)) {
			validationError.setError(true);
			validationError.setMessageType(MESSAGE);
			validationError.setMessage(CURRENTDATE + currentDate.toString() + ", no est� dentro del rango");
		} else {
			validationError.setError(false);
			validationError.setMessageType("");
			validationError.setMessage("");
		}
		return validationError;
	}
}