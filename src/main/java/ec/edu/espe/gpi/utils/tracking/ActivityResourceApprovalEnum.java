package ec.edu.espe.gpi.utils.tracking;

public enum ActivityResourceApprovalEnum {
	APPROVED, REJECTED
}
