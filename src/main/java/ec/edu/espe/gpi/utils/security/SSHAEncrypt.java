package ec.edu.espe.gpi.utils.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Base64;

public class SSHAEncrypt {

	private static final int SALT_LENGTH = 4;

	private SSHAEncrypt() {

	}

	public static String generateSSHA(String password) throws NoSuchAlgorithmException {

		byte[] byteArrray = password.getBytes();
		SecureRandom secureRandom = new SecureRandom();
		byte[] salt = new byte[SALT_LENGTH];
		secureRandom.nextBytes(salt);

		MessageDigest crypt = MessageDigest.getInstance("SHA-1");
		crypt.reset();
		crypt.update(byteArrray);
		crypt.update(salt);
		byte[] hash = crypt.digest();

		byte[] hashPlusSalt = new byte[hash.length + salt.length];
		System.arraycopy(hash, 0, hashPlusSalt, 0, hash.length);
		System.arraycopy(salt, 0, hashPlusSalt, hash.length, salt.length);

		return new StringBuilder().append("{SSHA}").append(Base64.getEncoder().encodeToString(hashPlusSalt)).toString();
	}
}