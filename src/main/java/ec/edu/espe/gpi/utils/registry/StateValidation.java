package ec.edu.espe.gpi.utils.registry;

import ec.edu.espe.gpi.model.security.UserModel;

public class StateValidation {
	private StateValidation() {

	}

	public static boolean roleModificationValidation(UserModel userModel) {
		if (userModel != null) {
			return userModel.getLogin();
		} else
			return false;
	}
}
