package ec.edu.espe.gpi.utils.tracking;

public enum GoalActivityEnum {
	DELETED, REGISTERED, EVALUATING, APPROVING, REJECTED
}
