package ec.edu.espe.gpi.utils;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

public class ImageTreatment {
	private ImageTreatment() {

	}

	// Función para crear imagen con n parátros de tipo String
	public static Optional<MultipartFile> createImage(String... textParam) {
		String fileName = UUID.randomUUID().toString() + ".png";
		Font font = new Font("Arial", Font.PLAIN, 48);
		BufferedImage img = new BufferedImage(800, 500, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = img.createGraphics();
		g2d.setFont(font);
		FontMetrics fm = g2d.getFontMetrics();
		String text = textParam[0];
		// Establecimiento de ancho de la imagen, de acuerdo al string mas largo
		for (int i = 1; i < textParam.length; i++) {
			if (textParam[i].length() > text.length()) {
				text = textParam[i];
			}
		}
		int width = fm.stringWidth(text) + 30;
		g2d.dispose();

		img = new BufferedImage(width, 200, BufferedImage.TYPE_INT_ARGB);
		g2d = img.createGraphics();

		// Renderación de imagen
		g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
		g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
		g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
		g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
		g2d.setFont(font);
		g2d.setColor(Color.RED);
		// Coordenada y para ubicar la imagen
		float y = 40;
		for (String draw : textParam) {
			g2d.drawString(draw, 20, y);
			y += 60;
			if (draw.equals(textParam[0])) {
				y += 20;
			}
		}
		g2d.dispose();
		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ImageIO.write(img, "png", baos);
			baos.flush();

			MultipartFile multipartFile = new MockMultipartFile(fileName, fileName,
					MediaType.MULTIPART_FORM_DATA.toString(), baos.toByteArray());

			return Optional.of(multipartFile);

		} catch (IOException e) {
			e.printStackTrace();
			return Optional.of(null);
		}

	}
}
