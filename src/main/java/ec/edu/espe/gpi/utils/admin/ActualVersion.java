package ec.edu.espe.gpi.utils.admin;

import lombok.Data;

@Data
public class ActualVersion {

	private String actual;
	
	private String author;
	
	private String checksum;
	
	private String created;
	
	private String name;
	
	private String size;

}
