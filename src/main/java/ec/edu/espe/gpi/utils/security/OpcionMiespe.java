package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class OpcionMiespe {
	private Object clase;
	private int crear;
	private int eliminar;
	private Object icono;
	private Object imprimir;
	private int modificar;
	private String opcion;
	private String url;
}