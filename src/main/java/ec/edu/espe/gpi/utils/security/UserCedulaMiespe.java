package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class UserCedulaMiespe {
	private String apellidos;
	private String calle_principal;
	private String calle_secundaria;
	private String canton_dn;
	private String canton_pr;
	private String cedula;
	private String celular;
	private String code_orgn;
	private Object contacto_celular;
	private String contacto_direccion;
	private String contacto_nombre;
	private String contacto_parentesco;
	private String contacto_telefono;
	private String correo_institucional;
	private String correo_personal;
	private String estado_civil;
	private String extension;
	private String fecha_nacimiento;
	private String fecha_original;
	private String genero;
	private Object grupo_sanguineo;
	private String id_banner;
	private String identificacion_etnia;
	private String nacionalidad;
	private String nivel;
	private String nombres;
	private String pais_dn;
	private String pais_pr;
	private String parroquia_dn;
	private String parroquia_pr;
	private String plurinacionalidad;
	private Object prefijo;
	private String programa;
	private String provincia_dn;
	private String provincia_pr;
	private String referencia;
	private String religion;
	private String sexo;
	private String status;
	private String telefono;
	private String tipo_parroquia;
	private String titulo;
}