package ec.edu.espe.gpi.utils.admin;

import lombok.Data;

@Data
public class UserDocument {

	private String userName;
	
	private String userPassword;
	
}
