package ec.edu.espe.gpi.utils;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import ec.edu.espe.gpi.utils.admin.UserDocument;

public class Headers {

	public static HttpHeaders getHeadersMultiparth(String fileName) {
		HttpHeaders header = new HttpHeaders();
		header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + fileName);
		header.add("Cache-Control", "no-cache, no-store, must-revalidate");
		header.add("Pragma", "no-cache");
		header.add("Expires", "0");
		return header;
	}

	public static HttpHeaders sendHeadersMultiparth(UserDocument userDocument) {
		HttpHeaders headers = new HttpHeaders();
		String plainCreds = userDocument.getUserName() + ":" + userDocument.getUserPassword();
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);
		headers.add("Authorization", "Basic " + base64Creds);
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		return headers;
	}

	public static HttpHeaders sendHeadersBody(UserDocument userDocument) {
		HttpHeaders headers = new HttpHeaders();
		String plainCreds = userDocument.getUserName() + ":" + userDocument.getUserPassword();
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "Basic " + base64Creds);

		return headers;
	}

	public static HttpHeaders sendHeaders(UserDocument userDocument) {
		HttpHeaders headers = new HttpHeaders();
		String plainCreds = userDocument.getUserName() + ":" + userDocument.getUserPassword();
		byte[] plainCredsBytes = plainCreds.getBytes();
		byte[] base64CredsBytes = Base64.encodeBase64(plainCredsBytes);
		String base64Creds = new String(base64CredsBytes);

		headers.add("Authorization", "Basic " + base64Creds);

		return headers;
	}

	public static HttpHeaders sendHeadersBasic() {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return headers;
	}
	
}
