package ec.edu.espe.gpi.utils.registry;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.registry.Project;

public class ProjectAssignment {
	public static Integer durationMonth(Date startDate, Date endingDate) {
		LocalDate dateProjectEnd = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		LocalDate dateProjectStart = endingDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		Period period = Period.between(dateProjectStart, dateProjectEnd);
		return period.getMonths();
	}

	public Project findProjectId(Long projectId, List<Project> projectList) {
		Iterator<Project> iterator = projectList.iterator();
		while (iterator.hasNext()) {
			Project project = iterator.next();
			if (project.getId().equals(projectId)) {
				return project;
			}
		}
		return null;
	}

	public static Project projectAssignment(Project project, ProjectDTO projectDTO) {
		project.setCriticalFactorSuccess(projectDTO.getCriticalFactorSuccess());
		project.setCurrentSituation(projectDTO.getCurrentSituation());
		project.setDiffusion(projectDTO.getDiffusion());
		project.setDurationMonth(durationMonth(projectDTO.getStartDate(), projectDTO.getEndDate()));
		project.setEndDate(projectDTO.getEndDate());
		project.setEnvironmentalImpactAnalysis(projectDTO.getEnvironmentalImpactAnalysis());
		project.setEspeCurrentRiskBudget(projectDTO.getEspeCurrentRiskBudget());
		project.setEspeInvestmentRiskBudget(projectDTO.getEspeInvestmentRiskBudget());
		project.setImpactEconomicResult(projectDTO.getImpactEconomicResult());
		project.setImpactOtherResult(projectDTO.getImpactOtherResult());
		project.setImpactPoliticalResult(projectDTO.getImpactPoliticalResult());
		project.setImpactScientificResult(projectDTO.getImpactScientificResult());
		project.setImpactSocialResult(projectDTO.getImpactSocialResult());
		project.setIntellectualProperty(projectDTO.getIntellectualProperty());
		project.setNameEnglish(projectDTO.getNameEnglish());
		project.setNameSpanish(projectDTO.getNameSpanish());
		project.setNumberParticipantOther(projectDTO.getNumberParticipantOther());
		project.setNumberParticipantProfessorFemale(projectDTO.getNumberParticipantProfessorFemale());
		project.setNumberParticipantProfessorMale(projectDTO.getNumberParticipantProfessorMale());
		project.setNumberParticipantStudentFemale(projectDTO.getNumberParticipantStudentFemale());
		project.setNumberParticipantStudentMale(projectDTO.getNumberParticipantStudentMale());
		project.setNumberPopulationDisabledPerson(projectDTO.getNumberPopulationDisabledPerson());
		project.setNumberPopulationFemale(projectDTO.getNumberPopulationFemale());
		project.setNumberPopulationIndirect(projectDTO.getNumberPopulationIndirect());
		project.setNumberPopulationMale(projectDTO.getNumberPopulationMale());
		project.setProblemDiagnosis(projectDTO.getProblemDiagnosis());
		project.setProjectBase(projectDTO.getProjectBase());
		project.setPrototype(projectDTO.getPrototype());
		project.setReasonProjectExecution(projectDTO.getReasonProjectExecution());
		project.setResearchMethodology(projectDTO.getResearchMethodology());
		project.setRestrictionAssumption(projectDTO.getRestrictionAssumption());
		project.setScientificArticle(projectDTO.getScientificArticle());
		project.setSocialSustainability(projectDTO.getSocialSustainability());
		project.setSpinOffs(projectDTO.getSpinOffs());
		project.setSponsorCurrentRiskBudget(projectDTO.getSponsorCurrentRiskBudget());
		project.setSponsorInvestmentRiskBudget(projectDTO.getSponsorInvestmentRiskBudget());
		project.setStartDate(projectDTO.getStartDate());
		project.setTechnicalViability(projectDTO.getTechnicalViability());
		project.setTechnologicalEquipment(projectDTO.getTechnologicalEquipment());
		project.setTechnologyTransfer(projectDTO.getTechnologyTransfer());
		project.setAncestralKnowledge(projectDTO.getAncestralKnowledge());
		project.setUnintentionalConsequences(projectDTO.getUnintentionalConsequences());
		return project;
	}
}