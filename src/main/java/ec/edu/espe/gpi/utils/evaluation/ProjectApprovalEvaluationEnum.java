package ec.edu.espe.gpi.utils.evaluation;

public enum ProjectApprovalEvaluationEnum {
	APPROVED, REJECTED
}