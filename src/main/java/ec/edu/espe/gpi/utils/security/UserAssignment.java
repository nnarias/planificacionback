package ec.edu.espe.gpi.utils.security;

import java.util.Date;

import ec.edu.espe.gpi.dto.security.UserDTO;
import ec.edu.espe.gpi.model.security.UserModel;

public class UserAssignment {

	public UserModel createUserAssignment(UserDTO userDTO, String username, String passwordBcrypt) {
		Date currentDate = new Date();
		UserModel userModel = new UserModel();
		userModel.setApprovedProjectNumber(0L);
		userModel.setBirthDate(userDTO.getBirthDate());
		userModel.setDisapprovedProjectNumber(0L);
		userModel.setCreationDate(currentDate);
		userModel.setEmail(userDTO.getEmail());
		userModel.setLastName(userDTO.getLastName());
		userModel.setLogin(true);
		userModel.setName(userDTO.getName());
		userModel.setPassword(passwordBcrypt);
		userModel.setPendingProjectNumber(0L);
		userModel.setState(true);
		userModel.setUsername(username);
		return userModel;
	}

	public UserModel userModificationAssignment(UserModel userModel, UserDTO userDTO) {
		userModel.setBirthDate(userDTO.getBirthDate());
		userModel.setEmail(userDTO.getEmail());
		userModel.setLastName(userDTO.getLastName());
		userModel.setName(userDTO.getName());
		return userModel;
	}
}