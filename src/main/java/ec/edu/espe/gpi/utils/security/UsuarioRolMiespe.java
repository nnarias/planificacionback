package ec.edu.espe.gpi.utils.security;

import java.util.List;

import lombok.Data;

@Data
public class UsuarioRolMiespe {
	private List<PerfilMiespe> perfil;
	private List<OpcionMiespe> opciones;
	private Usuario usuario;
}