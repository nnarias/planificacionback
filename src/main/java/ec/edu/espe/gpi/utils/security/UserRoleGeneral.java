package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class UserRoleGeneral {
	public Boolean internalUser;
	public String username;
}