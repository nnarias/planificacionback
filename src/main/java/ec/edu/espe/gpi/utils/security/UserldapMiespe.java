package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class UserldapMiespe {
	public String codId;
	public String nombreCompleto;
	public String password;
	public String userName;
}