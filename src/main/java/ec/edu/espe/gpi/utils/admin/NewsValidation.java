package ec.edu.espe.gpi.utils.admin;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import ec.edu.espe.gpi.model.admin.News;
import ec.edu.espe.gpi.utils.ValidationError;

public class NewsValidation {
	
	private NewsValidation() {
		
	}

	public static ValidationError currentNewsValidation(News news) {
		ValidationError validationError = new ValidationError();
		ZoneId zona = ZoneId.systemDefault();
		Date currentDate = Date.from(LocalDate.now().atStartOfDay(zona).toInstant());
		if (currentDate.after(news.getEndDate()) || currentDate.before(news.getStartDate())) {
			validationError.setError(true);
			validationError.setMessageType("mensaje");
			validationError.setMessage("La noticia no est� activa para esta fecha");
		} else {
			validationError.setError(false);
			validationError.setMessageType("");
			validationError.setMessage("");
		}
		return validationError;
	}
}