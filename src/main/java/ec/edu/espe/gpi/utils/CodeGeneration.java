package ec.edu.espe.gpi.utils;

import java.text.DateFormat;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.TimeZone;

import ec.edu.espe.gpi.dto.security.UserDTO;
import ec.edu.espe.gpi.model.registry.Project;

public class CodeGeneration {
	private static final String TIMEZONE = "Europe/Spain";

	public String generateDate(Date date) {
		String dateFormat = "";
		DateFormat outFormat = new SimpleDateFormat("dd/MM/yyyy");
		outFormat.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		dateFormat = outFormat.format(date);
		return dateFormat;
	}

	public Date generateDateFormt(String birthDateFormat) {
		Date date = new Date();
		String format;
		DateFormat hourDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		hourDate.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		try {
			format = birthDateFormat.split("T")[0];
			date = hourDate.parse(format);
			return date;
		} catch (ParseException e) {
			return null;
		}
	}

	public String projectCodeReferenceGeneration(Project project, long number) {
		String date = null;
		String name = null;
		String projectName = project.getNameSpanish().toLowerCase();
		String referenceCode = null;
		projectName = removeSpecialCharacters(projectName);
		String[] parts = projectName.split(" ");
		DateFormat outFormat = new SimpleDateFormat("yy");
		for (int i = 0; i < parts.length; i++) {
			if (!parts[i].contains("proyecto")) {
				name = parts[i].trim();
				break;
			}
		}
		outFormat.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		date = outFormat.format(project.getCreationDate());
		if (number == 0) {
			referenceCode = "pro-" + name + "-" + date;
		} else {
			referenceCode = "pro-" + name + number + "-" + date;
		}
		return referenceCode;
	}

	public String pwdGeneration(Date birthDate) {
		DateFormat outFormat = new SimpleDateFormat("ddMMyy");
		outFormat.setTimeZone(TimeZone.getTimeZone(TIMEZONE));
		return outFormat.format(birthDate);
	}

	public String removeSpecialCharacters(String word) {
		return Normalizer.normalize(word, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
	}

	public String usernameGeneration(UserDTO user, long number) {
		String prefix = "-e";
		StringBuilder builder = new StringBuilder();
		String lastname = removeSpecialCharacters(user.getLastName().split(" ")[0].trim());
		String username = "";
		String usernameId = "";
		String word = "";
		StringTokenizer wordToken = new StringTokenizer(removeSpecialCharacters(user.getName()));
		while (wordToken.hasMoreTokens()) {
			word = wordToken.nextToken();
			builder.append(word.substring(0, 1));
		}
		lastname = lastname.toLowerCase();
		usernameId = builder.toString().toLowerCase();
		if (number == 0) {
			username = usernameId + lastname + prefix;
		} else {
			username = usernameId + lastname + number + prefix;
		}
		return username;
	}
}