package ec.edu.espe.gpi.utils.tracking;

public enum GoalActivityApprovalEnum {
	APPROVED, REJECTED
}
