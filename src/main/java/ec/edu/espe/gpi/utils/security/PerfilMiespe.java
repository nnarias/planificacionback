package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class PerfilMiespe {
	private String perfil;
	private String sistema;
	private String url;
}