package ec.edu.espe.gpi.utils.security;

import lombok.Data;

@Data
public class UserUsernameMiespe {
	private String cedula;
	private String correoInstitucional;
	private String correoPersonal;
	private String id;
	private String nombres;
	private String pidm;
}