package ec.edu.espe.gpi.utils.registry;

import java.util.List;

import ec.edu.espe.gpi.dto.registry.CallDTO;
import ec.edu.espe.gpi.dto.registry.CallExtensionDTO;
import ec.edu.espe.gpi.model.evaluation.Evaluation;
import ec.edu.espe.gpi.model.registry.Area;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.model.registry.CallArea;
import ec.edu.espe.gpi.model.registry.CallRequirement;

public class CallAssignment {

	private CallAssignment() {

	}

	public static Call callAssignment(List<Area> areaList, Call call, CallArea callArea, CallDTO callDTO,
			Evaluation evaluation, List<CallRequirement> callRequirementList) {
		call.setAreaList(areaList);
		call.setCallArea(callArea);
		call.setDescription(callDTO.getDescription());
		call.setEndDate(callDTO.getEndDate());
		call.setEvaluation(evaluation);
		call.setExtension(callDTO.getExtension());
		call.setFinacingAmount(callDTO.getFinacingAmount());
		call.setName(callDTO.getName());
		call.setStartDate(callDTO.getStartDate());
		for (CallRequirement callRequirement : callRequirementList) {
			callRequirement.setCall(call);
		}
		if (call.getCallRequirementList() == null) {
			call.setCallRequirementList(callRequirementList);
		} else {
			call.getCallRequirementList().clear();
			call.getCallRequirementList().addAll(callRequirementList);
		}
		return call;
	}

	public static Call callExtensionAssignment(Call call, CallExtensionDTO callExtensionDTO) {
		call.setCallExtension(callExtensionDTO.getCallExtension());
		call.setExtension(true);
		call.setSupport(callExtensionDTO.getSupport());
		return call;
	}
}