package ec.edu.espe.gpi.utils.tracking;

public enum ActivityResourceEnum {
	DELETED, REGISTERED, EVALUATING, APPROVING, REJECTED
}
