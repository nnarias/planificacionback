package ec.edu.espe.gpi.utils.admin;

public enum TypeUserDocument {
	ADMIN, PROJECT_ADMIN, PROJECT_DIRECTOR
}
