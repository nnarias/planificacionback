package ec.edu.espe.gpi.utils.admin;

import lombok.Data;

@Data
public class Document {

	private String author;

	private String created;

	private String path;

	private String fileName;

	private String permissions;

	private String subscribed;

	private String uuid;

	ActualVersion ActualVersionObject;

	private String checkedOut;

	private String convertibleToPdf;

	private String convertibleToSwf;

	private String lastModified;

	private String locked;

	private String mimeType;

	private String signed;

	private String title;

}
