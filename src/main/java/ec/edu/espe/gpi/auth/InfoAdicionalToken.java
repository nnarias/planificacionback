package ec.edu.espe.gpi.auth;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Component;

import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.service.security.IMiespeService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.security.UserldapMiespe;

@Component
public class InfoAdicionalToken implements TokenEnhancer {

	@Autowired
	public IUserService userService;

	@Autowired
	private IMiespeService miespeService;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
		UserldapMiespe userldapMiespe = miespeService.findUserldap(authentication.getName());
		Map<String, Object> info = new HashMap<>();
		if (userldapMiespe.getUserName() != null) {
			info.put("username", userldapMiespe.getUserName());
			info.put("name", userldapMiespe.getNombreCompleto());
			info.put("internalUSer", true);
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
			return accessToken;
		} else {
			UserModel userModel = userService.findByUsername(authentication.getName());
			userModel.setLogin(false);
			userService.saveUser(userModel);
			info.put("username", userModel.getUsername());
			info.put("name", userModel.getName() + " " + userModel.getLastName());
			info.put("internalUSer", false);
			((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(info);
			return accessToken;
		}
	}
}