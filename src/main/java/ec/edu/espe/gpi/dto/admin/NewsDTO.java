package ec.edu.espe.gpi.dto.admin;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class NewsDTO {
	
	@NotBlank(message = "El cuerpo de la noticia esta vacio")
	private String body;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "La fecha de finalizaci�n esta vacia")
	private Date endDate;

	private Long id;
	private String image;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "La fecha de inicio esta vacia")
	private Date startDate;

	@NotBlank(message = "El subtitulo esta vacio")
	private String subtitle;
	
	@NotBlank(message = "El titulo esta vacio")
	private String title;
	
	private Boolean update;
}