package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectTypeDTO {
	private Long id;
	private String name;
}