package ec.edu.espe.gpi.dto.tracking;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;
import lombok.Data;

@Data
public class ProjectInvoiceDTO {

	@NotNull(message = "El id esta vacio")
	private Long id;

	private MultipartFile file;

	private String description;

	@NotNull(message = "El presupuesto de inversión esta vacio")
	private Double investmentBudget;

	@NotNull(message = "El presupuesto actual esta vacio")
	private Double currentBudget;
}
