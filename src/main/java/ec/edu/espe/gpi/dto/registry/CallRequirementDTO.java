package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CallRequirementDTO {
	
	@NotBlank(message = "El id del Convocatoria esta vacio")
	private Long callId;
	
	private Long id;
	private Boolean required;
	
	@NotBlank(message = "El id del Requerimiento esta vacio")
	private Long requirementId;
	
	private Boolean state;
}