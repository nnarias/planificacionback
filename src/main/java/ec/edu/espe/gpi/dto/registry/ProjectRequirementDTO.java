package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectRequirementDTO {
	private Long id;
	private String fileName;
	private Long requirementId;
	private Boolean updated;
}