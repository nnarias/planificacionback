package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class AnnexDTO {
	private Long id;

	@NotBlank(message = "El nombre esta vacio")
	private String name;

	@NotNull(message = "El tipo obligatorio esta vacio")
	private Boolean required;
}