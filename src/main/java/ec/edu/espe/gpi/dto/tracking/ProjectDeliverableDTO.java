package ec.edu.espe.gpi.dto.tracking;

import javax.validation.constraints.NotNull;

import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@Data
public class ProjectDeliverableDTO {

	@NotNull(message = "El id esta vacio")
	private Long id;

	private MultipartFile file;

	private String description;
}
