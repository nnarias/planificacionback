package ec.edu.espe.gpi.dto.evaluation;

import java.util.List;

import lombok.Data;

@Data
public class EvaluationDTO {
	private List<EvaluationCriteriaDTO> evaluationCriteriaList;
	private Long id;
	private Integer grade;
	private String name;
	private Boolean state;
}