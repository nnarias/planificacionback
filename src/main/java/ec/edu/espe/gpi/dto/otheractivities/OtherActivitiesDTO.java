package ec.edu.espe.gpi.dto.otheractivities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class OtherActivitiesDTO {
	private Long id;
	private Planning planning;
	private String name; 
	private Long duration; 
	private String link;
	private String aprobation;
	private String observations;
	
	public OtherActivitiesDTO(Long id, Planning planning, String name, Long duration, String link, String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.name = name;
		this.duration = duration;
		this.link = link;  
		this.aprobation = aprobation;
		this.observations = observations;
	}
	public OtherActivitiesDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
