package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectStatusDTO {
	private Long id;
	private String name;
}