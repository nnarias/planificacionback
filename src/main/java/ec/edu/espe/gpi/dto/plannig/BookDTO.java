package ec.edu.espe.gpi.dto.plannig;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class BookDTO {
	

	private Long id;
	
	private Integer codies;
	
	@NotBlank(message = "El tipo de la publicacion esta vacio")
	private String publicationType;
	
	@NotBlank(message = "El codigo de la publicacion de la esta vacio")
	private String publicationCode;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	@NotNull(message = "La fecha de publicacion esta vacia")
	private Date publicationDate;

	public BookDTO(Long id, Integer codies,
			@NotBlank(message = "El tipo de la publicacion esta vacio") String publicationType,
			@NotBlank(message = "El codigo de la publicacion de la esta vacio") String publicationCode,
			@NotNull(message = "La fecha de publicacion esta vacia") Date publicationDate) {
		super();
		this.id = id;
		this.codies = codies;
		this.publicationType = publicationType;
		this.publicationCode = publicationCode;
		this.publicationDate = publicationDate;
	}
	
	public BookDTO() {
			
	}
	
	
}
