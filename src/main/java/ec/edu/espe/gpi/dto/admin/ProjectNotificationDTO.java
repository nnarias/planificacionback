package ec.edu.espe.gpi.dto.admin;

import lombok.Data;

@Data
public class ProjectNotificationDTO {
	
	private String comment;
	
	private Long projectId;

	private String title;
	
	private String user;
}