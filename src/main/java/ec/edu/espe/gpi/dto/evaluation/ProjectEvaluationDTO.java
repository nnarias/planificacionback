package ec.edu.espe.gpi.dto.evaluation;

import lombok.Data;

@Data
public class ProjectEvaluationDTO {
	private Long evaluationCriteriaSubriteriaId;
	private Integer grade;
	private Long id;
	private Long projectId;
	private Boolean state;
}