package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ResearchTypeDTO {
	private Long id;

	@NotBlank(message = "El nombre esta vac�o")
	private String name;
}