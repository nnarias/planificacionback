package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class ProjectResponsibleDTO {
	private Long collegeDepartmentId;

	@NotBlank(message = "El correo esta vac�o")
	@Email(message = "El correo es incorrecto")
	private String email;

	private String externalInstitution;
	private Boolean externalResponsible;
	private Long id;

	@NotBlank(message = "El apellido esta vac�o")
	private String lastname;

	@NotBlank(message = "El nombre esta vac�o")
	private String name;
	
	private String numberIdentification;
	private Long projectResponsibleTypeId;
	private String telephone;
}