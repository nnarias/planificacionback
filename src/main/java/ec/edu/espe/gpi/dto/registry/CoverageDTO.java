package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CoverageDTO {

	@NotNull(message = "El ID del tipo de cobertura esta vacio")
	private Long coverageTypeId;

	private Long id;

	@NotBlank(message = "El nombre esta vacio")
	private String name;
}