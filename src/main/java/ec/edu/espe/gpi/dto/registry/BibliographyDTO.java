package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class BibliographyDTO {
	private String author;
	private String city;
	private String country;
	private String editorial;
	private Long id;
	private String pageName;
	private String title;
	private String url;
	private Boolean website;
	private Integer year;
}