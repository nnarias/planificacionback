package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectMembershipDTO {

	private String name;
}
