package ec.edu.espe.gpi.dto.evaluation;

import lombok.Data;

@Data
public class EvaluationCriteriaSubcriteriaDTO {
	private Long evaluationCriteriaId;
	private Integer grade;
	private Long id;
	private Boolean state;
	private SubcriteriaDTO subcriteria;
}