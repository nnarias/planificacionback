package ec.edu.espe.gpi.dto.users;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import lombok.Data;

@Data
public class UsersDTO {
	private String id;
	private String idCard;
	private String name;
	private String lastName;
	private String department;
	private String campus;
	private String type;
	private String academicDegree;
	public UsersDTO(String id, String idCard, String name, String lastName, String department, String campus,
			String type, String academicDegree) {
		super();
		this.id = id;
		this.idCard = idCard;
		this.name = name;
		this.lastName = lastName;
		this.department = department;
		this.campus = campus;
		this.type = type;
		this.academicDegree = academicDegree;
	}
	public UsersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
