package ec.edu.espe.gpi.dto.tracking;

import java.util.ArrayList;
import java.util.List;

import ec.edu.espe.gpi.model.tracking.ActivityResource;
import lombok.Data;

@Data
public class ResourceDTO {
	private List<ActivityResource> activityResourceList = new ArrayList<>();
	private Long id;
	private String name;
}