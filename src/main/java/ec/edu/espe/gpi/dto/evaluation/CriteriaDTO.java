package ec.edu.espe.gpi.dto.evaluation;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class CriteriaDTO {
	private Long id;
	
	@NotBlank(message = "El nombre esta vacio")
	private String name;
	
	private Boolean state;
}