package ec.edu.espe.gpi.dto.publications;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class PublicationsDTO {
	private Long id;
	private Planning planning;
	private String codIES;
	private String type;
	private String articleType;
	private String codPUB;
	private String title;
	private String indexBase;
	private String codeISS;
	private String magazineName;
	private String quartile;
	private String magazineNumber;
	private String sjr;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	private String detailField;
	private String condition;
	private String publicationLink;
	private String magazineLink;
	private Boolean filiation;
	private String competitor;
	private String observation;
	private Long duration;
	private String aprobation;
	private String observations;
	
	
	public PublicationsDTO(Long id, Planning planning, String codIES, String type, String articleType, String codPUB,
			String title, String indexBase, String codeISS, String magazineName, String quartile, String magazineNumber,
			String sJR, @NotNull(message = "La fecha de publicacion esta vacia") Date publicationDate,
			String detailField, String condition, String publicationLink, String magazineLink, Boolean filiation,
			String competitor, String observation, Long duration, String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.type = type;
		this.articleType = articleType;
		this.codPUB = codPUB;
		this.title = title;
		this.indexBase = indexBase;
		this.codeISS = codeISS;
		this.magazineName = magazineName;
		this.quartile = quartile;
		this.magazineNumber = magazineNumber;
		this.sjr = sJR;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.condition = condition;
		this.publicationLink = publicationLink;
		this.magazineLink = magazineLink;
		this.filiation = filiation;
		this.competitor = competitor;
		this.observation = observation;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}
	public PublicationsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
