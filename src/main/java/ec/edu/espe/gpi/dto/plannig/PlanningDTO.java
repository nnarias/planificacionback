package ec.edu.espe.gpi.dto.plannig;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.users.Users;
import lombok.Data;

@Data
public class PlanningDTO {
	private Long id;
	private Users users;
	//@NotBlank(message = "El tiempo de la duraci�n esta vac�o o es incorrecto")
	private Long maxDuration;
	private Boolean aprobation;
	private String responsibleId;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date registrationDate;
	private String observations;
	private String evaluatorId;
	private String degree;
	
	public PlanningDTO(Long id, Users users, Long maxDuration, Boolean aprobation, String responsibleId,
			Date registrationDate,  String observations, String evaluatorId, String degree) {
		super();
		this.id = id;
		this.users = users;
		this.maxDuration = maxDuration;
		this.aprobation = aprobation;
		this.responsibleId = responsibleId;
		this.registrationDate = registrationDate;
		this.observations = observations;
		this.evaluatorId = evaluatorId;
		this.degree=degree;
	}
	public PlanningDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
