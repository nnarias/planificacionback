package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectAnnexDTO {
	private Long annexId;
	private String description;
	private String fileName;
	private Long id;
	private Boolean updated;
}