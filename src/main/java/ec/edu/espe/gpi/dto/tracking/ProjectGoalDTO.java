package ec.edu.espe.gpi.dto.tracking;

import java.util.List;

import lombok.Data;

@Data
public class ProjectGoalDTO {
	private String assumption;
	private String deliverable;
	private Boolean general;
	private List<GoalActivityDTO> goalActivityList;
	private Long id;
	private String indicator;
	private String name;
}