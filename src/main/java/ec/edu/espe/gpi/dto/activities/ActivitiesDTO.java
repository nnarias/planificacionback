package ec.edu.espe.gpi.dto.activities;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class ActivitiesDTO {
	private Long id;
	private String name;
	private String degree;
	private Long duration;
	private Long maxDuration;
	private Boolean complete;
	private String aprobation;

}
