package ec.edu.espe.gpi.dto.evaluation;

import lombok.Data;

@Data
public class SubcriteriaDTO {
	private Long id;
	private String name;
	private Boolean state;
}