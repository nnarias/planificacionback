package ec.edu.espe.gpi.dto.books;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class BooksDTO {
	private Long id;
	private Planning planning;
	private String codIES;
	private String publicationType;
	private String codPUB;
	private String bookTitle;
	private String codISB;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	private String detailField;
	private String peerReviewed;
	private Boolean filiation;
	private String competitor;
	private Long duration;
	private String aprobation;
	private String observations;
	
	public BooksDTO(Long id, Planning planning, String codIES, String publicationType, String codPUB, String bookTitle,
			String codISB, Date publicationDate,
			String detailField, String peerReviewed, Boolean filiation, String competitor, Long duration , String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.publicationType = publicationType;
		this.codPUB = codPUB;
		this.bookTitle = bookTitle;
		this.codISB = codISB;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.peerReviewed = peerReviewed;
		this.filiation = filiation;
		this.competitor = competitor;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}
	public BooksDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
