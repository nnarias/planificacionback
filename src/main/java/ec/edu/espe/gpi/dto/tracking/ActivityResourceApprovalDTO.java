package ec.edu.espe.gpi.dto.tracking;

import ec.edu.espe.gpi.utils.tracking.ActivityResourceApprovalEnum;
import lombok.Data;

@Data
public class ActivityResourceApprovalDTO {

	private ActivityResourceApprovalEnum activityResourceApprovalEnum;

	private String comment;

	private Long id;
}
