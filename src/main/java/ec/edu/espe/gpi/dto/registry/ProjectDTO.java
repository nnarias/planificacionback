package ec.edu.espe.gpi.dto.registry;

import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.tracking.ProjectGoalDTO;
import ec.edu.espe.gpi.model.evaluation.ProjectEvaluation;
import lombok.Data;

@Data
public class ProjectDTO {
	@NotNull(message = "No se ha seleccionado el campo amplio del proyecto")
	private Long ampleAreaKnowledgeId;

	private String ancestralKnowledge;
	private List<BibliographyDTO> bibliographyList;

	@NotNull(message = "No se ha seleccionado la convocatoria del proyecto")
	private Long callId;

	@NotNull(message = "No se ha seleccionado la carrera del proyecto")
	private Long collegeCareerId;

	@NotNull(message = "No se ha seleccionado el departamento del proyecto")
	private Long collegeDepartmentId;

	private List<Long> coverageList;
	private String criticalFactorSuccess;
	private String currentSituation;
	private String departamentalEvaluer;

	@NotNull(message = "No se ha seleccionado el campo detallado del proyecto")
	private Long detailedAreaKnowledgeId;

	private String diffusion;

	@NotNull(message = "No se ha seleccionado el objetivo socio econ�mico del proyecto")
	private Long economicPartnerGoalId;

	@NotNull(message = "La fecha de fin esta vacia")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date endDate;

	private String environmentalImpactAnalysis;

	@NotNull(message = "No se ha seleccionado el �rea de conocimiento ESPE del proyecto")
	private Long espeAreaKnowledgeId;

	private Double espeCurrentRiskBudget;
	private Double espeInvestmentRiskBudget;
	private Integer evaluation;
	private String financialSupervisor;
	private Long id;
	private String impactEconomicResult;
	private String impactOtherResult;
	private String impactPoliticalResult;
	private String impactScientificResult;
	private String impactSocialResult;
	private List<Long> institutionInvolvedList;
	private String intellectualProperty;

	@NotNull(message = "No se ha seleccionado la l�nea de investigaci�n del proyecto")
	private Long lineResearchId;

	@NotBlank(message = "No se ha ingresado el t�tulo del proyecto en ingl�s")
	private String nameEnglish;

	@NotBlank(message = "No se ha ingresado el t�tulo del proyecto en espa�ol")
	private String nameSpanish;

	private Integer numberParticipantOther;
	private Integer numberParticipantProfessorFemale;
	private Integer numberParticipantProfessorMale;
	private Integer numberParticipantStudentFemale;
	private Integer numberParticipantStudentMale;
	private Integer numberPopulationDisabledPerson;
	private Integer numberPopulationFemale;
	private Integer numberPopulationIndirect;
	private Integer numberPopulationMale;
	private List<Long> policyPlanList;

	@NotNull(message = "No se ha seleccionado el programa de posgrado del proyecto")
	private Long postgraduateProgramId;

	private String problemDiagnosis;

	@NotNull(message = "No se ha seleccionado el programa (Dominios Acad�micos) del proyecto")
	private Long programId;

	private List<ProjectAnnexDTO> projectAnnexList;
	private String projectBase;
	private String projectDirector;
	private List<ProjectEvaluation> projectEvaluationList;
	private String projectEvaluer;
	private List<ProjectGoalDTO> projectGoalList;
	private List<ProjectRequirementDTO> projectRequirementList;
	private List<ProjectResponsibleDTO> projectResponsibleList;

	@NotNull(message = "No se ha seleccionado el estado del proyecto")
	private Long projectStatusId;

	private String prototype;
	private String reasonProjectExecution;

	@NotNull(message = "No se ha seleccionado el grupo de investigaci�n asociado del proyecto")
	private Long researchGroupId;

	private String researchMethodology;

	@NotNull(message = "No se ha seleccionado el tipo de investigaci�n del proyecto")
	private Long researchTypeId;

	private String restrictionAssumption;
	private String scientificArticle;

	@NotNull(message = "No se ha seleccionado la disciplina  del proyecto")
	private Long scientificDisciplineId;

	private String socialSustainability;

	@NotNull(message = "No se ha seleccionado el campo espec�fico del proyecto")
	private Long specificAreaKnowledgeId;

	private String spinOffs;
	private Double sponsorCurrentRiskBudget;
	private Double sponsorInvestmentRiskBudget;

	@NotNull(message = "La fecha de inicio esta vacia")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@NotNull(message = "No se ha seleccionado el sub �rea de conocimiento Unesco del proyecto")
	private Long subUnescoAreaKnowledgeId;

	private String technicalSupervisor;
	private String technicalViability;
	private String technologicalEquipment;
	private String technologyTransfer;
	private String unintentionalConsequences;
}