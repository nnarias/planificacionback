package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class SubUnescoAreaKnowledgeDTO {
	private Long id;

	@NotBlank(message = "El nombre esta vacio")
	private String name;
	private Long unescoAreaKnowledgeId;
}