package ec.edu.espe.gpi.dto.tracking;

import lombok.Data;

@Data
public class ActivityResourceDTO {
	private Double currentBudget;
	private Long goalActivityId;
	private Long id;
	private Double investmentBudget;
	private Long resourceId;
}