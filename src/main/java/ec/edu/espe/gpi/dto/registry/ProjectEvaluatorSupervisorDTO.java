package ec.edu.espe.gpi.dto.registry;

import lombok.Data;

@Data
public class ProjectEvaluatorSupervisorDTO {

	private Long idProject;
	
	private String username;
}
