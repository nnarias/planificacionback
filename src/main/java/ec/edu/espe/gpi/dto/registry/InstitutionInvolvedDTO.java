package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class InstitutionInvolvedDTO {
	private String address;
	
	@NotBlank(message = "El correo esta vac�o")
	@Email(message = "El correo es incorrecto")
	private String email;
	
	private String executingAgency;
	private String fax;
	private Long id;
	private String legalRepresentative;
	private String legalRepresentativeId;
	
	@NotBlank(message = "El nombre esta vac�o")
	private String name;
	
	private String phone;
	private String url;
}