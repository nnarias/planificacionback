package ec.edu.espe.gpi.dto.degress;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class DegreesDTO {
	private Long id;
	private String name;

}
