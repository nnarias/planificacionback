package ec.edu.espe.gpi.dto.congress;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class CongressDTO {
	private Long id;
	private Planning planning;
	private String codIES;
	private String type;
	private String articleType;
	private String codPUB;
	private String presentationName;
	private String eventName;
	private Long eventEdition;
	private String eventOrganizer;
	private String organizingCommite;
	private String country;
	private String city;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	private String detailField;
	private String competitor;
	private Long duration;
	private String aprobation;
	private String observations;
	
	public CongressDTO(Long id, Planning planning, String codeIES, String type, String articleType, String codPUB,
			String presentationName, String eventName, Long eventEdition, String eventOrganizer,
			String organizingCommite, String country, String city,
			Date publicationDate, String detailField,
			String competitor, Long duration, String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codeIES;
		this.type = type;
		this.articleType = articleType;
		this.codPUB = codPUB;
		this.presentationName = presentationName;
		this.eventName = eventName;
		this.eventEdition = eventEdition;
		this.eventOrganizer = eventOrganizer;
		this.organizingCommite = organizingCommite;
		this.country = country;
		this.city = city;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.competitor = competitor;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}

	public CongressDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
