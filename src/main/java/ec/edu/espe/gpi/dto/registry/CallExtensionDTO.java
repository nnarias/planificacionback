package ec.edu.espe.gpi.dto.registry;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class CallExtensionDTO {

	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date callExtension;

	private Long callId;
	private String support;
}