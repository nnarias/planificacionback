package ec.edu.espe.gpi.dto.tracking;

import ec.edu.espe.gpi.utils.tracking.GoalActivityApprovalEnum;
import lombok.Data;

@Data
public class GoalActivityApprovalDTO {

	private GoalActivityApprovalEnum goalActivityApprovalEnum;

	private String comment;

	private Long id;
}
