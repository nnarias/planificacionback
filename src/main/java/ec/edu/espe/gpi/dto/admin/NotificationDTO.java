package ec.edu.espe.gpi.dto.admin;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class NotificationDTO {

	@NotBlank(message = "La descripci�n de la noticia esta vacio")
	private String description;

	private Long id;

	@NotBlank(message = "El t�tulo de la noticia esta vacio")
	private String title;

	@NotBlank(message = "El username del usuario esta vacio")
	private String user;
}