package ec.edu.espe.gpi.dto.tracking;

import java.util.Date;
import java.util.List;

import ec.edu.espe.gpi.model.tracking.ProjectGoal;
import lombok.Data;

@Data
public class GoalActivityDTO {
	private List<ActivityResourceDTO> activityResourceList;
	private Double currentBudget;
	private String deliverable;
	private Date endDate;
	private Long id;
	private Double investmentBudget;
	private String name;
	private ProjectGoal projectGoal;
	private Date startDate;
}