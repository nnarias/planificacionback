package ec.edu.espe.gpi.dto.bookchapters;

import java.util.Date;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
public class BookChaptersDTO {
	private Long id;
	private Planning planning;
	private String codIES;
	private String publicationType;
	private String chapterCode;
	private String codPUB;
	private String capTitle;
	private String bookTitle;
	private String codISB;
	private String editor;
	private Long numberPages;
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	private String detailField;
	private Boolean filiation;
	private String competitor;
	private Long duration;
	private String aprobation;
	private String observations;
	
	public BookChaptersDTO(Long id, Planning planning, String codIES, String publicationType, String chapterCode,
			String codPUB, String capTitle, String bookTitle, String codISB, String editor, Long numberPages,Date publicationDate, String detailField,
			Boolean filiation, String competitor, Long duration, String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.publicationType = publicationType;
		this.chapterCode = chapterCode;
		this.codPUB = codPUB;
		this.capTitle = capTitle;
		this.bookTitle = bookTitle;
		this.codISB = codISB;
		this.editor = editor;
		this.numberPages = numberPages;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.filiation = filiation;
		this.competitor = competitor;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}
	public BookChaptersDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
