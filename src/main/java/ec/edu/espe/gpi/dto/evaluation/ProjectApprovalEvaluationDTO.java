package ec.edu.espe.gpi.dto.evaluation;

import ec.edu.espe.gpi.utils.evaluation.ProjectApprovalEvaluationEnum;
import lombok.Data;

@Data
public class ProjectApprovalEvaluationDTO {
	
	private String comment;
	 
	private ProjectApprovalEvaluationEnum projectApprovalEvaluationEnum;

	private Long projectId;
}