package ec.edu.espe.gpi.dto.registry;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;

@Data
public class CollegeCareerDTO {

	@NotNull(message = "El Id del Departamento Universitario esta vacio")
	private Long collegeDepartmentId;

	private Long id;

	@NotBlank(message = "El nombre esta vacio")
	private String name;
}