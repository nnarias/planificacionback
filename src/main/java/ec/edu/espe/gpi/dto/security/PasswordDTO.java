package ec.edu.espe.gpi.dto.security;

import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
public class PasswordDTO {

	@NotBlank(message = "La nueva contrase�a est� vac�a")
	private String newPassword;

	@NotBlank(message = "La confirmaci�n de la nueva contrase�a est� vac�a")
	private String newPassword2;

	@NotBlank(message = "La contrase�a antigua est� vac�a")
	private String oldPassword;
}