package ec.edu.espe.gpi.dto.evaluation;

import lombok.Data;

@Data
public class AssignEvaluatorDTO {

	private String departamentalEvaluator;

	private String evaluator;

	private Long projectId;
}