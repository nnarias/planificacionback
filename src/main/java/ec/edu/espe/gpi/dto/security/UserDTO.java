package ec.edu.espe.gpi.dto.security;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.security.Role;
import lombok.Data;

@Data
public class UserDTO {

	@NotNull(message = "La fecha esta vac�a")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date birthDate;

	@NotBlank(message = "El correo esta vac�o")
	@Email(message = "El correo es incorrecto")
	private String email;
	
	private Long id;

	@NotBlank(message = "El apellido esta vac�o")
	private String lastName;

	@NotBlank(message = "El nombre esta vac�o")
	private String name;

	private List<Role> roleList = new ArrayList<>();
}