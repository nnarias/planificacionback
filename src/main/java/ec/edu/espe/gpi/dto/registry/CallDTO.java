package ec.edu.espe.gpi.dto.registry;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
public class CallDTO {
	private List<Long> areaList;

	private Long callAreaId;
	private List<CallRequirementDTO> callRequirementList;

	private String description;

	@NotNull(message = "La fecha de finalización esta vacia")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date endDate;

	private Long evaluationId;
	private Boolean extension;
	private Double finacingAmount;
	private Long id;

	@NotBlank(message = "El nombre esta vacio")
	private String name;

	@NotNull(message = "La fecha de inicio esta vacia")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	private Date startDate;
}