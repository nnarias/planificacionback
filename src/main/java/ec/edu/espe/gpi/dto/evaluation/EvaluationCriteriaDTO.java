package ec.edu.espe.gpi.dto.evaluation;

import java.util.List;

import lombok.Data;

@Data
public class EvaluationCriteriaDTO {
	private CriteriaDTO criteria;
	private List<EvaluationCriteriaSubcriteriaDTO> evaluationCriteriaSubcriteriaList;
	private Long evaluationId;
	private Integer grade;
	private Long id;
	private Boolean state;
}