package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROSTSER", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPROSTSER_NAME", name = "UK_UZITPROSTSER_NAME"))
public class ProjectStateServer implements Serializable {

	@Id
	@Column(name = "UZITPROSTSER_CODE")
	@GeneratedValue(generator = "ProjectStateServer_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectStateServer_Sequence", sequenceName = "UZISPROSTSER_CODE")
	private Long id;

	@Column(name = "UZITPROSTSER_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPROSTSER_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}