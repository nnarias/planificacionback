package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITECPARGO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITECPARGO_NAME", name = "UK_UZITECPARGO_NAME"))
public class EconomicPartnerGoal implements Serializable {

	@Id
	@Column(name = "UZITECPARGO_CODE")
	@GeneratedValue(generator = "EconomicPartnerGoal_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "EconomicPartnerGoal_Sequence", sequenceName = "UZISECPARGO_CODE")
	private Long id;

	@Column(name = "UZITECPARGO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITECPARGO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}