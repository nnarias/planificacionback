package ec.edu.espe.gpi.model.books;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITBOOKS")
public class Books implements Serializable {

	@Id
	@Column(name = "UZITBOOKS_CODE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Books_Sequence")
	@SequenceGenerator(name = "Books_Sequence", sequenceName = "UZISBOOKS_CODE", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA PLANIFICACION
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITBOOKS_UZITPLANNING"), name = "UZITPLANNING_ID", nullable = true, referencedColumnName = "UZITPLANNING_ID")
	private Planning planning;
	//FIN CLAVE FORANEA
	
	@Column(name = "UZITBOOKS_CODIES")
	private String codIES;
	
	@Column(name = "UZITBOOKS_TIPPUB")
	private String publicationType;
	
	@Column(name = "UZITBOOKS_CODPUB")
	private String codPUB;
	
	@Column(name = "UZITBOOKS_BOOTIT")
	private String bookTitle;
	
	@Column(name = "UZITBOOKS_CODISB")
	private String codISB;
	
	@Column(name = "UZITBOOKS_PUB_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	 
	@Column(name = "UZITBOOKS_DETFIE")
	private String detailField;
	
	@Column(name = "UZITBOOKS_PEEREV")
	private String peerReviewed;
	
	@Column(name = "UZITBOOKS_FILIAT")
	private Boolean filiation;
	
	@Column(name = "UZITBOOKS_COMPETT")
	private String competitor;
		
	@Column(name = "UZITBOOKS_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITBOOKS_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITBOOKS_DURATI")
	private Long duration;
	
	@Column(name = "UZITBOOKS_APROBA")
	private String aprobation;
	
	@Column(name = "UZITBOOKS_OBSERV")
	private String observations;
	
	private static final long serialVersionUID = 1L;

	public Books(Long id, Planning planning, String codIES, String publicationType, String codPUB, String bookTitle,
			String codISB, Date publicationDate, String detailField, String peerReviewed, Boolean filiation,
			String competitor, Boolean complete, Boolean removed, Long duration, String aprobation, String observations ) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.publicationType = publicationType;
		this.codPUB = codPUB;
		this.bookTitle = bookTitle;
		this.codISB = codISB;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.peerReviewed = peerReviewed;
		this.filiation = filiation;
		this.competitor = competitor;
		this.complete = complete;
		this.removed = removed;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}

	public Books() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
