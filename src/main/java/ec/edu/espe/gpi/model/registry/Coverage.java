package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCOVER")
public class Coverage implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCOVER_UZITCOVERTYP"), name = "UZITCOVERTYP_CODE", nullable = false, referencedColumnName = "UZITCOVERTYP_CODE")
	private CoverageType coverageType;

	@Id
	@Column(name = "UZITCOVER_CODE")
	@GeneratedValue(generator = "Coverage_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Coverage_Sequence", sequenceName = "UZISCOVER_CODE")
	private Long id;

	@Column(name = "UZITCOVER_NAME", nullable = false)
	private String name;

	@Column(name = "UZITCOVER_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}
