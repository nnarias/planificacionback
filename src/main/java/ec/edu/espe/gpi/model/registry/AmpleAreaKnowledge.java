package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITAMPLAKNO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITAMPLAKNO_NAME", name = "UK_UZITAMPLAKNO_NAME"))
public class AmpleAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITAMPLAKNO_CODE")
	@GeneratedValue(generator = "AmpleAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "AmpleAreaKnowledge_Sequence", sequenceName = "UZISAMPLAKNO_CODE")
	private Long id;

	@Column(name = "UZITAMPLAKNO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITAMPLAKNO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}