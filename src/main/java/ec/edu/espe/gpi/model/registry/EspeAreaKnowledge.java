package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITESPEAKNO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITESPEAKNO_NAME", name = "UK_UZITESPEAKNO_NAME"))
public class EspeAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITESPEAKNO_CODE")
	@GeneratedValue(generator = "EspeAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "EspeAreaKnowledge_Sequence", sequenceName = "UZISESPEAKNO_CODE")
	private Long id;

	@Column(name = "UZITESPEAKNO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITESPEAKNO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}