package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROGRA", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPROGRA_NAME", name = "UK_UZITPROGRA_NAME"))
public class Program implements Serializable {

	@Id
	@Column(name = "UZITPROGRA_CODE")
	@GeneratedValue(generator = "Program_Sequence",strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1,name = "Program_Sequence", sequenceName = "UZISPROGRA_CODE")
	private Long id;

	@Column(name = "UZITPROGRA_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPROGRA_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}