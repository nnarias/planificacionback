package ec.edu.espe.gpi.model.users;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.planning.Book;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITUSERS")
public class Users implements Serializable {
	
	@Id
	@Column(name = "UZITUSERS_ID", nullable = false)
	private String id;
	
	@Column(name = "UZITUSERS_IDCARD", nullable = false)
	private String idCard;
	
	@Column(name = "UZITUSERS_NAME", nullable = false)
	private String name;
	
	@Column(name = "UZITUSERS_LASNAM", nullable = false)
	private String lastName;
	
	@Column(name = "UZITUSERS_DEPART", nullable = false)
	private String department;
	
	@Column(name = "UZITUSERS_CAMPUS", nullable = false)
	private String campus;
	
	@Column(name = "UZITUSERS_TYPE", nullable = false)
	private String type;
	
	@Column(name = "UZITUSERS_ACADEG", nullable = false)
	private String academicDegree;
	
	public Users() {
		
	}


	public Users(String id, String idCard, String name, String lastName, String department, String campus, String type,
			String academicDegree) {
		super();
		this.id = id;
		this.idCard = idCard;
		this.name = name;
		this.lastName = lastName;
		this.department = department;
		this.campus = campus;
		this.type = type;
		this.academicDegree = academicDegree;
	}



}
