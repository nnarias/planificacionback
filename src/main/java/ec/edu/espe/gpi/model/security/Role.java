package ec.edu.espe.gpi.model.security;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITROLE", uniqueConstraints = @UniqueConstraint(columnNames = "UZITROLE_ROLENAM", name = "UK_UZITROLE_ROLENAM"))
public class Role implements Serializable {

	@Id
	@Column(name = "UZITROLE_CODE")
	private Long id;

	@Column(name = "UZITROLE_NAME", nullable = false)
	private String name;

	@Column(length = 25, name = "UZITROLE_ROLENAM", nullable = false)
	private String roleName;

	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITROLE_UZITROUTE"), inverseForeignKey = @ForeignKey(name = "FK_UZITROUTE_UZITROLE"), inverseJoinColumns = @JoinColumn(name = "UZITROUTE_CODE"), joinColumns = @JoinColumn(name = "UZITROLE_CODE"), name = "UZITROLEROUTE", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITROLE_CODE", "UZITROUTE_CODE" }, name = "UK_UZITROLEROUTE") })
	private List<Route> routeList;

	@JsonIgnoreProperties("roleList")
	@ManyToMany(cascade = CascadeType.ALL, mappedBy = "roleList")
	private List<UserModel> userModelList;

	private static final long serialVersionUID = 1L;
}