package ec.edu.espe.gpi.model.tracking;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.edu.espe.gpi.utils.tracking.GoalActivityEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITGOALACTI")
public class GoalActivity implements Serializable {

	// RECURSOS DE LA ACTVIDAD
	@JsonIgnoreProperties("goalActivity")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "goalActivity")
	private List<ActivityResource> activityResourceList;

	@Column(name = "UZITGOALACTI_CRTBUD")
	private Double currentBudget;

	// nombre del entregable
	@Column(name = "UZITGOALACTI_DELI")
	private String deliverable;

	@Column(name = "UZITGOALACTI_DELI_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date deliverableDate;

	@Column(name = "UZITGOALACTI_DELI_DESCRIP")
	private String deliverableDescription;

	@Column(name = "UZITGOALACTI_DELI_FILENAME")
	private String deliverableFileName;

	@Column(name = "UZITGOALACTI_DELI_FILEUUID")
	private String deliverableFileUUID;

	@Column(name = "UZITGOALACTI_END_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = "EST")
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Id
	@Column(name = "UZITGOALACTI_CODE")
	@GeneratedValue(generator = "GoalActivity_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "GoalActivity_Sequence", sequenceName = "UZISGOALACTI_CODE")
	private Long id;

	@Column(name = "UZITGOALACTI_INVBUD")
	private Double investmentBudget;

	@Column(name = "UZITGOALACTI_NAME", nullable = false)
	private String name;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITGOALACTI_UZITPROJGOAL"), name = "UZITPROJGOAL_CODE", nullable = false, referencedColumnName = "UZITPROJGOAL_CODE")
	private ProjectGoal projectGoal;

	@Column(name = "UZITGOALACTI_START_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column(name = "UZITGOALACTI_STATE", nullable = false)
	private GoalActivityEnum state;

	private static final long serialVersionUID = 1L;
}