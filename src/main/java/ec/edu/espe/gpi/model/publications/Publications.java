package ec.edu.espe.gpi.model.publications;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPUBLICATIONS")
public class Publications implements Serializable{
	
	@Id
	@Column(name = "UZITPUBLICATIONS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Publications_Sequence")
	@SequenceGenerator(name = "Publications_Sequence", sequenceName = "UZISPUBLICATIONS_ID", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA PLANIFICACION
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPUB_UZITPLANNING"), name = "UZITPLANNING_ID", nullable = true, referencedColumnName = "UZITPLANNING_ID")
	private Planning planning;
	//FIN CLAVE FORANEA
	
	@Column(name = "UZITPUBLICATIONS_CODIES")
	private String codIES;
	
	@Column(name = "UZITPUBLICATIONS_TYPE")
	private String type;
	
	@Column(name = "UZITPUBLICATIONS_TYPART")
	private String articleType;
	
	@Column(name = "UZITPUBLICATIONS_CODPUB")
	private String codPUB;
	
	@Column(name = "UZITPUBLICATIONS_TITLE")
	private String title;
	
	@Column(name = "UZITPUBLICATIONS_INDBAS")
	private String indexBase;
	
	@Column(name = "UZITPUBLICATIONS_CODEISS")
	private String codeISS;
	
	@Column(name = "UZITPUBLICATIONS_MAGNAME")
	private String magazineName;
	
	@Column(name = "UZITPUBLICATIONS_QUARTI")
	private String quartile;
	
	@Column(name = "UZITPUBLICATIONS_MAGNUM")
	private String magazineNumber;
	
	@Column(name = "UZITPUBLICATIONS_SJR")
	private String sjr;
	
	@Column(name = "UZITPUBLICATIONS_PUBDAT")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	
	@Column(name = "UZITPUBLICATIONS_DETFIE")
	private String detailField;
	
	@Column(name = "UZITPUBLICATIONS_CONDIT")
	private String condition;
	
	@Column(name = "UZITPUBLICATIONS_PUBLIN")
	private String publicationLink;
	
	@Column(name = "UZITPUBLICATIONS_MAGLIN")
	private String magazineLink;
	
	@Column(name = "UZITPUBLICATIONS_FILIAT")
	private Boolean filiation;
	
	@Column(name = "UZITPUBLICATIONS_COMPETT")
	private String competitor;
	
	@Column(name = "UZITPUBLICATIONS_OBSERV")
	private String observation;
	
	@Column(name = "UZITPUBLICATIONS_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITPUBLICATIONS_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITPUBLICATIONS_DURATI")
	private Long duration;
	
	@Column(name = "UZITPUBLICATIONS_APROBA")
	private String aprobation;
	
	@Column(name = "UZITPUBLICATIONS_OBSERT")
	private String observations;
	
	private static final long serialVersionUID = 1L;

	public Publications(Long id, Planning planning, String codIES, String type, String articleType, String codPUB,
			String title, String indexBase, String codeISS, String magazineName, String quartile, String magazineNumber,
			String sJR, Date publicationDate, String detailField, String condition, String publicationLink,
			String magazineLink, Boolean filiation, String competitor, String observation, Boolean complete,
			Boolean removed, Long duration, String aprobation, String observations) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.type = type;
		this.articleType = articleType; 
		this.codPUB = codPUB;
		this.title = title;
		this.indexBase = indexBase;
		this.codeISS = codeISS;
		this.magazineName = magazineName;
		this.quartile = quartile;
		this.magazineNumber = magazineNumber;
		this.sjr = sJR;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.condition = condition;
		this.publicationLink = publicationLink;
		this.magazineLink = magazineLink;
		this.filiation = filiation;
		this.competitor = competitor;
		this.observation = observation;
		this.complete = complete;
		this.removed = removed;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}

	public Publications() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
