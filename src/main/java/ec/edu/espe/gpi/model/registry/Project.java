package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.edu.espe.gpi.model.evaluation.ProjectEvaluation;
import ec.edu.espe.gpi.model.tracking.ProjectGoal;
import ec.edu.espe.gpi.utils.registry.ProjectStateEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJEC", uniqueConstraints = { @UniqueConstraint(columnNames = { "UZITPROJEC_NAME_SPANISH",
		"UZITPROJEC_NAME_ENGLISH" }, name = "UK_UZITPROJEC") })
public class Project implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITAMPLAKNO"), name = "UZITAMPLAKNO_CODE", nullable = false, referencedColumnName = "UZITAMPLAKNO_CODE")
	private AmpleAreaKnowledge ampleAreaKnowledge;

	@Lob
	@Column(name = "UZITPROJEC_ANCKNOW")
	private String ancestralKnowledge;

	// BIBLIOGRAFÍA
	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<Bibliography> bibliographyList;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITCALL"), name = "UZITCALL_CODE", nullable = false, referencedColumnName = "UZITCALL_CODE")
	private Call call;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITCOLCAR"), name = "UZITCOLCAR_CODE", nullable = false, referencedColumnName = "UZITCOLCAR_CODE")
	private CollegeCareer collegeCareer;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITCOLDEP"), name = "UZITCOLDEP_CODE", nullable = false, referencedColumnName = "UZITCOLDEP_CODE")
	private CollegeDepartment collegeDepartment;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITCOVER"), inverseForeignKey = @ForeignKey(name = "FK_UZITCOVER_UZITPROJEC"), inverseJoinColumns = @JoinColumn(name = "UZITCOVER_CODE"), joinColumns = @JoinColumn(name = "UZITPROJEC_CODE"), name = "UZITPROJCOVER", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITPROJEC_CODE", "UZITCOVER_CODE" }, name = "UK_UZITPROJCOVER") })
	private List<Coverage> coverageList;

	@Column(name = "UZITPROJEC_CRE_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@Lob
	@Column(name = "UZITPROJEC_CRFACSUCC")
	private String criticalFactorSuccess;

	@Lob
	@Column(name = "UZITPROJEC_CURRSIT")
	private String currentSituation;

	// Nombre de usuario del evaluador departamental del proyecto
	@Column(name = "UZITPROJEC_DEPEVA")
	private String departmentalEvaluator;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITDETAAKNO"), name = "UZITDETAAKNO_CODE", nullable = false, referencedColumnName = "UZITDETAAKNO_CODE")
	private DetailedAreaKnowledge detailedAreaKnowledge;

	@Lob
	@Column(name = "UZITPROJEC_DIFFUSION")
	private String diffusion;

	@Column(name = "UZITPROJEC_DURATIONM", nullable = false)
	private Integer durationMonth;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITECPARGO"), name = "UZITECPARGO_CODE", nullable = false, referencedColumnName = "UZITECPARGO_CODE")
	private EconomicPartnerGoal economicPartnerGoal;

	@Column(name = "UZITPROJEC_END_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Lob
	@Column(name = "UZITPROJEC_ENVIIMPANALY")
	private String environmentalImpactAnalysis;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITESPEAKNO"), name = "UZITESPEAKNO_CODE", nullable = false, referencedColumnName = "UZITESPEAKNO_CODE")
	private EspeAreaKnowledge espeAreaKnowledge;

	@Column(name = "UZITPROJEC_ESPECURRB")
	private Double espeCurrentRiskBudget;

	@Column(name = "UZITPROJEC_ESPEINVRB")
	private Double espeInvestmentRiskBudget;

	@Column(name = "UZITPROJEC_EVA")
	private Integer evaluation;

	// Nombre de usuario del supervisor financiero del proyecto
	@Column(name = "UZITPROJEC_FINSUP")
	private String financialSupervisor;

	@Id
	@Column(name = "UZITPROJEC_CODE")
	@GeneratedValue(generator = "Project_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Project_Sequence", sequenceName = "UZISPROJEC_CODE")
	private Long id;

	@Lob
	@Column(name = "UZITPROJEC_IMPECORES")
	private String impactEconomicResult;

	@Lob
	@Column(name = "UZITPROJEC_IMPOTHRES")
	private String impactOtherResult;

	@Lob
	@Column(name = "UZITPROJEC_IMPPOLRES")
	private String impactPoliticalResult;

	@Lob
	@Column(name = "UZITPROJEC_IMPSCIRES")
	private String impactScientificResult;

	@Lob
	@Column(name = "UZITPROJEC_IMPSOCRES")
	private String impactSocialResult;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITPROJINST"), inverseForeignKey = @ForeignKey(name = "FK_UZITPROJINST_UZITPROJEC"), inverseJoinColumns = @JoinColumn(name = "UZITINSTINV_CODE"), joinColumns = @JoinColumn(name = "UZITPROJEC_CODE"), name = "UZITPROJINST", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITPROJEC_CODE", "UZITINSTINV_CODE" }, name = "UK_UZITPROJINST") })
	private List<InstitutionInvolved> institutionInvolvedList;

	@Lob
	@Column(name = "UZITPROJEC_INTELLEPRO")
	private String intellectualProperty;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITLINRES"), name = "UZITLINRES_CODE", nullable = false, referencedColumnName = "UZITLINRES_CODE")
	private LineResearch lineResearch;

	@Column(name = "UZITPROJEC_NAME_ENGLISH", nullable = false)
	private String nameEnglish;

	@Column(name = "UZITPROJEC_NAME_SPANISH", nullable = false)
	private String nameSpanish;

	@Column(name = "UZITPROJEC_NUMPAROTHER")
	private Integer numberParticipantOther;

	@Column(name = "UZITPROJEC_NUMPARPROFEMA")
	private Integer numberParticipantProfessorFemale;

	@Column(name = "UZITPROJEC_NUMPARPROMALE")
	private Integer numberParticipantProfessorMale;

	@Column(name = "UZITPROJEC_NUMPARSTUFEMA")
	private Integer numberParticipantStudentFemale;

	@Column(name = "UZITPROJEC_NUMPARSTUMALE")
	private Integer numberParticipantStudentMale;

	@Column(name = "UZITPROJEC_NUMPOPDISPERS")
	private Integer numberPopulationDisabledPerson;

	@Column(name = "UZITPROJEC_NUMPOPFEMA")
	private Integer numberPopulationFemale;

	@Column(name = "UZITPROJEC_NUMPOPINDE")
	private Integer numberPopulationIndirect;

	@Column(name = "UZITPROJEC_NUMPOPMALE")
	private Integer numberPopulationMale;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITPOLICYPL"), inverseForeignKey = @ForeignKey(name = "FK_UZITPOLICYPL_UZITPROJEC"), inverseJoinColumns = @JoinColumn(name = "UZITPOLICYPL_CODE"), joinColumns = @JoinColumn(name = "UZITPROJEC_CODE"), name = "UZITPROJPOLI", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITPROJEC_CODE", "UZITPOLICYPL_CODE" }, name = "UK_UZITPROJPOLI") })
	private List<PolicyPlan> policyPlanList;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITPOSPRO"), name = "UZITPOSPRO_CODE", nullable = false, referencedColumnName = "UZITPOSPRO_CODE")
	private PostgraduateProgram postgraduateProgram;

	@Lob
	@Column(name = "UZITPROJEC_PROBLDIAG")
	private String problemDiagnosis;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITPROGRA"), name = "UZITPROGRA_CODE", nullable = false, referencedColumnName = "UZITPROGRA_CODE")
	private Program program;

	// ANEXOS
	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectAnnex> projectAnnexList;

	@Lob
	@Column(name = "UZITPROJEC_PROJBASE")
	private String projectBase;

	// Nombre de usuario del director del proyecto
	@Column(name = "UZITPROJEC_DIRECTOR")
	private String projectDirector;

	// EVALUACIÓN DEL PROYECTO
	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectEvaluation> projectEvaluationList;

	// Nombre de usuario del evaluador del proyecto
	@Column(name = "UZITPROJEC_EVALUA")
	private String projectEvaluator;

	// OBJETIVOS DEL PROYECTO
	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectGoal> projectGoalList;

	// ARCHIVO PERFIL DEL PROYECTO
	@Column(name = "UZITPROJEC_FILEPROFILE")
	private String projectProfile;

	// REQUISITOS
	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectRequirement> projectRequirementList;

	@JsonIgnoreProperties("project")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
	private List<ProjectResponsible> projectResponsibleList;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITPROSTA"), name = "UZITPROSTA_CODE", nullable = false, referencedColumnName = "UZITPROSTA_CODE")
	private ProjectStatus projectStatus;

	@Lob
	@Column(name = "UZITPROJEC_PROTOTY")
	private String prototype;

	@Lob
	@Column(name = "UZITPROJEC_REAPROJEXE")
	private String reasonProjectExecution;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITRESGRO"), name = "UZITRESGRO_CODE", nullable = false, referencedColumnName = "UZITRESGRO_CODE")
	private ResearchGroup researchGroup;

	// METODOLOGÍA PARA INVESTIGACIÓN
	@Lob
	@Column(name = "UZITPROJEC_RESEMETHO")
	private String researchMethodology;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITRESTYP"), name = "UZITRESTYP_CODE", nullable = false, referencedColumnName = "UZITRESTYP_CODE")
	private ResearchType researchType;

	@Lob
	@Column(name = "UZITPROJEC_RESTASSUMP")
	private String restrictionAssumption;

	@Lob
	@Column(name = "UZITPROJEC_SCIENTARTI")
	private String scientificArticle;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITSCIDIS"), name = "UZITSCIDIS_CODE", nullable = false, referencedColumnName = "UZITSCIDIS_CODE")
	private ScientificDiscipline scientificDiscipline;

	@Lob
	@Column(name = "UZITPROJEC_SOCSUSTA")
	private String socialSustainability;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITSPEAKNOW"), name = "UZITSPEAKNOW_CODE", nullable = false, referencedColumnName = "UZITSPEAKNOW_CODE")
	private SpecificAreaKnowledge specificAreaKnowledge;

	@Lob
	@Column(name = "UZITPROJEC_SPINOFF")
	private String spinOffs;

	@Column(name = "UZITPROJEC_SPONCURRB")
	private Double sponsorCurrentRiskBudget;

	@Column(name = "UZITPROJEC_SPONINVRB")
	private Double sponsorInvestmentRiskBudget;

	@Column(name = "UZITPROJEC_START_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column(name = "UZITPROJEC_STATE", nullable = false)
	private ProjectStateEnum state;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJEC_UZITSUNEAKNO"), name = "UZITSUNEAKNO_CODE", nullable = false, referencedColumnName = "UZITSUNEAKNO_CODE")
	private SubUnescoAreaKnowledge subUnescoAreaKnowledge;

	// Nombre de usuario del supervisor técnico del proyecto
	@Column(name = "UZITPROJEC_TECHSUP")
	private String technicalSupervisor;

	// VIABILIDAD Y PLAN DE SOSTENIBILIDAD
	@Lob
	@Column(name = "UZITPROJEC_TECHVIAB")
	private String technicalViability;

	@Lob
	@Column(name = "UZITPROJEC_TECHEQUIP")
	private String technologicalEquipment;

	@Lob
	@Column(name = "UZITPROJEC_TECHNTRANS")
	private String technologyTransfer;

	@Lob
	@Column(name = "UZITPROJEC_UNINCONSE")
	private String unintentionalConsequences;

	private static final long serialVersionUID = 1L;
}