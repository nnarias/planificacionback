package ec.edu.espe.gpi.model.tracking;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITRES")
public class Resource implements Serializable {

	@Id
	@Column(name = "UZITRES_CODE")
	@GeneratedValue(generator = "Resource_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Resource_Sequence", sequenceName = "UZISRES_CODE")
	private Long id;

	@Column(name = "UZITRES_NAME", nullable = false)
	private String name;

	@Column(name = "UZITRES_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}