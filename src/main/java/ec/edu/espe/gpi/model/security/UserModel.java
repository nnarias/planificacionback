package ec.edu.espe.gpi.model.security;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITUSER", uniqueConstraints = @UniqueConstraint(columnNames = { "UZITUSER_EMAIL",
		"UZITUSER_USERNAME" }, name = "UK_UZITUSER"))
public class UserModel implements Serializable {

	@Column(name = "UZITUSER_APPRO_PROJ", nullable = false)
	private Long approvedProjectNumber;

	@Column(name = "UZITUSER_BIRTH_DATE", nullable = false)
	@Temporal(TemporalType.DATE)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	private java.util.Date birthDate;

	@Column(name = "UZITUSER_CREA_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@Column(name = "UZITUSER_DISAPPRO_PROJ", nullable = false)
	private Long disapprovedProjectNumber;

	@Column(name = "UZITUSER_EMAIL", nullable = false)
	private String email;

	@Id
	@Column(name = "UZITUSER_CODE")
	@GeneratedValue(generator = "User_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "User_Sequence", sequenceName = "UZISUSER_CODE")
	private Long id;

	@Column(name = "UZITUSER_LASTNAME", nullable = false)
	private String lastName;

	@Column(name = "UZITUSER_LOGIN", nullable = false)
	private Boolean login;

	@Column(name = "UZITUSER_NAME", nullable = false)
	private String name;

	@Column(length = 60, name = "UZITUSER_PASSWORD", nullable = false)
	private String password;

	@Column(name = "UZITUSER_PEND_PROJ", nullable = false)
	private Long pendingProjectNumber;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITUSER_UZITROLE"), inverseForeignKey = @ForeignKey(name = "FK_UZITROLE_UZITUSER"), inverseJoinColumns = @JoinColumn(name = "UZITROLE_CODE"), joinColumns = @JoinColumn(name = "UZITUSER_CODE"), name = "UZITUSEROLE", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITUSER_CODE", "UZITROLE_CODE" }, name = "UK_UZITUSEROLE") })
	private List<Role> roleList;

	@Column(name = "UZITUSER_STATE", nullable = false)
	private Boolean state;

	@Column(length = 20, name = "UZITUSER_USERNAME", nullable = false)
	private String username;

	private static final long serialVersionUID = 1L;
}