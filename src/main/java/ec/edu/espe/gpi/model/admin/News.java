package ec.edu.espe.gpi.model.admin;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITNEWS")
public class News implements Serializable {

	@Column(name = "UZITNEWS_BODY", nullable = false)
	private String body;

	@Column(name = "UZITNEWS_END_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@Column(name = "UZITNEWS_FILEUUID", nullable = false)
	private String fileUUID;

	@Column(name = "UZITNEWS_FILENAME", nullable = false)
	private String fileName;

	@Id
	@Column(name = "UZITNEWS_CODE")
	@GeneratedValue(generator = "News_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "News_Sequence", sequenceName = "UZISNEWS_CODE")
	private Long id;

	@Column(name = "UZITNEWS_START_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column(name = "UZITNEWS_SUBTITLE", nullable = false)
	private String subtitle;

	@Column(name = "UZITNEWS_TITLE", nullable = false)
	private String title;

	private static final long serialVersionUID = 1L;
}