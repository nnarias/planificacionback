package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCALLREQ")
public class CallRequirement implements Serializable {
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCALLREQ_UZITCALL"), name = "UZITCALL_CODE", nullable = false, referencedColumnName = "UZITCALL_CODE")
	private Call call;

	@Id
	@Column(name = "UZITCALLREQ_CODE")
	@GeneratedValue(generator = "CallRequirement_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "CallRequirement_Sequence", sequenceName = "UZITCALLREQ_SEQ")
	private Long id;

	@Column(name = "UZITCALLREQ_REQ")
	private Boolean required;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCALLREQ_UZITREQ"), name = "UZITREQ_CODE", nullable = false, referencedColumnName = "UZITREQ_CODE")
	private Requirement requirement;

	@Column(name = "UZITCALLREQ_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}