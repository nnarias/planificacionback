package ec.edu.espe.gpi.model.tracking;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROINVO")
public class ProjectInvoice implements Serializable {

	@Column(name = "UZITPROINVO_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date date;

	@Column(name = "UZITPROINVO_FILENAME", nullable = false)
	private String fileName;

	@Column(name = "UZITPROINVO_FILEUUID", nullable = false)
	private String fileUUID;

	@Id
	@Column(name = "UZITPROINVO_CODE")
	@GeneratedValue(generator = "ProjectInvoice_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectInvoice_Sequence", sequenceName = "UZISPROINVO_CODE")
	private Long id;

	private static final long serialVersionUID = 1L;
}