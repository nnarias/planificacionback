package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPOSPRO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPOSPRO_NAME", name = "UK_UZITPOSPRO_NAME"))
public class PostgraduateProgram implements Serializable {

	@Id
	@Column(name = "UZITPOSPRO_CODE")
	@GeneratedValue(generator = "PostgraduateProgram_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "PostgraduateProgram_Sequence", sequenceName = "UZISPOSPRO_CODE")
	private Long id;

	@Column(name = "UZITPOSPRO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPOSPRO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}