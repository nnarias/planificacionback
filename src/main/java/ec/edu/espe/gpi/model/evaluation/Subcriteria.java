package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITSUBCRI")
public class Subcriteria implements Serializable {

	@Id
	@Column(name = "UZITSUBCRI_CODE")
	@GeneratedValue(generator = "SubCriteria_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "SubCriteria_Sequence", sequenceName = "UZISSUBCRI_CODE")
	private Long id;

	@Lob
	@Column(name = "UZITSUBCRI_NAME", nullable = false)
	private String name;

	@Column(name = "UZITSUBCRI_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}