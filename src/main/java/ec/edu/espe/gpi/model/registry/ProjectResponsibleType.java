package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPRORESTY", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPRORESTY_NAME", name = "UK_UZITPRORESTY_NAME"))
public class ProjectResponsibleType implements Serializable {

	@Id
	@Column(name = "UZITPRORESTY_CODE")
	@GeneratedValue(generator = "ProjectResponsibleType_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectResponsibleType_Sequence", sequenceName = "UZISPRORESTY_CODE")
	private Long id;

	@Column(name = "UZITPRORESTY_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPRORESTY_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}