package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITINSTINV", uniqueConstraints = @UniqueConstraint(columnNames = "UZITINSTINV_NAME", name = "UK_UZITINSTINV_NAME"))
public class InstitutionInvolved implements Serializable {

	@Column(name = "UZITINSTINV_ADDRESS", nullable = false)
	private String address;

	@Column(name = "UZITINSTINV_MAIL")
	private String email;

	@Column(name = "UZITINSTINV_EXE")
	private String executingAgency;

	@Column(name = "UZITINSTINV_FAX")
	private String fax;

	@Id
	@Column(name = "UZITINSTINV_CODE")
	@GeneratedValue(generator = "InstitutionInvolved_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "InstitutionInvolved_Sequence", sequenceName = "UZISINSTINV_CODE")
	private Long id;

	@Column(name = "UZITINSTINV_LEGALREP")
	private String legalRepresentative;

	@Column(name = "UZITINSTINV_LEGALREPID")
	private String legalRepresentativeId;

	@Column(name = "UZITINSTINV_NAME", nullable = false)
	private String name;

	@Column(length = 20, name = "UZITINSTINV_PHONE")
	private String phone;

	@Column(name = "UZITEXECAGEN_STATE", nullable = false)
	private Boolean state;

	@Column(name = "UZITINSTINV_URL")
	private String url;

	private static final long serialVersionUID = 1L;
}