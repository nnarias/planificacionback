package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITEVACRISUB")
public class EvaluationCriteriaSubcriteria implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITEVACRISUB_UZITEVACRI"), name = "UZITEVACRI_CODE", nullable = false, referencedColumnName = "UZITEVACRI_CODE")
	private EvaluationCriteria evaluationCriteria;

	@Column(name = "UZITEVACRISUB_GRADE")
	private Integer grade;

	@Id
	@Column(name = "UZITEVACRISUB_CODE")
	@GeneratedValue(generator = "EvaluationCriteriaSubcriteria_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "EvaluationCriteriaSubcriteria_Sequence", sequenceName = "UZISEVACRISUB_CODE")
	private Long id;

	@Column(name = "UZITEVACRISUB_STATE", nullable = false)
	private Boolean state;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITEVACRISUB_UZITSUBCRI"), name = "UZITSUBCRI_CODE", nullable = false, referencedColumnName = "UZITSUBCRI_CODE")
	private Subcriteria subcriteria;

	private static final long serialVersionUID = 1L;
}