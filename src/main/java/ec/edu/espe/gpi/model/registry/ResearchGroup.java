package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITRESGRO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITRESGRO_NAME", name = "UK_UZITRESGRO_NAME"))
public class ResearchGroup implements Serializable {

	@Id
	@Column(name = "UZITRESGRO_CODE")
	@GeneratedValue(generator = "ResearchGroup_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ResearchGroup_Sequence", sequenceName = "UZISRESGRO_CODE")
	private Long id;

	@Column(name = "UZITRESGRO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITRESGRO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}