package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITGOALPL")
public class GoalPlan implements Serializable {

	@Id
	@Column(name = "UZITGOALPL_CODE")
	@GeneratedValue(generator = "GoalPlan_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "GoalPlan_Sequence", sequenceName = "UZISGOALPL_CODE")
	private Long id;

	@Lob
	@Column(name = "UZITGOALPL_NAME", nullable = false)
	private String name;

	@ManyToOne
	@JsonIgnoreProperties("goalPlanList")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITGOALPL_UZITPROJEC"), name = "UZITPROJEC_CODE", referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITGOALPL_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}