package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPOLICYPL")
public class PolicyPlan implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPOLICYPL_UZITGOALPL"), name = "UZITGOALPL_CODE", referencedColumnName = "UZITGOALPL_CODE")
	private GoalPlan goalPlan;

	@Id
	@Column(name = "UZITPOLICYPL_CODE")
	@GeneratedValue(generator = "PolicyPlan_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "PolicyPlan_Sequence", sequenceName = "UZISPOLICYPL_CODE")
	private Long id;

	@Lob
	@Column(name = "UZITPOLICYPL_NAME", nullable = false)
	private String name;

	@ManyToOne
	@JsonIgnoreProperties("policyPlanList")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPOLIPL_UZITPROJEC"), name = "UZITPROJEC_CODE", referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITPOLICYPL_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}