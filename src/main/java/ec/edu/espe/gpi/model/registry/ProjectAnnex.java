package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJANN")
public class ProjectAnnex implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJANN_UZITANN_CODE"), name = "UZITANN_CODE", nullable = false, referencedColumnName = "UZITANN_CODE")
	private Annex annex;

	@Column(name = "UZITPROJANN_DESCRIPTION")
	private String description;

	@Column(name = "UZITPROJANN_FILEUUID", nullable = false)
	private String fileUUID;

	@Column(name = "UZITPROJANN_FILENAME", nullable = false)
	private String fileName;

	@Id
	@Column(name = "UZITPROJANN_CODE")
	@GeneratedValue(generator = "ProjectAnnex_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectAnnex_Sequence", sequenceName = "UZISPROJANN_CODE")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJANN_UZITPROJEC"), name = "UZITPROJEC_CODE", nullable = false, referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITPROJANN_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}