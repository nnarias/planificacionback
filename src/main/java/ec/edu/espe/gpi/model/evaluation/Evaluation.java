package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITEVA", uniqueConstraints = @UniqueConstraint(columnNames = "UZITEVA_NAME", name = "UK_UZITEVA"))
public class Evaluation implements Serializable {

	@JsonIgnoreProperties("evaluation")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluation", orphanRemoval = true)
	private List<EvaluationCriteria> evaluationCriteriaList;

	@Id
	@Column(name = "UZITEVA_CODE")
	@GeneratedValue(generator = "Evaluation_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Evaluation_Sequence", sequenceName = "UZISEVA_CODE")
	private Long id;

	@Column(name = "UZITEVA_GRADE")
	private Integer grade;

	@Column(name = "UZITEVA_NAME", nullable = false)
	private String name;

	@Column(name = "UZITEVA_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}