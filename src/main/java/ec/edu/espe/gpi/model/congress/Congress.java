package ec.edu.espe.gpi.model.congress;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITCONGRESS")
public class Congress implements Serializable{
	@Id
	@Column(name = "UZITCONGRESS_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Congress_Sequence")
	@SequenceGenerator(name = "Congress_Sequence", sequenceName = "UZISCONGRESS_ID", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA PLANIFICACION
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCONGRESS_UZITPLANNING"), name = "UZITPLANNING_ID", nullable = true, referencedColumnName = "UZITPLANNING_ID")
	private Planning planning;
	//FIN CLAVE FORANEA
	
	@Column(name = "UZITCONGRESS_CODIES")
	private String codIES;
	
	@Column(name = "UZITCONGRESS_TYPE")
	private String type;
	
	@Column(name = "UZITCONGRESS_TYPART")
	private String articleType;
	
	@Column(name = "UZITCONGRESS_CODPUB")
	private String codPUB;
	
	@Column(name = "UZITCONGRESS_PRENAM")
	private String presentationName;
	
	@Column(name = "UZITCONGRESS_EVENAM")
	private String eventName;

	@Column(name = "UZITCONGRESS_EVEEDI")
	private Long eventEdition;
	
	@Column(name = "UZITCONGRESS_EVEORG")
	private String eventOrganizer;
	
	@Column(name = "UZITCONGRESS_ORGCOM")
	private String organizingCommite;
	
	@Column(name = "UZITCONGRESS_COUNTR")
	private String country;
	
	@Column(name = "UZITCONGRESS_CITY")
	private String city;
	
	@Column(name = "UZITCONGRESS_PUBDAT")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	
	@Column(name = "UZITCONGRESS_DETFIE")
	private String detailField;
	
	@Column(name = "UZITCONGRESS_COMPETT")
	private String competitor;
		
	@Column(name = "UZITCONGRESS_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITCONGRESS_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITCONGRESS_DURATI")
	private Long duration;
	
	@Column(name = "UZITCONGRESS_APROBA")
	private String aprobation;
	
	@Column(name = "UZITCONGRESS_OBSERV")
	private String observations;
	
	private static final long serialVersionUID = 1L;

	public Congress(Long id, Planning planning, String codeIES, String type, String articleType, String codPUB,
			String presentationName, String eventName, Long eventEdition, String eventOrganizer,
			String organizingCommite, String country, String city, Date publicationDate, String detailField,
			String competitor, Boolean complete, Boolean removed, Long duration, String aprobation, String observations ) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codeIES;
		this.type = type;
		this.articleType = articleType;
		this.codPUB = codPUB;
		this.presentationName = presentationName;
		this.eventName = eventName;
		this.eventEdition = eventEdition;
		this.eventOrganizer = eventOrganizer;
		this.organizingCommite = organizingCommite;
		this.country = country;
		this.city = city;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.competitor = competitor;
		this.complete = complete;
		this.removed = removed;
		this.duration = duration; 
		this.aprobation = aprobation;
		this.observations = observations; 
	}

	public Congress() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
