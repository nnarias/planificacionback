package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITBIBLIO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITBIBLIO_TITLE", name = "UK_UZITBIBLIO_TITLE"))
public class Bibliography implements Serializable {

	@Column(name = "UZITBIBLIO_AUTHOR")
	private String author;

	@Column(name = "UZITBIBLIO_CITY")
	private String city;

	@Column(name = "UZITBIBLIO_COUNTRY")
	private String country;

	@Column(name = "UZITBIBLIO_EDITORIAL")
	private String editorial;

	@Id
	@Column(name = "UZITBIBLIO_CODE")
	@GeneratedValue(generator = "Biblio_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Biblio_Sequence", sequenceName = "UZISBIBLIO_CODE")
	private Long id;

	@Column(name = "UZITBIBLIO_PAGE_NAME")
	private String pageName;

	@ManyToOne
	@JsonIgnoreProperties("bibliographyList")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITBIBLIO_UZITPROJEC"), name = "UZITPROJEC_CODE", referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITBIBLIO_STATE", nullable = false)
	private Boolean state;

	@Column(name = "UZITBIBLIO_TITLE", nullable = false)
	private String title;

	@Column(name = "UZITBIBLIO_URL")
	private String url;

	@Column(name = "UZITBIBLIO_WEBSITE", nullable = false)
	private Boolean website;

	@Column(name = "UZITBIBLIO_YEAR")
	private Integer year;

	private static final long serialVersionUID = 1L;
}