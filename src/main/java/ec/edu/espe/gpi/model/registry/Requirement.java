package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITREQ", uniqueConstraints = @UniqueConstraint(columnNames = "UZITREQ_NAME", name = "UK_UZITREQ_NAME"))
public class Requirement implements Serializable {

	@Id
	@Column(name = "UZITREQ_CODE")
	@GeneratedValue(generator = "Requirement_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Requirement_Sequence", sequenceName = "UZISREQ_CODE")
	private Long id;

	@Column(name = "UZITREQ_NAME", nullable = false)
	private String name;

	@Column(name = "UZITREQ_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}