package ec.edu.espe.gpi.model.planning;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITBOOk")
public class Book implements Serializable{

	@Id
	@Column(name = "UZITBOOk_CODE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Book_Sequence")
	@SequenceGenerator(name = "Book_Sequence", sequenceName = "UZISBOOk_CODE", allocationSize = 1)
	private Long id;
	
	@Column(name = "UZITBOOk_CODIES", nullable = false)
	private Integer codies;
	
	@Column(name = "UZITBOOk_TIPPUB")
	private String publicationType;
	
	@Column(name = "UZITBOOk_CODEPUB")
	private String publicationCode;
	
	@Column(name = "UZITBOOk_PUB_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	
	@Column(name = "UZITBOOk_STATE", nullable = false)
	private Boolean state;
	
	private static final long serialVersionUID = 1L;

	public Book(Long id, Integer codies, String publicationCode, Date publicationDate, String publicationType, 
			Boolean state) {
		super();
		this.id = id;
		this.codies = codies;
		this.publicationType = publicationType;
		this.publicationCode = publicationCode;
		this.publicationDate = publicationDate;
		this.state = state;
	}

	public Book() {
		super();
	}

	
	
}
