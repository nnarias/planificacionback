package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCOLCAR", uniqueConstraints = @UniqueConstraint(columnNames = "UZITCOLCAR_NAME", name = "UK_UZITCOLCAR_NAME"))
public class CollegeCareer implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCOLCAR_UZITCOLDEP"), name = "UZITCOLDEP_CODE", referencedColumnName = "UZITCOLDEP_CODE")
	private CollegeDepartment collegeDepartment;
	
	@Id
	@Column(name = "UZITCOLCAR_CODE")
	@GeneratedValue(generator = "CollegeCareer_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "CollegeCareer_Sequence", sequenceName = "UZISCOLCAR_CODE")
	private Long id;

	@Column(name = "UZITCOLCAR_NAME", nullable = false)
	private String name;

	@Column(name = "UZITCOLCAR_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}