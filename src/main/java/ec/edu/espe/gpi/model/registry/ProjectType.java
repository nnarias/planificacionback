package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROTY", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPROTY_NAME", name = "UK_UZITPROTY_NAME"))
public class ProjectType implements Serializable {

	@Id
	@Column(name = "UZITPROTY_CODE")
	@GeneratedValue(generator = "ProjectType_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectType_Sequence", sequenceName = "UZISPROTY_CODE")
	private Long id;

	@Column(name = "UZITPROTY_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPROTY_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}