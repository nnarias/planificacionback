package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJREQ")
public class ProjectRequirement implements Serializable {

	@Column(name = "UZITPROJREQ_FILENAME", nullable = false)
	private String fileName;

	@Column(name = "UZITPROJREQ_FILEUUID", nullable = false)
	private String fileUUID;

	@Id
	@Column(name = "UZITPROJREQ_CODE")
	@GeneratedValue(generator = "ProjectRequirement_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectRequirement_Sequence", sequenceName = "UZISPROJREQ_CODE")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJREQ_UZITPROJEC"), name = "UZITPROJEC_CODE", nullable = false, referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJREQ_UZITREQ"), name = "UZITREQ_CODE", nullable = false, referencedColumnName = "UZITREQ_CODE")
	private Requirement requirement;

	@Column(name = "UZITRES_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}