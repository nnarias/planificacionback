package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITLINRES", uniqueConstraints = @UniqueConstraint(columnNames = "UZITLINRES_NAME", name = "UK_UZITLINRES_NAME"))
public class LineResearch implements Serializable {

	@Id
	@Column(name = "UZITLINRES_CODE")
	@GeneratedValue(generator = "LineResearch_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "LineResearch_Sequence", sequenceName = "UZISLINRES_CODE")
	private Long id;

	@Column(name = "UZITLINRES_NAME", nullable = false)
	private String name;

	@Column(name = "UZITLINRES_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}