package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.edu.espe.gpi.model.evaluation.Evaluation;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITCALL", uniqueConstraints = @UniqueConstraint(columnNames = "UZITCALL_NAME", name = "UK_UZITCALL"))
public class Call implements Serializable {

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(foreignKey = @ForeignKey(name = "FK_UZITCALL_UZITAREA"), inverseForeignKey = @ForeignKey(name = "FK_UZITAREA_UZITCALL"), inverseJoinColumns = @JoinColumn(name = "UZITAREA_CODE"), joinColumns = @JoinColumn(name = "UZITCALL_CODE"), name = "UZITCALLAREA", uniqueConstraints = {
			@UniqueConstraint(columnNames = { "UZITAREA_CODE", "UZITCALL_CODE" }, name = "UK_UZITCALLAREA") })
	private List<Area> areaList;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCALL_UZITCALLAR"), name = "UZITCALLAR_CODE")
	private CallArea callArea;

	@Column(name = "UZITCALL_EXT_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date callExtension;

	// Requerimientos de convocatoria
	@JsonIgnoreProperties("call")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "call", orphanRemoval = true)
	private List<CallRequirement> callRequirementList;

	@Column(name = "UZITCALL_DESCRIPTION", nullable = false)
	private String description;

	@Column(name = "UZITCALL_END_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date endDate;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITCALL_UZITEVA"), name = "UZITEVA_CODE")
	private Evaluation evaluation;

	@Column(name = "UZITCALL_EVA_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date evaluationDate;

	@Column(name = "UZITCALL_EXTENSION", nullable = false)
	private Boolean extension;

	@Column(name = "UZITCALL_FINAMOUNT", nullable = false)
	private Double finacingAmount;

	@Id
	@Column(name = "UZITCALL_CODE")
	@GeneratedValue(generator = "Call_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Call_Sequence", sequenceName = "UZISCALL_CODE")
	private Long id;

	@Column(name = "UZITCALL_NAME", nullable = false)
	private String name;

	@Column(name = "UZITCALL_START_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date startDate;

	@Column(name = "UZITCALL_STATE", nullable = false)
	private Boolean state;

	@Column(name = "UZITCALL_SUPP")
	private String support;

	private static final long serialVersionUID = 1L;
}