package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITSPEAKNOW", uniqueConstraints = @UniqueConstraint(columnNames = "UZITSPEAKNOW_NAME", name = "UK_UZITSPEAKNOW_NAME"))
public class SpecificAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITSPEAKNOW_CODE")
	@GeneratedValue(generator = "SpecificAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "SpecificAreaKnowledge_Sequence", sequenceName = "UZISSPEAKNOW_CODE")
	private Long id;

	@Column(name = "UZITSPEAKNOW_NAME", nullable = false)
	private String name;

	@Column(name = "UZITSPEAKNOW_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}