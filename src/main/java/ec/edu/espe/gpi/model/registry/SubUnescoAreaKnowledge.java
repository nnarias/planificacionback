package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITSUNEAKNO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITSUNEAKNO_NAME", name = "UK_UZITSUNEAKNO_NAME"))
public class SubUnescoAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITSUNEAKNO_CODE")
	@GeneratedValue(generator = "SubUnescoAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "SubUnescoAreaKnowledge_Sequence", sequenceName = "UZISSUNEAKNO_CODE")
	private Long id;

	@Column(name = "UZITSUNEAKNO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITSUNEAKNO_STATE", nullable = false)
	private Boolean state;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITSUNEAKNO_UZITUNESAKNO"), name = "UZITUNESAKNO_CODE", nullable = false, referencedColumnName = "UZITUNESAKNO_CODE")
	private UnescoAreaKnowledge unescoAreaKnowledge;

	private static final long serialVersionUID = 1L;
}