package ec.edu.espe.gpi.model.bookchapters;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITBOOKCHAPTERS")
public class BookChapters implements Serializable{
	
	@Id
	@Column(name = "UZITBOOKCHAPTERS_CODE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BookChapters_Sequence")
	@SequenceGenerator(name = "BookChapters_Sequence", sequenceName = "UZISBOOKCHAPTERS_CODE", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA PLANIFICACION
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITBOOKCHAP_UZITPLANNING"), name = "UZITPLANNING_ID", nullable = true, referencedColumnName = "UZITPLANNING_ID")
	private Planning planning;
	//FIN CLAVE FORANEA
	
	@Column(name = "UZITBOOKCHAPTERS_CODIES")
	private String codIES;
	
	@Column(name = "UZITBOOKCHAPTERS_TIPPUB")
	private String publicationType;
	
	@Column(name = "UZITBOOKCHAPTERS_CHACOD")
	private String chapterCode;
	
	@Column(name = "UZITBOOKCHAPTERS_CODPUB")
	private String codPUB;
	
	@Column(name = "UZITBOOKCHAPTERS_CAPTIT")
	private String capTitle;
	
	@Column(name = "UZITBOOKCHAPTERS_BOOTIT")
	private String bookTitle;
	
	@Column(name = "UZITBOOKCHAPTERS_CODISB")
	private String codISB;
	
	@Column(name = "UZITBOOKCHAPTERS_EDITOR")
	private String editor;
	
	@Column(name = "UZITBOOKCHAPTERS_NUMPAG")
	private Long numberPages;
	
	@Column(name = "UZITBOOKCHAPTERS_PUB_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date publicationDate;
	
	@Column(name = "UZITBOOKCHAPTERS_DETFIE")
	private String detailField;
	
	@Column(name = "UZITBOOKCHAPTERS_FILIAT")
	private Boolean filiation;
	
	@Column(name = "UZITBOOKCHAPTERS_COMPETT")
	private String competitor;
		
	@Column(name = "UZITBOOKCHAPTERS_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITBOOKCHAPTERS_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITBOOKCHAPTERS_DURATI")
	private Long duration;
	
	@Column(name = "UZITBOOKCHAPTERS_APROBA")
	private String aprobation;
	
	@Column(name = "UZITBOOKCHAPTERS_OBSERV")
	private String observations;
	
	private static final long serialVersionUID = 1L;

	public BookChapters(Long id, Planning planning, String codIES, String publicationType, String chapterCode,
			String codPUB, String capTitle, String bookTitle, String codISB, String editor, Long numberPages,
			Date publicationDate, String detailField, Boolean filiation, String competitor, Boolean complete,
			Boolean removed, Long duration, String aprobation, String observations ) {
		super();
		this.id = id;
		this.planning = planning;
		this.codIES = codIES;
		this.publicationType = publicationType;
		this.chapterCode = chapterCode;
		this.codPUB = codPUB;
		this.capTitle = capTitle;
		this.bookTitle = bookTitle;
		this.codISB = codISB;
		this.editor = editor;
		this.numberPages = numberPages;
		this.publicationDate = publicationDate;
		this.detailField = detailField;
		this.filiation = filiation;
		this.competitor = competitor;
		this.complete = complete;
		this.removed = removed;
		this.duration = duration;
		this.aprobation = aprobation;
		this.observations = observations;
	}

	public BookChapters() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
