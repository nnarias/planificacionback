package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITANN", uniqueConstraints = @UniqueConstraint(columnNames = "UZITANN_NAME", name = "UK_UZITANN_NAME"))
public class Annex implements Serializable {

	@Id
	@Column(name = "UZITANN_CODE")
	@GeneratedValue(generator = "Annex_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Annex_Sequence", sequenceName = "UZISANN_CODE")
	private Long id;

	@Column(name = "UZITANN_NAME", nullable = false)
	private String name;

	@Column(name = "UZITANN_REQ", nullable = false)
	private Boolean required;

	@Column(name = "UZITANN_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}