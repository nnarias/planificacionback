package ec.edu.espe.gpi.model.admin;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.registry.Project;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJNOTI")
public class ProjectNotification {

	@Lob
	@Column(name = "UZITPROJNOTI_COMMENT", nullable = false)
	private String commentary;

	@Column(name = "UZITPROJNOTI_CREA_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@Id
	@Column(name = "UZITPROJNOTI_CODE")
	@GeneratedValue(generator = "ProjectNotification_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectNotification_Sequence", sequenceName = "UZISPROJNOTI_CODE")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJNOTI_UZITPROJEC"), name = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITPROJNOTI_TITLE", nullable = false)
	private String title;

	@Column(name = "UZITPROJNOTI_USER", nullable = false)
	private String user;

}