package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITAREA", uniqueConstraints = @UniqueConstraint(columnNames = "UZITAREA_NAME", name = "UK_UZITAREA_NAME"))
public class Area implements Serializable {

	@Id
	@Column(name = "UZITAREA_CODE")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Area_Sequence")
	@SequenceGenerator(name = "Area_Sequence", sequenceName = "UZISAREA_CODE", allocationSize = 1)
	private Long id;

	@Column(name = "UZITAREA_NAME", nullable = false)
	private String name;

	@Column(name = "UZITAREA_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}