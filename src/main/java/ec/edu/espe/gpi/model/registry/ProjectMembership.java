package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROMEM", uniqueConstraints = @UniqueConstraint(columnNames = "UZITPROMEM_NAME", name = "UK_UZITPROMEM_NAME"))
public class ProjectMembership implements Serializable {

	@Id
	@Column(name = "UZITPROMEM_CODE")
	@GeneratedValue(generator = "ProjectMembership_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectMembership_Sequence", sequenceName = "UZISPROMEM_CODE")
	private Long id;

	@Column(name = "UZITPROMEM_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPROMEM_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}