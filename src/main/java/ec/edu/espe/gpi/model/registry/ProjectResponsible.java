package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITPRORES")
public class ProjectResponsible implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPRORES_UZITCOLDEP"), name = "UZITCOLDEP_CODE", referencedColumnName = "UZITCOLDEP_CODE")
	private CollegeDepartment collegeDepartment;

	@Column(name = "UZITPRORES_EMAIL")
	private String email;

	@Column(name = "UZITPRORES_EXTERNAL_INST")
	private String externalInstitution;

	@Column(name = "UZITPRORES_EXTERNAL")
	private Boolean externalResponsible;

	@Id
	@Column(name = "UZITPRORES_CODE")
	@GeneratedValue(generator = "ProjectResponsible_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectResponsible_Sequence", sequenceName = "UZISPRORES_CODE")
	private Long id;

	@Column(name = "UZITPRORES_LASTNAME", nullable = false)
	private String lastname;

	@Column(name = "UZITPRORES_NAME", nullable = false)
	private String name;

	@Column(name = "UZITPRORES_NUMBERID")
	private String numberIdentification;

	@ManyToOne
	@JsonIgnoreProperties("projectResponsibleList")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPRORES_UZITPROJEC"), name = "UZITPROJEC_CODE", referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPRORES_UZITPRORESTY"), name = "UZITPRORESTY_CODE", referencedColumnName = "UZITPRORESTY_CODE")
	private ProjectResponsibleType projectResponsibleType;

	@Column(name = "UZITPRORES_STATE", nullable = false)
	private Boolean state;

	@Column(name = "UZITPRORES_TELEPHONE")
	private String telephone;

	private static final long serialVersionUID = 1L;
}