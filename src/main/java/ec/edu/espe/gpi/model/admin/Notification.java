package ec.edu.espe.gpi.model.admin;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITNOTIFI")
public class Notification implements Serializable {

	@Column(name = "UZITNOTIFI_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date date;

	@Lob
	@Column(name = "UZITNOTIFI_DESCRIP", nullable = false)
	private String description;

	@Id
	@Column(name = "UZITNOTIFI_CODE")
	@GeneratedValue(generator = "Notification_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Notification_Sequence", sequenceName = "UZISNOTIFI_CODE")
	private Long id;

	@Column(name = "UZITNOTIFI_REC_STATE", nullable = false)
	private Boolean recentState;

	@Column(name = "UZITNOTIFI_TITLE", nullable = false)
	private String title;

	@Column(name = "UZITNOTIFI_USER", nullable = false)
	private String user;

	private static final long serialVersionUID = 1L;
}