package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITDETAAKNO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITDETAAKNO_NAME", name = "UK_UZITDETAAKNO_NAME"))
public class DetailedAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITDETAAKNO_CODE")
	@GeneratedValue(generator = "DetailedAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "DetailedAreaKnowledge_Sequence", sequenceName = "UZISDETAAKNO_CODE")
	private Long id;

	@Column(name = "UZITDETAAKNO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITDETAAKNO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}