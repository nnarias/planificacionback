package ec.edu.espe.gpi.model.activities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.planning.Planning;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITACTIVITIES")
public class Activities implements Serializable {
	
	@Id
	@Column(name = "UZITACTIVITIES_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Activities_Sequence")
	@SequenceGenerator(name = "Activities_Sequence", sequenceName = "Activities_ID", allocationSize = 1)
	private Long id;
	
	@Column(name = "UZITACTIVITIES_NAME")
	private String name;
	
	@Column(name = "UZITACTIVITIES_DEGREE")
	private String degree;
	
	@Column(name = "UZITACTIVITIES_DURATI")
	private Long duration;
	
	@Column(name = "UZITACTIVITIES_MAXDUR")
	private Long maxDuration;
	
	@Column(name = "UZITACTIVITIES_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITACTIVITIES_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITACTIVITIES_APROBA")
	private String aprobation;
	
	private static final long serialVersionUID = 1L;
	

}
