package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITEVACRI")
public class EvaluationCriteria implements Serializable {

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITEVACRI_UZITCRI"), name = "UZITCRI_CODE", nullable = false, referencedColumnName = "UZITCRI_CODE")
	private Criteria criteria;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITEVACRI_UZITEVA"), name = "UZITEVA_CODE", nullable = false, referencedColumnName = "UZITEVA_CODE")
	private Evaluation evaluation;

	@JsonIgnoreProperties("evaluationCriteria")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "evaluationCriteria", orphanRemoval = true)
	private List<EvaluationCriteriaSubcriteria> evaluationCriteriaSubcriteriaList;

	@Column(name = "UZITEVACRI_GRADE")
	private Integer grade;

	@Id
	@Column(name = "UZITEVACRI_CODE")
	@GeneratedValue(generator = "EvaluationCriteria_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "EvaluationCriteria_Sequence", sequenceName = "UZISEVACRI_CODE")
	private Long id;

	@Column(name = "UZITEVACRI_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;

	public EvaluationCriteria() {
		this.evaluationCriteriaSubcriteriaList = new ArrayList<>();
	}
}