package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCRI", uniqueConstraints = @UniqueConstraint(columnNames = "UZITCRI_NAME", name = "UK_UZITCRI"))
public class Criteria implements Serializable{

	@Id
	@Column(name = "UZITCRI_CODE")
	@GeneratedValue(generator = "Criteria_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Criteria_Sequence", sequenceName = "UZISCRI_CODE")
	private Long id;
	
	@Column(name = "UZITCRI_NAME", nullable = false)
	private String name;
	
	@Column(name = "UZITCRI_STATE", nullable = false)
	private Boolean state;
	
	private static final long serialVersionUID = 1L;
}