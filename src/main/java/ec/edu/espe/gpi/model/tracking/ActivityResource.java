package ec.edu.espe.gpi.model.tracking;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.utils.tracking.ActivityResourceEnum;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITACTIRES")
public class ActivityResource implements Serializable {

	@Column(name = "UZITACTIRES_CRTBUD")
	private Double currentBudget;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITACTIRES_UZITGOALACTI"), name = "UZITGOALACTI_CODE", nullable = false, referencedColumnName = "UZITGOALACTI_CODE")
	private GoalActivity goalActivity;

	@Id
	@Column(name = "UZITACTIRES_CODE")
	@GeneratedValue(generator = "ActivityResource_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ActivityResource_Sequence", sequenceName = "UZISACTIRES_CODE")
	private Long id;

	@Column(name = "UZITACTIRES_INVBUD")
	private Double investmentBudget;

	@Column(name = "UZITACTIRES_INVO_CRTBUD")
	private Double invoiceCurrentBudget;

	@Column(name = "UZITACTIRES_INVO_DATE")
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date invoiceDate;

	@Column(name = "UZITACTIRESL_INVO_DESCRI")
	private String invoiceDescription;

	@Column(name = "UZITACTIRES_INVO_FILENAME")
	private String invoiceFileName;

	@Column(name = "UZITACTIRES_INVO_FILEUUID")
	private String invoiceFileUUID;

	@Column(name = "UZITACTIRES_INVO_INVBUD")
	private Double invoiceInvestmentBudget;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITACTIRES_UZITPROINVO"), name = "UZITPROINVO_CODE", referencedColumnName = "UZITPROINVO_CODE")
	private ProjectInvoice projectInvoice;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITACTIRES_UZITRES"), name = "UZITRES_CODE", nullable = false, referencedColumnName = "UZITRES_CODE")
	private Resource resource;

	@Column(name = "UZITRES_STATE", nullable = false)
	private ActivityResourceEnum state;

	private static final long serialVersionUID = 1L;
}