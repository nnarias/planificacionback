package ec.edu.espe.gpi.model.security;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITROUTE", uniqueConstraints = @UniqueConstraint(columnNames = "UZITROUTE_NAME", name = "UK_UZITROUTE_NAME"))
public class Route implements Serializable {

	@Column(name = "UZITROUTE_ICON", nullable = false)
	private String icon;

	@Id
	@Column(name = "UZITROUTE_CODE")
	private Long id;

	@JsonIgnoreProperties("RouteList")
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITROUTE_UZITMENU"), name = "UZITMENU_CODE", referencedColumnName = "UZITMENU_CODE")
	private Menu menu;

	@Column(name = "UZITROUTE_NAME", nullable = false)
	private String name;

	@Column(name = "UZITROUTE_CLASS")
	private String routeClass;

	@Column(name = "UZITROUTE_TITLE", nullable = false)
	private String title;

	private static final long serialVersionUID = 1L;
}