package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITRESTYP", uniqueConstraints = @UniqueConstraint(columnNames = "UZITRESTYP_NAME", name = "UK_UZITRESTYP_NAME"))
public class ResearchType implements Serializable {

	@Id
	@Column(name = "UZITRESTYP_CODE")
	@GeneratedValue(generator = "ResearchType_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ResearchType_Sequence", sequenceName = "UZISRESTYP_CODE")
	private Long id;

	@Column(name = "UZITRESTYP_NAME", nullable = false)
	private String name;

	@Column(name = "UZITRESTYP_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}