package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCOLDEP", uniqueConstraints = @UniqueConstraint(columnNames = "UZITCOLDEP_NAME", name = "UK_UZITCOLDEP_NAME"))
public class CollegeDepartment implements Serializable {

	@Id
	@Column(name = "UZITCOLDEP_CODE")
	@GeneratedValue(generator = "CollegeDepartment_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "CollegeDepartment_Sequence", sequenceName = "UZISCOLDEP_CODE")
	private Long id;

	@Column(name = "UZITCOLDEP_NAME", nullable = false)
	private String name;

	@Column(name = "UZITCOLDEP_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}