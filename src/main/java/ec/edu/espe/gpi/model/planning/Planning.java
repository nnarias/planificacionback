package ec.edu.espe.gpi.model.planning;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.evaluation.EvaluationCriteriaSubcriteria;
import ec.edu.espe.gpi.model.users.Users;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPLANNING")
public class Planning implements Serializable {
	
	@Id
	@Column(name = "UZITPLANNING_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Planning_Sequence")
	@SequenceGenerator(name = "Planning_Sequence", sequenceName = "UZISPLANNING_ID", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA UDU_ID
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPLANNING_UZITUSERS"), name = "UZITUSERS_ID", nullable = true, referencedColumnName = "UZITUSERS_ID")
	private Users users;
	//FIN CLAVE FORANEA 
	
	@Column(name = "UZITPLANNING_MAXDUR")
	private Long maxDuration;
	
	@Column(name = "UZITPLANNING_APROBA")
	private Boolean aprobation;
	
	@Column(name = "UZITPLANNING_RESPID")
	private String responsibleId;
	
	
	@Column(name = "UZITPLANNING_REGDAT", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date registrationDate;
	
	@Column(name = "UZITPLANNING_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITPLANNING_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITPLANNING_OBSERV")
	private String observations;
	
	@Column(name = "UZITPLANNING_EVALID")
	private String evaluatorId;
	
	@Column(name = "UZITPLANNING_DEGREE")
	private String degree;
	
	private static final long serialVersionUID = 1L;

	public Planning(Long id, Users users, Long maxDuration, Boolean aprobation, String responsibleId,
			Date registrationDate, Boolean complete, Boolean removed, String observations, String evaluatorId,String degree ) {
		super();
		this.id = id;
		this.users = users;
		this.maxDuration = maxDuration;
		this.aprobation = aprobation;
		this.responsibleId = responsibleId;
		this.registrationDate = registrationDate;
		this.complete = complete;
		this.removed = removed;
		this.observations = observations;
		this.evaluatorId = evaluatorId;
		this.degree=degree;
	}

	public Planning() {
		super();
		// TODO Auto-generated constructor stub
	}
	

}
