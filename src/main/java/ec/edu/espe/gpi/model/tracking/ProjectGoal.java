package ec.edu.espe.gpi.model.tracking;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import ec.edu.espe.gpi.model.registry.Project;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJGOAL")
public class ProjectGoal implements Serializable {

	@Column(name = "UZITPROJGOAL_ASSUMP", nullable = false)
	private String assumption;

	@Column(name = "UZITPROJGOAL_DELI", nullable = false)
	private String deliverable;

	@Column(name = "UZITPROJGOAL_GEN", nullable = false)
	private Boolean general;

	// ACTIVIDADES DEL OBJETIVO
	@JsonIgnoreProperties("projectGoal")
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "projectGoal")
	private List<GoalActivity> goalActivityList;

	@Id
	@Column(name = "UZITPROJGOAL_CODE")
	@GeneratedValue(generator = "GoalProject_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "GoalProject_Sequence", sequenceName = "UZISPROJGOAL_CODE")
	private Long id;

	@Column(name = "UZITPROJGOAL_INDICATOR", nullable = false)
	private String indicator;

	@Column(name = "UZITPROJGOAL_NAME", nullable = false)
	private String name;

	@ManyToOne
	@JsonIgnoreProperties("GoalList")
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJGOAL_UZITPROJEC"), name = "UZITPROJEC_CODE", referencedColumnName = "UZITPROJEC_CODE")
	private Project project;

	@Column(name = "UZITPROJGOAL_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}