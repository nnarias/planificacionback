package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITSCIDIS", uniqueConstraints = @UniqueConstraint(columnNames = "UZITSCIDIS_NAME", name = "UK_UZITSCIDIS_NAME"))
public class ScientificDiscipline implements Serializable {

	@Id
	@Column(name = "UZITSCIDIS_CODE")
	@GeneratedValue(generator = "ScientificDiscipline_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ScientificDiscipline_Sequence", sequenceName = "UZISSCIDIS_CODE")
	private Long id;

	@Column(name = "UZITSCIDIS_NAME", nullable = false)
	private String name;

	@Column(name = "UZITSCIDIS_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}