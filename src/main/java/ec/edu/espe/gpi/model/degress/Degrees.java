package ec.edu.espe.gpi.model.degress;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ec.edu.espe.gpi.model.activities.Activities;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITDEGREES")
public class Degrees implements Serializable{
	
	@Id
	@Column(name = "UZITDEGREES_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Degrees_Sequence")
	@SequenceGenerator(name = "Degrees_Sequence", sequenceName = "Degress_ID", allocationSize = 1)
	private Long id;
	
	@Column(name = "UZITDEGREES_NAME")
	private String name;
	
	@Column(name = "UZITDEGREES_REMOVE")
	private Boolean removed; 
	
	private static final long serialVersionUID = 1L; 

}
