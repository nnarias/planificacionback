package ec.edu.espe.gpi.model.otheractivities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITOTHERACTIVITIES")

public class OtherActivities implements Serializable {
	
	@Id
	@Column(name = "UZITOTHERACTIVITIES_ID")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "OActivities_Sequence")
	@SequenceGenerator(name = "OActivities_Sequence", sequenceName = "UZISOTHERACTIVITIES_ID", allocationSize = 1)
	private Long id;
	
	//CLAVE FORANEA PLANIFICACION
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITOTHERAC_UZITPLANNING"), name = "UZITPLANNING_ID", nullable = true, referencedColumnName = "UZITPLANNING_ID")
	private Planning planning;
	//FIN CLAVE FORANEA
	
	@Column(name = "UZITOTHERACTIVITIES_NAME")
	private String name;
	
	@Column(name = "UZITOTHERACTIVITIES_DURATI")
	private Long duration;
	
	//variable para cargar documento
	
	@Column(name = "UZITOTHERACTIVITIES_LINK")
	private String link;
	
	@Column(name = "UZITOTHERACTIVITIES_COMPLE")
	private Boolean complete;
	
	@Column(name = "UZITOTHERACTIVITIES_REMOVE")
	private Boolean removed;
	
	@Column(name = "UZITOTHERACTIVITIES_APROBA")
	private String aprobation;
	
	@Column(name = "UZITOTHERACTIVITIES_OBSERV")
	private String observations;
	
	private static final long serialVersionUID = 1L;

	public OtherActivities(Long id, Planning planning, String name, Long duration, String link, Boolean complete,
			Boolean removed, String aprobation, String observations ) {
		super();
		this.id = id;
		this.planning = planning;
		this.name = name;
		this.duration = duration;
		this.link = link;
		this.complete = complete;
		this.removed = removed;
		this.aprobation = aprobation;
		this.observations = observations;
	}

	public OtherActivities() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	


}
