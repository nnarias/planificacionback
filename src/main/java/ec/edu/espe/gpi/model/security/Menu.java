package ec.edu.espe.gpi.model.security;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITMENU", uniqueConstraints = @UniqueConstraint(columnNames = { "UZITMENU_IDENTIFIER",
		"UZITMENU_NAME" }, name = "UK_UZITMENU"))
public class Menu implements Serializable {

	@Column(name = "UZITMENU_ICON", nullable = false)
	private String icon;

	@Id
	@Column(name = "UZITMENU_CODE")
	private Long id;

	@Column(name = "UZITMENU_IDENTIFIER", nullable = false)
	private String identifier;

	@Column(name = "UZITMENU_NAME", nullable = false)
	private String name;

	@JsonIgnoreProperties("menu")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "menu", targetEntity = Route.class)
	private List<Route> routeList;

	private static final long serialVersionUID = 1L;
}