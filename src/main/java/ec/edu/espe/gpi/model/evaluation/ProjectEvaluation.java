package ec.edu.espe.gpi.model.evaluation;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ec.edu.espe.gpi.model.registry.Project;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROEVA")
public class ProjectEvaluation implements Serializable {
	
	@Lob
	@Column(name = "UZITPROEVA_COMMENT")
	private String comment;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROEVA_UZITEVACRISUB"), name = "UZITEVACRISUB_CODE", nullable = false, referencedColumnName = "UZITEVACRISUB_CODE")
	private EvaluationCriteriaSubcriteria evaluationCriteriaSubcriteria;

	@Column(name = "UZITPROEVA_GRADE")
	private Integer grade;

	@Id
	@Column(name = "UZITPROEVA_CODE")
	@GeneratedValue(generator = "ProjectEvaluation_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectEvaluation_Sequence", sequenceName = "UZISPROEVA_CODE")
	private Long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROEVA_UZITPROJEC_CODE"), name = "UZITPROJEC_CODE", nullable = false, referencedColumnName = "UZITPROJEC_CODE")
	private Project project;
	
	@Column(name = "UZITPROEVA_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}