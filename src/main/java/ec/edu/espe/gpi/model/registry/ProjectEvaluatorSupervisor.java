package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonFormat;

import ec.edu.espe.gpi.model.security.Role;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITPROJDEVSU")
public class ProjectEvaluatorSupervisor implements Serializable {

	@Id
	@Column(name = "UZITPROJDEVSU_CODE")
	@GeneratedValue(generator = "ProjectEvaluatorSupervisor_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "ProjectEvaluatorSupervisor_Sequence", sequenceName = "UZISPROJDEVSU_CODE")
	private Long id;

	@Column(name = "UZITPROJDEVSU_CRE_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date creationDate;

	@Column(name = "UZITPROJDEVSU_NUMPROJECT", nullable = false)
	private Integer numProject;

	@Column(name = "UZITPROJDEVSU_UPD_DATE", nullable = false)
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", shape = JsonFormat.Shape.STRING, timezone = JsonFormat.DEFAULT_TIMEZONE)
	@Temporal(TemporalType.DATE)
	private Date updateDate;

	@Column(name = "UZITPROJDEVSU_USERNAME", nullable = false)
	private String username;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITPROJDEVSU_UZITROLE"), name = "UZITROLE_CODE")
	private Role role;

	private static final long serialVersionUID = 1L;
}