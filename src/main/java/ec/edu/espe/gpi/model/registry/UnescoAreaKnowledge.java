package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITUNESAKNO", uniqueConstraints = @UniqueConstraint(columnNames = "UZITUNESAKNO_NAME", name = "UK_UZITUNESAKNO_NAME"))
public class UnescoAreaKnowledge implements Serializable {

	@Id
	@Column(name = "UZITUNESAKNO_CODE")
	@GeneratedValue(generator = "UnescoAreaKnowledge_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "UnescoAreaKnowledge_Sequence", sequenceName = "UZISUNESAKNO_CODE")
	private Long id;

	@Column(name = "UZITUNESAKNO_NAME", nullable = false)
	private String name;

	@Column(name = "UZITUNESAKNO_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}