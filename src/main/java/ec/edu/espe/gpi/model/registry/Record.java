package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import ec.edu.espe.gpi.model.security.UserModel;
import lombok.Data;

@Data
@Entity
@Table(name = "UZITRECORD")
public class Record implements Serializable {

	@Id
	@Column(name = "UZITRECORD_CODE")
	@GeneratedValue(generator = "Record_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "Record_Sequence", sequenceName = "UZISRECORD_CODE")
	private Long id;

	@Column(name = "UZITRECORD_NAME", nullable = false)
	private String name;

	@Column(name = "UZITRECORD_PATH", nullable = false)
	private String path;

	@Column(name = "UZITRECORD_STATE", nullable = false)
	private Boolean state;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(foreignKey = @ForeignKey(name = "FK_UZITRECORD_UZITUSER"), name = "UZITUSER_CODE", nullable = false, referencedColumnName = "UZITUSER_CODE")
	private UserModel userModel;

	private static final long serialVersionUID = 1L;
}