package ec.edu.espe.gpi.model.registry;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Data
@Entity
@Table(name = "UZITCOVERTYP", uniqueConstraints = @UniqueConstraint(columnNames = "UZITCOVERTYP_NAME", name = "UK_UZITCOVERTYP_NAME"))
public class CoverageType implements Serializable {

	@Id
	@Column(name = "UZITCOVERTYP_CODE")
	@GeneratedValue(generator = "CoverageType_Sequence", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(allocationSize = 1, name = "CoverageType_Sequence", sequenceName = "UZISCOVERTPY_CODE")
	private Long id;

	@Column(name = "UZITCOVERTYP_NAME", nullable = false)
	private String name;

	@Column(name = "UZITCOVERTYP_STATE", nullable = false)
	private Boolean state;

	private static final long serialVersionUID = 1L;
}
