package ec.edu.espe.gpi;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;

@SpringBootApplication
public class InvestigacionApplication extends SpringBootServletInitializer implements CommandLineRunner {
	private static final Logger log_logger = Logger.getLogger(InvestigacionApplication.class.getName());

	@Autowired
	private Environment env;

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(InvestigacionApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(InvestigacionApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log_logger.log(Level.INFO, env.getProperty("app.config.name"));
		log_logger.log(Level.INFO, env.getProperty("app.config.openKM.URL"));
		log_logger.log(Level.INFO, env.getProperty("app.config.openKM.URL.getDocument"));
		log_logger.log(Level.INFO, env.getProperty("app.config.openKM.URL.sendDocument"));
	}
}