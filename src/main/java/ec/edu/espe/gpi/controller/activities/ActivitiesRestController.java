package ec.edu.espe.gpi.controller.activities;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.activities.ActivitiesDTO;
import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.service.activities.IActivitiesService;
import ec.edu.espe.gpi.service.books.IBooksService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ActivitiesRestController {
	@Autowired
	private IActivitiesService activitiesService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activities/{id}")
	public ResponseEntity<Activities> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activitiesService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activities")
	public ResponseEntity<List<Activities>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activitiesService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activities/max/{maxDuration}")
	public ResponseEntity<List<Activities>> findMaxDuration(Authentication authentication, @PathVariable Long maxDuration) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activitiesService.findByMaxDuration(maxDuration), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/activities")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody ActivitiesDTO activitiesDTO) {
		userService.validateChangeUserRole(authentication);
		activitiesService.save(activitiesDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/activities/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		activitiesService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
