package ec.edu.espe.gpi.controller.planning;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.service.bookchapters.IBookChaptersService;
import ec.edu.espe.gpi.service.planning.IPlanningService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class PlanningRestController {
	@Autowired
	private IPlanningService planningService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/planning/{id}")
	public ResponseEntity<Planning> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(planningService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/planning")
	public ResponseEntity<List<Planning>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(planningService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/planning/activities/{id}")
	public ResponseEntity<List<Activities>> findAllActivities(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(planningService.findAllActivitiesbyPlanning(id), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/planningIR")
	public ResponseEntity<List<Planning>> findAllIR(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(planningService.findAllIR(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/planning")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody PlanningDTO planningDTO) {
		userService.validateChangeUserRole(authentication);
		planningService.save(planningDTO);
		return new ResponseEntity<>(HttpStatus.OK);  
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/planning/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		planningService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/planning/aprobation/{id}")
	public ResponseEntity<HttpStatus> updateAprobation(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		planningService.updateAprobation(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/planning/complete/{id}")
	public ResponseEntity<HttpStatus> updateComplete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		planningService.updateComplete(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
