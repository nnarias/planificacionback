package ec.edu.espe.gpi.controller.security;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.security.PasswordDTO;
import ec.edu.espe.gpi.dto.security.UserDTO;
import ec.edu.espe.gpi.model.security.UserModel;
import ec.edu.espe.gpi.service.security.IPasswordService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.security.UserGeneral;

@RestController
@RequestMapping("/api")
public class UserRestController {

	@Autowired
	private IUserService userService;

	@Autowired
	private IPasswordService passwordService;

	@Secured({ "ROLE_USER" })
	@PostMapping("/user/changePassword")
	public ResponseEntity<HttpStatus> changePassword(Authentication authentication,
			@Validated @RequestBody PasswordDTO passwordDTO) throws NoSuchAlgorithmException {
		userService.validateChangeUserRole(authentication);
		UserModel currentUser = userService.findByAuthentication(authentication);
		passwordService.changePassword(currentUser, passwordDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/user/{id}")
	public ResponseEntity<UserModel> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(userService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/user")
	public ResponseEntity<List<UserModel>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(userService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/user/username/{username}")
	public ResponseEntity<UserGeneral> findUserGeneralByUsername(Authentication authentication,
			@PathVariable String username) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(userService.findUserGeneralByUsername(username), HttpStatus.OK);
	}

	@Secured({ "ROLE_ADMIN" })
	@GetMapping("/user/passwordReset/{id}")
	public ResponseEntity<HttpStatus> passwordReset(Authentication authentication, @PathVariable Long id)
			throws NoSuchAlgorithmException {
		userService.validateChangeUserRole(authentication);
		passwordService.passwordReset(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_ADMIN" })
	@PostMapping("/user")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody UserDTO userDTO)
			throws NoSuchAlgorithmException {
		userService.validateChangeUserRole(authentication);
		userService.save(userDTO);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Secured({ "ROLE_ADMIN" })
	@PatchMapping("/user/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		userService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}