package ec.edu.espe.gpi.controller.registry;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.model.registry.ProjectRequirement;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.registry.IProjectRequirementService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

@RestController
@RequestMapping("/api")
public class ProjectRequirementRestController {

	@Autowired
	private IImpDocumentService impDocumentService;

	@Autowired
	private IProjectRequirementService projectRequirementService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/projectRequirement/{id}")
	public ResponseEntity<List<ProjectRequirement>> findAll(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectRequirementService.findAllByProject(id), HttpStatus.OK);
	}

	@GetMapping("/projectRequirement/requirement/{id}")
	public ResponseEntity<InputStreamResource> findFile(@PathVariable Long id) throws IOException {
		ProjectRequirement projectRequirement = projectRequirementService.find(id);
		InputStreamResource inputStreamResource = impDocumentService.getDocument(projectRequirement.getFileUUID(),
				TypeUserDocument.PROJECT_ADMIN);
		return new ResponseEntity<>(inputStreamResource,
				Headers.getHeadersMultiparth(impDocumentService.getFilenamePrefix(projectRequirement.getFileName())),
				HttpStatus.OK);
	}
}