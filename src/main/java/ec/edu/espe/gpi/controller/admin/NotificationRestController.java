package ec.edu.espe.gpi.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.admin.NotificationDTO;
import ec.edu.espe.gpi.model.admin.Notification;
import ec.edu.espe.gpi.service.admin.INotificationService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class NotificationRestController {

	@Autowired
	private INotificationService notificationService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@DeleteMapping("/notification/{id}")
	public ResponseEntity<HttpStatus> delete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		notificationService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/notification/{id}")
	public ResponseEntity<Notification> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(notificationService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/notification")
	public ResponseEntity<List<Notification>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(notificationService.findAll(), HttpStatus.OK);
	}

	@GetMapping("/notification/user")
	public ResponseEntity<List<Notification>> findAllByUser(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(notificationService.findAllByUser(authentication), HttpStatus.OK);
	}

	@GetMapping("/notification/user/{id}")
	public ResponseEntity<Notification> findByUser(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(notificationService.findByUser(authentication, id), HttpStatus.OK);
	}

	@GetMapping("/recentNotification/user")
	public ResponseEntity<List<Notification>> findByUserRecentNotification(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(notificationService.findByUserRecentNotification(authentication), HttpStatus.OK);
	}

	@PostMapping("/notification")
	public ResponseEntity<HttpStatus> saveNotification(Authentication authentication,
			@Validated @RequestBody NotificationDTO notificationDTO) {
		userService.validateChangeUserRole(authentication);
		notificationService.save(notificationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}