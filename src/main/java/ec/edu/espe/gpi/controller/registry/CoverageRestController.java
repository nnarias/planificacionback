package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.CoverageDTO;
import ec.edu.espe.gpi.model.registry.Coverage;
import ec.edu.espe.gpi.service.registry.ICoverageService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class CoverageRestController {

	@Autowired
	private ICoverageService coverageService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/coverage/{id}")
	public ResponseEntity<Coverage> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(coverageService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/coverage")
	public ResponseEntity<List<Coverage>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(coverageService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/coverage")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@Validated @RequestBody CoverageDTO coverageDTO) {
		userService.validateChangeUserRole(authentication);
		coverageService.save(coverageDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/coverage/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		coverageService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}