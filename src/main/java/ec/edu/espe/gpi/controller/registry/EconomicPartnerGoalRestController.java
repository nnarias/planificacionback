package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.EconomicPartnerGoalDTO;
import ec.edu.espe.gpi.model.registry.EconomicPartnerGoal;
import ec.edu.espe.gpi.service.registry.IEconomicPartnerGoalService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class EconomicPartnerGoalRestController {

	@Autowired
	private IEconomicPartnerGoalService economicPartnerGoalService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/economicPartnerGoal/{id}")
	public ResponseEntity<EconomicPartnerGoal> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(economicPartnerGoalService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/economicPartnerGoal")
	public ResponseEntity<List<EconomicPartnerGoal>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(economicPartnerGoalService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/economicPartnerGoal")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody EconomicPartnerGoalDTO economicPartnerGoalDTO) {
		userService.validateChangeUserRole(authentication);
		economicPartnerGoalService.save(economicPartnerGoalDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/economicPartnerGoal/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		economicPartnerGoalService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}