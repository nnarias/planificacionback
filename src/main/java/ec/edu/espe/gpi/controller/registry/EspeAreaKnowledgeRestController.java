package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.EspeAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.EspeAreaKnowledge;
import ec.edu.espe.gpi.service.registry.IEspeAreaKnowledgeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class EspeAreaKnowledgeRestController {

	@Autowired
	private IEspeAreaKnowledgeService espeAreaKnowledgeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/espeAreaKnowledge/{id}")
	public ResponseEntity<EspeAreaKnowledge> find(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(espeAreaKnowledgeService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/espeAreaKnowledge")
	public ResponseEntity<List<EspeAreaKnowledge>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(espeAreaKnowledgeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/espeAreaKnowledge")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody EspeAreaKnowledgeDTO espeAreaKnowledgeDTO) {
		userService.validateChangeUserRole(authentication);
		espeAreaKnowledgeService.save(espeAreaKnowledgeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/espeAreaKnowledge/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		espeAreaKnowledgeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}