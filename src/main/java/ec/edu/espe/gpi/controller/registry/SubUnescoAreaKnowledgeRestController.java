package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.SubUnescoAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.SubUnescoAreaKnowledge;
import ec.edu.espe.gpi.service.registry.ISubUnescoAreaKnowledgeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class SubUnescoAreaKnowledgeRestController {

	@Autowired
	private ISubUnescoAreaKnowledgeService subUnescoAreaKnowledgeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/subUnescoAreaKnowledge/{id}")
	public ResponseEntity<SubUnescoAreaKnowledge> find(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(subUnescoAreaKnowledgeService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/subUnescoAreaKnowledge")
	public ResponseEntity<List<SubUnescoAreaKnowledge>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(subUnescoAreaKnowledgeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/subUnescoAreaKnowledge/unescoAreaKnowledge/{id}")
	public ResponseEntity<List<SubUnescoAreaKnowledge>> findByUnesco(
			Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(subUnescoAreaKnowledgeService.findByUnesco(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/subUnescoAreaKnowledge")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody SubUnescoAreaKnowledgeDTO subUnescoAreaKnowledgeDTO) {
		userService.validateChangeUserRole(authentication);
		subUnescoAreaKnowledgeService.save(subUnescoAreaKnowledgeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/subUnescoAreaKnowledge/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		subUnescoAreaKnowledgeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}