package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.model.registry.ProjectType;
import ec.edu.espe.gpi.service.registry.IProjectTypeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectTypeRestController {

	@Autowired
	private IProjectTypeService projectTypeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_LEADER" })
	@GetMapping("/projectType")
	public ResponseEntity<List<ProjectType>> searchProjectTypeList(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectTypeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_LEADER" })
	@GetMapping("/projectType/{id}")
	public ResponseEntity<ProjectType> searchProjectType(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectTypeService.findById(id), HttpStatus.OK);
	}
}