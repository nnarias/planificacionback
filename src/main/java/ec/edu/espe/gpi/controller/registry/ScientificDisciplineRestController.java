package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.ScientificDisciplineDTO;
import ec.edu.espe.gpi.model.registry.ScientificDiscipline;
import ec.edu.espe.gpi.service.registry.IScientificDisciplineService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ScientificDisciplineRestController {

	@Autowired
	private IScientificDisciplineService scientificDisciplineService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/scientificDiscipline/{id}")
	public ResponseEntity<ScientificDiscipline> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(scientificDisciplineService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/scientificDiscipline")
	public ResponseEntity<List<ScientificDiscipline>> searchScientificDisciplineList(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(scientificDisciplineService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/scientificDiscipline")
	public ResponseEntity<HttpStatus> sve(Authentication authentication,
			@RequestBody ScientificDisciplineDTO scientificDisciplineDTO) {
		userService.validateChangeUserRole(authentication);
		scientificDisciplineService.save(scientificDisciplineDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/scientificDiscipline/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		scientificDisciplineService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}