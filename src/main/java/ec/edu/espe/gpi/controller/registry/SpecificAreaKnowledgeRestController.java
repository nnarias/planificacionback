package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.espe.gpi.dto.registry.SpecificAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.SpecificAreaKnowledge;
import ec.edu.espe.gpi.service.registry.ISpecificAreaKnowledgeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class SpecificAreaKnowledgeRestController {

	@Autowired
	private ISpecificAreaKnowledgeService specificAreaKnowledgeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/specificAreaKnowledge/{id}")
	public ResponseEntity<SpecificAreaKnowledge> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(specificAreaKnowledgeService.findById(id), HttpStatus.OK);

	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/specificAreaKnowledge")
	public ResponseEntity<List<SpecificAreaKnowledge>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(specificAreaKnowledgeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/specificAreaKnowledge")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@Validated @RequestBody SpecificAreaKnowledgeDTO specificAreaKnowledgeDTO) {
		userService.validateChangeUserRole(authentication);
		specificAreaKnowledgeService.save(specificAreaKnowledgeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/specificAreaKnowledge/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		specificAreaKnowledgeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}