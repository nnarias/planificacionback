package ec.edu.espe.gpi.controller.evaluation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.evaluation.EvaluationDTO;
import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.evaluation.Evaluation;
import ec.edu.espe.gpi.service.evaluation.IEvaluationService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class EvaluationRestController {
	@Autowired
	private IEvaluationService evaluationService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/evaluate")
	public ResponseEntity<HttpStatus> evaluate(Authentication authentication,
			@Validated @RequestBody ProjectDTO projectDTO) {
		userService.validateChangeUserRole(authentication);
		evaluationService.evaluate(projectDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/evaluation/{id}")
	public ResponseEntity<Evaluation> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(evaluationService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/evaluation")
	public ResponseEntity<List<Evaluation>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(evaluationService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/evaluation")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@Validated @RequestBody EvaluationDTO evaluationDTO) {
		userService.validateChangeUserRole(authentication);
		evaluationService.save(evaluationDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/evaluation/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		evaluationService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}