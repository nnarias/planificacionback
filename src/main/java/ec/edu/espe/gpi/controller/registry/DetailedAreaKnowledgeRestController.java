package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.DetailedAreaKnowledgeDTO;
import ec.edu.espe.gpi.model.registry.DetailedAreaKnowledge;
import ec.edu.espe.gpi.service.registry.IDetailedAreaKnowledgeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class DetailedAreaKnowledgeRestController {

	@Autowired
	private IDetailedAreaKnowledgeService detailedAreaKnowledgeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/detailedAreaKnowledge/{id}")
	public ResponseEntity<DetailedAreaKnowledge> find(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(detailedAreaKnowledgeService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/detailedAreaKnowledge")
	public ResponseEntity<List<DetailedAreaKnowledge>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(detailedAreaKnowledgeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/detailedAreaKnowledge")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody DetailedAreaKnowledgeDTO detailedAreaKnowledgeDTO) {
		userService.validateChangeUserRole(authentication);
		detailedAreaKnowledgeService.save(detailedAreaKnowledgeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/detailedAreaKnowledge/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		detailedAreaKnowledgeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}