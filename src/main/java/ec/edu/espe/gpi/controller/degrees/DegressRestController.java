package ec.edu.espe.gpi.controller.degrees;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.activities.ActivitiesDTO;
import ec.edu.espe.gpi.dto.degress.DegreesDTO;
import ec.edu.espe.gpi.model.activities.Activities;
import ec.edu.espe.gpi.model.degress.Degrees;
import ec.edu.espe.gpi.service.activities.IActivitiesService;
import ec.edu.espe.gpi.service.degrees.IDegreesService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class DegressRestController {
	@Autowired
	private IDegreesService degressService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/degrees/{id}")
	public ResponseEntity<Degrees> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(degressService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/degrees")
	public ResponseEntity<List<Degrees>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(degressService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/degrees")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody DegreesDTO degreesDTO) {
		userService.validateChangeUserRole(authentication);
		degressService.save(degreesDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/Degrees/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		degressService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
