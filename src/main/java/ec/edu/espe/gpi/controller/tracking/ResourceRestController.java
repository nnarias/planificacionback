package ec.edu.espe.gpi.controller.tracking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.tracking.ResourceDTO;
import ec.edu.espe.gpi.model.tracking.Resource;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.service.tracking.IResourceService;

@RestController
@RequestMapping("/api")
public class ResourceRestController {
	@Autowired
	private IResourceService resourceService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/resource/{id}")
	public ResponseEntity<Resource> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(resourceService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/resource")
	public ResponseEntity<List<Resource>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(resourceService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/resource")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @RequestBody ResourceDTO resourceDTO) {
		userService.validateChangeUserRole(authentication);
		resourceService.save(resourceDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/resource/{id}")
	public ResponseEntity<HttpStatus> updatState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		resourceService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}