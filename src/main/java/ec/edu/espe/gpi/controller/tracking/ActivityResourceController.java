package ec.edu.espe.gpi.controller.tracking;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ec.edu.espe.gpi.dto.tracking.ActivityResourceApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectInvoiceDTO;
import ec.edu.espe.gpi.model.tracking.ActivityResource;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.service.tracking.IActivityResourceService;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

@RestController
@RequestMapping("/api")
public class ActivityResourceController {

	@Autowired
	private IActivityResourceService activityResourceService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activityResource/{id}")
	public ResponseEntity<ActivityResource> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activityResourceService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activityResource/allPending/{id}")
	public ResponseEntity<List<ActivityResource>> findAllPending(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activityResourceService.findAllPending(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_FINANCIAL" })
	@GetMapping("/activityResource/allTracking/{id}")
	public ResponseEntity<List<ActivityResource>> findAllTracking(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activityResourceService.findAllTracking(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/activityResource/pending/{id}")
	public ResponseEntity<ActivityResource> findPending(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activityResourceService.findPending(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_FINANCIAL" })
	@GetMapping("/activityResource/tracking/{id}")
	public ResponseEntity<ActivityResource> findTracking(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(activityResourceService.findTracking(id, authentication), HttpStatus.OK);
	}

	@GetMapping("/activityResource/invoiceFile/{id}")
	public ResponseEntity<InputStreamResource> findFile(@PathVariable Long id) throws IOException {
		ActivityResource activityResource = activityResourceService.findInvoice(id);
		InputStreamResource inputStreamResource = impDocumentService.getDocument(activityResource.getInvoiceFileUUID(),
				TypeUserDocument.PROJECT_ADMIN);
		return new ResponseEntity<>(inputStreamResource, Headers.getHeadersMultiparth(
				impDocumentService.getFilenamePrefix(activityResource.getInvoiceFileName())), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/activityResource/invoice")
	public ResponseEntity<HttpStatus> saveInvoice(Authentication authentication,
			@Validated @ModelAttribute ProjectInvoiceDTO projectInvoiceDTO) throws IOException {
		userService.validateChangeUserRole(authentication);
		activityResourceService.saveInvoice(projectInvoiceDTO, authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_FINANCIAL" })
	@PostMapping("/activityResource/invoiceApproval")
	public ResponseEntity<HttpStatus> invoiceApproval(Authentication authentication,
			@Validated @RequestBody ActivityResourceApprovalDTO activityResourceApprovalDTO) {
		userService.validateChangeUserRole(authentication);
		activityResourceService.invoiceApproval(activityResourceApprovalDTO, authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}