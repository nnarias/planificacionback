package ec.edu.espe.gpi.controller.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.evaluation.AssignEvaluatorDTO;
import ec.edu.espe.gpi.service.evaluation.IProjectAssignmentService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectAssignmentRestController {
	@Autowired
	private IUserService userService;

	@Autowired
	private IProjectAssignmentService projectAssignmentService;

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/evaluatorAssignmet")
	public ResponseEntity<HttpStatus> assignEvaluatorProject(Authentication authentication,
			@Validated @RequestBody AssignEvaluatorDTO evaluatorAssigningDTO) {
		userService.validateChangeUserRole(authentication);
		projectAssignmentService.assignEvaluator(evaluatorAssigningDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}