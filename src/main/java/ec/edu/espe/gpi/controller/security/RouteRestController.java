package ec.edu.espe.gpi.controller.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.model.security.Menu;
import ec.edu.espe.gpi.model.security.Route;
import ec.edu.espe.gpi.service.security.IRouteService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class RouteRestController {

	@Autowired
	private IRouteService routeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_USER" })
	@GetMapping("/route")
	public ResponseEntity<List<Route>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(routeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/route/user")
	public ResponseEntity<List<Menu>> findMenu(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(routeService.findMenu(authentication), HttpStatus.OK);
	}
}