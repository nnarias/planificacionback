package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.registry.Project;
import ec.edu.espe.gpi.service.registry.IProjectService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectRestController {

	@Autowired
	private IProjectService projectService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/approving")
	public ResponseEntity<List<Project>> findAllApproving(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllApproving(authentication.getName()), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_DEPARTMENT_EVALUATOR" })
	@GetMapping("/project/departmentalEvaluator")
	public ResponseEntity<List<Project>> findAllByDepartmentalEvaluator(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByDepartmentalEvaluator(authentication), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/evaluatorAssigning/call/{callId}")
	public ResponseEntity<List<Project>> findAllByEvaluatorAssigning(Authentication authentication,
			@PathVariable Long callId) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByEvaluatorAssigning(callId), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_FINANCIAL" })
	@GetMapping("/project/financialSupervisor")
	public ResponseEntity<List<Project>> findAllByFinancialSupervisor(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByFinancialSupervisor(authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/projectDirector")
	public ResponseEntity<List<Project>> findAllByProjectDirector(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByProjectDirector(authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_EVALUATOR" })
	@GetMapping("/project/projectEvaluator")
	public ResponseEntity<List<Project>> findAllByProjectEvaluator(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByProjectEvaluator(authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_TECHNICAL" })
	@GetMapping("/project/technicalSupervisor")
	public ResponseEntity<List<Project>> findAllByTechnicalSupervisor(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllByTechnicalSupervisor(authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/developing")
	public ResponseEntity<List<Project>> findAllDeveloping(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllDeveloping(authentication.getName()), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/evaluating")
	public ResponseEntity<List<Project>> findAllEvaluating(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findAllEvaluating(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_DEPARTMENT_EVALUATOR" })
	@GetMapping("/project/departmentalEvaluator/{id}")
	public ResponseEntity<Project> findByDepartmentalEvaluator(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByDepartmentalEvaluator(authentication, id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_DEPARTMENT_EVALUATOR" })
	@GetMapping("/project/evaluatorAssigning/{id}")
	public ResponseEntity<Project> findByEvaluatorAssigning(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByEvaluatorAssigning(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_FINANCIAL" })
	@GetMapping("/project/financialSupervisor/{id}")
	public ResponseEntity<Project> findByFinancialSupervisor(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByFinancialSupervisor(authentication, id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/project/projectDirector/{id}")
	public ResponseEntity<Project> findByProjectDirector(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByProjectDirector(authentication, id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_EVALUATOR" })
	@GetMapping("/project/projectEvaluator/{id}")
	public ResponseEntity<Project> findByProjectEvaluator(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByProjectEvaluator(authentication, id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_TECHNICAL" })
	@GetMapping("/project/technicalSupervisor/{id}")
	public ResponseEntity<Project> findByTechnicalSupervisor(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectService.findByTechnicalSupervisor(authentication, id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/project")
	public ResponseEntity<HttpStatus> save(
			@RequestPart(name = "annexList", required = false) List<MultipartFile> annexList,
			Authentication authentication,
			@Validated @RequestPart(name = "project", required = true) ProjectDTO projectDTO,
			@RequestPart(name = "requirementList", required = true) List<MultipartFile> requirementList) {
		userService.validateChangeUserRole(authentication);
		projectService.save(annexList, authentication, projectDTO, requirementList);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/project/send")
	public ResponseEntity<HttpStatus> sendToEvaluation(Authentication authentication,
			@Validated @RequestBody ProjectDTO projectDTO) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/project/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		projectService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/project/upload/{nameFile:.+}")
	public ResponseEntity<Resource> viewFile(Authentication authentication, @PathVariable String nameFile) {
		userService.validateChangeUserRole(authentication);
		HttpHeaders headersFile = new HttpHeaders();
		Resource resourceFile = projectService.getFile(nameFile);
		headersFile.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resourceFile.getFilename() + "\"");
		return new ResponseEntity<>(resourceFile, headersFile, HttpStatus.OK);
	}
}