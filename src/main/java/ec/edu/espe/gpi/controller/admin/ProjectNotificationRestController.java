package ec.edu.espe.gpi.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.model.admin.ProjectNotification;
import ec.edu.espe.gpi.service.admin.IProjectNotificationService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectNotificationRestController {

	@Autowired
	private IProjectNotificationService projectNotificationService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_DEPARTMENT_EVALUER", "ROLE_DEPARTMENT_EVALUER", "ROLE_TECHNICAL",
			"ROLE_FINANCIAL" })
	@GetMapping("/projectNotification/user")
	public ResponseEntity<List<ProjectNotification>> findUser(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectNotificationService.findByUser(authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/projectNotification/project/{idProject}")
	public ResponseEntity<List<ProjectNotification>> findProject(Authentication authentication,
			@PathVariable Long idProject) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectNotificationService.findByProject(idProject), HttpStatus.OK);
	}
}