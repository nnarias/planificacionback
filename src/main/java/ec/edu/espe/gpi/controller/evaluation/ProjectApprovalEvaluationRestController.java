package ec.edu.espe.gpi.controller.evaluation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.evaluation.ProjectApprovalEvaluationDTO;
import ec.edu.espe.gpi.service.evaluation.IProjectApprovalEvaluationService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectApprovalEvaluationRestController {

	@Autowired
	private IProjectApprovalEvaluationService projectApprovalEvaluationService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_DEPARTMENT_EVALUATOR" })
	@PostMapping("/projectApproval")
	public ResponseEntity<HttpStatus> projectApproval(Authentication authentication,
			@Validated @RequestBody ProjectApprovalEvaluationDTO projectApprovalEvaluationDTO) {
		userService.validateChangeUserRole(authentication);
		projectApprovalEvaluationService.projectApproval(projectApprovalEvaluationDTO, authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}