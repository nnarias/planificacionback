package ec.edu.espe.gpi.controller.books;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.service.books.IBooksService;
import ec.edu.espe.gpi.service.planning.IBookService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class BooksRestController {
	@Autowired
	private IBooksService booksService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/books/{id}")
	public ResponseEntity<Books> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(booksService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/books")
	public ResponseEntity<List<Books>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(booksService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/books/planning/{planning}")
	public ResponseEntity<List<Books>> findAllByPlanning(Authentication authentication, @PathVariable Planning planning ) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(booksService.findAllByPlanning(planning), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/books")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody BooksDTO booksDTO) {
		userService.validateChangeUserRole(authentication);
		booksService.save(booksDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/books/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		booksService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/books/complete/{id}")
	public ResponseEntity<HttpStatus> updateComplete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		booksService.updateComplete(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
}
