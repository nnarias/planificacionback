package ec.edu.espe.gpi.controller.tracking;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.ProjectDTO;
import ec.edu.espe.gpi.model.tracking.ProjectGoal;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.service.tracking.IGoalService;

@RestController
@RequestMapping("/api")
public class GoalRestController {
	@Autowired
	private IGoalService goalService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/goal")
	public ResponseEntity<List<ProjectGoal>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/goal/user/{id}")
	public ResponseEntity<List<ProjectGoal>> findByProject(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalService.findByProject(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/goal")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @RequestBody ProjectDTO projectDTO) {
		userService.validateChangeUserRole(authentication);
		goalService.save(projectDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}