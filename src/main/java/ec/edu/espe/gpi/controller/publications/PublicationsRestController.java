package ec.edu.espe.gpi.controller.publications;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.service.otheractivities.IOtherActivitiesService;
import ec.edu.espe.gpi.service.publications.IPublicationsService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class PublicationsRestController {
	@Autowired
	private IPublicationsService publicationsService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/publications/{id}")
	public ResponseEntity<Publications> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(publicationsService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/publications")
	public ResponseEntity<List<Publications>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(publicationsService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/publications/planning/{planning}")
	public ResponseEntity<List<Publications>> findAllByPlanning(Authentication authentication, @PathVariable Planning planning ) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(publicationsService.findAllByPlanning(planning), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/publications")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody PublicationsDTO publicationsDTO) {
		userService.validateChangeUserRole(authentication);
		publicationsService.save(publicationsDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/publications/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		publicationsService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/publications/complete/{id}")
	public ResponseEntity<HttpStatus> updateComplete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		publicationsService.updateComplete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
