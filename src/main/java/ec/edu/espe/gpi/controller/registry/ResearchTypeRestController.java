package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.ResearchTypeDTO;
import ec.edu.espe.gpi.model.registry.ResearchType;
import ec.edu.espe.gpi.service.registry.IResearchTypeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ResearchTypeRestController {

	@Autowired
	private IResearchTypeService researchTypeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/researchType/{id}")
	public ResponseEntity<ResearchType> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(researchTypeService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/researchType")
	public ResponseEntity<List<ResearchType>> searchResearchTypeList(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(researchTypeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/researchType")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody ResearchTypeDTO researchTypeDTO) {
		userService.validateChangeUserRole(authentication);
		researchTypeService.save(researchTypeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/researchType/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		researchTypeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}