package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.ProjectResponsibleTypeDTO;
import ec.edu.espe.gpi.model.registry.ProjectResponsibleType;
import ec.edu.espe.gpi.service.registry.IProjectResponsibleTypeService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class ProjectResponsibleTypeRestController {

	@Autowired
	private IProjectResponsibleTypeService projectResponsibleTypeService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/projectResponsibleType/{id}")
	public ResponseEntity<ProjectResponsibleType> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectResponsibleTypeService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/projectResponsibleType")
	public ResponseEntity<List<ProjectResponsibleType>> searchProjectResponsibleTypeList(
			Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(projectResponsibleTypeService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/projectResponsibleType")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody ProjectResponsibleTypeDTO projectResponsibleTypeDTO) {
		userService.validateChangeUserRole(authentication);
		projectResponsibleTypeService.save(projectResponsibleTypeDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/projectResponsibleType/{id}")
	public ResponseEntity<HttpStatus> deleteProjectResponsibleType(Authentication authentication,
			@PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		projectResponsibleTypeService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}