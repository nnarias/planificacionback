package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.CollegeDepartmentDTO;
import ec.edu.espe.gpi.model.registry.CollegeDepartment;
import ec.edu.espe.gpi.service.registry.ICollegeDepartmentService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class CollegeDepartmentRestController {

	@Autowired
	private ICollegeDepartmentService collegeDepartmentService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/collegeDepartment/{id}")
	public ResponseEntity<CollegeDepartment> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(collegeDepartmentService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/collegeDepartment")
	public ResponseEntity<List<CollegeDepartment>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(collegeDepartmentService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/collegeDepartment")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@Validated @RequestBody CollegeDepartmentDTO collegeDepartmentDTO) {
		userService.validateChangeUserRole(authentication);
		collegeDepartmentService.save(collegeDepartmentDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/collegeDepartment/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		collegeDepartmentService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}