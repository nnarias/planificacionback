package ec.edu.espe.gpi.controller.tracking;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.tracking.GoalActivityApprovalDTO;
import ec.edu.espe.gpi.dto.tracking.ProjectDeliverableDTO;
import ec.edu.espe.gpi.model.tracking.GoalActivity;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.service.tracking.IGoalActivityService;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

@RestController
@RequestMapping("/api")
public class GoalActivityRestController {

	@Autowired
	private IGoalActivityService goalActivityService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Secured({ "ROLE_TECHNICAL" })
	@PostMapping("/goalActivity/deliverableApproval")
	public ResponseEntity<HttpStatus> deliverableApproval(Authentication authentication,
			@Validated @RequestBody GoalActivityApprovalDTO goalActivityApprovalDTO) {
		userService.validateChangeUserRole(authentication);
		goalActivityService.deliverableApproval(goalActivityApprovalDTO, authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/goalActivity/{id}")
	public ResponseEntity<GoalActivity> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalActivityService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/goalActivity/allPending/{id}")
	public ResponseEntity<List<GoalActivity>> findAllPending(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalActivityService.findAllPending(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_TECHNICAL" })
	@GetMapping("/goalActivity/allTracking/{id}")
	public ResponseEntity<List<GoalActivity>> findAllTracking(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalActivityService.findAllTracking(id, authentication), HttpStatus.OK);
	}

	@GetMapping("/goalActivity/deliverableFile/{id}")
	public ResponseEntity<InputStreamResource> findFile(@PathVariable Long id) throws IOException {
		GoalActivity goalActivity = goalActivityService.findByDeliverable(id);
		InputStreamResource inputStreamResource = impDocumentService.getDocument(goalActivity.getDeliverableFileUUID(),
				TypeUserDocument.PROJECT_ADMIN);
		return new ResponseEntity<>(inputStreamResource, Headers.getHeadersMultiparth(
				impDocumentService.getFilenamePrefix(goalActivity.getDeliverableFileName())), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/goalActivity/pending/{id}")
	public ResponseEntity<GoalActivity> findPending(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalActivityService.findPending(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_TECHNICAL" })
	@GetMapping("/goalActivity/tracking/{id}")
	public ResponseEntity<GoalActivity> findTracking(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(goalActivityService.findTracking(id, authentication), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/goalActivity/deliverable")
	public ResponseEntity<HttpStatus> saveDeliverable(Authentication authentication,
			@Validated @ModelAttribute ProjectDeliverableDTO projectDeliverableDTO) throws IOException {
		userService.validateChangeUserRole(authentication);
		goalActivityService.saveDeliverable(projectDeliverableDTO, authentication);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}