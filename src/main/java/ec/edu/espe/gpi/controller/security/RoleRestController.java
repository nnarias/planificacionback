package ec.edu.espe.gpi.controller.security;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.model.security.Role;
import ec.edu.espe.gpi.service.security.IRoleService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.security.UserRoleGeneral;

@RestController
@RequestMapping("/api")
public class RoleRestController {

	@Autowired
	private IRoleService roleService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_USER" })
	@GetMapping("/role/{id}")
	public ResponseEntity<Role> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(roleService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/role")
	public ResponseEntity<List<Role>> finaAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(roleService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/role/user/{id}")
	public ResponseEntity<List<Role>> findUserRole(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(roleService.findUserRole(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_USER" })
	@GetMapping("/role/userGeneral/{role}")
	public ResponseEntity<List<UserRoleGeneral>> findUserRoleGeneral(Authentication authentication,
			@PathVariable String role) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(roleService.findUserRoleGeneral(role), HttpStatus.OK);
	}

	@Secured({ "ROLE_ADMIN" })
	@PostMapping("/role/user/{id}")
	public ResponseEntity<HttpStatus> roleAssignment(@RequestBody List<Long> roleList, @PathVariable Long id) {
		roleService.updateRoles(roleList, id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}