package ec.edu.espe.gpi.controller.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.dto.users.UsersDTO;
import ec.edu.espe.gpi.service.publications.IPublicationsService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.service.users.IUsersService;

@RestController
@RequestMapping("/api")
public class UsersRestController {
	
	@Autowired
	private IUsersService usersService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/users")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody UsersDTO usersDTO) {
		userService.validateChangeUserRole(authentication);
		usersService.save(usersDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
