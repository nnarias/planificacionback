package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.InstitutionInvolvedDTO;
import ec.edu.espe.gpi.model.registry.InstitutionInvolved;
import ec.edu.espe.gpi.service.registry.IInstitutionInvolvedService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class InstitutionInvolvedRestController {

	@Autowired
	private IInstitutionInvolvedService institutionInvolvedService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/institutionInvolved/{id}")
	public ResponseEntity<InstitutionInvolved> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(institutionInvolvedService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/institutionInvolved")
	public ResponseEntity<List<InstitutionInvolved>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(institutionInvolvedService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/institutionInvolved")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody InstitutionInvolvedDTO institutionInvolvedDTO) {
		userService.validateChangeUserRole(authentication);
		institutionInvolvedService.save(institutionInvolvedDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@DeleteMapping("/institutionInvolved/{id}")
	public ResponseEntity<HttpStatus> deleteInstitutionInvolved(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		institutionInvolvedService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
