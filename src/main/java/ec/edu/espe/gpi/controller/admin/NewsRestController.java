package ec.edu.espe.gpi.controller.admin;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import ec.edu.espe.gpi.dto.admin.NewsDTO;
import ec.edu.espe.gpi.model.admin.News;
import ec.edu.espe.gpi.service.admin.IImpDocumentService;
import ec.edu.espe.gpi.service.admin.INewsService;
import ec.edu.espe.gpi.service.security.IUserService;
import ec.edu.espe.gpi.utils.Headers;
import ec.edu.espe.gpi.utils.admin.Document;
import ec.edu.espe.gpi.utils.admin.TypeUserDocument;

@RestController
@RequestMapping("/api")
public class NewsRestController {

	@Autowired
	private INewsService newsService;

	@Autowired
	private IUserService userService;

	@Autowired
	private IImpDocumentService impDocumentService;

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@DeleteMapping("/news/{id}")
	public ResponseEntity<HttpStatus> delete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		newsService.delete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/news/{id}")
	public ResponseEntity<News> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(newsService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/news")
	public ResponseEntity<List<News>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(newsService.findAll(), HttpStatus.OK);
	}

	@GetMapping("/news/file/{id}")
	public ResponseEntity<InputStreamResource> findFile(@PathVariable Long id) throws IOException {
		News news = newsService.find(id);
		InputStreamResource inputStreamResource = impDocumentService.getDocument(news.getFileUUID(),
				TypeUserDocument.PROJECT_ADMIN);
		return new ResponseEntity<>(inputStreamResource,
				Headers.getHeadersMultiparth(impDocumentService.getFilenamePrefix(news.getFileName())), HttpStatus.OK);
	}

	@GetMapping("/news/current")
	public ResponseEntity<List<News>> findLoginNews() {
		return new ResponseEntity<>(newsService.findLoginNews(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/news")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestPart(name = "file", required = false) MultipartFile file, @RequestPart("news") NewsDTO newsDTO)
			throws IOException {
		userService.validateChangeUserRole(authentication);
		String pathFile = "/okm:root/noticias";
		Document document = newsService.sendImage(file, newsDTO, pathFile, TypeUserDocument.PROJECT_ADMIN);
		newsService.save(document, newsDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}