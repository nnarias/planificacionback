package ec.edu.espe.gpi.controller.evaluation;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.evaluation.CriteriaDTO;
import ec.edu.espe.gpi.model.evaluation.Criteria;
import ec.edu.espe.gpi.service.evaluation.ICriteriaService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class CriteriaRestController {

	@Autowired
	private ICriteriaService criteriaService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/criteria/{id}")
	public ResponseEntity<Criteria> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(criteriaService.find(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/criteria")
	public ResponseEntity<List<Criteria>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(criteriaService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/criteria")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@Validated @RequestBody CriteriaDTO criteriaDTO) {
		userService.validateChangeUserRole(authentication);
		criteriaService.save(criteriaDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/criteria/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		criteriaService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}