package ec.edu.espe.gpi.controller.otheractivities;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.JsonPath;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.service.congress.ICongressService;
import ec.edu.espe.gpi.service.otheractivities.IOtherActivitiesService;
import ec.edu.espe.gpi.service.planning.IPlanningService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class OtherActivitiesRestController {
	@Autowired
	private IOtherActivitiesService otherActivitiesService;

	@Autowired
	private IUserService userService;
	
	@Autowired
	private IPlanningService planningService;
	
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/otheractivities/{id}")
	public ResponseEntity<OtherActivities> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(otherActivitiesService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/otheractivities")
	public ResponseEntity<List<OtherActivities>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(otherActivitiesService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/otheractivities/planning/{id}")
	public ResponseEntity<List<OtherActivities>> findAllByPlanning(Authentication authentication, @PathVariable Long id ) {
		userService.validateChangeUserRole(authentication);
		
		Planning planning=planningService.findById(id);
		return new ResponseEntity<>(otherActivitiesService.findAllByPlanning(planning), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/otheractivities")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody OtherActivitiesDTO otherActivitiesDTO) {
		userService.validateChangeUserRole(authentication);
		otherActivitiesService.save(otherActivitiesDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/otheractivities/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		otherActivitiesService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/otheractivities/complete/{id}")
	public ResponseEntity<HttpStatus> updateComplete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		otherActivitiesService.updateComplete(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
