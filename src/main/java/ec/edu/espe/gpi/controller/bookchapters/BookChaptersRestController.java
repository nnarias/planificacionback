package ec.edu.espe.gpi.controller.bookchapters;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.service.bookchapters.IBookChaptersService;
import ec.edu.espe.gpi.service.planning.IBookService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class BookChaptersRestController {
	
	@Autowired
	private IBookChaptersService bookChaptersService;

	@Autowired
	private IUserService userService;
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/bookchapters/{id}")
	public ResponseEntity<BookChapters> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(bookChaptersService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/bookchapters")
	public ResponseEntity<List<BookChapters>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(bookChaptersService.findAll(), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/bookchapters/planning/{planning}")
	public ResponseEntity<List<BookChapters>> findAllByPlanning(Authentication authentication, @PathVariable Planning planning ) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(bookChaptersService.findAllByPlanning(planning), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PostMapping("/bookchapters")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody BookChaptersDTO booChaptersDTO) {
		userService.validateChangeUserRole(authentication);
		bookChaptersService.save(booChaptersDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/bookchapters/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		bookChaptersService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@PatchMapping("/bookchapters/complete/{id}")
	public ResponseEntity<HttpStatus> updateComplete(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		bookChaptersService.updateComplete(id);
		return new ResponseEntity<>(HttpStatus.OK);

	}

}
