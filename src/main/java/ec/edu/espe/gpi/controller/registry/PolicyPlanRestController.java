package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.PolicyPlanDTO;
import ec.edu.espe.gpi.model.registry.PolicyPlan;
import ec.edu.espe.gpi.service.registry.IPolicyPlanService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class PolicyPlanRestController {

	@Autowired
	private IPolicyPlanService policyPlanService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/policyPlan/{id}")
	public ResponseEntity<PolicyPlan> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(policyPlanService.findById(id), HttpStatus.OK);

	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/policyPlan")
	public ResponseEntity<List<PolicyPlan>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(policyPlanService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/policyPlan/goalPlan/{id}")
	public ResponseEntity<List<PolicyPlan>> findByGoalPlan(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(policyPlanService.findByGoalPlan(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/policyPlan")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @RequestBody PolicyPlanDTO policyPlanDTO) {
		userService.validateChangeUserRole(authentication);
		policyPlanService.save(policyPlanDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/policyPlan/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		policyPlanService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}