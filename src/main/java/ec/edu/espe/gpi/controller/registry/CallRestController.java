package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.CallDTO;
import ec.edu.espe.gpi.dto.registry.CallExtensionDTO;
import ec.edu.espe.gpi.model.registry.Call;
import ec.edu.espe.gpi.service.registry.ICallService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class CallRestController {

	@Autowired
	private ICallService callService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/call/extension/{id}")
	public ResponseEntity<HttpStatus> extensionDate(Authentication authentication,
			@RequestBody CallExtensionDTO callExtensionDTO, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		callService.extensionDate(callExtensionDTO, id);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/call/{id}")
	public ResponseEntity<Call> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(callService.find(id), HttpStatus.OK);

	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/call/active")
	public ResponseEntity<List<Call>> findActive(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(callService.findActive(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/call")
	public ResponseEntity<List<Call>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(callService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/call/evaluationActive")
	public ResponseEntity<List<Call>> findAllByEvaluationActive(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(callService.findAllByEvaluationActive(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@GetMapping("/call/evaluationActive/{id}")
	public ResponseEntity<Call> findByEvaluationActive(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(callService.findByEvaluationActive(id), HttpStatus.OK);
	}
	
	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/call")
	public ResponseEntity<HttpStatus> save(Authentication authentication, @Validated @RequestBody CallDTO callDTO) {
		userService.validateChangeUserRole(authentication);
		callService.save(callDTO);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/call/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		callService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}