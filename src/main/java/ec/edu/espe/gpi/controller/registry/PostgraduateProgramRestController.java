package ec.edu.espe.gpi.controller.registry;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import ec.edu.espe.gpi.dto.registry.PostgraduateProgramDTO;
import ec.edu.espe.gpi.model.registry.PostgraduateProgram;
import ec.edu.espe.gpi.service.registry.IPostgraduateProgramService;
import ec.edu.espe.gpi.service.security.IUserService;

@RestController
@RequestMapping("/api")
public class PostgraduateProgramRestController {

	@Autowired
	private IPostgraduateProgramService postgraduateProgramService;

	@Autowired
	private IUserService userService;

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/postgraduateProgram/{id}")
	public ResponseEntity<PostgraduateProgram> find(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(postgraduateProgramService.findById(id), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN", "ROLE_PROJECT_DIRECTOR" })
	@GetMapping("/postgraduateProgram")
	public ResponseEntity<List<PostgraduateProgram>> findAll(Authentication authentication) {
		userService.validateChangeUserRole(authentication);
		return new ResponseEntity<>(postgraduateProgramService.findAll(), HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PostMapping("/postgraduateProgram")
	public ResponseEntity<HttpStatus> save(Authentication authentication,
			@RequestBody PostgraduateProgramDTO postgraduateProgramDTO) {
		userService.validateChangeUserRole(authentication);
		postgraduateProgramService.save(postgraduateProgramDTO);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@Secured({ "ROLE_PROJECT_ADMIN" })
	@PatchMapping("/postgraduateProgram/{id}")
	public ResponseEntity<HttpStatus> updateState(Authentication authentication, @PathVariable Long id) {
		userService.validateChangeUserRole(authentication);
		postgraduateProgramService.updateState(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}