package ec.edu.espe.gpi.congressTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
import ec.edu.espe.gpi.service.congress.ICongressService;
import ec.edu.espe.gpi.service.planning.IPlanningService;
@SpringBootTest


class TestCongressService {


	@Autowired
	ICongressService icongressService;
	private Planning planning;

	@Test
	void testIcongressServiceFindAll() {
		List<Congress> listCongress=icongressService.findAll();
		assertThat(listCongress).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	void testIcongressServiceFindById() {
		Long cod = 1L;
		Congress congress=icongressService.findById(cod);
		assertThat(congress.getId()).isEqualTo(cod);
	}
	
	@Test
	void testIcongressServiceSave() {
		CongressDTO congresDTO = new CongressDTO();
		congresDTO.setId(new Long(1));
		congresDTO.setPlanning(planning);
		congresDTO.setCodIES("001");
		congresDTO.setType("articulo");
		congresDTO.setArticleType("Revista");
		congresDTO.setCodPUB("l00hd");
		congresDTO.setPresentationName("Articulo a");
		congresDTO.setEventName("Iberoamericano");
		congresDTO.setEventEdition(new Long (123));
		congresDTO.setEventOrganizer("Maria Caicedo");
		congresDTO.setOrganizingCommite("Camilo, Julio");
		congresDTO.setCountry("Ecuador");
		congresDTO.setCity("Guayaquil");
		congresDTO.setPublicationDate((new Date(121, 5,3)));
		congresDTO.setDetailField("junio 2020");
		congresDTO.setCompetitor("Libro cientifico");
		congresDTO.setDuration(new Long (12));
		Congress congress=icongressService.findById(1L);
		assertThat(congress.getCompetitor()).isEqualTo("Libro cientifico");
	}
	
	@Test
	void testICongressServiceUpdateState() {
		icongressService.updateState(1L);
		Optional<Congress> congress=icongressService.findByIdStateFalse(1L);
		assertTrue(congress.get().getRemoved());
		//remove
	}


}
