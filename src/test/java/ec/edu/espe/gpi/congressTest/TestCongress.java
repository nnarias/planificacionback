package ec.edu.espe.gpi.congressTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
@SpringBootTest
class TestCongress {
	private Congress congress;
	private Planning planning;

	@Test
	void testPassNewObjectCongress() {
		congress=new Congress();
		congress.setId(new Long(1));
		congress.setPlanning(planning);
		congress.setCodIES("001");
		congress.setType("articulo");
		congress.setArticleType("Revista");
		congress.setCodPUB("l00hd");
		congress.setPresentationName("Articulo a");
		congress.setEventName("Iberoamericano");
		congress.setEventEdition(new Long (123));
		congress.setEventOrganizer("Maria Caicedo");
		congress.setOrganizingCommite("Camilo, Julio");
		congress.setCountry("Ecuador");
		congress.setCity("Guayaquil");
		congress.setPublicationDate((new Date(121, 5,3)));
		congress.setDetailField("junio 2020");
		congress.setCompetitor("Libro cientifico");
		congress.setComplete(true);
		congress.setRemoved(false);
		congress.setDuration(new Long (12));
		congress.setAprobation("Espera");
		congress.setObservations("Ninguna");
		assertEquals(congress, new Congress(new Long(1),planning,"001","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna"));
		
	}
	@Test
	void testFailNewObjectCongress() {
		congress=new Congress();
		congress.setId(new Long(1));
		congress.setPlanning(planning);
		congress.setCodIES("001");
		congress.setType("articulo");
		congress.setArticleType("Revista");
		congress.setCodPUB("l00hd");
		congress.setPresentationName("Articulo a");
		congress.setEventName("Iberoamericano");
		congress.setEventEdition(new Long (123));
		congress.setEventOrganizer("Maria Caicedo");
		congress.setOrganizingCommite("Camilo, Julio");
		congress.setCountry("Ecuador");
		congress.setCity("Guayaquil");
		congress.setPublicationDate((new Date(121, 5,3)));
		congress.setDetailField("junio 2020");
		congress.setCompetitor("Libro cientifico");
		congress.setComplete(true);
		congress.setRemoved(false);
		congress.setDuration(new Long (12));
		congress.setAprobation("Espera");
		congress.setObservations("Ninguna");
		assertNotSame(congress, new Congress(new Long(1),planning,"002","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna"));
	}
	@Test
	void testPassExistObjectCongress() {
		congress=new Congress(new Long(1),planning,"002","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna");
		assertThat(congress.getCompetitor()).isEqualTo("Libro cientifico");
	}
	
	@Test
	void testFailExistObjectCongress() {
		congress=new Congress(new Long(1),planning,"002","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna");
		assertNotSame(congress, new Congress(new Long(1),planning,"002","articulos","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna"));
	}

}
