package ec.edu.espe.gpi.congressTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Planning;

class TestCongressDto {

	@Autowired
	CongressDTO congresDTO;
	private Planning planning;
	@Test
	void testCreateNewCongressDTO(){
		congresDTO = new CongressDTO();
		
		congresDTO=new CongressDTO();
		congresDTO.setId(new Long(1));
		congresDTO.setPlanning(planning);
		congresDTO.setCodIES("001");
		congresDTO.setType("articulo");
		congresDTO.setArticleType("Revista");
		congresDTO.setCodPUB("l00hd");
		congresDTO.setPresentationName("Articulo a");
		congresDTO.setEventName("Iberoamericano");
		congresDTO.setEventEdition(new Long (123));
		congresDTO.setEventOrganizer("Maria Caicedo");
		congresDTO.setOrganizingCommite("Camilo, Julio");
		congresDTO.setCountry("Ecuador");
		congresDTO.setCity("Guayaquil");
		congresDTO.setPublicationDate((new Date(121, 5,3)));
		congresDTO.setDetailField("junio 2020");
		congresDTO.setCompetitor("Libro cientifico");
		congresDTO.setDuration(new Long (12));
		congresDTO.setAprobation("Espera");
		congresDTO.setObservations("Ninguna");
		assertEquals(congresDTO, new CongressDTO (new Long(1),planning,"001","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",new Long (12),"Espera","Ninguna"));
	}
	@Test
	void testExistCongressDTO(){
		congresDTO=new CongressDTO(new Long(1),planning,"001","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",new Long (12),"Espera","Ninguna");
		assertThat(congresDTO.getCompetitor()).isEqualTo("Libro cientifico");
		
	}

}
