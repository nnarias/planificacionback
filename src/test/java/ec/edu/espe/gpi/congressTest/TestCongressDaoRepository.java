package ec.edu.espe.gpi.congressTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import ec.edu.espe.gpi.dao.congress.ICongressDao;
import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Planning;
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)

class TestCongressDaoRepository {
	@Autowired
	ICongressDao congressRepository;
	private Planning planning;
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreateCongress() {
		Long cod=1L;
		Congress congress = new Congress(new Long(1),planning,"001","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna");
		Congress saveCongress=congressRepository.save(congress);
		assertNotNull(saveCongress);
		
	}
	@Test
	@Order(2)
	public void testFindyPlanningByIdExist() {
		Long cod=1L;
		Optional<Congress> findCongress=congressRepository.findByIdEnable(cod);
		assertThat(findCongress.get().getId()).isEqualTo(cod);
	}
	@Test
	@Order(3)
	public void testFindyCongressByIdExistNotExist() {
		Optional<Congress> findPlanning=congressRepository.findByIdEnable(3L);
		//assertEquals(true, findPlanning.isEmpty());
		
	}
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdateCongress() {
		Long cod=1L;
		Congress congress = new Congress(new Long(1),planning,"001","articulo","Revista","l00hd","Articulo a","Iberoamericano",new Long (123),"Maria Caicedo","Camilo, Julio","Ecuador","Guayaquil",new Date(121, 5,3),"junio 2020","Libro cientifico",true, false,new Long (12),"Espera","Ninguna");
		congressRepository.save(congress);
		Optional<Congress> updateCongress=congressRepository.findById(cod);
		assertThat(updateCongress.get().getId()).isEqualTo(cod);
		
	}
	
	@Test
	@Order(5)
	public void testListCongress() {
		List<Congress> listCongress=(List<Congress>)congressRepository.findAll();
		assertThat(listCongress).size().isGreaterThan(0);
		
	}
	
	/*@Test
	@Rollback(false)
	@Order(6)
	public void testDeleteCongress() {
		Long cod=1L;
		boolean afterDelete=congressRepository.findByIdEnable(cod).isPresent();
		congressRepository.deleteById(cod);
		boolean beforeDelete=congressRepository.findByIdEnable(cod).isPresent();
		assertTrue(afterDelete);
		assertFalse(beforeDelete);		
	}*/


}
