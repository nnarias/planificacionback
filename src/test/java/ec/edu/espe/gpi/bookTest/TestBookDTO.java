package ec.edu.espe.gpi.bookTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Book;

class TestBookDTO {
	
	@Autowired
	BookDTO bookDTO;
	
	@Test
	void testCreateNewBookDTO(){
		bookDTO = new BookDTO();
		bookDTO.setId(1L);
		bookDTO.setCodies(20);
		bookDTO.setPublicationType("Articulo");
		bookDTO.setPublicationCode("1-BC");
		bookDTO.setPublicationDate(new Date(121, 0,3));	
		assertEquals(bookDTO, new BookDTO(1L,20,"Articulo","1-BC",new Date(121, 0,3)));
	}
	@Test
	void testExistBookDTO(){
		bookDTO=new BookDTO(1L,20,"Articulo","1-BC",new Date(121, 0,3));
		assertThat(bookDTO.getId()).isEqualTo(1L);
		
	}
	

}
