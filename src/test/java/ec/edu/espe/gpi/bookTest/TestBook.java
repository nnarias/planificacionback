package ec.edu.espe.gpi.bookTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotSame;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.planning.Book;

@SpringBootTest
public class TestBook {
	private Book book;
	
	@Test
	void testPassNewObjectBook() {
		book=new Book();
		book.setId(new Long(1));
		book.setCodies(10);
		book.setPublicationCode("1A");
		book.setPublicationDate(new Date(121, 5,3));
		book.setPublicationType("Activo");
		book.setState(true);
		assertEquals(book, new Book(new Long(1),10,"1A",new Date(121, 5,3),"Activo",true));
	}
	@Test
	void testFailNewObjectBook() {
		book=new Book();
		book.setId(new Long(1));
		book.setCodies(10);
		book.setPublicationCode("1A");
		book.setPublicationDate(new Date(121, 5,3));
		book.setPublicationType("Activo");
		book.setState(true);
		assertNotSame(book, new Book(new Long(2),10,"2A",new Date(121, 5,3),"Activo",true));
	}
	@Test
	void testPassExistObjectBook() {
		book=new Book(new Long(1), 101, "3A", new Date(121, 5,3), "Activo", true);
		assertThat(book.getPublicationType()).isEqualTo("Activo");
	}
	
	@Test
	void testFailExistObjectBook() {
		book=new Book(new Long(1), 101, "3A", new Date(121, 5,3), "Activo", true);
		assertNotSame(book, new Book(new Long(2),10,"2A",new Date(121, 5,3),"Activo",true));
	}
	

}
