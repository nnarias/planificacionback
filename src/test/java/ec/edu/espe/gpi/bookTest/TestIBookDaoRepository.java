package ec.edu.espe.gpi.bookTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import ec.edu.espe.gpi.dao.planning.IBookDao;
import ec.edu.espe.gpi.model.planning.Book;



@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)
class TestIBookDaoRepository {

	@Autowired
	private IBookDao bookRepository;
	
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreateBook() {
		Long cod=1L;
		Book book = new Book(cod,10,"1A",new Date(121, 5,3),"Activo",true);
		Book saveBook=bookRepository.save(book);
		assertNotNull(saveBook);
		
	}
	@Test
	@Order(2)
	public void testFindyBookByIdExist() {
		Long cod=1L;
		Optional<Book> findBook=bookRepository.findByIdEnable(cod);
		assertThat(findBook.get().getId()).isEqualTo(cod);
	}
	@Test
	@Order(3)
	public void testFindyBookByIdExistNotExist() {
		Optional<Book> findBook=bookRepository.findByIdEnable(3L);
		//assertEquals(true, findBook.isEmpty());
		
	}
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdateBook() {
		Long cod=1L;
		Book book = new Book(cod,10,"1BBB",new Date(121, 6,3),"Activo",true);
		bookRepository.save(book);
		Optional<Book> updateBook=bookRepository.findById(cod);
		assertThat(updateBook.get().getId()).isEqualTo(cod);
		
	}
	
	@Test
	@Order(5)
	public void testListBook() {
		List<Book> listBook=(List<Book>)bookRepository.findAll();
		assertThat(listBook).size().isGreaterThan(0);
		
	}
	
	/*@Test
	@Rollback(false)
	@Order(6)
	public void testDeleteBook() {
		Long cod=1L;
		boolean afterDelete=bookRepository.findByIdEnable(cod).isPresent();
		bookRepository.deleteById(cod);
		boolean beforeDelete=bookRepository.findByIdEnable(cod).isPresent();
		assertTrue(afterDelete);
		assertFalse(beforeDelete);		
	}*/
}
