package ec.edu.espe.gpi.bookTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.service.planning.IBookService;

@SpringBootTest

class TestIBookService {
	
	@Autowired
	IBookService ibookService;

	@Test
	void testIbookServiceFindAll() {
		List<Book> listBook=ibookService.findAll();
		assertThat(listBook).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	void testIbookServiceFindById() {
		Book book=ibookService.findById(1L);
		assertThat(book.getId()).isEqualTo(1L);
	}
	
	@Test
	void testIbookServiceSave() {
		BookDTO bookDTO = new BookDTO();
		bookDTO.setId(1L);
		bookDTO.setCodies(20);
		bookDTO.setPublicationType("Articulo");
		bookDTO.setPublicationCode("1-BC");
		bookDTO.setPublicationDate(new Date(121, 0,3));	
		ibookService.save(bookDTO);
		Book book=ibookService.findById(1L);
		assertThat(book.getPublicationType()).isEqualTo("Articulo");
	}
	
	@Test
	void testIbookServiceUpdateState() {
		ibookService.updateState(1L);
		Optional<Book> book=ibookService.findByIdStateFalse(1L);
		assertFalse(book.get().getState());
	}

}
