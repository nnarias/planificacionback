package ec.edu.espe.gpi.planningTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;


class TestPlanningDto {


	@Autowired
	PlanningDTO planningDTO;
	private Users users;
	
	@Test
	void testCreateNewPlanningDTO(){
		planningDTO = new PlanningDTO();
		
		planningDTO.setId(new Long(1));
		planningDTO.setUsers(users);
		planningDTO.setMaxDuration(new Long(9));
		planningDTO.setAprobation(true);
		planningDTO.setResponsibleId("Adminitrador");
		planningDTO.setRegistrationDate(new Date(121, 5,3));
		planningDTO.setObservations("Ninguna");
		planningDTO.setEvaluatorId("Evaluador");
		planningDTO.setDegree("PHD");
		assertEquals(planningDTO, new PlanningDTO(new Long(1),users,new Long(9),true,"Adminitrador",new Date(121, 5,3),"Ninguna","Evaluador","PHD"));
	}
	@Test
	void testExistBookDTO(){
		planningDTO=new PlanningDTO(new Long(1),users,new Long(9),true,"Adminitrador",new Date(121, 5,3),"Ninguna","Evaluador","PHD");
		assertThat(planningDTO.getAprobation()).isEqualTo(true);
		
	}

}
