package ec.edu.espe.gpi.planningTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
@SpringBootTest
class TestPlanning {
	private Users users;
	private Planning plannnig;
	
	
	@Test
	void testPassNewObjectPlanning() {
		
		plannnig=new Planning();
		plannnig.setId(new Long(1));
		plannnig.setUsers(users);
		plannnig.setMaxDuration(new Long(9));
		plannnig.setAprobation(true);
		plannnig.setResponsibleId("Adminitrador");
		plannnig.setRegistrationDate(new Date(121, 5,3));
		plannnig.setComplete(true);
		plannnig.setRemoved(true);
		plannnig.setObservations("Ninguna");
		plannnig.setEvaluatorId("Evaluador");
		plannnig.setDegree("PHD");
		assertEquals(plannnig, new Planning(new Long(1),users,new Long(9),true,"Adminitrador",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD"));
	}
	@Test
	void testFailNewObjectPlanning() {
		plannnig=new Planning();
		plannnig.setId(new Long(1));
		plannnig.setUsers(users);
		plannnig.setMaxDuration(new Long(9));
		plannnig.setAprobation(true);
		plannnig.setResponsibleId("Adminitrador");
		plannnig.setRegistrationDate(new Date(121, 5,3));
		plannnig.setComplete(true);
		plannnig.setRemoved(true);
		plannnig.setObservations("Ninguna");
		plannnig.setEvaluatorId("Evaluador");
		plannnig.setDegree("PHD");
		assertNotSame(plannnig, new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD"));
	}
	@Test
	void testPassExistObjectPlanning() {
		plannnig=new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD");
		assertThat(plannnig.getResponsibleId()).isEqualTo("Gerente");
	}
	
	@Test
	void testFailExistObjectBook() {
		plannnig=new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD");
		assertNotSame(plannnig, new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, false,"Ninguna","Evaluador","PHD"));
	}

}
