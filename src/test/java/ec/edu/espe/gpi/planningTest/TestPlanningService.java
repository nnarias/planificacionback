package ec.edu.espe.gpi.planningTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
import ec.edu.espe.gpi.service.planning.IPlanningService;
@SpringBootTest
class TestPlanningService {

	@Autowired
	IPlanningService iplanningService;
	private Users users;

	@Test
	@Rollback(false)
	@Order(1)
	void testIplanningServiceFindAll() {
		List<Planning> listPlanning=iplanningService.findAll();
		assertThat(listPlanning).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	@Order(2)
	void testIplanningServiceFindById() {
		Long cod = 1L;
		Planning planning=iplanningService.findById(cod);
		assertThat(planning.getId()).isEqualTo(cod);
	}
	
	@Test
	@Order(3)
	void testIplanningServiceSave() {
		PlanningDTO plannnigDTO = new PlanningDTO();
		plannnigDTO.setId(new Long(1));
		plannnigDTO.setUsers(users);
		plannnigDTO.setMaxDuration(new Long(9));
		plannnigDTO.setAprobation(true);
		plannnigDTO.setResponsibleId("Adminitrador");
		plannnigDTO.setRegistrationDate(new Date(121, 5,3));
		Planning planning=iplanningService.findById(1L);
		assertThat(planning.getResponsibleId()).isEqualTo("Administrador");
	}
	
	@Test
	@Order(4)
	void testIPlanningServiceUpdateState() {
		iplanningService.updateState(1L);
		Optional<Planning> book=iplanningService.findByIdStateFalse(1L);
		assertTrue(book.get().getRemoved());
	}


}
