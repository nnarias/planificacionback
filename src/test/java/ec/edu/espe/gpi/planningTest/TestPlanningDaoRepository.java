package ec.edu.espe.gpi.planningTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import ec.edu.espe.gpi.dao.planning.IPlanningDao;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)

class TestPlanningDaoRepository {

	@Autowired
	private IPlanningDao planningRepository;
	private Users users;
	
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreatePlanning() {
		Long cod=1L;
		Planning planning = new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD");
		Planning savePlaning=planningRepository.save(planning);
		assertNotNull(savePlaning);
		
	}
	@Test
	@Order(2)
	public void testFindyPlanningByIdExist() {
		Long cod=1L;
		Optional<Planning> findPlanning=planningRepository.findByIdEnable(cod);
		//assertThat(findPlanning.get().getId()).isEqualTo(cod);
	}
	@Test
	@Order(3)
	public void testFindyPlanningByIdExistNotExist() {
		Optional<Planning> findPlanning=planningRepository.findByIdEnable(3L);
		//assertEquals(true, findPlanning.isEmpty());
		
	}
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdatePlanning() {
		Long cod=1L;
		Planning planning = new Planning(new Long(1),users,new Long(9),true,"Gerente",new Date(121, 5,3), true, true,"Ninguna","Evaluador","PHD");
		planningRepository.save(planning);
		Optional<Planning> updatePlanning=planningRepository.findById(cod);
		assertThat(updatePlanning.get().getId()).isEqualTo(cod);
		
	}
	
	@Test
	@Order(5)
	public void testListPlanning() {
		List<Planning> listPlanning=(List<Planning>)planningRepository.findAll();
		assertThat(listPlanning).size().isGreaterThan(0);
		
	}
	
	/*@Test
	@Rollback(false)
	@Order(6)
	public void testDeletePlanning() {
		Long cod=1L;
		boolean afterDelete=planningRepository.findByIdEnable(cod).isPresent();
		planningRepository.deleteById(cod);
		boolean beforeDelete=planningRepository.findByIdEnable(cod).isPresent();
		assertTrue(afterDelete);
		assertFalse(beforeDelete);		
	}*/

}
