package ec.edu.espe.gpi.ChapterBooksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.bookchapters.BookChaptersDTO;
import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.planning.Planning;

class TestBookChapterDTO {


	@Autowired
	BookChaptersDTO bookschapaterDTO;
private Planning planning;
	
	@Test
	void testCreateNewBooksDTO(){
		bookschapaterDTO=new BookChaptersDTO();
		bookschapaterDTO.setId(new Long(1));
		bookschapaterDTO.setPlanning(planning);
		bookschapaterDTO.setCodIES("1234");
		bookschapaterDTO.setPublicationType("1A");
		bookschapaterDTO.setChapterCode("L3456");
		bookschapaterDTO.setCodPUB("123eg");
		bookschapaterDTO.setCapTitle("Tecnologia y TI");
		bookschapaterDTO.setBookTitle("Test Driven Development");
		bookschapaterDTO.setCodISB("123GUB");
		bookschapaterDTO.setEditor("Marvella Cabascango");
		bookschapaterDTO.setNumberPages(new Long(80));
		bookschapaterDTO.setPublicationDate(new Date(121, 5,3));
		bookschapaterDTO.setDetailField("Tecnologia y gestion de TI");
		bookschapaterDTO.setFiliation(true);
		bookschapaterDTO.setCompetitor("Henry Cadena");	
		bookschapaterDTO.setDuration(new Long(9));
		bookschapaterDTO.setAprobation("Espera");
		bookschapaterDTO.setObservations("Ninguna");
		assertEquals(bookschapaterDTO, new BookChaptersDTO(new Long(1), planning, "1234", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena",new Long(9),"Espera","Ninguna"));
	
	}
	@Test
	void testExistBookDTO(){
		bookschapaterDTO=new BookChaptersDTO(new Long(1), planning, "1234", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena",new Long(9),"Espera","Ninguna");
		assertThat(bookschapaterDTO.getId()).isEqualTo(1L);
		
	}
}
