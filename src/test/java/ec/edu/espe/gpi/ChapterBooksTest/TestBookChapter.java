package ec.edu.espe.gpi.ChapterBooksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Planning;
@SpringBootTest
class TestBookChapter {
	private BookChapters bookChapter;
	private Planning planning;
		
		@Test
		void testPassNewObjectBooks() {
			bookChapter=new BookChapters();
			bookChapter.setId(new Long(1));
			bookChapter.setPlanning(planning);
			bookChapter.setCodIES("1234");
			bookChapter.setPublicationType("1A");
			bookChapter.setChapterCode("L3456");
			bookChapter.setCodPUB("123eg");
			bookChapter.setCapTitle("Tecnologia y TI");
			bookChapter.setBookTitle("Test Driven Development");
			bookChapter.setCodISB("123GUB");
			bookChapter.setEditor("Marvella Cabascango");
			bookChapter.setNumberPages(new Long(80));
			bookChapter.setPublicationDate(new Date(121, 5,3));
			bookChapter.setDetailField("Tecnologia y gestion de TI");
			bookChapter.setFiliation(true);
			bookChapter.setCompetitor("Henry Cadena");		
			bookChapter.setComplete(true);
			bookChapter.setRemoved(true);
			bookChapter.setDuration(new Long(9));
			bookChapter.setAprobation("Espera");
			bookChapter.setObservations("Ninguna");
			assertEquals(bookChapter, new BookChapters(new Long(1), planning, "1234", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
		
		}
		@Test
		void testFailNewObjectBooks() {
			bookChapter=new BookChapters();
			bookChapter.setId(new Long(1));
			bookChapter.setPlanning(planning);
			bookChapter.setCodIES("1234");
			bookChapter.setPublicationType("1A");
			bookChapter.setChapterCode("L3456");
			bookChapter.setCodPUB("123eg");
			bookChapter.setCapTitle("Tecnologia y TI");
			bookChapter.setBookTitle("Test Driven Development");
			bookChapter.setCodISB("123GUB");
			bookChapter.setEditor("Marvella Cabascango");
			bookChapter.setNumberPages(new Long(80));
			bookChapter.setPublicationDate(new Date(121, 5,3));
			bookChapter.setDetailField("Tecnologia y gestion de TI");
			bookChapter.setFiliation(true);
			bookChapter.setCompetitor("Henry Cadena");		
			bookChapter.setComplete(true);
			bookChapter.setRemoved(true);
			bookChapter.setDuration(new Long(9));
			bookChapter.setAprobation("Espera");
			bookChapter.setObservations("Ninguna");
			assertNotSame(bookChapter, new BookChapters(new Long(1), planning, "1234789", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
		}
		@Test
		void testPassExistObjectBooks() {
			bookChapter=new BookChapters(new Long(1), planning, "1234789", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
		
			assertThat(bookChapter.getPublicationType()).isEqualTo("1A");
		}
		
		@Test
		void testFailExistObjectBooks() {
			bookChapter=new BookChapters(new Long(1), planning, "1234789", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
			assertNotSame(bookChapter, new BookChapters(new Long(1), planning, "1234789", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
		}
	

}
