package ec.edu.espe.gpi.ChapterBooksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import ec.edu.espe.gpi.dao.bookchapters.IBookChaptersDao;
import ec.edu.espe.gpi.dao.books.IBooksDao;
import ec.edu.espe.gpi.model.bookchapters.BookChapters;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Planning;

@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)
class TestBookChapterDaoRepository {
	@Autowired
	private IBookChaptersDao bookscchapterRepository;
	private Planning planning;
	
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreateBook() {
		Long cod=1L;
		BookChapters book = new BookChapters(new Long(1), planning, "1234", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
		
		BookChapters saveBooks=bookscchapterRepository.save(book);
		assertNotNull(saveBooks);
		
	}
	@Test
	@Order(2)
	public void testFindyBookssByIdExist() {
		Long cod=1L;
		Optional<BookChapters> findBook=bookscchapterRepository.findByIdEnable(cod);
		assertThat(findBook.get().getId()).isEqualTo(cod);
	}
	@Test
	@Order(3)
	public void testFindyBooksByIdExistNotExist() {
		Optional<BookChapters> findBook=bookscchapterRepository.findByIdEnable(3L);
		//assertEquals(true, findBook.isEmpty());
		
	}
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdateBook() {
		Long cod=1L;
		BookChapters book = new BookChapters(new Long(1), planning, "1234", "1A","L3456", "123eg","Tecnologia y TI","Test Driven Development","123GUB","Marvella Cabascango",new Long(80),new Date(121, 5,3),"Tecnologia y gestion de TI",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
		bookscchapterRepository.save(book);
		Optional<BookChapters> updateBook=bookscchapterRepository.findById(cod);
		assertThat(updateBook.get().getId()).isEqualTo(cod);
		
	}
	
	@Test
	@Order(5)
	public void testListBook() {
		List<BookChapters> listBook=(List<BookChapters>)bookscchapterRepository.findAll();
		assertThat(listBook).size().isGreaterThan(0);
		
	}
	
	/*@Test
	@Rollback(false)
	@Order(6)
	public void testDeleteBooks() {
		Long cod=1L;
		boolean afterDelete=bookscchapterRepository.findByIdEnable(cod).isPresent();
		bookscchapterRepository.deleteById(cod);
		boolean beforeDelete=bookscchapterRepository.findByIdEnable(cod).isPresent();
		assertTrue(afterDelete);
		assertFalse(beforeDelete);		
	}*/
}
