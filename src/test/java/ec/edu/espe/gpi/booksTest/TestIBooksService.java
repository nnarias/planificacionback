package ec.edu.espe.gpi.booksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.service.books.IBooksService;
import ec.edu.espe.gpi.service.planning.IBookService;

class TestIBooksService {

	@Autowired
	IBooksService ibookService;
	private Planning planning;

	@Test
	void testIbookServiceFindAll() {
		List<Books> listBook=ibookService.findAll();
		assertThat(listBook).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	void testIbookServiceFindById() {
		Books book=ibookService.findById(1L);
		assertThat(book.getId()).isEqualTo(1L);
	}
	
	@Test
	void testIbookServiceSave() {
		BooksDTO booksDTO = new BooksDTO();
		booksDTO.setId(new Long(1));
		booksDTO.setPlanning(planning);
		booksDTO.setCodIES("1234");
		booksDTO.setPublicationType("1A");
		booksDTO.setCodPUB("123eg");
		booksDTO.setBookTitle("Tecnologia y TI");
		booksDTO.setCodISB("123GUB");
		booksDTO.setPublicationDate(new Date(121, 5,3));
		booksDTO.setDetailField("Tecnologia y gestion de TI");
		booksDTO.setPeerReviewed("Revisado e indexado");
		booksDTO.setFiliation(true);
		booksDTO.setCompetitor("Henry Cadena");	
		booksDTO.setDuration(new Long(9));	
		ibookService.save(booksDTO);
		Books book=ibookService.findById(1L);
		assertThat(book.getPublicationType()).isEqualTo("1A");
	}
	
	@Test
	void testIbookServiceUpdateState() {
		ibookService.updateState(1L);
		Optional<Books> book=ibookService.findByIdStateFalse(1L);
		assertTrue(book.get().getRemoved());
	}

}
