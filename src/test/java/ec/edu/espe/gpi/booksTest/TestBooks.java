package ec.edu.espe.gpi.booksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Book;
import ec.edu.espe.gpi.model.planning.Planning;

@SpringBootTest
class TestBooks {
private Books book;
private Planning planning;
	
	@Test
	void testPassNewObjectBooks() {
		book=new Books();
		book.setId(new Long(1));
		book.setPlanning(planning);
		book.setCodIES("1234");
		book.setPublicationType("1A");
		book.setCodPUB("123eg");
		book.setBookTitle("Tecnologia y TI");
		book.setCodISB("123GUB");
		book.setPublicationDate(new Date(121, 5,3));
		book.setDetailField("Tecnologia y gestion de TI");
		book.setPeerReviewed("Revisado e indexado");
		book.setFiliation(true);
		book.setCompetitor("Henry Cadena");		
		book.setComplete(true);
		book.setRemoved(true);
		book.setDuration(new Long(9));
		book.setAprobation("Espera");
		book.setObservations("Ninguna");
		assertEquals(book, new Books(new Long(1), planning, "1234", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
	}
	@Test
	void testFailNewObjectBooks() {
		book=new Books();
		book.setId(new Long(1));
		book.setPlanning(planning);
		book.setCodIES("1234");
		book.setPublicationType("1A");
		book.setCodPUB("123eg");
		book.setBookTitle("Tecnologia y TI");
		book.setCodISB("123GUB");
		book.setPublicationDate(new Date(121, 5,3));
		book.setDetailField("Tecnologia y gestion de TI");
		book.setPeerReviewed("Revisado e indexado");
		book.setFiliation(true);
		book.setCompetitor("Henry Cadena");		
		book.setComplete(true);
		book.setRemoved(true);
		book.setDuration(new Long(9));
		book.setAprobation("Espera");
		book.setObservations("Ninguna");
		assertNotSame(book, new Books(new Long(1), planning, "1234456", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
	}
	@Test
	void testPassExistObjectBooks() {
		book=new Books(new Long(1), planning, "1234567", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
	
		assertThat(book.getPublicationType()).isEqualTo("1A");
	}
	
	@Test
	void testFailExistObjectBooks() {
		book=new Books(new Long(1), planning, "1234567", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna");
		assertNotSame(book, new Books(new Long(1), planning, "1234", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena", true,true,new Long(9),"Espera","Ninguna"));
	}
}
