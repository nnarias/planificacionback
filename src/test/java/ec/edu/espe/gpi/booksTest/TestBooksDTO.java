package ec.edu.espe.gpi.booksTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.books.BooksDTO;
import ec.edu.espe.gpi.dto.plannig.BookDTO;
import ec.edu.espe.gpi.model.books.Books;
import ec.edu.espe.gpi.model.planning.Planning;

class TestBooksDTO {

	@Autowired
	BooksDTO booksDTO;
private Planning planning;
	
	@Test
	void testCreateNewBooksDTO(){
		booksDTO=new BooksDTO();
		booksDTO.setId(new Long(1));
		booksDTO.setPlanning(planning);
		booksDTO.setCodIES("1234");
		booksDTO.setPublicationType("1A");
		booksDTO.setCodPUB("123eg");
		booksDTO.setBookTitle("Tecnologia y TI");
		booksDTO.setCodISB("123GUB");
		booksDTO.setPublicationDate(new Date(121, 5,3));
		booksDTO.setDetailField("Tecnologia y gestion de TI");
		booksDTO.setPeerReviewed("Revisado e indexado");
		booksDTO.setFiliation(true);
		booksDTO.setCompetitor("Henry Cadena");	
		booksDTO.setDuration(new Long(9));
		booksDTO.setAprobation("Espera");
		booksDTO.setObservations("Ninguna");
		assertEquals(booksDTO, new BooksDTO(new Long(1), planning, "1234", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena",new Long(9),"Espera","Ninguna"));
	}
	@Test
	void testExistBookDTO(){
		booksDTO=new BooksDTO(new Long(1), planning, "1234", "1A", "123eg","Tecnologia y TI","123GUB",new Date(121, 5,3),"Tecnologia y gestion de TI","Revisado e indexado",true,"Henry Cadena",new Long(9),"Espera","Ninguna");
		assertThat(booksDTO.getId()).isEqualTo(1L);
		
	}
	
}
