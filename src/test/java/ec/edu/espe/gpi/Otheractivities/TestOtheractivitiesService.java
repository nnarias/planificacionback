package ec.edu.espe.gpi.Otheractivities;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.dto.plannig.PlanningDTO;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.users.Users;
import ec.edu.espe.gpi.service.otheractivities.IOtherActivitiesService;
import ec.edu.espe.gpi.service.planning.IPlanningService;
@SpringBootTest
class TestOtheractivitiesService {
	@Autowired
	IOtherActivitiesService iOtheractivitiesService;
	private Planning planning;

	@Test
	void testIOtheractivitiesServiceFindAll() {
		List<OtherActivities> listOtherActivities=iOtheractivitiesService.findAll();
		assertThat(listOtherActivities).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	void testIOtheractivitiesServiceFindById() {
		Long cod = 1L;
		OtherActivities OtherActivities=iOtheractivitiesService.findById(cod);
		assertThat(OtherActivities.getId()).isEqualTo(cod);
	}
	
	@Test
	void testIOtheractivitiesServiceSave() {
		OtherActivitiesDTO otheractivitiesDTO = new OtherActivitiesDTO();
		
		otheractivitiesDTO=new OtherActivitiesDTO();
		otheractivitiesDTO.setId(new Long(1));
		otheractivitiesDTO.setPlanning(planning);
		otheractivitiesDTO.setName("Tutoria");
		otheractivitiesDTO.setDuration(new Long(12L));
		otheractivitiesDTO.setLink("https.iberoameircana");
		assertThat(otheractivitiesDTO.getName()).isEqualTo("Tutoria");
	}
	
	@Test
	void testIOtheractivitiesServiceUpdateState() {
		iOtheractivitiesService.updateState(1L);
		Optional<OtherActivities> otheractivities=iOtheractivitiesService.findByIdStateFalse(1L);
		assertTrue(otheractivities.get().getRemoved());
	}

}
