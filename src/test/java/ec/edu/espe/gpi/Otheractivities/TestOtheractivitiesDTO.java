package ec.edu.espe.gpi.Otheractivities;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;

class TestOtheractivitiesDTO {
	@Autowired
	OtherActivitiesDTO otheractivities;
	private Planning planning;
	@Test
	void testCreateNewOtherActivitiesDTO(){
		otheractivities = new OtherActivitiesDTO();
		
		otheractivities=new OtherActivitiesDTO();
		otheractivities.setId(new Long(1));
		otheractivities.setPlanning(planning);
		otheractivities.setName("Tutoria");
		otheractivities.setDuration(new Long(12L));
		otheractivities.setLink("https.iberoameircana");
		otheractivities.setAprobation("Espera");
		otheractivities.setObservations("Ninguna");
		assertEquals(otheractivities, new OtherActivitiesDTO (new Long(1),planning,"Tutoria",new Long(12L),"https.iberoameircana","Espera","Ninguna"));
	}
	@Test
	void testExistOtherActivitiesDTO(){
		otheractivities=new OtherActivitiesDTO(new Long(1),planning,"Tutoria",new Long(12L),"https.iberoameircana","Espera","Ninguna");
		assertThat(otheractivities.getName()).isEqualTo("Tutoria");
		
	}
}
