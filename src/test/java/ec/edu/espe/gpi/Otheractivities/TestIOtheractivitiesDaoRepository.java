package ec.edu.espe.gpi.Otheractivities;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.congress.Congress;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
@SpringBootTest


class TestIOtheractivitiesDaoRepository {
	private Planning planning;
	private OtherActivities otheractivities;

	@Test
	void testPassNewObjectOtherActivities() {
		otheractivities=new OtherActivities();
		otheractivities.setId(new Long(1));
		otheractivities.setPlanning(planning);
		otheractivities.setName("Tutoria");
		otheractivities.setDuration(new Long(12L));
		otheractivities.setLink("https.iberoameircana");
		otheractivities.setComplete(true);
		otheractivities.setRemoved(false);
		otheractivities.setAprobation("Espera");
		otheractivities.setObservations("Ninguna");
		assertEquals(otheractivities, new OtherActivities(new Long(1),planning,"Tutoria",new Long(12L),"https.iberoameircana",true, false,"Espera","Ninguna"));
		
	}
	@Test
	void testFailNewObjectOtherActivities() {
		otheractivities=new OtherActivities();
		otheractivities=new OtherActivities();
		otheractivities.setId(new Long(1));
		otheractivities.setPlanning(planning);
		otheractivities.setName("Tutoria");
		otheractivities.setDuration(new Long(12L));
		otheractivities.setLink("https.iberoameircana");
		otheractivities.setComplete(true);
		otheractivities.setRemoved(false);
		otheractivities.setAprobation("Espera");
		otheractivities.setObservations("Ninguna");
		assertNotSame(otheractivities, new OtherActivities(new Long(1),planning,"Tutorias",new Long(12L),"https.iberoameircana",true, false,"Espera","Ninguna"));
		
	}
	@Test
	void testPassExistObjectOtherActivities() {
		otheractivities=new OtherActivities(new Long(1),planning,"Tutorias",new Long(12L),"https.iberoameircana",true, false,"Espera","Ninguna");
		assertThat(otheractivities.getLink()).isEqualTo("https.iberoameircana");
	}
	
	@Test
	void testFailExistObjectOtherActivities() {
		otheractivities=new OtherActivities(new Long(1),planning,"Tutorias",new Long(12L),"https.iberoameircana",true, false,"Espera","Ninguna");
		assertNotSame(otheractivities, new OtherActivities(new Long(1),planning,"Tutoria",new Long(12L),"https.iberoameircana",true, false,"Espera","Ninguna"));
	}

}
