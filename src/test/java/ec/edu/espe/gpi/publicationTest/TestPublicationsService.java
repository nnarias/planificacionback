package ec.edu.espe.gpi.publicationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.dto.otheractivities.OtherActivitiesDTO;
import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.service.otheractivities.IOtherActivitiesService;
import ec.edu.espe.gpi.service.publications.IPublicationsService;
@SpringBootTest
class TestPublicationsService {

	@Autowired
	IPublicationsService iPublicationService;
	Publications publications;
	private Planning planning;

	@Test
	void testIPublicationsServiceFindAll() {
		List<Publications> listpublication=iPublicationService.findAll();
		assertThat(listpublication).size().isGreaterThan(0);
		//assertEquals(0,listBook.size());
	}
	
	@Test
	void testIPublicationsServiceFindById() {
		Long cod = 1L;
		Publications publications=iPublicationService.findById(cod);
		assertThat(publications.getId()).isEqualTo(cod);
	}
	
	@Test
	void testIPublicationsServiceSave() {
		PublicationsDTO publications = new PublicationsDTO();
		publications.setId(new Long(1));
		publications.setPlanning(planning);
		publications.setCodIES("L001");
		publications.setType("Articulo");
		publications.setArticleType("Art");
		publications.setCodPUB("L987");
		publications.setTitle("TED E ICC");
		publications.setIndexBase("index1");
		publications.setCodeISS("L654");
		publications.setMagazineName("Tecnologia y ciencia");
		publications.setQuartile("3 cuartil");
		publications.setMagazineNumber("2567");
		publications.setSjr("SJR");
		publications.setPublicationDate((new Date(121, 5,3)));
		publications.setDetailField("Articulo de tecnologia");
		publications.setCondition("Publicado en 2 meses");
		publications.setPublicationLink("https.tecnologiaycienci");
		publications.setMagazineLink("https.revista.indexa");
		publications.setFiliation(true);
		publications.setCompetitor("Juan Jose Ruiz");
		publications.setObservation("Articulo de primer categoria");
		publications.setDuration(new Long (12));
		assertThat(publications.getCompetitor()).isEqualTo("Juan Jose Ruiz");
	}
	
	@Test
	void testIPublicationsServiceUpdateState() {
		iPublicationService.updateState(1L);
		Optional<Publications> publications=iPublicationService.findByIdStateFalse(1L);
		assertTrue(publications.get().getRemoved());
	}
}
