package ec.edu.espe.gpi.publicationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import ec.edu.espe.gpi.dto.congress.CongressDTO;
import ec.edu.espe.gpi.dto.publications.PublicationsDTO;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;

class TestPublicationsDTO {

	@Autowired
	PublicationsDTO publications;
	private Planning planning;
	@Test
	void testCreateNewPublicationsDTO(){
		publications = new PublicationsDTO();
		
		publications=new PublicationsDTO();
		publications.setId(new Long(1));
		publications.setPlanning(planning);
		publications.setCodIES("L001");
		publications.setType("Articulo");
		publications.setArticleType("Art");
		publications.setCodPUB("L987");
		publications.setTitle("TED E ICC");
		publications.setIndexBase("index1");
		publications.setCodeISS("L654");
		publications.setMagazineName("Tecnologia y ciencia");
		publications.setQuartile("3 cuartil");
		publications.setMagazineNumber("2567");
		publications.setSjr("SJR");
		publications.setPublicationDate((new Date(121, 5,3)));
		publications.setDetailField("Articulo de tecnologia");
		publications.setCondition("Publicado en 2 meses");
		publications.setPublicationLink("https.tecnologiaycienci");
		publications.setMagazineLink("https.revista.indexa");
		publications.setFiliation(true);
		publications.setCompetitor("Juan Jose Ruiz");
		publications.setObservation("Articulo de primer categoria");
		publications.setDuration(new Long (12));
		publications.setAprobation("Espera");
		publications.setObservations("Ninguna");
		assertEquals(publications, new PublicationsDTO(new Long(1),planning,"L001","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",new Long (12),"Espera","Ninguna"));
	
	}
	@Test
	void testExistPublicationsDTO(){
		publications=new PublicationsDTO(new Long(1),planning,"L001","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",new Long (12),"Espera","Ninguna");
		
		assertThat(publications.getCompetitor()).isEqualTo("Juan Jose Ruiz");
		
	}

}
