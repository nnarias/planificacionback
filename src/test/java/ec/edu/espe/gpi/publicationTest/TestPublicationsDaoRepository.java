package ec.edu.espe.gpi.publicationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import ec.edu.espe.gpi.dao.planning.IPlanningDao;
import ec.edu.espe.gpi.dao.publications.IPublicationsDao;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
import ec.edu.espe.gpi.model.users.Users;
@SpringBootTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@TestMethodOrder(OrderAnnotation.class)
class TestPublicationsDaoRepository {
	@Autowired
	private IPublicationsDao publicationsRepository;
	private Planning planning;
	
	@Test
	@Rollback(false)
	@Order(1)
	public void testCreatePublications() {
		Long cod=1L;
		Publications publications = new Publications(new Long(1),planning,"L0012","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna");
		
		Publications savepublications=publicationsRepository.save(publications);
		assertNotNull(savepublications);
		
	}
	@Test
	@Order(2)
	public void testFindyPublicationsByIdExist() {
		Long cod=1L;
		Optional<Publications> findPublications=publicationsRepository.findByIdEnable(cod);
		//assertThat(findPlanning.get().getId()).isEqualTo(cod);
	}
	@Test
	@Order(3)
	public void testFindyPublicationsByIdExistNotExist() {
		Optional<Publications> findPublication=publicationsRepository.findByIdEnable(3L);
		//assertEquals(true, findPlanning.isEmpty());
		
	}
	
	@Test
	@Rollback(false)
	@Order(4)
	public void testUpdatePublications() {
		Long cod=1L;
		Publications publications = new Publications(new Long(1),planning,"L0012","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna");
		publicationsRepository.save(publications);
		Optional<Publications> updatePublications=publicationsRepository.findById(cod);
		assertThat(updatePublications.get().getId()).isEqualTo(cod);
		
	}
	
	@Test
	@Order(5)
	public void testListPublications() {
		List<Publications> listPublications=(List<Publications>)publicationsRepository.findAll();
		assertThat(listPublications).size().isGreaterThan(0);
		
	}
	
	/*@Test
	@Rollback(false)
	@Order(6)
	public void testDeletePublications() {
		Long cod=1L;
		boolean afterDelete=publicationsRepository.findByIdEnable(cod).isPresent();
		publicationsRepository.deleteById(cod);
		boolean beforeDelete=publicationsRepository.findByIdEnable(cod).isPresent();
		assertTrue(afterDelete);
		assertFalse(beforeDelete);		
	}*/
}
