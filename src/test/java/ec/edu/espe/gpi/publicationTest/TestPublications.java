package ec.edu.espe.gpi.publicationTest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.Date;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import ec.edu.espe.gpi.model.otheractivities.OtherActivities;
import ec.edu.espe.gpi.model.planning.Planning;
import ec.edu.espe.gpi.model.publications.Publications;
@SpringBootTest
class TestPublications {

	private Planning planning;
	private Publications publications;

	@Test
	void testPassNewObjectPublication() {
		publications=new Publications();
		publications.setId(new Long(1));
		publications.setPlanning(planning);
		publications.setCodIES("L001");
		publications.setType("Articulo");
		publications.setArticleType("Art");
		publications.setCodPUB("L987");
		publications.setTitle("TED E ICC");
		publications.setIndexBase("index1");
		publications.setCodeISS("L654");
		publications.setMagazineName("Tecnologia y ciencia");
		publications.setQuartile("3 cuartil");
		publications.setMagazineNumber("2567");
		publications.setSjr("SJR");
		publications.setPublicationDate((new Date(121, 5,3)));
		publications.setDetailField("Articulo de tecnologia");
		publications.setCondition("Publicado en 2 meses");
		publications.setPublicationLink("https.tecnologiaycienci");
		publications.setMagazineLink("https.revista.indexa");
		publications.setFiliation(true);
		publications.setCompetitor("Juan Jose Ruiz");
		publications.setObservation("Articulo de primer categoria");
		publications.setComplete(true);
		publications.setRemoved(true);
		publications.setDuration(new Long (12));
		publications.setAprobation("Espera");
		publications.setObservations("Ninguna");
		assertEquals(publications, new Publications(new Long(1),planning,"L001","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna"));
		}
	@Test
	void testFailNewObjectPublication() {
		publications=new Publications();
		publications.setId(new Long(1));
		publications.setPlanning(planning);
		publications.setCodIES("L001");
		publications.setType("Articulo");
		publications.setArticleType("Art");
		publications.setCodPUB("L987");
		publications.setTitle("TED E ICC");
		publications.setIndexBase("index1");
		publications.setCodeISS("L654");
		publications.setMagazineName("Tecnologia y ciencia");
		publications.setQuartile("3 cuartil");
		publications.setMagazineNumber("2567");
		publications.setSjr("SJR");
		publications.setPublicationDate((new Date(121, 5,3)));
		publications.setDetailField("Articulo de tecnologia");
		publications.setCondition("Publicado en 2 meses");
		publications.setPublicationLink("https.tecnologiaycienci");
		publications.setMagazineLink("https.revista.indexa");
		publications.setFiliation(true);
		publications.setCompetitor("Juan Jose Ruiz");
		publications.setObservation("Articulo de primer categoria");
		publications.setComplete(true);
		publications.setRemoved(true);
		publications.setDuration(new Long (12));
		publications.setAprobation("Espera");
		publications.setObservations("Ninguna");
		assertNotSame(publications, new Publications(new Long(1),planning,"L0012","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna"));
		
	}
	@Test
	void testPassExistObjectPublications() {
		publications=new Publications(new Long(1),planning,"L0012","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna");
		assertThat(publications.getPublicationLink()).isEqualTo("https.tecnologiaycienci");
	}
	
	@Test
	void testFailExistObjectPublications() {
		publications=new Publications(new Long(1),planning,"L0012","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna");
		assertNotSame(publications, new Publications(new Long(1),planning,"L00123","Articulo","Art","L987","TED E ICC","index1","L654","Tecnologia y ciencia","3 cuartil","2567","SJR",new Date(121, 5,3),"Articulo de tecnologia","Publicado en 2 meses","https.tecnologiaycienci","https.revista.indexa",true,"Juan Jose Ruiz","Articulo de primer categoria",true, false,new Long (12),"Espera","Ninguna"));
	}

}
